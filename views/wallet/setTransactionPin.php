<?php
?>
<div class="bg-White border-all clearfix innerAll inner-2x Authentication text-Light-Black">
   <h4 class="text-Blue margin-top-none f-sz20"> Create New Transaction Key</h4>
        <p class="f-sz15 l-h16">Transaction key are unique piece of information specifically associated with your payment gateway account. Be sure to store these values securely and changes the Transaction Key regularly to further strengthen the security of your account. </p>
       
        <div class="bg-grayscale-fb border-all innerAll ">
            <div class="row">
                <div class="col-xs-4 border-right">
                    <div class="Authentication-step text-grayscale-4">
                        <div class=" text-center">
                            <span class="Authentication-step-index"> Step 1</span>
                            <span class="Authentication-step-title"> SETUP </span>
                            <span class="Authentication-step-icon innerT"> 
                          <span class="fa-stack fa-sm">
                            <i class="fa fa-gears f-sz24 l-h12"></i> 
                          </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 border-right">
                    <div class="Authentication-step ">
                        <div class="Authentication-step text-center text-grayscale-be">
                            <span class="Authentication-step-index"> Step 2</span>
                            <span class="Authentication-step-title">VERIFY</span>
                            <span class="Authentication-step-icon innerT">                        
                         <span class="fa-stack fa-sm">
                            <i class="fa fa-certificate fa-stack-2x f-sz30 "></i>
                            <i class="fa fa-circle-thin fa-stack-1x text-white f-sz20 l-h15"></i>
                            <i class="fa fa-check fa-stack-1x text-White f-sz10"></i>
                          </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 text-Gray ">
                    <div class="Authentication-step">
                        <div class="Authentication-step text-center  text-grayscale-be">
                            <span class="Authentication-step-index"> Step 3</span>
                            <span class="Authentication-step-title"> CONFIRM</span>
                            <span class="Authentication-step-icon innerT">  
                            <i class="fa fa-check-circle fa-stack-2x f-sz30 "></i>
                           </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====  End of steps strip  ====-->
        <div>
        <h6 class="text-Blue margin-top f-sz20"> Enter your 6 digit Tansaction pin</h6>
        </div>

        <div class="row"> 
            <div class="col-xs-12">
                <p class="text-red">Note:Your Transaction pin must contain combination of both number and alphabets to make pin more secure</p>
                <form>
					<div class="form-group row  googleCode">
					  <label for="text-input" class="col-xs-3 col-form-label">Enter Transaction pin:</label>
					  <div class="col-xs-4">
					    <input type="password"  class="form-control text-blue  bg-grayscale-fb"" value="" id="text-input-1" 
					    placeholder="*********">
					  </div>
					</div>

					<div class="form-group row googleCode">
					  <label for="text-input" class="col-xs-3 col-form-label">Confirm Transaction pin:</label>
					  <div class="col-xs-4">
					    <input type="password" class="form-control text-Blue text-Text-Primary-Blue placeholder-Text-Primary-Blue  bg-grayscale-fb""  value="" id="text-input-2" placeholder="*********">
					  </div>
					</div>
					<h5 id="pin-err" style="color: red;"></h5>
				</form>
            </div>
            <div class="col-lg-12">
                <div class=" innerT clearfix ">
                    <button type="button" id="trans-verify-btn" class="btn  pull-right  btn-trading-wid-auto  pull-right btn-trading-dark" onclick="setTransactionPin();">Next</button>
                    <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto " onclick="location.reload();">Previous</button>
                </div>
            </div>
        </div>
    </div>