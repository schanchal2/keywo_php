<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$docrootpath = "../../"; 

include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");
print("<script>"); print("var sessionMsg = ''; "); print("</script>");

/* FTUE status check */
$getFtUserDetail = getFirstTimeUserStatus($kwdConn,$email,$_SESSION['id']);

if(noError($getFtUserDetail)){
    $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
    if($row['ftue_status'] == 0){
        print("<script>");
        print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
        print("</script>");
        exit();
    }
}else{
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

if(isset($_SESSION['errMsg'])){
	print("<script>"); print("var sessionMsg = '".$_SESSION['errMsg']."'; "); print("</script>");	
	unset($_SESSION['errMsg']);
	unset($_SESSION['errCode']);
}

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$trans_type_array = array(
    "purchase_itd"=>"Purchase Credit",
    "cashout_fees"=>"Cashout Fees",
    "keyword_purchase"=>"Keyword Purchase",
    "post_creator_earning"=>"Content Upload",
    "post_sharer_earning" =>"Content Share",
    "post_viewer_earning"=>"Content Consumption",
    "trade"=>"Trade",
    "renewal_fees"=>"Renewal Fees",
    "trade_fees"=>"Trade Fees",
    "affiliate_earnings"=>"Affiliate Earning",
    "social_referral_earnings"=>"Social Referral Earning",
    "cashout"=>"Cashout"
    );

// printArr($getUserDetails); die;
if(noError($getUserDetails)){
  $getUserDetails = $getUserDetails['errMsg'];
  $contenConsumptionEarning = $getUserDetails['social_content_sharer_earnings']+$getUserDetails['social_content_view_earnings'];
  $contentUploadEarning = $getUserDetails['social_content_creator_earnings'];
  $referralEarning = $getUserDetails['affiliate_earning'] + $getUserDetails['search_affiliate_earnings'];
  $keywordEarning = $getUserDetails['total_kwd_income'];
}



 ?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT innerB WT5">
    <div class="container  padding-none row-10">
        <div class="row">
            <?php
            $page = "index"; 
            include("rightMenu.php"); ?>      
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-xs-3 innerMB">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $contenConsumptionEarning." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$contenConsumptionEarning, 4, '.', '')." ".$keywoDefaultCurrencyName; ?></div>
                            <div class="keyword-num-status">Content Consumption</div>
                        </div>
                    </div>
                    <div class="col-xs-3 innerMB">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key-dark"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $contentUploadEarning." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$contentUploadEarning, 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
                            <div class="keyword-num-status light-blue-col">Content Upload</div>
                        </div>
                    </div>
                    <div class="col-xs-3 innerMB">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $referralEarning." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$referralEarning, 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
                            <div class="keyword-num-status ">Referral</div>
                        </div>
                    </div>
                    <div class="col-xs-3 innerMB">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key-dark"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $keywordEarning." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$keywordEarning, 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
                            <div class="keyword-num-status light-blue-col">Keyword</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 filter">
                        <form class="form-inline innerMT">
                            <div class="form-group innerR">
                                <label class="text-Gray f-sz15">Sort By : </label>
                                <div class="input-group">
                                    <div class="Sort-by">
                                        <label class="dropdownOptions pull-right">
                                            <select class="selectpicker" id="tr-type" name="All" onchange="getsortedTransaction();">
                                                <option value="">All</option>
                                                <option value="transfer-by_sender">Sent</option>
                                                <option value="transfer-by_receiver">Received</option>
                                                <?php foreach ($trans_type_array as $key => $value) { ?>
                                                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                                
                                                <!-- <option value="cashout">Cashout</option>
                                                <option value="cashout_fees">Cashout fees</option>
                                                <option value="purchase_itd">Purchase <?php // echo $keywoDefaultCurrencyName; ?></option>
                                                <option value="keyword_purchase">Keyword Purchase</option>  --> 
                                            </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="text-Gray f-sz15">Date : </label>
                                <div class="input-group text-Blue dateRange">
                                    <input type="text" readonly class="form-control text-Blue" value="10/9/2017-12/9/2017" placeholder="" id="daterange-to" name="daterange" style="background-color: #fff;">
                                    <span class="input-group-addon  bg-white text-Blue"> <i class="fa fa-calendar" id=""></i> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                 <div class="input-group ">
                                    <button type="button" class="btn bg-Lightest-Blue text-White innerLR" data-toggle="modal" data-target="#DownloadTransactionHistory"> <i class="fa fa-download"></i> </button>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="search" placeholder="Search">
                                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">  <i class="fa fa-search text-grayscale-6"></i></button>
                                </span>
                                </div>
                            </div>
                        </form>
                        <?php if(isset($_SESSION['err_msg']) || isset($_SESSION['succ_msg'])){ ?>
                        <div style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                        <div style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                        <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); } ?>
                    </div>
                </div>

           <div id="transction">
            <?php include("walletTransactions.php"); ?>
            </div>
          </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->

     <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="bg-White border-all clearfix ">
                <form action="" method="POST" class="form-horizontal" role="form">
                    
                    <div class="modal-body" id="transction-details">
                        
                        
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a> -->
    <div class="modal fade" id="DownloadTransactionHistory">
        <div class="modal-dialog">
            <div class="modal-content border-all">
                <div class="modal-header bg-color-Light-Grey text-color-White">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close f-sz15"></i> </button>
                    <h4 class="modal-title f-sz16">Transaction History</h4>
                </div>
                <div class="modal-body padding-none">
                    <div id='InlineMenu'></div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-block text-color-White bg-color-Lightest-Blue" onclick="downloadTrans();"> <i class="fa fa-download"></i> Download Report</button>
                </div>
            </div>
        </div>
    </div>

 </main>
    <?php include("../layout/transparent_footer.php");?>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript">
// $('#DownloadTransactionHistory').modal('show');
$('#DownloadTransactionHistory').on('shown.bs.modal', function(e) {

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }


    $("#InlineMenu").MonthPicker({
        // SelectedMonth: '<?php echo date("m"); ?>/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        MaxMonth: 0,
        SelectedMonth:0,
        i18n: {
            months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        }
    });
})

     $('input[name="daterange"]').daterangepicker({ startDate: '<?php echo date("m/d/Y",strtotime("-3 Months")); ?>', endDate: '<?php echo date("m/d/Y"); ?>' });
    $('#daterange-to').on('apply.daterangepicker', function(ev, picker) {
      getsortedTransaction()
    });
   
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });

	$(document).ready(function() {

		if(sessionMsg != ''){
			showToast("success",sessionMsg);
		}
		
		$("#date-range").change(function(){
      		getsortedTransaction();
 		});
 		 $("#search").keyup(function(){
 		 	var search = $("#search").val();
 		 	var len = search.length;
 		 	if(len > 2){
        		getsortedTransaction();
        	}else if(len == 0){
        		getsortedTransaction();
        	}
    	});
  });

var first_bkwd_cursor = '<?php echo $bkwd_cursor_offset_from; ?>';

//to download transaction report
function downloadTrans(){
  var month_string = $(".ui-state-active").text();
  var year_string = $(".month-picker-title").text();
  var year = year_string.split(" ").pop();
  var month = "01";
  month_string = month_string.toLowerCase();

  if(month_string == 'jan.'){
    month = "01";
  }else if(month_string == 'feb.'){
    month = "02";
  }else if(month_string == 'mar.'){
    month = "03";
  }else if(month_string == 'apr.'){
    month = "04";
  }else if(month_string == 'may'){
    month = "05";
  }else if(month_string == 'june'){
    month = "06";
  }else if(month_string == 'july'){
    month = "07";
  }else if(month_string == 'aug.'){
    month = "08";
  }else if(month_string == 'sep.'){
    month = "09";
  }else if(month_string == 'oct.'){
    month = "10";
  }else if(month_string == 'nov.'){
    month = "11";
  }else if(month_string == 'dec.'){
    month = "12";
  }


  window.location.replace("../../controllers/wallet/transactionReportsController.php?month="+month+"&year="+year);
  $('#DownloadTransactionHistory').modal('toggle');

} 

function showOlder(bkwd_cursor,offset_from){
  		var pageCount = $("#page-count").val();
  		$("#page-count").val(parseInt(pageCount)+1);
  		var pageCount1 = parseInt(pageCount)+1 ;
  		var tr_type = $("#tr-type").val();
  		var date_range = $("#date-range").val();
  		var daterange_to = $("#daterange-to").val();
  		var daterange_from = $("#daterange-from").val();
  		var search = $("#search").val();
  		var status = $("#tr-status").val();
  		$('#transction').load('walletTransactions.php?bkwd_cursor='+bkwd_cursor+'&status='+encodeURI(status)+'&offset_from='+offset_from+'&sort_by='+tr_type+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search)+'&pageCount='+pageCount1+'&first_bkwd_cursor='+first_bkwd_cursor);
  }

	function showNewer(fwd_cursor,offset_from){
		var pageCount = $("#page-count").val();
  		$("#page-count").val(parseInt(pageCount)-1);
  		var pageCount1 = parseInt(pageCount)-1 ;
		var tr_type = $("#tr-type").val();
		var date_range = $("#date-range").val(); 
		var search = $("#search").val();
		var daterange_to = $("#daterange-to").val();
  		var daterange_from = $("#daterange-from").val();
		var status = $("#tr-status").val();
  		$('#transction').load('walletTransactions.php?fwd_cursor='+fwd_cursor+'&status='+encodeURI(status)+'&offset_from='+offset_from+'&sort_by='+tr_type+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search)+'&pageCount='+pageCount1+'&first_bkwd_cursor='+first_bkwd_cursor);
  }
	function getsortedTransaction(){
		
  		var tr_type = $("#tr-type").val();
  		var date_range = $("#date-range").val();
  		var daterange_to = $("#daterange-to").val();
  		var daterange_from = $("#daterange-from").val();
  		var search = $("#search").val();	
  		var status = $("#tr-status").val();
  		if (typeof status == 'undefined'){
  			status = '';
  		}
  		$('#transction').load('walletTransactions.php?sort_by='+tr_type+'&status='+encodeURI(status)+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search));
  }
   function showDetails(transction_id,transaction_boundary){
  		$("#err-msg").text('');
  		$('#transction-details').load('walletTransctionDetails.php?transction_id='+transction_id+'&transaction_boundary='+encodeURI(transaction_boundary));
  		$('#myModal').modal('show');

  }
</script>
<!--========================================================================================================================================        
            ( 16-02-17)  
            table base created 
           
            /**
            
                TODO:
                    ( 17-02-17) 
                - appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
                - color name comflict in '.bg-blue-key-dark, .bg-blue-key'
                - looks better if .my-info-card bottom padding is removed
                    ( 18-02-17) 
                - input highlighting can be improoved. 
             */
            



                                                                                 
            


=========================================================================================================================================-->
</body></html>