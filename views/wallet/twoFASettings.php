<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

$email = $_SESSION["email"];
$veifyEmail = (isset($_GET['email']))?cleanXSS(urldecode($_GET['email'])):'';
$authToken = (isset($_GET['auth']))?cleanXSS(urldecode($_GET['auth'])):'';

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
}
print("<script>");
print("var invalidSession = false;");
print("</script>");
if(isset($_GET['email']) && isset($_GET['auth']) && $veifyEmail == $email){
    $type = 'transaction_pin_verify_email';
    $mode = 'email';
    $payload = '';
    $payloadDetails = getPayload($conn,$email,$authToken,$payload,$type,$mode);
    if($_SESSION['security_preference'] != 3){
        $verify = verifyAuthToken($conn,$email,$authToken,$payload,$type,$mode);
        if(noError($verify)){
            $transPin = setTransactionPin($payloadDetails['payload']['payload']);
            if(noError($transPin)){
                $_SESSION['security_preference'] = 3;
                $verification_status = 'verified';        
                $returnArr['errMsg'] = 'Congratulations, your account has been secured with Transaction Pin';
                $returnArr['errCode'] = -1;
            }else{

                $verification_status = 'unverified';
                $returnArr['errMsg'] = "Your email id verification process has been failed";
                $returnArr['errCode'] = 2;
            }            
        }else{
            $verification_status = 'unverified';
            $returnArr['errMsg'] = "The email verification link has expired";
            $returnArr['errCode'] = 2;   
        }
    }else{
        $verification_status = 'expired';
        $returnArr['errMsg'] = "You have already set transaction pin as a security preference";
        $returnArr['errCode'] = 2;
    }
    $errMsg = $returnArr['errMsg'];

}else if(isset($_GET['email']) && isset($_GET['auth']) && $veifyEmail != $email){
    $invalidSession = true;
    print("<script>");
    print("var invalidSession = true;");
    print("</script>");
}

$securityPreference = $_SESSION['security_preference'];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerTB">
    <div class="container padding-none row-10">
        <div class="row">
            <?php
            $page = "security_settings"; 
            include("rightMenu.php"); ?>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Security Settings</h4>
                            <input type="hidden" id="text-input" name="">
                        <div class="row">
                            <div class="col-lg-12" id="page-container">
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
</main>

<!--      Confirmation dialog while click on payment now     -->
<div id="popup-confirmation" class="modal fade in keyword-popup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Invalid Session</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" id="response">You are currently logged in with some other account, To verify transaction pin please log in with same user</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <input value="Login" type="button" onclick="window.location.href='<?php echo $rootUrl; ?>views/user/logout.php';" id="payment-confirm-btn" class="yes-btn btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                    <input value="No" id="payment-cancel-btn" type="button" class="btn-trading-wid-auto innerMTB" onclick="window.location.href='<?php echo $rootUrl; ?>views/social/index.php';">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .defaultInput
    {
     width: 100px;
     height:25px;
     padding: 5px;
    }
    .error
    {
     border:1px solid red;
    }
</style>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript">
    $( document ).ready(function() {
        var security_preference = '<?php echo $securityPreference; ?>';
        var verification_status = '<?php echo $verification_status ?>';
        var errMsg = '<?php echo $errMsg; ?>';
        verification_link = '<?php echo $link = (isset($_GET['auth']) && !empty($_GET['auth']))?'verification_link':''; ?>';
        if(invalidSession){
            $("#popup-confirmation").modal("show");
            loadSecurityIndex();
        }else{
            if(verification_link !='' && verification_status != "expired"){
                loadEmailVerifyConfirm();
            }else if(verification_link !='' && verification_status == "expired"){
                var err = "You have already set transaction pin";
                loadSecurityIndex(err);
            }else{
                loadSecurityIndex();
            }
        }        

    });

    function loadSecurityIndex(err){
        var securityPreference = <?php echo $securityPreference; ?>;
        $("#page-container").load('securityIndex.php?securityPreference='+securityPreference+'&err='+encodeURIComponent(err));
    }


    function set2FAcode(){
         $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 2){
            $("#preference-err").text("You have already set Google Authenticator code");
        }else{
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/setPreferenceController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    preference: 2
                },
                async: true,
                success:function(data){
                    console.log(data);
                    if(data.errCode == -1){
                        //location.reload(true);
                        window.location.href = "../two_factor_auth/index.php";           
                    }else{
                        $("#preference-err").text("Failed to set password security preference"); 
                    }
                    
                },
                error: function (xhr, status, error) {
                    location.reload();
                }
            });
            
        }
    }

    function transactionPin() {
         $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 3){
            $("#preference-err").text("You have already set transaction pin");
        }else{
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/setPreferenceController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    preference: 3
                },
                async: true,
                success:function(data){
                    console.log(data);
                    if(data.errCode == -1){
                        //location.reload(true);
                        window.location.href = "../two_factor_auth/index.php";           
                    }else{
                        $("#preference-err").text("Failed to set password security preference"); 
                    }
                    
                },
                error: function (xhr, status, error) {
                    location.reload();
                }
            });
            //loadSetTransactionPin();
        }
    }

    function setPasswordAuth(){
        $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 1){
            $("#preference-err").text("You have already set password as security method");
        }else{
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/setPreferenceController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    preference: 1
                },
                async: true,
                success:function(data){
                    console.log(data);
                    if(data.errCode == -1){
                        //location.reload(true);
                        window.location.href = "../two_factor_auth/index.php";           
                    }else{
                        $("#preference-err").text("Failed to set password security preference"); 
                    }
                    
                },
                error: function (xhr, status, error) {
                    location.reload();
                }
            });
            //setPasswordSecurityAuth();
        }
    }

    function setPasswordSecurityAuth(){
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/setPassowordAuthController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                trans_pin: ''
            },
            async: true,
            success:function(data){
                console.log(data);
                if(data.errCode == -1){
                    location.reload(true);           
                }else{
                    $("#preference-err").text("Failed to set password security preference"); 
                }
                
            },
            error: function (xhr, status, error) {
               location.reload();
            }
        });
    }

    function loadSetTransactionPin(){
        $("#page-container").load('setTransactionPin.php');
    }

    function loadManageBackupCodes(){
        $("#page-container").load('manageBackupCodes.php');
    }

    function setTransactionPin(){
        $("#text-input-1").removeClass('error');
        $("#text-input-2").removeClass('error');
        $("#pin-err").text('');
        $("#trans-verify-btn").text('Sending...');
        var pin_input_1 = $("#text-input-1").val();
        var pin_input_2 = $("#text-input-2").val();
        var is_valid = false;
        if(pin_input_1.length == 0 && pin_input_2.length == 0){
            $("#text-input-1").addClass('error');
            $("#text-input-2").addClass('error');
        }else if(pin_input_1.length == 0){
            $("#text-input-1").addClass('error');
        }else if(pin_input_2.length == 0){
            $("#text-input-2").addClass('error');
        }else if(pin_input_1 != pin_input_2){
            $("#pin-err").text('Both transaction pins should be identical');
        }else if(pin_input_1.length != 6){
            $("#pin-err").text('Please enter 6 digit transaction pin');
        }else if (pin_input_1.match(/[a-z]/i) && pin_input_1.match(/[0-9]/i)) {
            is_valid = true;
        }else{
            $("#pin-err").text('Transaction pin must contain combination of both number and alphabets');
        }


        if(is_valid){
            sendTransactionPinVerifyEmail(pin_input_1);
        }else{
            $("#trans-verify-btn").text('Next');
        }
    }


    function sendTransactionPinVerifyEmail(trans_pin){
        var verification_path = '<?php echo $rootUrl; ?>views/wallet/twoFASettings.php';
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/sendTransVerifyEmailController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                trans_pin: encodeURI(trans_pin),verification_path: encodeURI(verification_path)
            },
            async: true,
            success:function(data){
                console.log(data);
                if(data.errCode == -1){
                    $("#trans-verify-btn").text('Next');
                    $("#resend-btn").text('Resend Email');
                    loadSendVerifySuccess();
                    $("#text-input").val(trans_pin);             
                }else{
                    $("#trans-verify-btn").text('Next');
                    $("#resend-btn").text('Resend Email');
                    $("#pin-err").text('Failed to sent verification email,Please try again later');
                }
                
            },
            error: function (xhr, status, error) {
                location.reload();
            }
        });
    }

    function resendEmail(){
       $("#resend-btn").text('Sending...'); 
       var trans_pin = $("#text-input").val(); 
       if(trans_pin.length == 6){
        sendTransactionPinVerifyEmail(trans_pin);
       }else{
        $("#resend-btn").text('Resend Email');
       }
    }

    function loadSendVerifySuccess(){
        $("#page-container").load('emailSentSuccess.php');
    }

    function loadEmailVerifyConfirm(){
        var verification_status = '<?php echo $verification_status; ?>';
        var errMsg = '<?php echo $errMsg; ?>';
        $("#page-container").load('confirmEmailVerification.php?verification_status='+verification_status+'&errMsg='+encodeURIComponent(errMsg));
    }
</script>

<?php 
/*=============================================
=            Section comment block            =
=============================================
(28-2-17)
    BUG:EG-300
    Issue List:
        1)Text font color are different as compare to design sheet.
        2)Choose Your Security Method box background shadow color are different as compare to design sheet.   @done (17-02-28 13:26) 
=====  End of Section comment block  ======*/
?>
