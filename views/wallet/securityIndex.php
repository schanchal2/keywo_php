<?php
$securityPreference = $_GET['securityPreference'];
$errMsg = (isset($_GET['err']) && $_GET['err'] != 'undefined')?$_GET['err']:'';
?>
<div class="bg-White border-all clearfix innerAll inner-2x Authentication text-grayscale-4 ">
    <p class="f-sz15 l-h16">Two-factor Authentication adds an extra layer of security to your account. Once enabled, Sign-In will require you to enter a unique verification code generated by an app on your mobile device or sent via SMS text message, In addition to your username and password.</p>
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-Blue margin-top-none f-sz16">Choose Your Security Method <span id="preference-err" style="color: red;"><?php echo $errMsg; ?></span></h4>
        </div>
          <div class="col-xs-4 ">
            <a onclick="setPasswordAuth();">
                <div class="bg-grayscale-fb border-all innerAll text-grayscale-4">
                    <div class="image-wrapper">
                        <img src="../../images/wallet/accountpassword.png" alt="">
                    </div>
                    <h4 class="text-center f-sz16">Account Password</h4>
                    <div class="btn-block text-center innerAll">
                        <?php if($securityPreference != 1){ ?> 
                            <i class="fa fa-check-square fa-2x text-grayscale-80 f-sz20"></i>
                        <?php }else{ ?>
                            <i class="fa fa-check-square fa-2x text-App-Success f-sz20"></i>
                        <?php } ?>
                    </div>
                    <p class="text-center  f-sz13">An authenticator lets you generate codes without needing to receive text messages / voice calls during sign-in</p>
                    <?php if($securityPreference == 2 ){ ?> 
                        <div class="text-skyeblue text-center" style="color: #fbfbfb;"><h5> sdf</h5></div>
                    <?php } ?>
                </div>
            </a>
        </div>
        <div class="col-xs-4 ">
            
                <div class="bg-grayscale-fb border-all innerAll text-grayscale-4">
                    <a class="text-grayscale-4" onclick="set2FAcode();">
                        <div class="image-wrapper">
                            <img src="../../images/wallet/authenticator.png" alt="">
                        </div>
                        <h4 class="text-center f-sz16">Google Authenticator</h4>
                        <div class="btn-block text-center innerAll">
                            <?php if($securityPreference != 2){ ?> 
                                <i class="fa fa-check-square fa-2x text-grayscale-80 f-sz20"></i>
                            <?php }else{ ?>
                                <i class="fa fa-check-square fa-2x text-App-Success f-sz20"></i>
                            <?php } ?>
                        </div>
                        <p class="text-center  f-sz13">An authenticator lets you generate codes without needing to receive text messages / voice calls during sign-in</p>
                    </a>
                    <?php if($securityPreference == 2 ){ ?> 
                        <a onclick="loadManageBackupCodes();">
                            <div class="text-skyeblue text-center" ><h5>Manage Backup Code</h5></div>
                        </a>
                    <?php } ?>
                </div>
            
        </div>
        <div class="col-xs-4">
            <a onclick="transactionPin();">
                <div class="bg-grayscale-fb border-all innerAll text-grayscale-4">
                    <div class="image-wrapper">
                        <img src="../../images/wallet/tranaction.png" alt="">
                    </div>
                    <h4 class="text-center f-sz16">Transaction pin</h4>
                    <div class="btn-block text-center innerAll">
                        <?php if($securityPreference != 3){ ?> 
                            <i class="fa fa-check-square fa-2x text-grayscale-80 f-sz20"></i>
                        <?php }else{ ?>
                            <i class="fa fa-check-square fa-2x text-App-Success f-sz20"></i>
                        <?php } ?>
                    </div>
                    <p class="text-center  f-sz13">Transaction pin lets you authenticate your identity while making transations.</p>
                    <?php if($securityPreference == 2 ){ ?> 
                        <div class="text-skyeblue text-center" style="color: #fbfbfb;"><h5>asd</h5></div>
                    <?php } ?>
                </div>
            </a>

        </div>
    </div>
    <div class=" innerT inner-2x" style="display: none;">
        <button type="button" class="btn  pull-right  btn-trading-wid-auto  innerMT  half">Next</button>
    </div>
</div>
