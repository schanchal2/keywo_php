<?php
session_start();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
} 

$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

$email =  $_SESSION["email"];
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
    $walletDetails = getWalletAddress($email);
    if(noError($walletDetails)){
        $walletDetails = $walletDetails['errMsg'];
    }
    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
    if(noError($adminSettings)){
        $adminSettings = $adminSettings['data'];
    }else{
        print('Error in fetching admin settings');
        exit;
    }
}    

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$currpreff = strtolower($_SESSION['CurrPreference']);
$conversionRate = $getExchangeRate['usd'] / $getExchangeRate[$currpreff] ;
$btcConversionRate = $getExchangeRate['usd'] / $getExchangeRate['btc'] ;
$usdConversionRate = $getExchangeRate['usd'] / $getExchangeRate['usd'] ;
$sgdConversionRate = $getExchangeRate['usd'] / $getExchangeRate['sgd'] ;
$session_id = session_id();

echo "<script> var sessionMsg = ''; </script>";

if(isset($_SESSION['errMsg'])){
    print("<script>"); print("var sessionMsg = '".$_SESSION['errMsg']."'; "); print("</script>");   
    unset($_SESSION['errMsg']);
    unset($_SESSION['errCode']);
}

echo "<script> var session_id = '" . $session_id . "'; </script>";
echo "<script> var conversionRate = " . $conversionRate . "; </script>";
echo "<script> var btcConversionRate = " . $btcConversionRate . "; </script>";
echo "<script> var usdConversionRate = " . $usdConversionRate . "; </script>";
echo "<script> var sgdConversionRate = " . $sgdConversionRate . "; </script>";
echo "<script> var minPurchaseLimit = " . $minPurchaseLimit . "; </script>";
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerTB WT5">
    <div class="container padding-none row-10">
        <div class="row">
            <?php
            $page = "purchase"; 
            include("rightMenu.php"); ?> 
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Purchase</h4>
                        <div class="card bg-White border-all   padding-bottom-none innerMB f-sz15">
                            <div class="row">
                                <div class="col-xs-12 innerMT inner-2x">
                                <?php if(isset($_SESSION['err_msg']) || isset($_SESSION['succ_msg'])){ ?>
                                    <div style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                                    <div style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                                    <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); ?><br>
                                <?php } ?>
                                    <form class="form-horizontal">
                                        <!-- 
                                     -->
                                        <div class="form-group  margin-bottom-none filter ">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right  text-grayscale-6">Enter Amount : </label>
                                            <div class="col-sm-4">
                                                <div class="input-group text-grayscale-4">
                                                    <input type="number" id="itd-amount" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" step="0.0001" value="" onkeyup="convertAmount();" placeholder="0.0000">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                                <small id="err-msg-itd-amount" class="text-Red"></small>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group text-Gray">
                                                   <input placeholder="0.0000" type="number" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" id="crr-preff-amt" onkeyup="convertAmount1();" step="0.0001" value="">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6" id="crr-preff"><?php echo $_SESSION['CurrPreference']; ?></span>
                                                </div>
                                            </div>
                                            <!-- <button type="submit" class="bg-Light-Blue btn-xs btn  col-sm-2 text-White pull-right wallet-Request">Proceed</button> -->
                                            <div class="col-md-2 text-center keyword-checkout">
                                                <input value="Proceed" type="button" onclick="validateCashoutAmt();" class="btn-pay-now f-sz18 l-h12">
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="current-wallet-balance innerMT inner-2x clearfix" id="payout-mode-box" style="display: none;">
                            <div class="row innerAll">
                                <div class="col-md-7">
                                    <div class="current-w1">
                                        <h5 class="f-sz16">Current Wallet Balance is : <span id="wallet-balance"></span> <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your payment amount : <span id="cashout-amt"></span> <?php echo $keywoDefaultCurrencyName; ?></h5>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h5 class="f-sz16">Purchase <?php echo $keywoDefaultCurrencyName; ?> with<i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16">
                                                <input type="hidden" name="valid-itd-amount" id="valid-itd-amount">
                                                <input type="radio" name="method" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label class="" for="Bitcoin"> Bitcoin</label>
                                            </div>
                                        </div>
                                        <div class="col pull-right">
                                            <span class="pull-right f-sz16" id="bitcoin-net-amt">0.45682021</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16">
                                                <input type="radio" name="method" id="PayPal" value="PayPal">
                                                <label class="" for="PayPal"> PayPal</label>
                                            </div>
                                        </div>
                                        <div class="col pull-right">
                                            <span class="pull-right f-sz16" id="paypal-net-amt">100.00</span></div>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-md-12 text-center innerMB inner-2x keyword-checkout">
                                <input value="Proceed to Purchase" type="button" class="btn-pay-now f-sz16 l-h12" onclick="prePamentProcess();">
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- col-xs-9 -->
    </div>
    </div>
    <!-- container -->

    <!--      Confirmation dialog while click on payment now     -->
    <div id="keyword-popup-confirmation" class="modal fade in keyword-popup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content row">
                <div class="modal-header custom-modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="">
                            <div class="row">
                                <div class="col-md-12 innerMB text-center">
                                    <span class="text-black" id="response">Are you sure you want to buy keyword</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 innerMB">
                                    <div class="text-left">
                                        <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                        <input type="hidden" name="order_id" id="order-id">
                                        <input type="hidden" name="payment_mode" id="payment-mode">
                                        <input type="hidden" name="final_itd_amt" id="final-itd-amt">
                                    </div>
                                </div>
                                <div class="col-md-6 innerMB">
                                    <div class="text-left pull-right">
                                        <input value="Yes" type="button" onclick="paymentAfterConfirmation();" id="payment-confirm-btn" class="yes-btn btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                        <input value="No" id="payment-cancel-btn" type="button" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <form style="display:block;" id="buyByPaypalForm" method="post" action="<?php echo $rootUrl; ?>controllers/paypal/Paypal-Express/process.php">
            <input  name="itemname" id="keywordBasketPaypal" value="purchase<?php echo $keywoDefaultCurrencyName; ?>" type="hidden" />
            <input  name="itemnumber" id="itemnumberPaypal" value="" type="hidden"/>
            <input  name="itemdesc" value="<?php echo $keywoDefaultCurrencyName; ?> Purchase at Keywo.com" type="hidden"/>
            <input  name="grandTotal" id="totalPricePaypal" type="hidden"/>
            <input  name="itemprice" id="itemPricePaypal" type="hidden"/>
            <input  name="itemQty" id="itemQtyPaypal" value="<?php echo $usercartCount;?>" type="hidden"/>
            <input  name="custom" id="customPaypal" type="hidden"/>
            <input  name="cancelURL" id="cancelURL" value="<?php echo $rootUrl; ?>views/wallet/purchaseITD.php" type="hidden"/>
        </form>
    </div>

    <!--Bitgo payment dailog box -->
    <div id="procceedToPayment" class="modal fade paymentModal" role="dialog">
    </div>
    <?php include("../layout/transparent_footer.php");?>


    <script type="text/javascript">

    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });

    $(document).ready(function() {

        if(sessionMsg != ''){
            showToast("success",sessionMsg);
        }
    });

    function convertAmount(){
        $('#payout-mode-box').hide();
        var itd_amount = $("#itd-amount").val();
        var covertedAmount = itd_amount/conversionRate;
        var currPrefAmt = covertedAmount.toFixed(5);

        var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        if(currPreff == 'BTC'){
            var currPrefAmt = covertedAmount.toFixed(8);
        }
        $("#crr-preff-amt").val(currPrefAmt);
        $("#crr-preff").text(currPreff);
    }

    function convertAmount1(){
        $('#payout-mode-box').hide();
        var usd_amount = $("#crr-preff-amt").val();
        var covertedAmount = usd_amount*conversionRate;
        var currPrefAmt = covertedAmount.toFixed(2);
        //var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        $("#itd-amount").val(currPrefAmt);
       // $("#crr-preff").text(currPreff);
    } 

    function validateCashoutAmt(){
        $('#err-msg-itd-amount').text('');
        $('#payout-mode-box').hide();
        var itd_amount = $('#itd-amount').val();
        var current_balance = '<?php echo $userBalance; ?>';
        
        if(itd_amount == 0 || parseFloat(itd_amount) < parseFloat(minPurchaseLimit)){
            if(parseFloat(itd_amount) < parseFloat(minPurchaseLimit) ){
                $('#err-msg-itd-amount').text('Please enter amount greater than '+minPurchaseLimit+' '+keywoDefaultCurrency);
            }else{
                $('#err-msg-itd-amount').text('Please enter valid amount');
            }
            
            return false;
        }else{
           
            //var total_amount = parseFloat(itd_amount) + parseFloat(cashout_fees);
            if(parseFloat(current_balance) > parseFloat(itd_amount) || true){

                var btc_net_amt = itd_amount/btcConversionRate;
                var paypal_net_amt = itd_amount/sgdConversionRate;
                $('#payout-mode-box').show();
                $('#wallet-balance').text(current_balance);
                $('#cashout-amt').text(itd_amount);
                $('#bitcoin-net-amt').text(btc_net_amt.toFixed(8)+' BTC');
                $('#paypal-net-amt').text(paypal_net_amt.toFixed(4)+' SGD');
                $('#valid-itd-amount').val(itd_amount);

            }else{
                $('#err-msg-itd-amount').text('Your available balance is not sufficient');
            }
        
        }
    }

    function prePamentProcess(){
        var itd_amount = $("#valid-itd-amount").val();
        var payment_mode = '';
        var paypal_check = $('#PayPal').is(':checked');
        var bitcoin_check = $('#Bitcoin').is(':checked');

        if(bitcoin_check){
            payment_mode = "bitcoin";            
        }else if(paypal_check){
            payment_mode = "paypal"; 
        }

        if(payment_mode != '' && itd_amount != ''){
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/prePaymentController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    payment_mode: payment_mode,itd_amount:itd_amount
                },
                async: true,
                success:function(data){
                    console.log(data);
                    if(data.errCode == -1){
                        $("#order-id").val(data.errMsg['orderId']);
                        $("#payment-mode").val(payment_mode);
                        $("#final-itd-amt").val(itd_amount);
                        var purchaseITD = $('#valid-itd-amount').val();
                        if(payment_mode == "paypal"){
                            var sgd_amt = purchaseITD/sgdConversionRate;     
                            Msg = "Are you sure, You want to buy "+purchaseITD+" "+keywoDefaultCurrency+" at "+sgd_amt.toFixed(6)+" SGD?";
                        }else{
                            Msg = "Are you sure, You want to buy "+purchaseITD+" "+keywoDefaultCurrency+"?";                            
                        }


                        $("#response").text('');
                        $("#response").text(Msg);
                        $("#keyword-popup-confirmation").modal("show");
                    }else{
                        if(data.errCode == 100){
                            location.reload();
                        }

                    }
 
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
            
        }
        
    }

    function paymentAfterConfirmation(){
        var payment_mode = $("#payment-mode").val();
        var itd_amount = $("#final-itd-amt").val();
        var order_id = $("#order-id").val();

        if(payment_mode == "bitcoin"){
            purchaseITDByBitgo(payment_mode,itd_amount,order_id);
        }else if(payment_mode == "paypal"){
            purchaseITDByPaypal(payment_mode,itd_amount,order_id);
        }

    }


    function purchaseITDByPaypal(payment_mode,itd_amount,order_id){
        $('#payment-confirm-btn').val('Processing...');
        $('#payment-cancel-btn').css("visibility","hidden");
        var sgd_amt = itd_amount/sgdConversionRate;
            sgd_amt = sgd_amt.toFixed(2);
        var custom = "<?php echo $_SESSION['email'].'~'; ?>"+sgd_amt+"<?php echo '~'.$getExchangeRate['sgd'].'~'.$getExchangeRate['usd']; ?>";
        var kwdBasketPaypal = "itd_purchase_only";

        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/paypalPrePaymentController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                client_sid: session_id,finalAmount:itd_amount,mode:payment_mode,order_id:order_id
            },
            async: true,
            success:function(data){
                if(data.errCode == -1){
                    $("#totalPricePaypal").val("");
                    $("#totalPricePaypal").val(sgd_amt);
                    $("#itemPricePaypal").val("");
                    $("#itemPricePaypal").val(sgd_amt);
                    $("#keywordBasketPaypal").val("");
                    $("#keywordBasketPaypal").val(kwdBasketPaypal);
                    $("#customPaypal").val("");
                    $("#customPaypal").val(custom);
                    $("#itemnumberPaypal").val("");
                    $("#itemnumberPaypal").val(order_id);
                    $("#buyByPaypalForm").submit();
                }else{
                    location.reload();
                }

            },
            error: function (xhr, status, error) {
                location.reload();
            }
        });


    }

    function purchaseITDByBitgo(payment_mode,itd_amount,order_id){
        $('#payment-confirm-btn').val('Processing...');
        var finalAmount = itd_amount * usdConversionRate;
        var keywordBasket = '';
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/keywords/getPaymentAddressForWallet.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                client_sid: session_id,finalAmount:finalAmount,mode:payment_mode,encryptedCustom:order_id
            },
            async: true,
            success:function(data){
                $('#payment-confirm-btn').val('Yes');
                $("#keyword-popup-confirmation").modal("hide");
                if(data.errCode == -1){
                    window.localStorage.clear();
                    btcAddress = data.paymentAddress;
                    // load payment dailog
                    $('#procceedToPayment').load("bitgo_payment_dialog.php", {client_id: session_id, finalBtcAmount:data.amountInBTC, finalUsdAmount:data.amountInUSD, recieverAddress:data.paymentAddress, keywordWithTags: keywordBasket, orderId: order_id});

                    $('#procceedToPayment').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }else{

                }

            },
            error: function (xhr, status, error) {
                alert(error);
            }
        });
    }

    // Check for first confirmation from bitgo
    function checkForBitgoConfirmation(){

        var encryptedCustom = $("#order-id").val();
        var btcAddress = $.trim($("#btcAddress").val());
        var client_sid = session_id;

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: rootUrl+"controllers/keywords/getPaymentConfirmation.php",
            data: {
                client_sid: encodeURIComponent(client_sid),
                orderId: encryptedCustom,
                recieverAddress: btcAddress
            },

            success: function(data){
                if(data.errCode == -1){
                    var status = data.errMsg.status;
                    stopTimer();
                    $('.sucess').css('display','block');
                    $('.dailogMess').css('display','none');
                    $('#paymentStatus').html('');
                    $('#paymentStatus').html('Payment success');
                    $('#paymentStatus').attr('onclick','window.location.href="index.php"');
                    $('.message').html('');
                    if(status == 'paid' || status == 'exact')
                    {
                        $('#paymentStatus').css('background','green');
                        $('.message').html("We have recieved your payment amount "+data.errMsg.amount_recieved+" successfully");
                    } else if(status == 'mispaid' || status == 'overpaid'){
                        $('.message').html("We have recieved your payment amount "+data.errMsg.amount_recieved+", Please contact searchtrade for refund "+status+" amount");
                    } else {
                        $('.message').html("We did not recieved your payment");
                    }


                    //var t = setTimeout(window.location='https://searchtrade.com/dashbord.php', 000);
                    //Display message to user for success
                }else{
                    console.log('Pending Confirmation')
                }
            }

        }); 
    }

    // function purchaseITDByPaypal(){
    //     $('#payment-confirm-btn').val('Yes');
    //     $("#keyword-popup-confirmation").modal("hide");
    //     alert("Comming Soon...");
    // }
    </script>
</main>
<!--========================================================================================================================================  
(24-2-17)
    BUG:EG-305
        - input text field changed typ from number to text.
    BUG:EG-306
        - input text field changed typ from number to text.
(27-2-17)
    




=========================================================================================================================================-->
