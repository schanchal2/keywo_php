<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$docrootpath = "../../"; 
include("../layout/header.php");
require_once("../../models/wallet/walletModel.php");
require_once('../../models/wallet/walletKYCModel.php');

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

//Search database connection
$searchConn = createDbConnection('dbsearch');

if (noError($searchConn)) {
    $searchConn = $searchConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}else{
	$kyc_level = 1;
}

$kycStablDetails = getKYCSlabByLevel($searchConn);
if(noError($kycStablDetails)){
    $kycStablDetails = $kycStablDetails['errMsg'];
}

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container padding-none row-10">
        <div class="row">
            <?php
            $page = "account_level"; 
            include("rightMenu.php"); ?>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Account Level</h4>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll">
                                        <span class="level-verified-tick half innerT">
                            <i class="fa fa-check-circle text-App-Success pull-right"></i>
                        </span>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 1</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15">  Email Verified</span>
                                        <span class="text-Blue btn-block margin-none text-center"><?php echo $email; ?></span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send <span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['send_fund_limit']." <?php echo $keywoDefaultCurrencyName; ?>"; ?>"><?php echo number_format((float)$kycStablDetails[0]['send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['yearly_send_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[0]['yearly_send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Year</span></li>
                                            <!-- <li class="innerT f-sz16">Request<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['receive_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[0]['receive_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                                </span> /Day</span></li> -->
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[0]['withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['yearly_withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[0]['yearly_withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Income<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[0]['yearly_income_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[0]['yearly_income_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15"><?php echo $kycStablDetails[0]['intraction']; ?>/Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15"><?php echo $kycStablDetails[0]['transaction_fees']; ?>%</span></li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-App-Success innerMT text-White">Completed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll ">
                                        <?php if($kyc_level == 1){ $verify = "Verify mobile"; ?>
                                            <span class="level-verified-tick half innerT"></span>                                  
                                        <?php }else{ $verify = "Mobile Verified"; ?>
                                            <span class="level-verified-tick half innerT"><i class="fa fa-check-circle text-App-Success pull-right"></i></span>
                                        <?php } ?>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 2</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15">  <?php echo $verify; ?></span>
                                        <span class="text-Blue btn-block margin-none text-center" style="<?php echo $style=(empty($mobile_number))?'padding:10px;':''; ?>"> <?php echo $mobile_number; ?> </span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send <span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['send_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['yearly_send_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['yearly_send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Year</span></li>
                                            <!-- <li class="innerT f-sz16">Request<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['receive_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['receive_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                                </span> /Day</span></li> -->
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['yearly_withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['yearly_withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Income<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[1]['yearly_income_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[1]['yearly_income_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15"><?php echo $kycStablDetails[1]['intraction']; ?>/Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15"><?php echo $kycStablDetails[1]['transaction_fees']; ?>%</span></li>
                                        </ul>
                                        <?php if($kyc_level == 1){?>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-Blue innerMT text-White" onclick="window.location = 'verifyMobileNo.php'">Apply Now</button>
                                        </div>                                        
                                        <?php }else{ ?>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-App-Success innerMT text-White">Completed</button>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll ">
                                        <?php if($kyc_level != 3){?>
                                            <span class="level-verified-tick half innerT"></span>                                  
                                        <?php }else{ ?>
                                            <span class="level-verified-tick half innerT"><i class="fa fa-check-circle text-App-Success pull-right"></i></span>
                                        <?php } ?>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 3</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15"> Verify photo ID and Address Proof</span>
                                        <span class="text-Blue btn-block margin-none text-center">  &nbsp;</span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send <span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['send_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['yearly_send_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['yearly_send_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Year</span></li>
                                            <!-- <li class="innerT f-sz16">Request<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['receive_fund_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['receive_fund_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                                </span> /Day</span></li> -->
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['yearly_withdrawal_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['yearly_withdrawal_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Income<span class="pull-right f-sz15">
                                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kycStablDetails[2]['yearly_income_limit']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$kycStablDetails[2]['yearly_income_limit'], 2, '.', '')." ".$keywoDefaultCurrencyName; ?></span>/Day</span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15"><?php echo $kycStablDetails[2]['intraction']; ?>/Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15"><?php echo $kycStablDetails[2]['transaction_fees']; ?>%</span></li>
                                        </ul>

                                        <?php if($kyc_level == 2){?>
                                        <div class="text-center">
                                            <button type="button" onclick="window.location = 'submitDocuments.php'" class="btn bg-Blue innerMT text-White">Apply Now</button>
                                        </div>                                      
                                        <?php }else if($kyc_level != 2 && $kyc_level != 3){?>
                                        <div class="text-center">
                                            <button type="button" disabled class="btn bg-Blue innerMT text-White">Apply Now</button>
                                        </div>                                      
                                        <?php }else if($kyc_level == 3){ ?>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-App-Success innerMT text-White">Completed</button>
                                        </div>
                                        <?php } ?>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->

    <!-- Modal -->
    <div id="OTPVerification" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close text-color-Gray" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-blue">Mobile Verification</h4>
          </div>
          <div class="modal-body text-center">
            <div class="">
              <h5 class="text-dark-green">Please enter OTP that you received</h5>
            </div>
            <div class="">
              <input type="number" name="" value="">
            </div>
            <div class="innerMTB inner-2x">
              <input type="button" class="btn-social-wid-auto btn-xs innerMR" value="Resend OTP">
              <input type="button" class="btn-social-dark btn-xs" value="Continue">
            </div>
          </div>
        </div>

      </div>
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================
              21-2-17



              (24-2-17)
              BUG:  Eg-289
                1)removed
                2)Transaction fees text spelling is wrong from Level 2 box.                                             @done   corrected
                3)Need to make small size of account level boxes.(See the design sheet).                                @done   modified model header section
                4)Bad spacing in between Level text and Email Verified (All three Level boxes.)                         @done   added ".margin-none"
                5)Place mobile number instead of Email address (level2 Box.)                                            @done   replaced




=========================================================================================================================================-->

