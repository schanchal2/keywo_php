<?php 


?>

<h3 style="color: #0b6796;">Send</h3>
<div class="row" style="background-color: #fff;padding: 85px; border: 1px solid #ccc;">
	<div class="col-xs-8">
	<form action="../../controllers/wallet/walletSendMoneyController.php" method="POST">
	<label>Recipient :</label> <input type="text" class="wallet-input" name="email_handle" placeholder="Enter email or handle" required /><br>
	<label style="padding-left: 10px;">Amount :</label> <input type="number" onkeyup="validate();" step="0.1" class="wallet-input-amount" id="amount_itd" name="amount_itd" placeholder="Amount in IT$" required/> <input type="number" step="0.1" class="wallet-input-amount"  id="amount_usd" name="amount_usd" placeholder="Amount in USD" /><br>
	<label style="padding-left: 29px;">Note : </label> <textarea class="wallet-input" name="note" placeholder="Write an optional message"></textarea><br>
	<button class="send-button">Send</button>
	</form>
	</div>
	<div class="col-xs-4"></div>
</div>

<script type="text/javascript">
function validate(){
	var itd = $('#amount_itd').val();
	$('#amount_usd').val(itd);
}	
</script>