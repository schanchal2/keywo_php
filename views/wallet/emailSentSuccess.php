<?php
session_start();
$email = $_SESSION['email'];
?>
<div class="bg-White border-all clearfix innerAll inner-2x Authentication text-Light-Black">
<h4 class="text-Blue margin-top-none f-sz20">Verify Your Transaction Key</h4>
    <p class="f-sz15 l-h16">We will send verification link to the email address below to activate transaction Pin.</p>
   
    <div class="bg-grayscale-fb border-all innerAll ">
        <div class="row">
            <div class="col-xs-4 border-right">
                <div class="Authentication-step text-greenscale-4">
                    <div class=" text-center">
                        <span class="Authentication-step-index"> Step 1</span>
                        <span class="Authentication-step-title"> SETUP </span>
                        <span class="Authentication-step-icon innerT"> 
                      <span class="fa-stack fa-sm">
                        <i class="fa fa-gears f-sz24 l-h12"></i> 
                      </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 border-right">
                <div class="Authentication-step ">
                    <div class="Authentication-step text-center text-darkgrayscale-be">
                        <span class="Authentication-step-index"> Step 2</span>
                        <span class="Authentication-step-title">VERIFY</span>
                        <span class="Authentication-step-icon innerT">                        
                     <span class="fa-stack fa-sm">
                        <i class="fa fa-certificate fa-stack-2x f-sz30 "></i>
                        <i class="fa fa-circle-thin fa-stack-1x text-white f-sz20 l-h15"></i>
                        <i class="fa fa-check fa-stack-1x text-White f-sz10"></i>
                      </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 text-Gray ">
                <div class="Authentication-step">
                    <div class="Authentication-step text-center  text-grayscale-be">
                        <span class="Authentication-step-index"> Step 3</span>
                        <span class="Authentication-step-title"> CONFIRM</span>
                        <span class="Authentication-step-icon innerT">  
                        <i class="fa fa-check-circle fa-stack-2x f-sz30 "></i>
                 </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====  End of steps strip  ====-->
    <div class="row">
    	<div class="col-xs-12 innerT">
    		<p class="f-sz-15">The verification link has been sent to <span class="text-blue"> <?php echo $email; ?></span></p>
    		<p>Please click the link to activate transaction Pin</p>	
    	</div>
    </div>
		
    
    <div class="col-lg-12">
        <div class=" innerT clearfix ">
            <button type="button" class="btn  pull-right  btn-trading-wid-auto  pull-right btn-trading-dark" id="resend-btn" onclick="resendEmail();">Resend Email</button>
            <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto " onclick="loadSetTransactionPin();">Previous</button>
        </div>
    </div>
</div>