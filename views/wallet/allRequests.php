<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	echo "Authorisation failure";
  	exit;
}

require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");

error_reporting(0);
$status = isset($_GET['status'])?$_GET['status']:'';
$dateRange = isset($_GET['date_range'])?$_GET['date_range']:'';
$search = isset($_GET['search'])?$_GET['search']:'';
$sortBy1 = isset($_GET['sort_by'])?explode("-", $_GET['sort_by']):'';
$sortBy = $sortBy1[0];
$deepFilter = $sortBy1[1];
$daterange_to = isset($_GET['daterange_to'])?$_GET['daterange_to']:'';
$daterange_from = isset($_GET['daterange_from'])?$_GET['daterange_from']:'';
$email = $_SESSION["email"];
$first_bkwd_cursor = isset($_GET['first_bkwd_cursor'])?$_GET['first_bkwd_cursor']:'';

if(!empty($daterange_from) || !empty($daterange_to)){
	$date = array();
	$date = explode("-",$daterange_to);
	$daterange_to = trim($date[1]);
	$daterange_from = trim($date[0]);
	
	$daterange_from = $daterange_from." 00:00:00";
	$daterange_to = $daterange_to." 23:59:59";

	$from = strtotime(trim($daterange_from)) * 1000;
	$to = strtotime(trim($daterange_to)) * 1000;
}

if(isset($_GET['fwd_offset_from'])){
	$bkwd_cursor = '';
	$fwd_cursor = $_GET['fwd_offset_from'];
	$offset_from = '';
}elseif (isset($_GET['offset_from'])) {
	$fwd_cursor = '';
	$offset_from = $_GET['offset_from'];
}else{
	$fwd_cursor = '';
	$bkwd_cursor = '';
	$offset_from = '';
}

$userRequiredFields = $userRequiredFields.",email,account_handle";

//fetch details of sender and receiver                  	
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
$userDetails = $userDetails['errMsg'];	

$result = getUsersAllRequests($userDetails['email'],$userDetails['_id'],$status,$sortBy,$from,$to,$search,$bkwd_cursor,$fwd_cursor,$offset_from,$deepFilter);
if(noError($result)){
	$bkwd_cursor_offset_from = $result['errMsg']['bkwd_cursor_offset_from']; 
	if($result['errMsg']['fwd_cursor_offset_from'] == -1){
		$_SESSION['first_bkwd_cursor'] = $result['errMsg']['bkwd_cursor_offset_from'];
	}
	$first_bkwd_cursor = $_SESSION['first_bkwd_cursor'];
}else{
	print("Error in fetching transaction history");
	exit();
}

?>
<div class="row ">
    <div class="col-xs-12 ">
        <div class="table-responsive wallet__trasactionHistory text-Light-Black ">
            <table class="table table-hover border-all text-center ">
                <thead class="text-White ">
                    <tr><th class="text-left ">Request Type </th>
                        <th> Quantity </th>
                        <th>Discription </th>
                        <th>
                            <label class="dropdownOptions ">
                                <select class="tr-type" id="tr-status"  onchange="getsortedTransaction();" style="background-color: #90a1a9;border: none;">
							  		<option selected disabled>Status</option>
                                	<option value="" <?php if($status==""){ echo "selected"; }?>>All</option>
								    <option value="pending" <?php if($status=="pending"){ echo "selected"; }?>>Pending</option>
								    <option value="rejected" <?php if($status=="rejected"){ echo "selected"; }?>>Rejected</option>
								</select>
                            </label>
                            </th>
                            <th>Date </th>
                            <th>   </th>
                            <th>   </th>
                        </tr>
                </thead>
                <tbody>
                	<?php foreach ($result['errMsg']['batched_container'] as $key => $value) { ?>
                    <tr>
                     <?php 
                        if($value['request_sender']== $email){ 
                        	$type = "Sent";
                        	$senderDesc = sprintf($descriptionArray['req_sender_desc'], $value['amount'], 'IT$', $value['request_receiver']);

                        	$description = $senderDesc; 
                        }elseif($value['request_sender'] != $email){
                        	$type = "Received"; 
                        	$receiverDesc = sprintf($descriptionArray['req_receiver_desc'], $value['amount'], 'IT$', $value['request_sender']);
                        	$description = $receiverDesc; 
                        }else{
                        	$type = $value['type'];
                        } 

                        ?>
                        <td class="text-left text-grayscale-4"> <?php echo $type; ?> </td>
                        <td class="text-Dark-Blue "> <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $value['amount']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$value['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></td>
                        <td class="text-grayscale-4"> <?php echo urldecode($description); ?>  </td>
                        <?php if($value['transaction_status'] == 'pending'){ ?>
                        <td class="text-orange ">  <?php echo ucfirst($value['transaction_status']); ?> </td>
                        <?php }elseif($value['transaction_status'] == 'rejected'){ ?>
                        <td class="text-Red">  <?php echo ucfirst($value['transaction_status']); ?> </td>
                        <?php }else{ ?>
                        <td class="ttext-grayscale-4">  <?php echo ucfirst($value['transaction_status']); ?> </td>
                        <?php } ?>
                        <td class="text-grayscale-4">  <?php echo date("jS M, Y",$value['time']/1000); ?> </td>
                        <td><a onclick="showDetails('<?php echo $value['_id']; ?>');">Details</a></td>
                        <td>  <?php if($value['request_sender'] != $email && $value['transaction_status'] != "rejected" ){ ?>  
                        <div class="btn-group action-btn" role="group">   
                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" onclick="showAcceptPopup('<?php echo urlencode(json_encode($value)); ?>','<?php echo $value['amount']; ?>','<?php echo $value['request_sender']; ?>');" >  <i class="fa fa-check"> </i>  </button>  
                            <button type="button" class="btn text-Light-Black innerLR padding-right-none"  onclick="showRejectPopup('<?php echo urlencode(json_encode($value)); ?>','<?php echo $value['amount']; ?>');" >  <i class="fa fa-close"> </i>  </button>     
                        </div>
                        <?php } ?>             
                        </td>
                         <?php } ?>
                    </tr>
               </tbody>
            </table>
            <?php if(!isset($result['errMsg']['batched_container']) || empty($result['errMsg']['batched_container'])){ ?>
        <h4 style="text-align: center;">No relevant data found !!!</h4>
        <?php } ?>
            <!--===============================================
        =            avoid this in INTEGRATION            =
        ================================================-->
            <!--====  End of avoid this in INTEGRATION  ====-->
        </div>
    </div>
</div>
<div class="row keyword-marketplace-data innerT inner-2x">
    <div class="col-md-5">
        <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
    </div>
    <div class="col-md-7">
        <div class="pagination-cont pull-right">

            <ul class="pagination">

                <?php if($first_bkwd_cursor != $result['errMsg']['bkwd_cursor_offset_from']){ ?>
                    <li ><a onclick="showNewer('<?php echo $result['errMsg']['fwd_cursor_offset_from']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <?php }else{ ?>
                    <li class="disabled"><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <?php } ?>

                <?php if($result['errMsg']['bkwd_cursor_offset_from'] != -1){ ?>
                <li><a onclick="showOlder('<?php echo $result['errMsg']['bkwd_cursor_offset_from']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                <?php }else{ ?>
                    <li class="disabled"><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                <?php } ?>

                
                
            </ul>
        </div>
    </div>
</div>