<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

include("../layout/header.php");

$currpreff = strtolower($_SESSION['CurrPreference']);
$conversionRate = $getExchangeRate['usd'] / $getExchangeRate[$currpreff] ;

echo "<script> var conversionRate = " . $conversionRate . "; </script>";

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}
unset($_SESSION['verify_status']);
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container  padding-none row-10">
        <div class="row">
            <?php
            $page = "send"; 
            include("rightMenu.php"); ?> 

              <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20">Send</h4>
                        <div class="card bg-White border-all innerAll inner-2x text-Light-Black">
                            <div class="row">
                            <div class="send-err" id="send-err-id" style="display: none; color: red; text-align: center;"></div>
                            <div class="send-err" style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                            <div class="send-err" style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                            <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); ?><br>
                                <div class="col-xs-9 col-xs-offset-1">
                                    <form id="send-form" class="form-horizontal" action="../../controllers/wallet/walletSendMoneyController.php" method="POST">
                                        <div class="form-group filter">
                                            <label for="" class="col-sm-2 control-label text-right f-sz16 padding-right-none" ">Recipient :</label>
                                        <div class="col-sm-10 ">
                                            <div class="input-group text-Gray ">
                                                <input placeholder="Enter email or handle " type="email " class="form-control text-Gray " name="email_handle" required>
                                                <span class="input-group-addon bg-white text-Gray "> <i class="fa fa-group " ></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 
                                     -->
                                    <div class="form-group filter ">
                                        <label for=" " class="col-sm-2 control-label text-right f-sz16 padding-right-none "">Amount :</label>
                                            <div class="col-sm-5">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="0.0000" type="number" class="form-control text-Gray" onkeyup="validate();" id="amount_itd" step="0.00001" name="amount_itd" required>
                                                    <span class="input-group-addon  bg-white text-Gray"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group text-Gray">
                                                     <input placeholder="0.0000" type="number" class="form-control text-Gray" step="<?php echo $step =($_SESSION['CurrPreference']== 'BTC')?'0.00000001':'0.00001' ?>" id="amount_usd" onkeyup="validate1();" name="amount_usd" >
                                                    <span class="input-group-addon  bg-white text-Gray" id="crr-preff"><?php echo $_SESSION['CurrPreference']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group filter">
                                            <label for="" class="col-sm-2 control-label text-right f-sz16 padding-right-none" ">Note :</label>
                                        <div class="col-sm-10 ">
                                            <div class="input-group text-Blue btn-block ">
                                                <textarea placeholder="Write an optional message" maxlength="100" name="note" id="input" class="form-control noresize" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-sm-offset-2 col-sm-10 ">
                                            <button type="submit" id="send-itd-btn" class="btn pull-right btn-trading-wid-auto f-sz14">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
<?php include("../layout/transparent_footer.php");?>
    <script type="text/javascript">

    
    $( "#send-form" ).submit(function( event ) {
       var min_request_amt = <?php echo $minRequestAmount; ?> 
      $("#send-itd-btn").text('Sending...');
      $(".send-err").hide();   
      var itd_amount = $('#amount_itd').val();
      if(itd_amount < min_request_amt){
        $("#send-err-id").show();
        $("#send-itd-btn").text('Send');
        $("#send-err-id").text("Please enter amount greater than "+min_request_amt+" "+keywoDefaultCurrency);
        return false;
      }

    });

	function validate(){
		var itd_amount = $('#amount_itd').val();
        var covertedAmount = itd_amount/conversionRate;
        var currPrefAmt = covertedAmount.toFixed(5);
        var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        if(currPreff == 'BTC'){
            var currPrefAmt = covertedAmount.toFixed(8);
        }
        $("#amount_usd").val(currPrefAmt);
        $("#crr-preff").text(currPreff);
	}	

    function validate1(){
        var usd_amount = $('#amount_usd').val();
        var covertedAmount = usd_amount*conversionRate;
        var currPrefAmt = covertedAmount.toFixed(2);
        //var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        $("#amount_itd").val(currPrefAmt);
        //$("#crr-preff").text(currPreff);
    }

    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<!--========================================================================================================================================        
            ( 16-02-17)  
            table base created 
           
            /**
            
                TODO:
                    ( 17-02-17) 
                - appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
                - color name comflict in '.bg-blue-key-dark, .bg-blue-key'
                - looks better if .my-info-card bottom padding is removed
                    ( 18-02-17) 
                - input highlighting can be improoved. 
             */
            



                                                                                 
            


=========================================================================================================================================-->
