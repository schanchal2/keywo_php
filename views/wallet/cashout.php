<?php
session_start();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
} 

$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

$email =  $_SESSION["email"];
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
    $walletDetails = getWalletAddress($email);
    if(noError($walletDetails)){
        $walletDetails = $walletDetails['errMsg'];
    }
    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
    if(noError($adminSettings)){
        $adminSettings = $adminSettings['data'];
        $minCashoutAmt = $adminSettings['minimum_withdrawal_amount'];
    }else{
        print('Error in fetching admin settings');
        exit;
    }
} 

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}   

$currpreff = strtolower($_SESSION['CurrPreference']);
$conversionRate = $getExchangeRate['usd'] / $getExchangeRate[$currpreff] ;
$btcConversionRate = $getExchangeRate['usd'] / $getExchangeRate['btc'] ;
$usdConversionRate = $getExchangeRate['usd'] / $getExchangeRate['usd'] ;

echo "<script> var conversionRate = " . $conversionRate . "; </script>";
echo "<script> var btcConversionRate = " . $btcConversionRate . "; </script>";
echo "<script> var usdConversionRate = " . $usdConversionRate . "; </script>";
echo "<script> var minCashoutAmt = " . $minCashoutAmt . "; </script>";
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerTB WT5">
    <div class="container padding-none row-10">
        <div class="row">
            <?php
            $page = "cashout"; 
            include("rightMenu.php"); ?> 
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row ">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Cashout </h4>
                        <div class="card bg-White border-all  padding-bottom-none innerMB f-sz15">
                            <div class="row">
                                <div class="col-xs-12 innerMTB inner-2x">
                                <?php if(isset($_SESSION['err_msg']) || isset($_SESSION['succ_msg'])){ ?>
                                    <div style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                                    <div style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                                    <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); ?><br>
                                <?php } ?>
                                    <form class="form-horizontal">
                                        <!-- 
                                     -->
                                        <div class="form-group  margin-bottom-none filter  clearfix">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right  text-grayscale-6">Enter Amount : </label>
                                            <div class="col-sm-4">
                                                <div class="input-group text-grayscale-4">
                                                    <input type="number" id="itd-amount" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" step="0.0001" value="" onkeyup="convertAmount();" placeholder="0.0000">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"><?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                                <small id="err-msg-itd-amount" class="text-Red"></small>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="0.0000" type="number" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" id="crr-preff-amt" step="0.0001" value="" onkeyup="convertAmount1();">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6" id="crr-preff"><?php echo $_SESSION['CurrPreference']; ?></span>
                                                </div>
                                            </div>
                                            <!-- <button type="submit" class="bg-Light-Blue btn-xs btn  col-sm-2 text-White pull-right wallet-Request">Proceed</button> -->
                                            <div class="col-md-2 text-center keyword-checkout">
                                                <input value="Proceed" type="button" onclick="validateCashoutAmt();" class="btn-pay-now f-sz18 l-h12">
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="current-wallet-balance innerMT innerAll inner-2x" id="payout-mode-box" style="display: none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="current-w1">
                                        <h5 class="f-sz16 margin-top-none">Current Wallet Balance is : <span id="wallet-balance"></span> <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your cashout amount : <span id="cashout-amt"></span> <?php echo $keywoDefaultCurrencyName; ?></h5>
                                    </div>
                                </div>
                                 <form id="cashout-form" action="../../controllers/wallet/cashoutRequestController.php" method="POST">
                                <div class="col-md-6">
                                    <h5 class="f-sz16 text-center margin-top-none">Cashout <?php echo $keywoDefaultCurrencyName; ?> with <i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                    <div class="row leadCorrect-t ">
                                        <div class="col method">Method</div>
                                        <div class="col text-center fees ">Fees</div>
                                        <div class="col text-center netAmmount">Net Ammount</div>
                                        <div class="col currency"></div>
                                    </div>
                                    <hr class="margin-top-none">
                                    <div class="row leadCorrect-t">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16">
                                                <input type="hidden" name="valid-itd-amount" id="valid-itd-amount">
                                                <input type="radio" name="method" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label class="" for="Bitcoin"> Bitcoin</label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees " id="bitcoin-cashout-fees">0.00083</div>
                                        <div class="col text-center netAmmount" id="bitcoin-net-amt" >0.08</div>
                                        <div class="col currency text-right">BTC</div>
                                    </div>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16">
                                                <input type="radio" name="method" id="PayPal" value="PayPal">
                                                <label class="" for="PayPal"> PayPal</label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees " id="paypal-cashout-fees">1.00</div>
                                        <div class="col text-center netAmmount" id="paypal-net-amt">99.00</div>
                                        <div class="col currency text-right">USD</div>
                                    </div>
                                
                                   <!--  <div class="row leadCorrect-t">
                                        <div class="col method">
                                            <div class="radio radio-primary radio-div margin-top-none f-sz16 ">
                                                <input type="hidden" name="valid-itd-amount" id="valid-itd-amount">
                                                <input type="radio" name="mode" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label class="" for="Bitcoin">
                                                    Bitcoin
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees " id="bitcoin-cashout-fees">0.00083</div>
                                        <div class="col text-center netAmmount" id="bitcoin-net-amt" >0.08</div>
                                        <div class="col currency text-right">BTC</div>
                                    </div>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-primary radio-div margin-top-none f-sz16 ">
                                                <input type="radio" name="mode" id="PayPal" value="PayPal">
                                                <label class="" for="Bitcoin">
                                                    PayPal
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees " id="paypal-cashout-fees">1.00</div>
                                        <div class="col text-center netAmmount" id="paypal-net-amt">99.00</div>
                                        <div class="col currency text-right">USD</div>
                                    </div> -->
                                </div>
                                </form>
                                <div class="col-md-12 text-center innerMB keyword-checkout">
                                    <input value="Proceed to Cashout" onclick="sendCashoutRequest();" type="button" class="btn-pay-now f-sz16 l-h12 innerMT inner-2x">
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- 

                         -->
                            <div class="row  inner-2x innerT">
                                <div class="col-xs-6 ">
                                    <div class="bg-White border-all clearfix cashout-card">
                                        <div class="modal-header padding-none">
                                            <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> <p class="leadCorrect-b">Current Bitcoin Cashout Address </p></h5> </div>
                                        <div class="modal-body padding-none">
                                            <p class="innerAll text-grayscale-6 f-sz16 margin-bottom-none l-h10 inner-2x"><b>Bitcoin Wallet</b> Address</p>
                                            <form class="form-horizontal innerLR inner-2x">
                                                <div class="form-group margin-bottom-none">
                                                    <div class="col-sm-12">
                                                        <div class="half innerMLR">
                                                            <input type="email" id="bitcoin-address" class="form-control b-shadow-none inputAddress bg-grayscale-fb change-input" value="<?php echo $walletDetails['current_wallet_address']; ?>" disabled>
                                                            <small id="err-msg-bitcoin-address" class="text-Red cash-err"></small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="innerAll inner-2x clearfix">
                                            <button type="button" onclick="window.location.href='cashoutSettings.php'" class="btn btn-primary pull-right btn-trading-wid-auto f-sz14 change-input"><?php echo $btn=(empty($walletDetails['current_wallet_address'])?'Set Address':'Change'); ?></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 ">
                                    <div class="bg-White border-all clearfix cashout-card">
                                        <div class="modal-header padding-none">
                                            <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> <p class="leadCorrect-b">Current Paypal Cashout Address</p> </h5> </div>
                                        <div class="modal-body padding-none">
                                            <p class="innerAll text-grayscale-6 f-sz16 margin-bottom-none l-h10 inner-2x"><b>PayPal Email</b> Address</p>
                                            <form class="form-horizontal innerLR inner-2x">
                                                <div class="form-group margin-bottom-none">
                                                    <div class="col-sm-12">
                                                        <div class="half innerMLR">
                                                            <input type="email" id="paypal-address" class="form-control b-shadow-none inputAddress bg-grayscale-fb change-input" value="<?php echo $walletDetails['current_paypal_address']; ?>" disabled>
                                                            <small id="err-msg-paypal-address" class="text-Red cash-err"></small>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="innerAll clearfix inner-2x">
                                            <button type="button" onclick="window.location.href='cashoutSettings.php'" class="btn btn-primary pull-right btn-trading-wid-auto f-sz14 change-input"><?php echo $btn=(empty($walletDetails['current_paypal_address'])?'Set Address':'Change'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 

                             -->
                        </div>
                    </div>
                </div>
                <div id="cashout-requests">
                    <?php include("allCashoutRequest.php"); ?>
                </div>
                
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="bg-White border-all clearfix ">
                    <div class="modal-body" id="transction-details">
                        
                        
                    </div>
            </div>
        </div>
    </div>
    <!-- container -->
    <?php include("../layout/transparent_footer.php");?>
    <script type="text/javascript">
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });



    jQuery(document).ready(function($) {
        $("button.change-input").on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $(this).text("Verify Now").attr('type', 'submit');
            $(this).parents(".cashout-card").find('input.change-input').removeAttr('disabled')
        });
    });

    function convertAmount(){
        $('#payout-mode-box').hide();
        var itd_amount = $("#itd-amount").val();
        var covertedAmount = itd_amount/conversionRate;
        var currPrefAmt = covertedAmount.toFixed(5);
        var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        if(currPreff == 'BTC'){
            var currPrefAmt = covertedAmount.toFixed(8);
        }
        $("#crr-preff-amt").val(currPrefAmt);
        $("#crr-preff").text(currPreff);
    }

    function convertAmount1(){
        $('#payout-mode-box').hide();
        var usd_amount = $("#crr-preff-amt").val();
        var covertedAmount = usd_amount*conversionRate;
        var currPrefAmt = covertedAmount.toFixed(2);
        //var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        $("#itd-amount").val(currPrefAmt);
        //$("#crr-preff").text(currPreff);
    }
    
    function changeValue(e){
        alert(e);
    }

    function removeCashoutRequest(status, cashout_id, request_date){
        var r = confirm("Are you sure want to remove cashout request?");
        if (r == true) {
        } else {
            return false;
        }
        if(status == 'pending' || status == 'pending_manual_verification'){
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/removeCashoutRequestController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    status: status,cashout_id:cashout_id,request_date:request_date
                },
                async: true,
                success:function(data){
                    console.log(data);
                    if(data.errCode == -1){
                        location.reload();
                    }else{ 
                        location.reload();
                    }
                    
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
        }else{
            alert("You can only remove pending cashout requests");
        }
    }

    function validateCashoutAmt(){
        $('#err-msg-itd-amount').text('');
        $('#payout-mode-box').hide();
        var itd_amount = $('#itd-amount').val();
        var wallet_address = $('#bitcoin-address').val();
        var paypal_address = $('#paypal-address').val();
        var current_balance = '<?php echo $userBalance; ?>';
        var cashout_fees = '<?php echo $adminSettings['withdrawal_fees']; ?>';  
        // alert(wallet_address);
        if(itd_amount == 0 || parseFloat(itd_amount) < minCashoutAmt){
            if(parseFloat(itd_amount) < minCashoutAmt ){
                $('#err-msg-itd-amount').text('Please enter amount greater than '+minCashoutAmt+' '+keywoDefaultCurrency);
            }else{
                $('#err-msg-itd-amount').text('Please enter valid amount');
            }
            
            return false;
        }else{
            if( wallet_address == '' && paypal_address == ''){
                $('#err-msg-bitcoin-address').text('Please set your bitcoin wallet address');
                $('#err-msg-paypal-address').text('Please set your paypal email address');
                return false;
            }else{
                //var total_amount = parseFloat(itd_amount) + parseFloat(cashout_fees);
                if(parseFloat(current_balance) >= parseFloat(itd_amount)){
                    var net_amount = itd_amount - cashout_fees;
                    var btc_cashout_fees = cashout_fees/btcConversionRate;
                    var btc_net_amt = net_amount/btcConversionRate;
                    var paypal_cashout_fees = cashout_fees/usdConversionRate;
                    var paypal_net_amt = net_amount/usdConversionRate;

                    if(paypal_net_amt > 0){
                        $('#payout-mode-box').show();
                        $('#wallet-balance').text(current_balance);
                        $('#cashout-amt').text(itd_amount);
                        $('#bitcoin-cashout-fees').text(btc_cashout_fees.toFixed(8));
                        $('#bitcoin-net-amt').text(btc_net_amt.toFixed(8));
                        $('#paypal-cashout-fees').text(paypal_cashout_fees.toFixed(6));
                        $('#paypal-net-amt').text(paypal_net_amt.toFixed(6));
                        $('#valid-itd-amount').val(itd_amount);
                    }else{
                        $('#err-msg-itd-amount').text('Please enter amount greater than '+paypal_cashout_fees+' '+keywoDefaultCurrency);
                    }
                    

                }else{
                    $('#err-msg-itd-amount').text('Your available balance is not sufficient for this transaction');
                }
            }
        }   

    }

    function sendCashoutRequest(){
        var itd_amount = $("#valid-itd-amount").val();
        var payment_mode = '';
        var paypal_check = $('#PayPal').is(':checked');
        var bitcoin_check = $('#Bitcoin').is(':checked');
        var wallet_address = $('#bitcoin-address').val();
        var paypal_address = $('#paypal-address').val();

        if(bitcoin_check && wallet_address.lenght == 0){
            $('#err-msg-bitcoin-address').text('Please set your bitcoin wallet address');
            return false;
        }else if(paypal_check && paypal_address.lenght == 0){
            $('#err-msg-paypal-address').text('Please set your paypal email address');
            return false;
        }

        if(bitcoin_check){
            payment_mode = "bitcoin";            
        }else if(paypal_check){
            payment_mode = "paypal"; 
        }

        if(payment_mode != '' && itd_amount != ''){
            $("#cashout-form").submit();
        }

    }

    //onclick of older button
    function showOlderRequest(backward_cursor){

        $("#cashout-requests").load("allCashoutRequest.php?backward_cursor="+encodeURIComponent(backward_cursor));
    }

    //onclick of newer button
    function showNewerRequest(forword_cursor){

        $("#cashout-requests").load("allCashoutRequest.php?forword_cursor="+encodeURIComponent(forword_cursor));
    }

    //to sort by cashout status
    function getsortedRequests(status){
        $("#cashout-requests").load("allCashoutRequest.php?status="+encodeURIComponent(status));
    }

    //to show request details
    function showRequestDetails(request_id,request_date){
        $('#transction-details').load('cashoutRequestDetails.php?cashout_id='+encodeURI(request_id)+"&request_date="+encodeURI(request_date));
        $('#myModal').modal('show');
    }
    </script>
</main>

<?php 

/*=============================================
(22-2-17)
    -placehoder color is kept same as it of in "/views/prafull_wallet/CashoutSetting.php" and of  "CashoutSetting UI Design"

(27-2-17)
    BUG:EG-291
        - changes made to siblings copied to this page. 
        -------------------------------------------------- 
        -1)Enter amount text and amount font color is different as per design sheet of cash out page.                               @done
        -2)Bad spacing  at start of Label in proceed and proceed to purchase boxes.                                                 @done                                       
        -3)Change payment text with cashout from proceed to purchase boxes.                                                         @done
        -4)Large text font size of proceed to purchase boxes as compare to design sheet.                                            @photoshop unavailable  
        -5)Current Bitcoin Cashout Address and Current PayPal Cashout Address text font are different as compare to design sheet.   @photoshop unavailable
        --------------------------------------------------
        
        
            TODO:
            - 1.input type can b changed to text from number as it is done in page "views/prafull_wallet/purchase.php"              @done
            - 2.vertical spacing can be incresed in "purchase boxes" to match layout with PSD 
            - 3.compare this page with "views/prafull_wallet/purchase.php".
    
=============================================*/
?>
