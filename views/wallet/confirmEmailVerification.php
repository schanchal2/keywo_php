<?php
$verification_status = $_GET['verification_status'];
$errMsg = trim(urldecode($_GET['errMsg']));
?>
<div class="bg-White border-all clearfix innerAll inner-2x Authentication text-Light-Black">
	<p class="f-sz15 l-h16">Two-factor Authentication adds an extra layer of security to your account. Once enabled, sign-In will require you to enter a unique verification code generated by an app on your mobile device or sent via SMS text message, In addition to your username and password</p>

	<div class="bg-grayscale-fb border-all innerAll ">
	    <div class="row">
	        <div class="col-xs-4 border-right">
	            <div class="Authentication-step text-greenscale-4">
	                <div class=" text-center">
	                    <span class="Authentication-step-index"> Step 1</span>
	                    <span class="Authentication-step-title"> SETUP </span>
	                    <span class="Authentication-step-icon innerT"> 
	                  <span class="fa-stack fa-sm">
	                    <i class="fa fa-gears f-sz24 l-h12"></i> 
	                  </span>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-xs-4 border-right">
	            <div class="Authentication-step ">
	                <div class="Authentication-step text-center text-greenscale-4">
	                    <span class="Authentication-step-index"> Step 2</span>
	                    <span class="Authentication-step-title">VERIFY</span>
	                    <span class="Authentication-step-icon innerT">                        
	                 <span class="fa-stack fa-sm">
	                    <i class="fa fa-certificate fa-stack-2x f-sz30 "></i>
	                    <i class="fa fa-circle-thin fa-stack-1x text-white f-sz20 l-h15"></i>
	                    <i class="fa fa-check fa-stack-1x text-White f-sz10"></i>
	                  </span>
	                    </span>
	                </div>
	            </div>
	        </div>
	        <div class="col-xs-4 text-Gray ">
	            <div class="Authentication-step">
	            <?php if($verification_status == 'verified'){ ?>
	                <div class="Authentication-step text-center text-greenscale-4">
	                    <span class="Authentication-step-index"> Step 3</span>
	                    <span class="Authentication-step-title"> CONFIRM</span>
	                    <span class="Authentication-step-icon innerT">  
	                    	<i class="fa fa-check-circle fa-stack-2x f-sz30 "></i>
	             		</span>
	                </div>
	            <?php }else{ ?>
	            	<div class="Authentication-step text-center text-redscale-4">
                        <span class="Authentication-step-index"> Step 3</span>
                        <span class="Authentication-step-title"> CONFIRM</span>
                        <span class="Authentication-step-icon innerT">  
                        	<i class="fa fa-times-circle fa-stack-2x f-sz30 "></i>
                 		</span>
                    </div>
	            <?php } ?>    
	            </div>
	        </div>
	    </div>
	</div>
	<!--====  End of steps strip  ====-->
	<?php if($verification_status == 'verified'){ ?>
	<div class="row">
		<div class="col-xs-12 text-center margin-top ">
			<p><?php echo $errMsg; ?></p>
		</div>
	</div>
	<?php }else{ ?>
	<div class="row">
    	<div class="col-xs-12 text-center margin-top ">
    		<p class="text-redscale-4"><?php echo $errMsg; ?></p>
    	</div>
    </div>	
	<?php } ?>
    <div class="col-lg-7">
        <div class=" innerT clearfix ">
          <button type="button" class="btn center-block innerMR btn-trading-wid-auto " onclick="window.location.href='twoFASettings.php';">Back to Security Settings</button>
        </div>
    </div>
</div>