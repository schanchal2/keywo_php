<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");
$transction_id = isset($_GET['transction_id'])?$_GET['transction_id']:'';
$transaction_boundary = '';
$email = $_SESSION["email"];
$userRequiredFields = $userRequiredFields.",email,account_handle";

//fetch details of sender and receiver                  	
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
$userDetails = $userDetails['errMsg'];

$dateTime = new DateTime(); 
$timezone = $dateTime->format('T');
$seconds = date_offset_get(new DateTime);
$minutes = $seconds/60;
$offset = ($minutes >= 0)?"+".$minutes:$minutes;

$result = getRequestDetails($userDetails['email'],$userDetails['_id'],$transction_id,$transaction_boundary);
// printArr($result);
$tranDetails = $result['errMsg']; 
if($email != $tranDetails['request_sender']){ 
	$transType = "Received";
	 }else if($email == $tranDetails['request_sender']){ 
$transType = "Sent"; }else{
	$transType = $tranDetails['type'];
} 
?>
<?php if($tranDetails['type'] == 'paymentrequest'){ ?>	
 
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-Text-Primary-Blue"><?php echo $transType; ?></h4> </div>

<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Quantity :</div>
    <div class="col-sm-9 text-Blue margin-none h4"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $tranDetails['amount']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$tranDetails['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
</div><div class="row innerMTB">
    
<?php if($email != $tranDetails['request_sender']){ 
	$receiverDesc = sprintf($descriptionArray['req_receiver_desc'], $tranDetails['amount'], $keywoDefaultCurrencyName, $tranDetails['request_sender']);
	$description = $receiverDesc;
    $note = $tranDetails['meta_details']['note'];
	?>
<div class="col-sm-3 text-Blue">From :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['request_sender']; ?></div>
<?php }else{ 
$senderDesc = sprintf($descriptionArray['req_sender_desc'], $tranDetails['amount'], $keywoDefaultCurrencyName, $tranDetails['request_receiver']);
$description = $senderDesc;
$note = $tranDetails['meta_details']['note'] ;
	?>
<div class="col-sm-3 text-Blue">To :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['request_receiver']; ?></div>
<?php } ?>	
    
</div>
<!-- <div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Country :</div>
    <div class="col-sm-9 text-Gray">India</div>
</div> -->
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Description :</div>
    <div class="col-sm-9 text-Gray"><?php echo $description; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Status :</div>
    <div class="col-sm-9 text-orange"><?php echo ucfirst($tranDetails['transaction_status']); ?> <i class="fa fa-circle"></i>   </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Date  :</div>
    <div class="col-sm-9 text-Gray"><?php echo date("jS M, Y H:i a",$tranDetails['time']/1000)." GMT {$offset}({$timezone})"; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">IP :</div>
    <div class="col-sm-9 text-Gray"><?php echo $tranDetails['origin_ip']; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Request Note :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $note; ?> </div>
</div>
<?php if(isset($tranDetails['meta_details']['reason'])){ ?>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Reason for Reject :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $tranDetails['meta_details']['reason']; ?> </div>
</div>
<?php } ?>


<?php }else{ ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-Text-Primary-Blue"><?php echo $transType; ?></h4> </div>

<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Quantity :</div>
    <div class="col-sm-9 text-Blue margin-none h4"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $tranDetails['amount']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$tranDetails['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
</div><div class="row innerMTB">
    
<?php if($email != $tranDetails['request_sender']){ 
	$receiverDesc = sprintf($descriptionArray['req_receiver_desc'], $tranDetails['amount'], $keywoDefaultCurrencyName, $tranDetails['request_sender']);
	$description = $receiverDesc;
	?>
<div class="col-sm-3 text-Blue">From :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['request_sender']; ?></div>
<?php }else{ 
$senderDesc = sprintf($descriptionArray['req_sender_desc'], $tranDetails['amount'], $keywoDefaultCurrencyName, $tranDetails['request_receiver']);
$description = $senderDesc;
	?>
<div class="col-sm-3 text-Blue">To :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['request_receiver']; ?></div>
<?php } ?>	
    
</div>
<!-- <div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Country :</div>
    <div class="col-sm-9 text-Gray">India</div>
</div> -->
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Description :</div>
    <div class="col-sm-9 text-Gray"><?php echo $description; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Status :</div>
    <div class="col-sm-9 text-orange"><?php echo ucfirst($tranDetails['transaction_status']); ?> <i class="fa fa-circle"></i>   </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Date  :</div>
    <div class="col-sm-9 text-Gray"><?php echo date("jS M, Y H:i a",$tranDetails['time']/1000)." GMT {$offset}({$timezone})"; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">IP :</div>
    <div class="col-sm-9 text-Gray"><?php echo $tranDetails['origin_ip']; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Note :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $tranDetails['meta_details']['note']; ?> </div>
</div>

<?php } ?>
