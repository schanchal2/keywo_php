<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

include("../layout/header.php");

require_once("{$docrootpath}models/wallet/walletUserModel.php");

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

$email =  $_SESSION["email"];
$conn = createDBConnection("dbsearch");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
    $allCurrencies = getAllCurrencies($conn);
    if(noError($allCurrencies)){
    	$allCurrencies = $allCurrencies['errMsg'];
    }else{
    	echo "Failed to fetch all currencies";
    	exit;
    }
   
}else{
	echo "Failed to connect with search database";
	exit;
} 

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$currpreff = strtolower($_SESSION['CurrPreference']);
$conversionRate = $getExchangeRate['usd'] / $getExchangeRate[$currpreff] ;

echo "<script> var conversionRate = " . $conversionRate . "; </script>";

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container  padding-none row-10">
        <div class="row">
            <?php
            $page = "currency_peferences"; 
            include("rightMenu.php"); ?>  
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none"> Currency Preferences </h4>
                        <div class="card bg-White border-all innerTB inner-2x">
                        	<?php if(isset($_SESSION['err_msg']) || isset($_SESSION['succ_msg'])){ ?>
                        	<div style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                                <div style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                                <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); ?><br>
                                <?php } ?>
                            <div class="row innerLR">
                                <div class="col-xs-12">
                                    <p class="text-grayscale-4"> Please select your display Currency (for easy reference only) </p>
                                    <form class="form-horizontal  half innerLR" action="../../controllers/wallet/currencyPrefController.php" method="POST">
                                        <div class="form-group margin-bottom-none">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <label class="text-grayscale-3 f-sz15 f-wt4" for="exampleInputEmail1">Current Display Currency : <span class="text-Blue f-sz16"><?php echo $_SESSION['CurrPreference']; ?></span></label>
                                                    <div class="input-group">
                                                        <div class="Sort-by materialize">
                                                            <label class="dropdownOptions pull-right">
                                                                <select class="selectpicker " name="curr_pref">
                                                                    <!-- <option disabled="">Select Currency</option> -->
                                                                    <?php foreach($allCurrencies as $currency){ ?>
                                                                    <?php $selected = ($currency['shortforms'] ==  $_SESSION['CurrPreference'])? 'selected' : ''; ?>
                                                                    <option value="<?php echo $currency['shortforms']; ?>" <?php echo $selected; ?>><?php echo $currency['shortforms']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                    
                                    <div class="clearfix ">
                                        <button type="submit" class="btn  pull-right  innerMR btn-trading-wid-auto "> &nbsp; Save Changes &nbsp; </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================        
 BUG:EG-296
    Issue List:
    1)Need to removed cancel button from Currency Preferences box.                  @done(17-03-02 19:00) 
    2)Text font color are different as compare to design sheet.                     @done(17-03-02 19:00) 
    3)By default USD  Currency should be display inside Currency dropdown text box. @done(17-02-28 16:12) 

=========================================================================================================================================-->
