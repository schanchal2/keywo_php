<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	echo "Authorisation failure";
  	exit;
}

require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");
require_once("../../models/wallet/walletCashoutModel.php");

//connect to keywords database
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
}
$userEmail = $_SESSION['email'];
$cashout_id = $_GET['cashout_id'];
$request_date = $_GET['request_date'];

$dateTime = new DateTime(); 
$timezone = $dateTime->format('T');
$seconds = date_offset_get(new DateTime);
$minutes = $seconds/60;
$offset = ($minutes >= 0)?"+".$minutes:$minutes;

$cashoutRequest = getCashoutRequestById($conn,$cashout_id,$request_date,$userEmail);
if(noError($cashoutRequest)){
	$cashoutRequest = $cashoutRequest['errMsg'][0];
}
if(trim($cashoutRequest['status']) =='approved_tbc'){
    $requestStatus = 'Verified';
    //}elseif(trim($requests['status']) =='pending_manual_verification'){
}elseif(strlen(strstr(strtolower(trim($cashoutRequest['status'])), 'pending_'))>0){
    $requestStatus = 'Pending for Manual Verification';
}
elseif(strlen(strstr(strtolower(trim($cashoutRequest['status'])), 'hold'))>0 ){
    $requestStatus = 'Hold for Manual Verification';
}
elseif(strlen(strstr(strtolower(trim($cashoutRequest['status'])), 'reject'))>0 ){
    $requestStatus = 'Rejected';
}
else{
    $requestStatus = $cashoutRequest['status'];
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-Text-Primary-Blue">Cashout Request</h4> </div>

<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Quantity :</div>
    <div class="col-sm-9 text-Blue margin-none h4"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $cashoutRequest['payment_amount']+$cashoutRequest['cashout_fees']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)($cashoutRequest['payment_amount']+$cashoutRequest['cashout_fees']), 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
</div><div class="row innerMTB">    
<div class="col-sm-3 text-Blue">To :</div>
<div class="col-sm-9 text-Gray">itduser@keywo.com</div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Description :</div>
    <div class="col-sm-9 text-Gray">Cashout request of <?php echo $cashoutRequest['payment_amount']+$cashoutRequest['cashout_fees']; ?> <?php echo $keywoDefaultCurrencyName; ?> </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Status :</div>
    <div class="col-sm-9 text-orange"><?php echo ucfirst($requestStatus); ?> <i class="fa fa-circle"></i>   </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Date  :</div>
    <div class="col-sm-9 text-Gray"><?php echo date("jS M, Y H:i a",strtotime($cashoutRequest['request_date']))." GMT {$offset}({$timezone})"; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">IP :</div>
    <div class="col-sm-9 text-Gray"><?php echo $cashoutRequest['origin_ip']; ?></div>
</div>

