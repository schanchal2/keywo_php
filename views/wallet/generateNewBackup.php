<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/wallet2FAModel.php'); 
require '../../views/wallet/googleAuth/vendor/autoload.php';
$returnArr = array();
$authenticator = new PHPGangsta_GoogleAuthenticator();
if(!isset($_GET['new_codes'])){
	$newBackupCodes = array(); 
	for($i=1;$i<6;$i++){ 
		$secret = $authenticator->createSecret();
		$newBackupCodes[] = $secret; 
	}
	$returnArr['errCode'] = -1;
	$returnArr['errMsg'] = $newBackupCodes;
	echo json_encode($returnArr); die;
}
$securityPreference = 2;
$email = $_SESSION["email"];

$newBackupCodes = explode(",", $_GET['new_codes']); 
$response = setbackupCodes(json_encode($newBackupCodes),$securityPreference,$email);

?>
<?php
    foreach ($newBackupCodes as $key => $value) { 
    ?>
<tr>
    <td class="innerAll half padding-left-none"><?php echo $value; ?></td>
    <td class="innerAll half text-right padding-right-none text-color-App-Success">Available</td>
</tr>            	
<?php } ?>