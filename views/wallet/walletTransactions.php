<?php
session_start();

//check for session
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

//include necessary files
require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");
error_reporting(0);
$status = isset($_GET['status'])?$_GET['status']:'';
$dateRange = isset($_GET['date_range'])?$_GET['date_range']:'';
$search = isset($_GET['search'])?$_GET['search']:'';
$sortBy1 = isset($_GET['sort_by'])?explode("-", $_GET['sort_by']):'';
$sortBy = $sortBy1[0];
$deepFilter = $sortBy1[1];
$daterange_to = isset($_GET['daterange_to'])?$_GET['daterange_to']:'';
$daterange_from = isset($_GET['daterange_from'])?$_GET['daterange_from']:'';
$email = $_SESSION["email"];
$first_bkwd_cursor = isset($_GET['first_bkwd_cursor'])?$_GET['first_bkwd_cursor']:'';

//set from and to date
if(!empty($daterange_to)){
    $date = array();
    $date = explode("-",$daterange_to);
    $daterange_to = trim($date[1]);
    $daterange_from = trim($date[0]);

    $daterange_from = $daterange_from." 00:00:00";
    $daterange_to = $daterange_to." 23:59:59";

    $from = strtotime(trim($daterange_from)) * 1000;
    $to = strtotime(trim($daterange_to)) * 1000;
}

if(isset($_GET['fwd_cursor'])){
    $bkwd_cursor = '';
    $fwd_cursor = $_GET['fwd_cursor'];
    $offset_from = $_GET['offset_from'];
}elseif (isset($_GET['bkwd_cursor'])) {
    $fwd_cursor = '';
    $bkwd_cursor = $_GET['bkwd_cursor'];
    $offset_from = $_GET['offset_from'];
}else{
    $fwd_cursor = '';
    $bkwd_cursor = '';
    $offset_from = '';
}
$trans_type_array = array(
    "purchase_itd"=>"Purchase Credit",
    "cashout_fees"=>"Cashout Fees",
    "search_earning"=>"Search Earning",
    "keyword_purchase"=>"Keyword Purchase",
    "post_creator_earning"=>"Content Upload",
    "post_sharer_earning" =>"Content Share",
    "post_viewer_earning"=>"Content Consumption",
    "trade"=>"Trade",
    "renewal_fees"=>"Renewal Fees",
    "trade_fees"=>"Trade Fees",
    "affiliate_earnings"=>"Affiliate Earning",
    "social_referral_earnings"=>"Social Referral Earning",
    "search_referral_earnings"=>"Search Referral Earning",
    "cashout"=>"Cashout"
    );

$userRequiredFields = $userRequiredFields.",email,account_handle";

//fetch details of sender and receiver                      
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
$userDetails = $userDetails['errMsg'];  

$result = getUsersTransctionsHistory($userDetails['email'],$userDetails['_id'],$status,$sortBy,$from,$to,$search,$bkwd_cursor,$fwd_cursor,$offset_from,$deepFilter);

if(noError($result)){
    $bkwd_cursor_offset_from = $result['errMsg']['bkwd_cursor_offset_from'];
    if($result['errMsg']['fwd_cursor_offset_from'] == -1){
        $_SESSION['first_bkwd_cursor'] = $result['errMsg']['bkwd_cursor_offset_from'];
    }
    $first_bkwd_cursor = $_SESSION['first_bkwd_cursor'];
}else{
    print("Error in fetching transaction history");
    exit();
}
?>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive wallet__trasactionHistory  text-Light-Black">
                <table class="table table-hover border-all text-center">
                    <thead class="text-White">
                        <tr>
                            <th class="text-left">Type </th>
                            <th> Quantity </th>
                            <th>Discription </th>
                            <th>
                                <label class="dropdownOptions ">
                                    <select class="selectpicker12" id="tr-status" onchange="getsortedTransaction();" style="background-color: #90a1a9;border: none;">
                                        <option selected disabled>Status</option>
                                        <option value="" <?php if($status=="" ){ echo "selected"; }?>>All</option>
                                        <option value="completed" <?php if($status=="completed" ){ echo "selected"; }?>>Completed</option>
                                        <option value="confirmed" <?php if($status=="confirmed" ){ echo "selected"; }?>>Confirmed</option>
                                        <option value="accepted" <?php if($status=="accepted" ){ echo "selected"; }?>>Accepted</option>
                                        <option value="rejected" <?php if($status=="rejected" ){ echo "selected"; }?>>Rejected</option>
                                    </select>
                                </label>
                            </th>
                            <th>Date </th>
                            <th> &nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($result['errMsg']['batched_container'] as $key => $value) { ?>
                        <tr>
                            <?php 
                                if($value['sender']== $email){ 

                                    $type = ($value['type'] == "transfer")? "Sent": $value['type'] ;                                    
                                    $senderDesc = sprintf($descriptionArray['sender_desc'], number_format($value['amount'],4), $keywoDefaultCurrencyName, $value['receiver']);
                                    if( isset($trans_type_array[$value['type']])){
                                        $type = $trans_type_array[$value['type']];
                                    }elseif($value['type'] != "transfer"){
                                        $type = $value['type'];
                                    }    
                                    $description = $senderDesc; 
                                }elseif($value['sender'] != $email){
                                    $type = ($value['type'] == "transfer")? "Received": $value['type'] ;
                                    if( isset($trans_type_array[$value['type']])){
                                        $type = $trans_type_array[$value['type']];
                                    }elseif($value['type'] != "transfer"){
                                        $type = $value['type'];
                                    } 
                                    $receiverDesc = sprintf($descriptionArray['reciver_desc'], number_format($value['amount'],4), $keywoDefaultCurrencyName, $value['sender']);
                                    $description = $receiverDesc; 
                                }else{
                                    $type = $value['type'];
                                } 

                                ?>
                            <td class="text-left">
                                <?php echo $type; ?> </td>
                            <td class="text-Dark-Blue"> <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $value['amount']." ".$keywoDefaultCurrencyName." "; ?>"><?php echo number_format((float)$value['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?></span></td>
                            <td>
                                <?php echo urldecode($description); ?> </td>
                            <?php if($value['transaction_status']=='confirmed' || $value['transaction_status']=='completed' ){ ?>
                            <td class="text-App-Success">
                                <?php echo ucfirst($value['transaction_status']); ?>
                            </td>
                            <?php }else{ ?>
                            <td>
                                <?php echo ucfirst($value['transaction_status']); ?>
                            </td>
                            <?php } ?>
                            <td>
                                <?php echo date("jS M, Y",$value['time']/1000); ?>
                            </td>
                            <td><a onclick="showDetails('<?php echo $value['_id']; ?>','<?php echo $value['transaction_boundary']; ?>');">Details</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php if(empty($result['errMsg']['batched_container'])){ ?>
                <h4 style="text-align: center;">No relevant data found!!!</h4>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row keyword-marketplace-data innerT inner-2x">
        <div class="col-md-5">
            <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
        </div>
        <div class="col-md-7">
            <div class="pagination-cont pull-right">
                <ul class="pagination">
                    <?php if(!empty($result['errMsg']['fwd_cursor']) && $first_bkwd_cursor != $result['errMsg']['bkwd_cursor_offset_from']){ ?>
                    <li><a class="prev-arrow-pagination" onclick="showNewer('<?php echo $result['errMsg']['fwd_cursor']; ?>','<?php echo $result['errMsg']['fwd_cursor_offset_from']; ?>');"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <?php }else{ ?>
                    <li class="disabled"><a class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <?php } ?>
                    <?php if(!empty($result['errMsg']['bkwd_cursor'])){ ?>
                    <li><a class="prev-arrow-pagination" onclick="showOlder('<?php echo $result['errMsg']['bkwd_cursor']; ?>','<?php echo $result['errMsg']['bkwd_cursor_offset_from']; ?>');"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <?php }else{ ?>
                    <li class="disabled"><a class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
