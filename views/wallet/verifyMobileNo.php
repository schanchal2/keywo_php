<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$docrootpath = "../../"; 
include("../layout/header.php");
require_once('../../config/country_codes.php');
require_once("{$docrootpath}models/wallet/walletModel.php");
$countries = json_decode($countyCodes,true);
$countriesCodes = $countries['countries'];

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}
if($kyc_level !=1 ){
	print("<script>");
    print("var t = setTimeout(\"window.location='../wallet/accountLevelSettings.php';\", 000);");
    print("</script>");
    die;
}

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
  <div class="container">
    <div class="mobile-container card" style="height:200px;">
      <div class="text-blue border-bottom">
        <h4 class="inner-2x innerMT innerL">Mobile verification</h4>
      </div>
      <div class="mobile-detail text-center">
          <div class="innerMT inner-2x">
            Enter your 10 digit mobile no.
          </div>
          <div class="innerMTB">
          	<!-- <select class="country-code">
          	<?php //foreach ($countriesCodes as $key => $value) { ?>
          		<option value="<?php //echo $value['code']; ?>"><?php //echo $value['code']; ?></option>
          	<?php //} ?>	
			</select> -->
            <input type="text"  id="country-code" name="country_code" value="+91" class="country-code"/>
            <input type="number" id="mobile-no" name="mobile_no" placeholder="" value="">

          </div>
          <small id="err-msg-mobile" class="text-Red cash-err"></small>
          <h5 id="mob-err" style="color: red;"></h5>
          <div class="">
            <input type="button" class="btn-social-wid-auto btn-xs" id='verify-btn' onclick="verifyOTP();" value="Submit">
          </div>
      </div>
    </div>
  </div>
</main>

<!-- Modal -->
    <div id="OTPVerification" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close text-color-Gray" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-blue">Mobile Verification</h4>
          </div>
          <div class="modal-body text-center">
            <div class="">
              <h5 class="text-dark-green">Please enter OTP that you received</h5>
            </div>
            <div class="">
              <input type="number" id="otp" name="otp" value="">
              <input type="hidden" id="mobile-no-otp" name="mobile_no">
              <input type="hidden" id="country-code-otp" name="country_code">
            </div>
            <h5 id="modal-err" style="display: none;"></h5>
            <div class="innerMTB inner-2x">
              <input type="button" class="btn-social-wid-auto btn-xs innerMR" id="resend-btn" onclick="resendOTP();" value="Resend OTP">
              <input type="button" class="btn-social-dark btn-xs" value="Continue" id="check-otp-btn" onclick="checkOTP();">
            </div>
          </div>
        </div>

      </div>
    </div>

<style type="text/css">
	.defaultInput
    {
     width: 100px;
     height:25px;
     padding: 5px;
    }
	.error
	{
	 border:1px solid red;
	}
</style>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript">
	function verifyOTP(){
		$("#err-msg-mobile").text('');
		$('#verify-btn').val('Submitting...');
		$("#country-code").removeClass('error');
		$("#mobile-no").removeClass('error');
		var mobile_no = $("#mobile-no").val();
		var country_code = $("#country-code").val();
		if((mobile_no.length == 0 && country_code.length == 0) || parseInt(mobile_no)==0 || parseInt(country_code)==0){
			$("#mobile-no").addClass('error');
			$("#country-code").addClass('error');
			$('#verify-btn').val('Submit');
			return false;
		}else if(mobile_no.length == 0 || mobile_no.length < 6){
			$("#mobile-no").addClass('error');
			$('#verify-btn').val('Submit');
			return false;
		}else if(country_code.length == 0){
			$("#country-code").addClass('error');
			$('#verify-btn').val('Submit');
			return false;
		}else{
			$.ajax({
	            type: 'POST',
	            dataType: 'json',
	            url: rootUrl+"controllers/wallet/accountLevelController.php",
	            data: {
	                mobile_no: encodeURIComponent(mobile_no),
	                country_code: encodeURIComponent(country_code),
	                action:encodeURIComponent('send_otp')
	            },

	            success: function(data){
	            	 console.log(data);
	                if(data.errCode == -1){
	                		$('#country-code-otp').val(country_code);
	                		$('#mobile-no-otp').val(mobile_no);
	                		$('#OTPVerification').modal('show');

	                }else{
	                    $("#err-msg-mobile").text(data.errMsg);
	                }
	                $('#verify-btn').val('Submit');
	            },
	            error: function (xhr, status, error) {
	                location.reload();
	            }

	        });
		}
		
	}


	function resendOTP(){
		$('#modal-err').hide();
		$('#resend-btn').val('Sending...');
		$("#country-code").removeClass('error');
		$("#mobile-no").removeClass('error');
		var mobile_no = $("#mobile-no-otp").val();
		var country_code = $("#country-code-otp").val();
		if(mobile_no.length == 0 && country_code.length == 0){
			$("#mobile-no").addClass('error');
			$("#country-code").addClass('error');
			return false;
		}else if(mobile_no.length == 0){
			$("#mobile-no").addClass('error');
			return false;
		}else if(country_code.length == 0){
			$("#country-code").addClass('error');
			return false;
		}else{
			$.ajax({
	            type: 'POST',
	            dataType: 'json',
	            url: rootUrl+"controllers/wallet/accountLevelController.php",
	            data: {
	                mobile_no: encodeURIComponent(mobile_no),
	                country_code: encodeURIComponent(country_code),
	                action:encodeURIComponent('send_otp')
	            },

	            success: function(data){
	            	 console.log(data);
	                if(data.errCode == -1){
	                	$('#modal-err').show();
	                	$('#modal-err').css('color','green');
	                	$('#modal-err').text('OTP has been sent to your mobile number');
	                }else{
	                	$('#modal-err').show();
	                	$('#modal-err').css('color','red');
	                	$('#modal-err').text('Failed to send OTP');
	                }
	                $('#resend-btn').val('Resend OTP');
	            },
	            error: function (xhr, status, error) {
	                location.reload();
	            }

	        });
		}
		
	}


	function checkOTP(){
		$('#modal-err').hide();
		$('#check-otp-btn').val('Processing...');
		$("#otp").removeClass('error');
		
		var mobile_no = $("#mobile-no-otp").val();
		var country_code = $("#country-code-otp").val();
		var otp = $("#otp").val(); 
		if(otp.length == 0){
			$("#otp").addClass('error');
			$('#check-otp-btn').val('Continue');
			return false;
		}else if(mobile_no.length == 0){
			$("#mobile-no").addClass('error');
			$('#check-otp-btn').val('Continue');
			return false;
		}else if(country_code.length == 0){
			$("#country-code").addClass('error');
			$('#check-otp-btn').val('Continue');
			return false;
		}else{
			$.ajax({
	            type: 'POST',
	            dataType: 'json',
	            url: rootUrl+"controllers/wallet/accountLevelController.php",
	            data: {
	                mobile_no: encodeURIComponent(mobile_no),
	                country_code: encodeURIComponent(country_code),
	                otp: encodeURIComponent(otp),
	                action:encodeURIComponent('verify_otp')
	            },

	            success: function(data){
	            	 console.log(data);
	                if(data.errCode == -1){
	                	$('#modal-err').show();
	                	$('#modal-err').css('color','green');
	                	$('#modal-err').text('Your mobile number has bess successfully verified');

						setTimeout(function(){ window.location.href='accountLevelSettings.php'; }, 3000);
	                		// $('#country-code-otp').val(country_code);
	                		// $('#mobile-no-otp').val(mobile_no);
	                		// $('#OTPVerification').modal('show');
	                }else{
	                	$('#modal-err').show();
	                	$('#modal-err').css('color','red');
	                	if(data.errCode == 10){
	                		$('#OTPVerification').modal('toggle');
	                		$("#err-msg-mobile").text(data.errMsg);
	                	}else{
	                		$('#modal-err').text('Invalid OTP,Please enter correct OTP');
	                	}	          
	                }
	                $('#check-otp-btn').val('Continue');
	            },
	            error: function (xhr, status, error) {
	                location.reload();
	            }

	        });
		}
		
	}

</script>