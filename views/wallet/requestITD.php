<?php
session_start();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
} 

include("../layout/header.php");

$currpreff = strtolower($_SESSION['CurrPreference']);
$conversionRate = $getExchangeRate['usd'] / $getExchangeRate[$currpreff] ;

echo "<script> var conversionRate = " . $conversionRate . "; </script>";

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<!-- Latest compiled and minified CSS -->
<!-- Latest compiled and minified JavaScript -->

<main class="social-main-container inner-7x innerTB WT5">
    <div class="container  padding-none row-10">
        <div class="row">
            <?php
            $page = "request"; 
            include("rightMenu.php"); ?>  
        <!-- col-xs-3 -->

            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none"> Request</h4>
                        <div class="card bg-White border-all innerAll inner-2x">
                            <div class="row">
                                <div class="req-err" id="req-err-id" style="display: none; color: red; text-align: center;"></div>
                                <div class="req-err" style="color: red; text-align: center;"><?php echo $_SESSION['err_msg']; ?></div>
                                <div class="req-err" style="color: green; text-align: center;"><?php echo $_SESSION['succ_msg']; ?></div>
                                <?php unset($_SESSION['err_msg']); unset($_SESSION['succ_msg']); ?><br>

                                <div class="col-xs-9 col-xs-offset-1">
                                    <form class="form-horizontal" id="request-form" action="../../controllers/wallet/requestITDController.php" method="POST">
                                        <div class="form-group filter">
                                            <label for="inputPassword3" class="col-sm-2 control-label  text-right padding-right-none text-grayscale-5 f-sz16">Form :</label>
                                            <div class="col-sm-10">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="Enter email or handle" type="text" name="email_handle" class="form-control text-Gray" required>
                                                    <span class="input-group-addon  bg-white text-grayscale-be "> <i class="fa fa-group" ></i> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group filter">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right padding-right-none text-grayscale-5 f-sz16">Amount :</label>
                                            <div class="col-sm-5">
                                                <div class="input-group placeholder-grayscale-4 text-grayscale-4">
                                                    <input placeholder="0.0000" type="number" id="amount_itd" onkeyup="validate();" step="0.0001" class="form-control text-Gray" name="amount_itd" required>
                                                    <span class="input-group-addon  bg-white f-sz15"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group placeholder-grayscale-4 text-grayscale-4">
                                                    <input placeholder="0.0000" type="number" step="<?php echo $step =($_SESSION['CurrPreference']== 'BTC')?'0.00000001':'0.00001' ?>" onkeyup="validate1();" class="form-control text-Gray" id="amount_usd" name="amount_usd" >
                                                    <span class="input-group-addon  bg-white f-sz15" id="crr-preff"><?php echo $_SESSION['CurrPreference']; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group filter">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right padding-right-none text-grayscale-5 f-sz16">Note :</label>
                                            <div class="col-sm-10">
                                                <div class="input-group text-Blue btn-block">
                                                    <textarea placeholder="Write an optional message" name="note" id="input" class="form-control  placeholder-grayscale-be text-grayscale-be f-sz14 noresize" maxlength="100" rows="3" required="required"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" id="request-itd-btn" class="btn-trading-wid-auto btn-xs btn text-White pull-right wallet-Request">Request</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 filter">
                        <form class="form-inline innerMT inner-2x">
                            <!-- 

                        -->
                            <div class="form-group innerR ">
                                <label class="f-sz15 text-grayscale-6 ">Sort By : </label>
                                <div class="input-group ">
                                    <div class="Sort-by ">
                                        <label class="dropdownOptions pull-right ">
                                            <select class="selectpicker" id="tr-type" name="All" onchange="getsortedTransaction();">
                                            <option value="">All</option>
                                            <option value="paymentrequest-by_sender">Sent</option>
                                            <option value="paymentrequest-by_receiver">Received</option>
                                        </select>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- 

                         -->
                            <div class="form-group ">
                               <label class="text-Gray f-sz15">Date : </label>
                                <div class="input-group text-Blue dateRange">
                                    <input readonly type="text" class="form-control text-Blue" value="10/9/2017-12/9/2017" placeholder="" id="daterange-to" name="daterange" style="background-color: #fff;">
                                    <span class="input-group-addon  bg-white text-Blue" > <i class="fa fa-calendar" id=""></i> </span>
                                </div>
                            </div>
                            <div class="form-group " style="visibility: hidden;">
                                <div class="input-group ">
                                    <button type="button " class="btn bg-Lightest-Blue text-White innerLR "> <i class="fa fa-download "></i> </button>
                                </div>
                            </div>
                            <div class="form-group pull-right ">
                                <div class="input-group ">
                                    <input type="text " class="form-control f-sz14 placeholder-grayscale-be text-grayscale-be " id="search" placeholder="Search">
                                    <span class="input-group-btn ">
                                <button class="btn btn-default " type="button">  <i class="fa fa-search text-grayscale-6 "></i></button>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        
            
             <div id="transction">
            <?php include("allRequests.php"); ?>
            </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
    <!--===========================================
  =            Accept request pop-up            =
  ============================================-->
    <div class="modal fade" id="AcceptRequest" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="bg-White border-all clearfix ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-Text-Primary-Blue">Accept Request</h4> </div>
                <div class="modal-body innerAll clearfix">
                    <p class=" text-Gray">Are you shure you want to send <span id="confirm-amount" class="text-Blue">100 <?php echo $keywoDefaultCurrencyName; ?> </span> to <span id="confirm-reciver">Lorem ipsum</span>?</p>
                    <form action="" method="POST">
                    <div class="form-group margin-bottom-none">
                        <label for="textarea" class="col-sm-3 text-Gray"><small>Accept Note :</small></label>
                        <div class="col-sm-9">
                            <textarea name="" id="accept-note" class="form-control" rows="3" maxlength="100" required="required"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="request-details" name="requestDetails">
                    </form>
                </div>
                <div id="accept-err-msg"></div>
                <div class=" innerAll clearfix ">
                    <button type="button" class="btn btn-primary pull-right btn-trading-dark innerMR" data-dismiss="modal">No</button>
                    <button type="button" id="accept-btn" class="btn btn-primary pull-right btn-trading innerMR" onclick="acceptITDRequest();">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <!--====  End of Accept request pop-up  ====-->
    <!--===================================
    =            RejectRequest            =
    ====================================-->
    <div class="modal fade" id="RejectRequest" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="bg-White border-all clearfix ">
                <form action="" method="POST" class="form-horizontal" role="form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-Text-Primary-Blue">Reject Request</h4> </div>
                    <div class="modal-body">

                        <div class="form-group margin-bottom-none">
                            <label for="textarea" class="col-sm-3 text-Gray"><small>Reason for Rejection :</small></label>
                            <div class="col-sm-9">
                                <textarea name="" id="reject-note" class="form-control" rows="3" maxlength="100" required="required"></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="reject-request-details" name="requestDetails">
                    </div>
                   <div id="reject-err-msg"></div>
                    <div class=" innerAll clearfix ">
                        <button type="button" id="reject-btn" class="btn btn-primary pull-right btn-trading-dark innerMR" onclick="rejectITDRequest();">OK</button>
                        <button type="button" class="btn btn-primary pull-right btn-trading innerMR" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--====  End of RejectRequest  ====-->
    <!--============================
    =            detail            =
    =============================-->

     <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="bg-White border-all clearfix ">
            <form action="" method="POST" class="form-horizontal" role="form">
                
                <div class="modal-body" id="transction-details">
                    
                    
                </div>
            </form>
        </div>
    </div>
    </div>


    <
    <!--====  End of detail  ====-->
    </main>
<?php include("../layout/transparent_footer.php");?>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <script type="text/javascript">

     $('input[name="daterange"]').daterangepicker({ startDate: '<?php echo date("m/d/Y",strtotime("-3 Months")); ?>', endDate: '<?php echo date("m/d/Y"); ?>' });
    $('#daterange-to').on('apply.daterangepicker', function(ev, picker) {
      getsortedTransaction()
    });


    $( "#request-form" ).submit(function( event ) {
       var min_request_amt = <?php echo $minRequestAmount; ?> 
      $("#request-itd-btn").text('Requesting...');
      $(".req-err").hide();   
      var itd_amount = $('#amount_itd').val();
      if(itd_amount < min_request_amt){
        $("#req-err-id").show();
        $("#request-itd-btn").text('Request');
        $("#req-err-id").text("Please enter amount greater than "+min_request_amt+" "+keywoDefaultCurrency);
        return false;
      }

    });
     
    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
 

    function validate(){
        var itd_amount = $('#amount_itd').val();
        var covertedAmount = itd_amount/conversionRate;
        var currPrefAmt = covertedAmount.toFixed(5);
        var currPreff = '<?php echo $_SESSION['CurrPreference']; ?>';
        if(currPreff == 'BTC'){
            var currPrefAmt = covertedAmount.toFixed(8);
        }
        $("#amount_usd").val(currPrefAmt);
        $("#crr-preff").text(currPreff);
    }  

    function validate1(){
        var usd_amount = $('#amount_usd').val();
        var covertedAmount = usd_amount*conversionRate;
        var currPrefAmt = covertedAmount.toFixed(2);
        $("#amount_itd").val(currPrefAmt);
        
    } 

    $(document).ready(function() {

        $("#date-range").change(function(){
            getsortedTransaction();
        });


         $("#search").keyup(function(){
            var search = $("#search").val();
            var len = search.length;
            if(len > 2){
                getsortedTransaction();
            }else if(len == 0){
                getsortedTransaction();
            }
        });
  });

var first_bkwd_cursor = '<?php echo $bkwd_cursor_offset_from; ?>';

function showOlder(offset_from){
        var pageCount = $("#page-count").val();
        $("#page-count").val(parseInt(pageCount)+1);
        var pageCount1 = parseInt(pageCount)+1 ;
        var tr_type = $("#tr-type").val();
        var date_range = $("#date-range").val();
        var daterange_to = $("#daterange-to").val();
        var daterange_from = $("#daterange-from").val();
        var search = $("#search").val();
        var status = $("#tr-status").val();
        $('#transction').load('allRequests.php?status='+encodeURI(status)+'&offset_from='+offset_from+'&sort_by='+tr_type+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search)+'&pageCount='+pageCount1+'&first_bkwd_cursor='+first_bkwd_cursor);
  }

    function showNewer(fwd_offset_from){
        var pageCount = $("#page-count").val();
        $("#page-count").val(parseInt(pageCount)-1);
        var pageCount1 = parseInt(pageCount)-1 ;
        var tr_type = $("#tr-type").val();
        var date_range = $("#date-range").val(); 
        var search = $("#search").val();
        var daterange_to = $("#daterange-to").val();
        var daterange_from = $("#daterange-from").val();
        var status = $("#tr-status").val();
        $('#transction').load('allRequests.php?fwd_offset_from='+fwd_offset_from+'&status='+encodeURI(status)+'&sort_by='+tr_type+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search)+'&pageCount='+pageCount1+'&first_bkwd_cursor='+first_bkwd_cursor);
  }
    function getsortedTransaction(){
        var tr_type = $("#tr-type").val();
        var date_range = $("#date-range").val();
        var daterange_to = $("#daterange-to").val();
        var daterange_from = $("#daterange-from").val();
        var search = $("#search").val();    
        var status = $("#tr-status").val();
        if (typeof status == 'undefined'){
            status = '';
        }
        $('#transction').load('allRequests.php?sort_by='+tr_type+'&status='+encodeURI(status)+'&daterange_to='+encodeURI(daterange_to)+'&daterange_from='+encodeURI(daterange_from)+"&search="+encodeURI(search));
  }
   function showDetails(transction_id){
        $("#err-msg").text('');
        $('#transction-details').load('requestDetails.php?transction_id='+transction_id);
        $('#myModal').modal('show');

  }

  function showAcceptPopup(requestDetails,$amount,reciver){
        $("#confirm-amount").text($amount+" "+keywoDefaultCurrency+" "+"("+$amount+" USD)");
        $("#confirm-reciver").text(reciver);
        $("#request-details").val(requestDetails);
        $('#AcceptRequest').modal('show');
  }
    function showRejectPopup(requestDetails,$amount){
        $("#reject-request-details").val(requestDetails);
        $('#RejectRequest').modal('show');
  }

  function acceptITDRequest() {
    $("#accept-err-msg").text('');
    $("#accept-btn").prop('disabled', true);
    var accept_Details = $("#request-details").val();
    var accept_note = $("#accept-note").val();
    if(accept_note.length == 0){
            $("#accept-err-msg").css("color","red");
            $("#accept-err-msg").css("text-align","center");
            $("#accept-err-msg").text("Please enter accept note");
            $("#accept-btn").prop('disabled', false);
            return false;
    }else{
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/acceptRejectRequestController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                trans_details: accept_Details,action: "accept",note:accept_note
            },
            async: true,
            success:function(data){
                console.log(data);
                if(data.errCode == -1){
                    window.location.href = "../two_factor_auth/index.php";
                    /*$("#accept-err-msg").css("color","green");
                     $("#accept-err-msg").css("text-align","center");
                    $("#accept-err-msg").text("Request has been successfully accepted");
                   setTimeout(function(){
                      $('#AcceptRequest').modal('toggle');
                      location.reload();
                    }, 3000);*/
                     
                }else{
                    $("#accept-err-msg").css("color","red");
                    $("#accept-err-msg").css("text-align","center");
                    $("#accept-err-msg").html(data.errMsg);
                    setTimeout(function(){
                      $('#AcceptRequest').modal('toggle');
                      location.reload();
                    }, 3000);
                }
            },
            error: function (xhr, status, error) {
                location.reload();
            }
        });
    }
    
  }

    function rejectITDRequest() {
        $("#reject-err-msg").text('');
        $("#reject-btn").prop('disabled', true);
        var reject_details = $("#reject-request-details").val();
        var reject_note = $("#reject-note").val();
        if(reject_note.length == 0){
            $("#reject-err-msg").css("color","red");
            $("#reject-err-msg").css("text-align","center");
            $("#reject-err-msg").text("Please enter reject note");
            $("#reject-btn").prop('disabled', false);
            return false;
        }else{
            $.ajax({ 
                type: "POST",
                dataType:"json",
                url: "../../controllers/wallet/rejectRequestController.php",
                beforeSend: function() {
                } ,
                complete: function(){
                },
                data: {
                    trans_details: reject_details,action: "reject",note: encodeURI(reject_note)
                },
                async: true,
                success:function(data){
                    if(data.errCode == -1){
                        $("#reject-err-msg").css("color","green");
                        $("#reject-err-msg").css("text-align","center");
                        $("#reject-err-msg").text("Request has been successfully rejected");
                        setTimeout(function(){
                          $('#RejectRequest').modal('toggle');
                          location.reload();
                        }, 3000);
                         
                    }else{
                        $("#reject-err-msg").css("color","red");
                        $("#reject-err-msg").css("text-align","center");
                        $("#reject-err-msg").text(data.errMsg);
                        setTimeout(function(){
                          $('#RejectRequest').modal('toggle');
                          location.reload();
                        }, 3000);
                    }
                },
                error: function (xhr, status, error) {
                    location.reload();
                }
            });
        }
        
        }

</script>
<!--========================================================================================================================================        
            ( 16-02-17)  
            table base created 
           
            /**
            
                TODO:
                    ( 17-02-17) 
                - appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
                - color name comflict in '.bg-blue-key-dark, .bg-blue-key'
                - looks better if .my-info-card bottom padding is removed
                    ( 18-02-17) 
                - input highlighting can be improoved. 
             */
            