<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

/* Check user ftue status, if false or 0 the redirect to ftue profile */
if($userFteStatusFlag == false){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$email =  $_SESSION["email"];
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
    //if user try to verify wallet address
    $veifyEmail = (isset($_GET['email']))?cleanXSS(urldecode($_GET['email'])):'';
    $payload = (isset($_GET['payload']))?cleanXSS(urldecode($_GET['payload'])):'';
    $authToken = (isset($_GET['auth']))?cleanXSS(urldecode($_GET['auth'])):'';
    $addressMode = (isset($_GET['mode']))?cleanXSS(urldecode($_GET['mode'])):'';

    $walletDetails = getWalletAddress($email);
    if(noError($walletDetails)){
        $walletDetails = $walletDetails['errMsg'];
    }

       //printArr($walletDetails); die;
    print("<script>");
    print("var invalidSession = false;");
    print("</script>");

    if(isset($_GET['payload']) && isset($_GET['email']) && isset($_GET['auth']) && isset($_GET['mode']) && $veifyEmail == $email){
        $type = 'wallet_address_verif_email';
        $mode = 'email';
        $verify = verifyAuthToken($conn,$email,$authToken,$payload,$type,$mode);
        if(noError($verify)){
            if(!isset($walletDetails['cashout_id'])){
                $result = setWalletAddress($email,$payload,$addressMode);
            }else{
                $result = updatedWalletAddress($email,$payload,$addressMode,$walletDetails['cashout_id']);                
            }

            $walletDetails = getWalletAddress($email);
            if(noError($walletDetails)){
                $walletDetails = $walletDetails['errMsg'];
            }
            
            if(noError($result)){
                $returnArr['errMsg'] = 'Succesfully verified';
                $returnArr['errCode'] = -1;  
                $_SESSION['errCode'] = -1;
                $_SESSION['addressMode'] = $addressMode;
                print("<script>");
                print("var t = setTimeout(\"window.location='cashoutSettings.php';\", 000);");
                print("</script>");
                die;
            }else{
                $returnArr['errMsg'] = 'Failed to set wallet address.';
                $returnArr['errCode'] = 2;
            }
        }else{
            $returnArr['errMsg'] = $verify['errMsg'];
            $returnArr['errCode'] = 2;   
        }
    }else if(isset($_GET['payload']) && isset($_GET['email']) && isset($_GET['auth']) && isset($_GET['mode']) && $veifyEmail != $email){
        $invalidSession = true;
        print("<script>");
        print("var invalidSession = true;");
        print("</script>");
    }

    // printArr($returnArr);die;

}
?>

<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container  padding-none row-10">
        <div class="row">
            <?php
            $page = "cashout_setting"; 
            include("rightMenu.php"); ?> 
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20">Cashout setting</h4>
                        <div>
                            <div class="inner-2x  text-Gray">
                                <div class="row">
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                            <?php if(!empty($walletDetails['current_wallet_address'])){ ?>
                                                <h5 id="verify-bitcoin-head" class="modal-title innerAll innerMT l-h10    text-Text-Primary-Blue f-sz18">Current Bitcoin Cashout Address</h5>
                                            <?php }else{ ?>
                                                <h5 id="verify-bitcoin-head" class="modal-title innerAll innerMT l-h10    text-Text-Primary-Blue f-sz18">Please set your Bitcoin Cashout Address</h5>
                                             <?php } ?> 
                                             </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4 ">
                                                <p class="innerMB inner-2x">Enter <b>Bitcoin Wallet</b> Address</p>
                                                <div class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="Enter here bitcoin wallet address" type="text" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" id="bitcoin-add" name="bitcoin-add" value="<?php echo $walletDetails['current_wallet_address']; ?>">
                                                                <?php if($_SESSION['addressMode'] =='bitcoin' &&  $_SESSION['errCode']==-1){ ?>
                                                                <small class="text-App-Success cash-err">Succesfully verified</small>
                                                                <?php } ?>
                                                                <?php if($addressMode=='bitcoin' &&  $returnArr['errCode'] != -1){ ?>
                                                                <small id="err-msg-bitcoin" class="text-Red cash-err"><?php echo $returnArr['errMsg']; ?></small>
                                                                <?php }else{ ?>
                                                                <small id="err-msg-bitcoin" class="text-Red cash-err"></small>    
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <?php if(!empty($walletDetails['current_wallet_address'])){ ?>
                                                <button type="button" id="verify-bitcoin-btn" onclick="validateBitcouin();"  class="btn btn-primary pull-right btn-trading">Change</button>
                                                <?php }else{ ?>
                                                 <button type="button" id="verify-bitcoin-btn" onclick="validateBitcouin();"  class="btn btn-primary pull-right btn-trading">Verify Now</button>
                                                 <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                            <?php if(!empty($walletDetails['current_paypal_address'])){ ?>
                                                <h5 id="verify-email-head" class="modal-title innerAll innerMT l-h10  text-Text-Primary-Blue f-sz18">Current Paypal Cashout Address</h5> 
                                            <?php }else{ ?>
                                                <h5 id="verify-email-head" class="modal-title innerAll innerMT l-h10  text-Text-Primary-Blue f-sz18">Please set your Paypal Cashout Address</h5> 
                                             <?php } ?>
                                            </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Paypal Email</b> Address</p>
                                                <div class="form-horizontal half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="Enter here Paypal email address" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" id="paypal-add" name="paypal-add" value="<?php echo $walletDetails['current_paypal_address']; ?>">
                                                                <?php if($_SESSION['addressMode'] =='paypal' &&  $_SESSION['errCode']==-1){ ?>
                                                                <small class="text-App-Success cash-err">Succesfully verified</small>
                                                                <?php } ?>
                                                                <?php if($addressMode=='paypal' &&  $returnArr['errCode'] != -1){ ?>
                                                                <small id="err-msg-paypal" class="text-Red cash-err"><?php echo $returnArr['errMsg']; ?></small>
                                                                <?php }else{ ?>
                                                                <small id="err-msg-paypal" class="text-Red cash-err"></small>    
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <?php if(!empty($walletDetails['current_paypal_address'])){ ?>
                                                <button type="button" id="verify-email-btn" onclick="validatePaypalEmail();" class="btn btn-primary pull-right btn-trading">Change</button>
                                            <?php }else{ ?>
                                                <button type="button" id="verify-email-btn" onclick="validatePaypalEmail();" class="btn btn-primary pull-right btn-trading">Verify Now</button>
                                             <?php } ?>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>

    <!--      Confirmation dialog while click on payment now     -->
    <div id="popup-confirmation" class="modal fade in keyword-popup" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content row">
                <div class="modal-header custom-modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Invalid Session</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="">
                            <div class="row">
                                <div class="col-md-12 innerMB text-center">
                                    <span class="text-black" id="response">You are currently logged in with some other account, To verify cashout address please log in with same user</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 innerMB">
                                    <div class="text-left">
                                    </div>
                                </div>
                                <div class="col-md-6 innerMB">
                                    <div class="text-left pull-right">
                                        <input value="Login" type="button" onclick="window.location.href='<?php echo $rootUrl; ?>views/user/logout.php';" id="payment-confirm-btn" class="yes-btn btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                        <input value="No" id="payment-cancel-btn" type="button" class="btn-trading-wid-auto innerMTB" onclick="window.location.href='<?php echo $rootUrl; ?>views/social/index.php';">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php unset($_SESSION['errCode']); unset($_SESSION['addressMode']); ?>
    <!-- container -->                               
    <script type="text/javascript">

    $(document).ready(function() {
        if(invalidSession){
            $("#popup-confirmation").modal("show");
        }
    });    
    

    var current_paypal_address = '<?php echo $walletDetails['current_paypal_address']; ?>';
    var current_wallet_address = '<?php echo $walletDetails['current_wallet_address']; ?>'
    function validateBitcouin(){
         
        $(".cash-err").text('');
		var bitcoin_add = $("#bitcoin-add").val();
        var button_text = $("#verify-bitcoin-btn").text();
        $("#verify-bitcoin-btn").text('Sending...');
		if(bitcoin_add !== '' && current_wallet_address !== bitcoin_add){
		
			$.ajax({ 
	            type: "GET",
	            dataType:"json",
	            url: "../../controllers/wallet/validateController.php",
	            beforeSend: function() {
	            } ,
	            complete: function(){
	            },
	            data: {
	                bitcoin_add: bitcoin_add
	            },
	            async: true,
	            success:function(data){
	            	if(data.errCode == -1){
	            		$("#err-msg-bitcoin").text('');	
	            		sendVerificationLink(bitcoin_add,"bitcoin_address");
	            	}else{
                        if(data.errCode == 100){
                            location.reload();
                        }else{
                            $("#verify-bitcoin-btn").text(button_text);
                            $("#err-msg-bitcoin").text("Enter valid bitcoin address");
                        }
	
	            	}
	            	
	            },
	            error: function (xhr, status, error) {
	                alert(error);
	            }
	        });
		}else{
            $("#verify-bitcoin-btn").text(button_text);
            if(bitcoin_add == ''){
                $("#err-msg-bitcoin").text("Enter valid bitcoin address");
            }else{
                $("#err-msg-bitcoin").text("Enter different bitcoin address"); 
            }
			
		}
	}

	function validatePaypalEmail(){
        $(".cash-err").text('');
		var paypal_add = $("#paypal-add").val();
        var button_text = $("#verify-email-btn").text();
        $("#verify-email-btn").text('Sending...');
		if(paypal_add !== '' && current_paypal_address !== paypal_add){		
			if( !validateEmail(paypal_add)) { 
				$("#err-msg-paypal").text("Enter valid email address"); 
                $("#verify-email-btn").text(button_text);
			}else{
				$("#err-msg-paypal").text(''); 
				sendVerificationLink(paypal_add,"paypal_email");
			}
		}else{
            $("#verify-email-btn").text(button_text);
            if(paypal_add == ''){
                $("#err-msg-paypal").text("Enter valid email address"); 
            }else{
                $("#err-msg-paypal").text("Enter different email address"); 
            }
		}
	}

	function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  return emailReg.test( $email );
	}

	function sendVerificationLink(address,mode){
		$.ajax({ 
	            type: "POST",
	            dataType:"json",
	            url: "../../controllers/wallet/sendVerificationLinkController.php",
	            beforeSend: function() {
	            } ,
	            complete: function(){
	            },
	            data: {
	                address: address,mode: mode
	            },
	            async: true,
	            success:function(data){
	            	console.log(data);
	            	if(data.errCode == -1){
                        	
                        if(mode == "paypal_email"){
                            $("#verify-email-btn").text('Resend');
                            $("#verify-email-head").text('Awaiting Verification');
                            $("#err-msg-paypal").text("A verification link has been send to your registerd email");  
                        }else{
                            $("#verify-bitcoin-btn").text('Resend');
                            $("#verify-bitcoin-head").text('Awaiting Verification');
                            $("#err-msg-bitcoin").text("A verification link has been send to your registerd email");                             
                        }

	            	}else{
                        if(data.errCode == 100){
                            location.reload();
                        }else{
                            if(mode == "paypal_email"){
                                $("#verify-email-btn").text('Verify Now');
                                if(data.errCode == 7){
                                    $("#err-msg-paypal").text(data.errMsg);
                                }else{
                                   $("#err-msg-paypal").text("Failed to send verification email!!!"); 
                                }
                                    
                            }else{
                                $("#verify-bitcoin-btn").text('Verify Now');
                                if(data.errCode == 7){
                                    $("#err-msg-bitcoin").text(data.errMsg);
                                }else{
                                   $("#err-msg-bitcoin").text("Failed to send verification email!!!"); 
                                }
                                                                
                            }
                        }
	            	}
	            	
	            },
	            error: function (xhr, status, error) {
	                alert(error);
	            }
	        });
	}

    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================        
           
=========================================================================================================================================-->
