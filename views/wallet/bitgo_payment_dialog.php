<?php
session_start();

$clientSessionId = $_POST['client_id'];
$totalPriceInBtc = $_POST['finalBtcAmount'];
$totalPriceInUSD = $_POST['finalUsdAmount'];
$recieverAddress = $_POST['recieverAddress'];
$orderId  = $_POST['encryptedCustom'];
$keywordWithTags = $_POST['keywordWithTags'];
$keywordWithTags = rtrim($keywordWithTags,',');

$clientSessionId           = urldecode($clientSessionId);
//get current session Id and refferer page
$currentSessionId    = session_id();

if ($clientSessionId == $currentSessionId) {?>
    <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border: 0;">
                <button type="button" class="close" data-dismiss="modal" style="margin-right: 5px;color: white;z-index: 5;">&times;</button>
                <h4 class="modal-title"> <image style="width: 100%;margin-top: -20px;" src ="../../images/imgpsh_fullsize.png"></h4>
            </div>
            <div class="modal-body dailogMess" style="float:left;border-top: 1px solid #ececec;border-bottom: 1px solid #ececec;">
                <div class="col-md-12" style="margin-bottom: 15px;">
                    <div class="col-md-9" style="font-weight: bold;">
                       Purchase <?php echo $keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="col-md-3" style="font-weight: bold;">
                        <?php echo $totalPriceInUSD." ".$keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="col-md-9" id="bucket" style="color: #abaaaa;">
                        Purchase of <?php echo $keywoDefaultCurrencyName; ?> <!--#hotel, #flight, #bank-->
                    </div>
                    <div class="col-md-3" style="color: #abaaaa;">
                        <?php echo $totalPriceInBtc." BTC"; ?>
                    </div>
                </div>
                <hr style="float: left;width: 100%;margin: 0;">
                <div class="col-md-12">
                    <h6 style="#00809d">  USE BITCOIN ADDRESS</h6>
                </div>
                <div class="col-md-3">
                    <img src="https://chart.googleapis.com/chart?chs=135x150&cht=qr&chl=bitcoin:<?php echo $recieverAddress; ?>" alt = 'Bitcon Address'>
                </div>
                <div class="col-md-9">
                    <h5 style="color: #cccccc;"> Send exactly <font style="color: gray;font-weight: bold;"><?php echo $totalPriceInBtc." BTC"; ?></font> to this address:</h5>
                    <input type="text" id="btcAddress" value="<?php echo $recieverAddress; ?>" style="width:100%;float:left;border: 1px solid #e8e8e8;border-radius: 3px" readonly>
                    <button class="btn btn-defalut" id="copyTextButton" style="margin: 5px 0px;background: grey;color: white;"> copy address</button> <span id="msg" style="color: green"></span><br>
                    <div id="countdown" style="float: right;margin: 5px 0px;color: #afafaf;"></div>
                </div>
            </div>

            <!--Display when payment is success-->
            <div class="modal-body sucess" style="display:none;float:left;border-top: 1px solid #ececec;border-bottom: 1px solid #ececec;">
                <div class="col-md-12 message" style="margin-bottom: 15px;">
                    <!--Content for display message when success-->
                </div>
            </div>
            <div class="modal-footer" style="border: 0;text-align:center;">
                <div style="text-align: center;margin-top: 15px;background: grey;color: white;" class="btn" id="paymentStatus">Waiting for Payment</div>
            </div>
        </div>

    </div>

    <script>
        var start = new Date();
        var end = new Date();
        var t,c;
        end.setMinutes(end.getMinutes()+30);
        //var seconds = 900;
        var seconds = ((end - start)/1000) ;

        function secondPassed() {
            var minutes = Math.round((seconds - 30) / 60);
            var remainingSeconds = seconds % 60;
            if (remainingSeconds < 10) {
                remainingSeconds = "0" + remainingSeconds;
            }
            if(parseInt(minutes) == 0 && parseInt(remainingSeconds)==0){
                //$("#procceedToPayment").modal('toggle');
                $('.sucess').css('display','block');
                $('.dailogMess').css('display','none');
                $('#paymentStatus').html('');
                $('#paymentStatus').html('Close');
                $('#paymentStatus').attr('onclick','window.location.href="index.php"');
                $('.message').html('We will update you when your transaction gets completed');
            }
            document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds;
            console.log(minutes + ":" + remainingSeconds)

        }

        function countdown() {
            // starts countdown
            secondPassed();
            if (seconds != 0) {
                seconds--;
                t = setTimeout(countdown, 1000);
            } else {
                stopTimer();
            }
        }

        function stopTimer() {
            //pauses countdown
            clearTimeout(t);
            clearInterval(c);
        }

        //Check confirmation for payment on every 10 seconds
        function checkConfirmation()
        {
            c = setInterval(checkForBitgoConfirmation, 10000);
        }

        $(document).ready(function(){
            countdown();
            checkConfirmation();

        });

        //Code to copy text to clipboard
        document.getElementById("copyTextButton").addEventListener("click", function() {
            copyToClipboardMsg(document.getElementById("btcAddress"), "msg");
        });

        function copyToClipboardMsg(elem, msgElem) {
            var succeed = copyToClipboard(elem);
            var msg;
            if (!succeed) {
                msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
            } else {
                msg = "Text copied to the clipboard."
            }
            if (typeof msgElem === "string") {
                msgElem = document.getElementById(msgElem);
            }
            msgElem.innerHTML = msg;
            setTimeout(function() {
                msgElem.innerHTML = "";
            }, 2000);
        }


        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
    </script>

<?php } else {


}

?>