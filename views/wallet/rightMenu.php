<?php 
  $accountHandle = $_SESSION["account_handle"];
    if(isset($getUserInteractionDetails['errMsg']['profile_pic']) && !empty($getUserInteractionDetails['errMsg']['profile_pic'])){
        global $cdnSocialUrl;
        global $rootUrlImages;

        $extensionUP  = pathinfo($getUserInteractionDetails['errMsg']['profile_pic'], PATHINFO_EXTENSION);
        //CDN image path
        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $getUserInteractionDetails['errMsg']['profile_pic'] . '.40x40.' . $extensionUP;

        //server image path
        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$getUserInteractionDetails['errMsg']['profile_pic'];

        // check for image is available on CDN
        $file = $imageFileOfCDNUP;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $imgSrc = $imageFileOfLocalUP;
        } else {
            $imgSrc = $imageFileOfCDNUP;
        }
    }else{
        $imgSrc = $rootUrlImages."default_profile.jpg";
    }

?>
<div class="col-xs-3">
    <div class="social-left-panel">
        <!-- my-info-card  -->
        <div class="my-info-card bg-White border-all text-Light-Black">
            <div class="clearfix innerAll ">
                <div class="my-info-img pull-left innerMR">
                    <img class="img-responsive" src="<?php echo $imgSrc; ?>">
                </div>
                <!-- my-picture  -->
                <div class="my-info-detail pull-left">
                    <div class="my-info-name-container innerMB">
                        <!-- my-name  -->
                        <span class="my-info-name"><?php echo ucfirst(utf8_decode($_SESSION['first_name'])) . " " . ucfirst(utf8_decode($_SESSION['last_name']));?></span>
                    </div>
                    <!-- my-status  -->
                    <div class="my-balance-status">
                        Balance
                        <span class="my-balance-count text-Blue btn-block ">
                          <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $userBalance." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$userBalance, 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                          </span> 
                        </span>
                    </div>
                </div>
            </div>
            <!-- my-info-detail  -->
        </div>
    </div>
    <br>
<!-- social-left-panel  -->
<!--====================================
=            wallwt sidebar            =
=====================================-->
    <div class="keymarketplace-data key-mar-data-floatingmenu">
        <div class="row">
            <div class="col-md-12 inner-2x">
               <div class="list-group list-cust border-all" style="padding:0px !important;">
                  <a href="index.php" class="f-sz16 list-group-item"> Wallet</a>
                  <a href="index.php" class="f-sz16 list-group-item<?php echo $active=($page=='index')?' active':''; ?>"> Transaction History</a>
                  <a href="sendITD.php" class="f-sz16 list-group-item<?php echo $active=($page=='send')?' active':''; ?>"> Send</a>
                  <a href="requestITD.php" class="f-sz16 list-group-item<?php echo $active=($page=='request')?' active':''; ?>"> Request</a>
                  <a href="purchaseITD.php" class="f-sz16 list-group-item<?php echo $active=($page=='purchase')?' active':''; ?>"> Purchase</a>
                  <a href="cashout.php" class="f-sz16 list-group-item<?php echo $active=($page=='cashout')?' active':''; ?>"> Cashout</a>
                  <a href="currencyPreference.php" class="f-sz16 list-group-item<?php echo $active=($page=='currency_peferences')?' active':''; ?>"> Currency Peferences</a>
                  <a href="cashoutSettings.php" class="f-sz16 list-group-item<?php echo $active=($page=='cashout_setting')?' active':''; ?>"> Cashout setting</a>
                  <a href="twoFASettings.php" class="f-sz16 list-group-item<?php echo $active=($page=='security_settings')?' active':''; ?>"> Security Settings</a>
                  <a href="accountLevelSettings.php" class="f-sz16 list-group-item<?php echo $active=($page=='account_level')?' active':''; ?>"> Account level   <span class="pull-right"><i class="fa <?php echo $kyc=($kyc_level >=1)?'fa-circle text-Blue':'fa-circle-thin text-Dark-Gray'; ?>"></i> <i class="fa <?php echo $kyc=($kyc_level >=2)?'fa-circle text-Blue':'fa-circle-thin text-Dark-Gray';  ?>"></i> <i class="fa <?php echo $kyc=($kyc_level >=3)?'fa-circle text-Blue':'fa-circle-thin text-Dark-Gray';  ?>"></i> </span>   </a>
                  </div>
            </div>
        </div>
        <!-- row ends here -->
    </div>
<!--====  End of wallwt sidebar  ====-->
  </div>