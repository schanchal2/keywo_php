<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
if(!isset($_SESSION['redirect_url'])){
    print("<script>");
    print("var t = setTimeout(\"window.location='twoFASettings.php';\", 000);");
    print("</script>");
    die; 
}
$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

$email = $_SESSION["email"];
$veifyEmail = (isset($_GET['email']))?cleanXSS(urldecode($_GET['email'])):'';
$authToken = (isset($_GET['auth']))?cleanXSS(urldecode($_GET['auth'])):'';

if($kyc_current_level=='kyc_1'){
    $kyc_level = 1;
}elseif($kyc_current_level=='kyc_2'){
    $kyc_level = 2;
}elseif($kyc_current_level=='kyc_3'){
    $kyc_level = 3;
}

$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
}

if(isset($_GET['email']) && isset($_GET['auth']) && $veifyEmail == $email){
    $type = 'transaction_pin_verify_email';
    $mode = 'email';
    $payload = '';
    $payloadDetails = getPayload($conn,$email,$authToken,$payload,$type,$mode);
    $verify = verifyAuthToken($conn,$email,$authToken,$payload,$type,$mode);
    if(noError($verify)){
        $transPin = setTransactionPin($payloadDetails['payload']['payload']);
        if(noError($transPin)){
            $_SESSION['security_preference'] = 3;
            $verification_status = 'verified';        
            $returnArr['errMsg'] = 'Succesfully verified';
            $returnArr['errCode'] = -1;
        }else{
            $verification_status = 'unverified';
            $returnArr['errMsg'] = $verify['errMsg'];
             $returnArr['errCode'] = 2;
        }
                       
    }else{
        $verification_status = 'unverified';
        $returnArr['errMsg'] = $verify['errMsg'];
        $returnArr['errCode'] = 2;   
    }

}

unset($_SESSION['redirect_url']);
$securityPreference = $_SESSION['security_preference'];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerTB">
    <div class="container padding-none row-10">
        <div class="row">
            <?php
            $page = "security_settings"; 
            include("rightMenu.php"); ?>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Setting Settings</h4>
                            <input type="hidden" id="text-input" name="">
                        <div class="row">
                            <div class="col-lg-12" id="page-container">
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
</main>
<style type="text/css">
    .defaultInput
    {
     width: 100px;
     height:25px;
     padding: 5px;
    }
    .error
    {
     border:1px solid red;
    }
</style>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript">
    $( document ).ready(function() {
        verification_link = '<?php echo $link = (isset($_GET['auth']) && !empty($_GET['auth']))?'verification_link':''; ?>'
        if(verification_link !=''){
            loadEmailVerifyConfirm();
        }else{
            loadSetTransactionPin();
        }
    });

    function loadSecurityIndex(){
        var securityPreference = <?php echo $securityPreference; ?>;
        $("#page-container").load('securityIndex.php?securityPreference='+securityPreference);
    }


    function set2FAcode(){
         $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 2){
            $("#preference-err").text("You have already set Google Authenticator code");
        }else{
        	
            window.location.href = "setGoogleAuth.php";
        }
    }

    function transactionPin() {
         $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 3){
            $("#preference-err").text("You have already set transaction pin");
        }else{
            loadSetTransactionPin();
        }
    }

    function setPasswordAuth(){
        $("#preference-err").text('');
        var security_preference = '<?php echo $securityPreference; ?>';

        if(security_preference == 1){
            $("#preference-err").text("You have already set password as security method");
        }else{
            setPasswordSecurityAuth();
        }
    }

    function setPasswordSecurityAuth(){
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/setPassowordAuthController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                trans_pin: ''
            },
            async: true,
            success:function(data){
                console.log(data);
                if(data.errCode == -1){
                    location.reload(true);           
                }else{
                    $("#preference-err").text("Failed to set password security preference"); 
                }
                
            },
            error: function (xhr, status, error) {
                location.reload();
            }
        });
    }

    function loadSetTransactionPin(){
        $("#page-container").load('setTransactionPin.php');
    }

    function loadManageBackupCodes(){
        $("#page-container").load('manageBackupCodes.php');
    }

    function setTransactionPin(){
        $("#text-input-1").removeClass('error');
        $("#text-input-2").removeClass('error');
        $("#pin-err").text('');
        $("#trans-verify-btn").text('Sending...');
        var pin_input_1 = $("#text-input-1").val();
        var pin_input_2 = $("#text-input-2").val();
        var is_valid = false;
        if(pin_input_1.length == 0 && pin_input_2.length == 0){
            $("#text-input-1").addClass('error');
            $("#text-input-2").addClass('error');
        }else if(pin_input_1.length == 0){
            $("#text-input-1").addClass('error');
        }else if(pin_input_2.length == 0){
            $("#text-input-2").addClass('error');
        }else if(pin_input_1 != pin_input_2){
            $("#pin-err").text('Both transaction pins should be identical');
        }else if(pin_input_1.length != 6){
            $("#pin-err").text('Please enter 6 digit transaction pin');
        }else if (pin_input_1.match(/[a-z]/i) && pin_input_1.match(/[0-9]/i)) {
            is_valid = true;
        }else{
            $("#pin-err").text('Transaction pin must contain combination of both number and alphabets');
        }


        if(is_valid){
            sendTransactionPinVerifyEmail(pin_input_1);
        }else{
            $("#trans-verify-btn").text('Next');
        }
    }


    function sendTransactionPinVerifyEmail(trans_pin){
        var verification_path = '<?php echo $rootUrl; ?>views/wallet/twoFASettings.php';
        $.ajax({ 
            type: "POST",
            dataType:"json",
            url: "../../controllers/wallet/sendTransVerifyEmailController.php",
            beforeSend: function() {
            } ,
            complete: function(){
            },
            data: {
                trans_pin: encodeURI(trans_pin),verification_path: encodeURI(verification_path)
            },
            async: true,
            success:function(data){
                console.log(data);
                if(data.errCode == -1){
                    $("#trans-verify-btn").text('Next');
                    $("#resend-btn").text('Resend Email');
                    loadSendVerifySuccess();
                    $("#text-input").val(trans_pin);             
                }else{
                    $("#trans-verify-btn").text('Next');
                    $("#resend-btn").text('Resend Email');
                    $("#pin-err").text('Failed to sent verification email,Please try again later');
                }
                
            },
            error: function (xhr, status, error) {
                location.reload();
            }
        });
    }

    function resendEmail(){
       $("#resend-btn").text('Sending...'); 
       var trans_pin = $("#text-input").val(); 
       if(trans_pin.length == 6){
        sendTransactionPinVerifyEmail(trans_pin);
       }else{
        $("#resend-btn").text('Resend Email');
       }
    }

    function loadSendVerifySuccess(){
        $("#page-container").load('emailSentSuccess.php');
    }

    function loadEmailVerifyConfirm(){
        var verification_status = '<?php echo $verification_status; ?>';
        $("#page-container").load('confirmEmailVerification.php?verification_status='+verification_status);
    }
</script>

<?php 
/*=============================================
=            Section comment block            =
=============================================
(28-2-17)
    BUG:EG-300
    Issue List:
        1)Text font color are different as compare to design sheet.
        2)Choose Your Security Method box background shadow color are different as compare to design sheet.   @done (17-02-28 13:26) 
=====  End of Section comment block  ======*/
?>
