<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$email = $_SESSION["email"];
$docrootpath = "../../"; 
include("../layout/header.php");
require_once("{$docrootpath}models/wallet/walletModel.php");

$userRequiredFields = $userRequiredFields . ",currencyPreference,purchase_itd,kyc_current_level,mobile_number,kyc_3_doctype_1,kyc_3_doctype_2,kyc_3_doctype_3,kyc_3_doctype_4";
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
if(noError($userDetails)){
	$userDetails = $userDetails['errMsg'];
}


$photo_id_proof = array('PAN Card ','Passport','Aadhaar Card','Voters Identity Card',' Driving License','Bank Passbook');
$address_proof = array('Passport','Voters Identity Card','Bank Passbook','Aadhaar Card');
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
  <div class="container">
    <div class="row card">
      <div class="col-xs-12 border-bottom">
        <h4 class="text-blue">Document Verification</h4>
      </div>
      <div class="col-xs-12">
        <div class="innerMT">
          User have to select the type of document and feed the document number, name as in document and attached the scanned copy of that particular document.
        </div>
        <div>
          <h6><span class="note-text">Note:-&nbsp</span><span>The attached scanned copy of document should be attested</span></h6>
        </div>
        <div>
          <div class="text-blue">
            Document required for KYC updation
          </div>
          <div class="">
            - Two photo id proofs <br>
            - Two address id proofs
          </div>
        </div>
        <div class="">
          <div class="text-blue innerMTB">
            <strong>Update your Id proof KYC information</strong> <span id="id-proof-err" style="color: red;"></span>
          </div>
          <div class="id-proof-table">
            <table class="table table-bordered">
              <thead class="bg-grey text-white">
                <th>
                  Documents
                </th>
                <th>
                  Select Document Type
                </th>
                <th>
                  Upload Scanned Document
                </th>
                <th>
                  &nbsp;
                </th>
                <th>
                  Status
                </th>
              </thead>
              <tbody>
                <form id="idProof1-form" method="post"  enctype="multipart/form-data">
                <tr>
                  <td>
                    Document 1
                    <input type="hidden" name="belongs_to" value="photo_id_proof"/>
                    <input type="hidden" name="doc_no" value="1"/>
                  </td>
                  <td>
                    <select class="" id="idProof1-doc" name="doc_type">
                      <option value="">Select type</option>
                      <?php foreach ($photo_id_proof as $key => $value) { ?>
                      	<option <?php echo $selected =($userDetails['kyc_3_doctype_1']['doc_type']==$value)?'selected':''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                      <?php } ?>                      
                    </select>
                  </td>
                  <td>
                    <div class="choose-file-box">
                      <input type="file" name="doc_file1" class="choose-id-proof" id="idProof1-file" data-id="idProof1"/>
                      <label for="idProof1-file" class="choose-file-text text-white pull-left">
                        Choose file
                      </label>
                      <div class="ellipses ellipses-wid file-name-text" id="idProof1">
                        No file choosen
                      </div>
                    </div>

                  </td>
                  <td>
                    <!-- <input type="button" class="btn-social-dark btn-xs" id="idProof1-btn" data-target="#documentConfirmation" data-toggle="modal" value="Submit"/> -->
                    <?php
                      if(empty($userDetails['kyc_3_doctype_1'])){ ?>
                    <input type="button" class="btn-social-dark btn-xs" id="idProof1-btn" onclick="submitDoc('idProof1');" value="Submit"/>
                      <?php }else{ ?>
                    <input type="button" class="btn-social-dark btn-xs" id="idProof1-btn" onclick="submitDoc('idProof1');" value="Update"/>
                      <?php } ?>
                  </td>
                  <td>
                    <?php
                      if(empty($userDetails['kyc_3_doctype_1'])){
                        $status = 'Not Submitted';
                        $class = '';
                      }else if($userDetails['kyc_3_doctype_1']['status'] == 'pending'){
                        $status = 'Pending';
                        $class = 'text-light-blue';
                      }elseif($userDetails['kyc_3_doctype_1']['status'] == 'approved'){
                        $status = 'Approved';
                        $class = 'text-dark-green';
                      }elseif($userDetails['kyc_3_doctype_1']['status'] == 'in progress'){
                        $class = 'text-yellow';
                        $status = 'In progress';
                      }elseif($userDetails['kyc_3_doctype_1']['status'] == 'rejected'){
                        $status = 'rejected';
                        $class = 'error-color';
                      }

                    ?>
                    <div class="<?php echo $class; ?>">
                      <strong><?php echo $status; ?></strong>
                    </div>
                  </td>
                </tr>
                </form>

                <form id="idProof2-form" method="post"  enctype="multipart/form-data">
                <tr>
                  <td>
                    Document 2
                    <input type="hidden" name="belongs_to" value="photo_id_proof"/>
                    <input type="hidden" name="doc_no" value="2"/>                    
                  </td>
                  <td>
                    <select class="" id="idProof2-doc" name="doc_type">
                      <option value="">Select type</option>
                      <?php foreach ($photo_id_proof as $key => $value) { ?>
                        <option <?php echo $selected =($userDetails['kyc_3_doctype_2']['doc_type']==$value)?'selected':''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                      <?php } ?>                      
                    </select>

                  </td>
                  <td>
                    <div class="choose-file-box">
                      <input type="file" name="doc_file2" class="choose-id-proof" id="idProof2-file" data-id="idProof2"/>
                      <label for="idProof2-file" class="choose-file-text text-white pull-left">
                        Choose file
                      </label>
                      <div class="ellipses ellipses-wid file-name-text" id="idProof2">
                        No file choosen
                      </div>
                    </div>

                  </td>
                  <td>
                    <?php
                      if(empty($userDetails['kyc_3_doctype_2'])){ ?>
                    <input type="button" class="btn-social-dark btn-xs" id="idProof2-btn" onclick="submitDoc('idProof2');" value="Submit"/>
                      <?php }else{ ?>
                    <input type="button" class="btn-social-dark btn-xs" id="idProof2-btn" onclick="submitDoc('idProof2');" value="Update"/>
                      <?php } ?>
                  </td>
                  <td>
                    <?php
                      if(empty($userDetails['kyc_3_doctype_2'])){
                        $status = 'Not Submitted';
                        $class = '';
                      }else if($userDetails['kyc_3_doctype_2']['status'] == 'pending'){
                        $status = 'Pending';
                        $class = 'text-light-blue';
                      }elseif($userDetails['kyc_3_doctype_2']['status'] == 'approved'){
                        $status = 'Approved';
                        $class = 'text-dark-green';
                      }elseif($userDetails['kyc_3_doctype_2']['status'] == 'in progress'){
                        $class = 'text-yellow';
                        $status = 'In progress';
                      }elseif($userDetails['kyc_3_doctype_2']['status'] == 'rejected'){
                        $status = 'rejected';
                        $class = 'error-color';
                      }

                    ?>
                    <div class="<?php echo $class; ?>">
                      <strong><?php echo $status; ?></strong>
                    </div>
                  </td>
                </tr>
                </form>
              </tbody>
            </table>
          </div>
        </div>
        <div class="">
          <div class="text-blue innerMTB">
            <strong>Update your Address proof KYC information</strong> <span id="address-proof-err" style="color: red;"></span>
          </div>
          <div class="address-proof-table">
              <table class="table table-bordered">
                <thead class="bg-grey text-white">
                  <th>
                    Documents
                  </th>
                  <th>
                    Select Document Type
                  </th>
                  <th>
                    Upload Scanned Document
                  </th>
                  <th>
                    &nbsp;
                  </th>
                  <th>
                    Status
                  </th>
                </thead>
                <tbody>
                <form id="idProof3-form" method="post"  enctype="multipart/form-data">
                  <tr>
                    <td>
                      Document 1
                      <input type="hidden" name="belongs_to" value="address_proof"/>
                      <input type="hidden" name="doc_no" value="3"/> 
                    </td>
                    <td>
                      <select class="" id="idProof3-doc" name="doc_type">
                        <option value="">Select type</option>
                        <?php foreach ($photo_id_proof as $key => $value) { ?>
                          <option <?php echo $selected =($userDetails['kyc_3_doctype_3']['doc_type']==$value)?'selected':''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php } ?>                      
                      </select>
                    </td>
                    <td>
                      <div class="choose-file-box">
                        <input type="file" name="doc_file3" class="choose-id-proof" id="idProof3-file" data-id="idProof3"/>
                        <label for="idProof3-file" class="choose-file-text text-white pull-left">
                          Choose file
                        </label>
                        <div class="ellipses ellipses-wid file-name-text" id="idProof3">
                          No file choosen
                        </div>
                      </div>

                    </td>
                    <td>
                      <?php
                      if(empty($userDetails['kyc_3_doctype_3'])){ ?>
                      <input type="button" class="btn-social-dark btn-xs" id="idProof3-btn" onclick="submitDoc('idProof3');" value="Submit"/>
                        <?php }else{ ?>
                      <input type="button" class="btn-social-dark btn-xs" id="idProof3-btn" onclick="submitDoc('idProof3');" value="Update"/>
                        <?php } ?>
                    </td>
                    <td>
                      <?php
                        if(empty($userDetails['kyc_3_doctype_3'])){
                          $status = 'Not Submitted';
                          $class = '';
                        }else if($userDetails['kyc_3_doctype_3']['status'] == 'pending'){
                          $status = 'Pending';
                          $class = 'text-light-blue';
                        }elseif($userDetails['kyc_3_doctype_3']['status'] == 'approved'){
                          $status = 'Approved';
                          $class = 'text-dark-green';
                        }elseif($userDetails['kyc_3_doctype_3']['status'] == 'in progress'){
                          $class = 'text-yellow';
                          $status = 'In progress';
                        }elseif($userDetails['kyc_3_doctype_3']['status'] == 'rejected'){
                          $status = 'rejected';
                          $class = 'error-color';
                        }

                      ?>
                      <div class="<?php echo $class; ?>">
                        <strong><?php echo $status; ?></strong>
                      </div>
                    </td>
                  </tr>
                  </form>

                  <form id="idProof4-form" method="post"  enctype="multipart/form-data">
                  <tr>
                    <td>
                      Document 2
                      <input type="hidden" name="belongs_to" value="address_proof"/>
                      <input type="hidden" name="doc_no" value="4"/>
                    </td>
                    <td>
                      <select class="" id="idProof4-doc" name="doc_type">
                        <option value="">Select type</option>
                        <?php foreach ($photo_id_proof as $key => $value) { ?>
                          <option <?php echo $selected =($userDetails['kyc_3_doctype_4']['doc_type']==$value)?'selected':''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php } ?>                      
                      </select>

                    </td>
                    <td>
                      <div class="choose-file-box">
                        <input type="file" name="doc_file4" class="choose-id-proof" id="idProof4-file" data-id="idProof4"/>
                        <label for="idProof4-file" class="choose-file-text text-white pull-left">
                          Choose file
                        </label>
                        <div class="ellipses ellipses-wid file-name-text" id="idProof4">
                          No file choosen
                        </div>
                      </div>

                    </td>
                    <td>
                      <?php
                        if(empty($userDetails['kyc_3_doctype_4'])){ ?>
                        <input type="button" class="btn-social-dark btn-xs" id="idProof4-btn" onclick="submitDoc('idProof4');" value="Submit"/>
                          <?php }else{ ?>
                        <input type="button" class="btn-social-dark btn-xs" id="idProof4-btn" onclick="submitDoc('idProof4');" value="Update"/>
                          <?php } ?>
                    </td>
                    <td>
                      <?php
                        if(empty($userDetails['kyc_3_doctype_4'])){
                          $status = 'Not Submitted';
                          $class = '';
                        }else if($userDetails['kyc_3_doctype_4']['status'] == 'pending'){
                          $status = 'Pending';
                          $class = 'text-light-blue';
                        }elseif($userDetails['kyc_3_doctype_4']['status'] == 'approved'){
                          $status = 'Approved';
                          $class = 'text-dark-green';
                        }elseif($userDetails['kyc_3_doctype_4']['status'] == 'in progress'){
                          $class = 'text-yellow';
                          $status = 'In progress';
                        }elseif($userDetails['kyc_3_doctype_4']['status'] == 'rejected'){
                          $status = 'rejected';
                          $class = 'error-color';
                        }

                      ?>
                      <div class="<?php echo $class; ?>">
                        <strong><?php echo $status; ?></strong>
                      </div>
                    </td>
                  </tr>
                  </form>
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div id="documentConfirmation" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         <!--  <button type="button" class="close text-color-Gray" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title text-blue">Document Confirmation</h4>
        </div>
        <div class="modal-body text-center">
          <div class="">
            <h4 class="text-dark-green">We have received your document and they are now being  processed</h4>
          </div>
          <div class="">
            You will receive email once your verification is completed.
          </div>
          <div class="innerMTB inner-2x">
            <input type="button" class="btn-social-wid-auto btn-xs" onclick="location.reload();" value="Continue">
          </div>
        </div>
      </div>

    </div>
  </div>
</main>


<?php include("../layout/transparent_footer.php");?>
<script>

  $('.choose-id-proof').bind('change', function() {

  	var fileExtension = ['jpeg', 'jpg', 'png', 'pdf'];
  	var id = "#"+$(this).data("id");
    $(id+'-btn').prop('disabled', false);
    $('#id-proof-err').text('');
    $('#address-proof-err').text('');

    var show_err = '';
    if(id == '#idProof1' || id == '#idProof2'){
      show_err = '#id-proof-err';
    }else{
      show_err = '#address-proof-err';
    }

    $(id).html($(this).val().replace(/C:\\fakepath\\/i, ''));

    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      $(show_err).text("Only formats are allowed : "+fileExtension.join(', '));
        //alert("Only formats are allowed : "+fileExtension.join(', '));
        $(id+'-btn').prop('disabled', true);
        return false;
    }
  	
  	if(this.files[0].size > 1966223){
      $(show_err).text("File size is too large,Upload file smaller than 2MB");
      $(id+'-btn').prop('disabled', true);
      return false;
    }
  	//this.files[0].size gets the size of your file.
  	console.log(this.files[0]);
  	//alert(this.files[0].size);

  });

  function submitDoc(idProof){
    var btn = $("#"+idProof+"-btn").val();
    $("#"+idProof+"-btn").val('Uploading...');
    $('#id-proof-err').text("");
    $('#address-proof-err').text('');
    var doc_type = $("#"+idProof+"-doc").val();
    var file_name = $("#"+idProof+"-file").val();
    

    var show_err = '';
    if(idProof == 'idProof1' || idProof == 'idProof2'){
      show_err = '#id-proof-err';
    }else{
      show_err = '#address-proof-err';
    }

    if(doc_type.length == 0){
      $(show_err).text("Please select document type");
      $("#"+idProof+"-btn").val(btn);
    }else if(file_name.length == 0){
      $(show_err).text("Please choose file to upload");
      $("#"+idProof+"-btn").val(btn);
    }else{  
      //var myform = $("#"+idProof+"-btn").closest("form"); //parent form
      //alert(myform);
      var form = $("#"+idProof+"-form")[0]; // You need to use standart javascript object here
      var formData = new FormData(form);
      $.ajax({
          type: "POST",
          url: '../../controllers/wallet/accountLevel3Controller.php',
          data:formData,
          contentType: false,
          processData: false,
          async:false,
          dataType: 'json',
       success: function(data)
       {
        console.log(data);
        $("#"+idProof+"-btn").val(btn);
        if(data.errCode == -1){
          $("#documentConfirmation").modal('show');
          // location.reload();
        }else{
          $(show_err).text("Failed to update KYC document");
        }
           //alert(data); // show response from the php script.
       }
     });
    }
      
    
   // alert(file_name);
  } 

</script>
