<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once ('../../../models/keywords/tradeModel.php');

$email = $_SESSION["email"];

// get request parameters
//$reportForMonth = trim(urldecode($_POST["reportformonth"]));
$type = trim(urldecode($_POST["type"]));
$reportName = trim(urldecode($_POST["reportName"]));
$returnArr = array();
$xls = array();
$data = array();

header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=" . $reportName. ".xls");
header("Pragma: no-cache");
header("Expires: 0");

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    if($type == 'all'){

        $getMyKeyword = getMyKeywordDetails($kwdDbConn,$email);
        if(noError($getMyKeyword)) {
            $getMyKeyword = $getMyKeyword["errMsg"];

            $myKeywords = $getMyKeyword["transaction_details"];
            $myKeywords = json_decode($myKeywords, true);

            foreach($myKeywords as $key => $value){
                $data[$key]["keyword"] = $value["keyword"];
            }

            $getMyBidDetails = getMyBidDetails($email, $kwdDbConn);
            if(noError($getMyBidDetails)){

                $getMyBidDetails = $getMyBidDetails["errMsg"][0];
                $myBids = $getMyBidDetails["bid_details"];
                $myBids = json_decode($myBids, true);

                foreach($myBids as $key => $bidKwd) {
                    $bidKwd = explode("~~", $bidKwd);

                    $keyword = $bidKwd[6];
                    $bidPrice = $bidKwd[3];

                    $data[$key]["keyword"] = $keyword;
                    $data[$key]["my_bid"] = $bidPrice;
                }
            }

            foreach($data as $key => $value){
                $keyword = $value["keyword"];
                $myBidAmt = $value["my_bid"];

                $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                if(noError($getKwdOwnershipDetails)){
                    $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                    $buyerId = $getKwdOwnershipDetails["buyer_id"];
                    $kwdPrice = $getKwdOwnershipDetails["kwd_price"];
                    $askPrice = $getKwdOwnershipDetails["ask_price"];
                    $highestBidAmt = $getKwdOwnershipDetails["highest_bid_amount"];
                    $purchaseTime = $getKwdOwnershipDetails["purchase_timestamp"];
                    $noOfBids = $getKwdOwnershipDetails["no_of_active_bids"];

                    if (empty($askPrice)) {
                        $askPrice = 0;
                    }

                    $returnArr[$key]["keyword"] = $keyword;
                    $returnArr[$key]["buyer_id"] = $buyerId;
                    $returnArr[$key]["kwd_price"] =$kwdPrice;
                    $returnArr[$key]["ask_price"] = $askPrice;
                    $returnArr[$key]["highest_bid_amt"] = $highestBidAmt;
                    $returnArr[$key]["no_of_bids"] = $noOfBids;
                    $returnArr[$key]["my_bid_amt"] = $myBidAmt;

                }else{
                    print('Error: Fetching keyword ownership details');
                }
            }

        }else{
            print('Error: Fetching keyword details');
        }

    }else if($type == 'bidReceive'){

        $getMyKeyword = getMyKeywordDetails($kwdDbConn,$email);
        if(noError($getMyKeyword)){
            $getMyKeyword = $getMyKeyword["errMsg"];
            if(count($getMyKeyword) > 0) {
                $myKeywords = $getMyKeyword["transaction_details"];
                $myKeywords = json_decode($myKeywords, true);

                foreach($myKeywords as $key => $value){
                    $keyword = $value["keyword"];

                    $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                    if(noError($getKwdOwnershipDetails)) {
                        $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                        $buyerId = $getKwdOwnershipDetails["buyer_id"];
                        $kwdPrice = $getKwdOwnershipDetails["kwd_price"];
                        $askPrice = $getKwdOwnershipDetails["ask_price"];
                        $highestBidAmt = $getKwdOwnershipDetails["highest_bid_amount"];
                        $purchaseTime = $getKwdOwnershipDetails["purchase_timestamp"];
                        $noOfBids = $getKwdOwnershipDetails["no_of_active_bids"];


                        if (empty($askPrice)) {
                            $askPrice = 0;
                        }

                        $returnArr[$key]["keyword"] = $keyword;
                        $returnArr[$key]["buyer_id"] = $buyerId;
                        $returnArr[$key]["kwd_price"] =$kwdPrice;
                        $returnArr[$key]["ask_price"] = $askPrice;
                        $returnArr[$key]["highest_bid_amt"] = $highestBidAmt;
                        $returnArr[$key]["no_of_bids"] = $noOfBids;
                    }
                }
            }
        }else{
            print('Error: Fetching keyword details');
        }

    }else if($type == 'bidSent'){
        $getMyBidDetails = getMyBidDetails($email, $kwdDbConn);
        if(noError($getMyBidDetails)){

            $getMyBidDetails = $getMyBidDetails["errMsg"][0];
            $myBids = $getMyBidDetails["bid_details"];
            $myBids = json_decode($myBids, true);

            foreach($myBids as $key => $bidKwd) {
                $bidKwd = explode("~~", $bidKwd);

                $keyword = $bidKwd[6];
                $bidPrice = $bidKwd[3];

                $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                if(noError($getKwdOwnershipDetails)){
                    $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                    $buyerId = $getKwdOwnershipDetails["buyer_id"];
                    $kwdPrice = $getKwdOwnershipDetails["kwd_price"];
                    $askPrice = $getKwdOwnershipDetails["ask_price"];
                    $highestBidAmt = $getKwdOwnershipDetails["highest_bid_amount"];
                    $purchaseTime = $getKwdOwnershipDetails["purchase_timestamp"];
                    $noOfBids = $getKwdOwnershipDetails["no_of_active_bids"];

                    if (empty($askPrice)) {
                        $askPrice = 0;
                    }

                    $returnArr[$key]["keyword"] = $keyword;
                    $returnArr[$key]["buyer_id"] = $buyerId;
                    $returnArr[$key]["kwd_price"] =$kwdPrice;
                    $returnArr[$key]["ask_price"] = $askPrice;
                    $returnArr[$key]["highest_bid_amt"] = $highestBidAmt;
                    $returnArr[$key]["no_of_bids"] = $noOfBids;
                    $returnArr[$key]["my_bid_amt"] = $bidPrice;

                }else{
                    print('Error: Fetching keyword ownership details');
                }
            }

        }else{
            print('Error: Fetching active bids details');
        }
    }

?>
    <table>
        <tr>
            <th>Keyword</th>
            <th>keyword Owner</th>
            <th>Keyword Price</th>
            <th>Ask Price</th>
            <th>Highest Bid Amount</th>
            <th>Number of Bids</th>

            <?php
                if($type == 'bidSent' || $type == 'all'){
                    ?>
                    <th>My Bid</th>
                    <?php
                }

            ?>
        </tr>
        <tbody>
        <?php


            foreach($returnArr as $key => $value){
                ?>
                <tr>
                    <td> <?php echo $value["keyword"]; ?></td>
                    <td> <?php echo $value["buyer_id"]; ?></td>
                    <td> <?php echo $value["kwd_price"]; ?></td>
                    <td> <?php echo $value["ask_price"]; ?></td>
                    <td> <?php echo $value["highest_bid_amt"]; ?></td>
                    <td> <?php echo $value["no_of_bids"]; ?></td>

                    <?php
                    if($type == 'bidSent' || $type == 'all'){
                        ?>
                        <td> <?php echo $value["my_bid_amt"]; ?></td>
                        <?php
                    }

                    ?>
                </tr>
                <?php
            }

        ?>
        </tbody>
    </table>

<?php
}else{
    print('Error: Database connection');
}





?>