
<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/header/headerModel.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once("../../../helpers/sessionHelper.php");
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn))
{
    $kwdDbConn = $kwdDbConn["connection"];
}
else
{
    print('Error: Database connection');
    exit;
}

$email = $_SESSION["email"];

// This function is used for Trading History.

$getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
if(noError($getMyKeywordDetails))
{
    $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
    $myKeywords = $getMyKeywordDetails["transaction_details"];
    $myKeywords = json_decode($myKeywords, true);
    $myKeywords = array_reverse($myKeywords);
    $kwdCount = count($myKeywords);
}
foreach($myKeywords as $getDataMyKeywords)
{
    $kwdPrice    = $getDataMyKeywords["kwd_price"];
    $kwdPrice1[] = $getDataMyKeywords["kwd_price"];
    $kwdPrices     = array_sum($kwdPrice1);
}

$tradingJson = $rootUrl."json_directory/keywords/trading_history/";
$tradingJson = $tradingJson.$email.".json";
$tradingData = file_get_contents($tradingJson);
$data      = json_decode($tradingData, TRUE);
$dataCount = count($data);

foreach($data as $getData)
{
    $kwd_last_traded_price1[] = $getData["kwd_last_traded_price"];
    $kwd_last_traded_price = array_sum($kwd_last_traded_price1);
}

$keywordEarning = $kwdPrices+$kwd_last_traded_price;

if(empty($kwd_last_traded_price))
{
    $kwd_last_traded_price = '0';
}

$referralInteractions = getReferralInteraction($email,$NotificationURL);
if(noError($referralInteractions))
{
    $errMsg          = $referralInteractions["errMsg"];
    $refInteractions = $errMsg["referral_interactions"];
}

$requiredFields = $userRequiredFields . ",affiliate_earning";
$commission = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
if(noError($commission))
{
    $errMsgs = $commission["errMsg"];
    $commissions = $errMsgs["affiliate_earning"];
}

$referralUserEarning = $refInteractions+$commissions;
$referralUserEarnings = number_format($referralUserEarning,2);

$equationOfPieChart = $totalConsumptionEarn+$totalUploadEarn+$keywordEarning+$referralUserEarning;

$contentConsumption  = intval($totalConsumptionEarn/$equationOfPieChart*100);
$contentupload       = intval($totalUploadEarn/$equationOfPieChart*100);
$keyword             = intval($keywordEarning/$equationOfPieChart*100);
$referralZone        = intval($referralUserEarning/$equationOfPieChart*100);

?>
<div class="row">
    <div class="col-md-12 bg-white border-all">
        <div class="row">
            <div class="col-md-6 border-right half innerLR innerT">
                <div class="row">
                    <div class="col-md-5">
                        <!-- graph div starts here -->
                        <div id="container"></div>
                        <!-- graph div ends here -->
                    </div>
                    <div class="col-md-7">
                        <div id="my-earning-unordered-list">
                            <span class="keyword-grey-span f-sz16">Earning Type</span>
                            <ul class="padding-left-none margin-none l-h18">
                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Content Consumption</span></li>
                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Content Upload</span></li>
                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Keyword</span></li>
                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Referral</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 half  innerLR innerT">
                <div class="row">
                    <div class="col-md-6 text-center keyword-marketplace">
                        <!-- added keyword-marketplace class for text style -->
                        <div id="my-earning-unordered-list">
                            <!-- added my-earning-unordered-list class for  ul li css -->
                            <span class="keyword-grey-span f-sz16">Earnings</span>

                            <li>
                                <?php ?>
                                <label class="text-black ellipses margin-bottom-none">
                                    <div class="currency display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                         origPrice="<?php echo number_format("{$totalConsumptionEarn}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalConsumptionEarn}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalConsumptionEarn}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div>
                                </label>
                            </li>
                            <li>
                                <?php  ?>
                                <label class="text-black ellipses margin-bottom-none">
                                    <div class="currency display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                         origPrice="<?php echo number_format("{$totalUploadEarn}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalUploadEarn}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalUploadEarn}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div>
                                </label>
                            </li>
                            <li>

                                <label class="text-black ellipses margin-bottom-none" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$keywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$keywordEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$keywordEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                            </li>
                            <li>
                                <label class="text-black ellipses margin-bottom-none" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$referralUserEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$referralUserEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$referralUserEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                            </li>

                        </div>
                    </div>
                    <div class="col-md-6 text-center keyword-marketplace">
                        <!-- added keyword-marketplace class for text style -->

                        <!-- added my-earning-unordered-list class for  ul li css -->
                        <span class="keyword-grey-span f-sz16">Interactions</span>
                        <ul class="padding-left-none half innerMT margin-bottom-none">
                            <li>
                                <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="<?php echo $totalConsumptionInteraction; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalConsumptionInteraction}", 0); ?></a></label>
                            </li>
                            <li>
                                <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="<?php echo $totalUploadInteraction; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalUploadInteraction}", 0); ?></a></label>
                            </li>
                            <li>
                                <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="<?php echo $kwd_last_traded_price; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwd_last_traded_price}",0); ?></a></label>
                            </li>
                            <li>
                                <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="0" data-toggle="tooltip" data-placement="bottom">0</a></label>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        Highcharts.setOptions({
            colors: ['#5cc0f2', '#009eec', '#006496', '#004161']
        });
        // Create the chart
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            title: {
                text: ' '
            },
            yAxis: {
                title: {
                    text: 'Total percent market share'
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        //  stroke-width:'0',
                        //  stroke:"#ff0000",
                        color: '#ffffff',
                        distance: -18,
                        textShadow: "0px 0px #ff0000"

                    },
                    shadow: false
                }
            },
            tooltip: {
                // enabled: false,
                formatter: function () {
                    // return + this.y + ' %';
                    return this.y + '%';  // dis shows tooltip content
                }
            },
            series: [{
                type: 'pie',
                name: 'Earning Type',
                innerSize: '44%',
                //  startAt:'-10',
                size: '140%',
                data: [

                    ['<?php echo "{$referralZone}%"; ?>', <?php echo $referralZone; ?>],
                    ['<?php echo "{$keyword}%"; ?>', <?php echo $keyword; ?>],
                    ['<?php echo "{$contentupload}%"; ?>', <?php echo $contentupload; ?>],
                    ['<?php echo "{$contentConsumption}%"; ?>', <?php echo $contentConsumption; ?>],

                    {
                        name: 'Proprietary or Undetectable',
                        y: 0.0,
                        dataLabels: {
                            enabled: false
                        }
                    }
                ]
            }]
        });
    });

    // $('[data-toggle="tooltip"]').tooltip({
    //     trigger:'hover'
    // });

</script>