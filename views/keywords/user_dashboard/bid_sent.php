<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once ('../../../models/keywords/tradeModel.php');

$email = $_SESSION["email"];

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}else{
    print('Error: Database connection');
    exit;
}

$limit = cleanQueryParameter($kwdDbConn, cleanXss($_POST["limit"]));

    if(isset($_POST["page"]))
    {
    $page_number = (int)filter_var($_POST["page"],FILTER_SANITIZE_NUMBER_INT,FILTER_FLAG_STRIP_HIGH);
         if (!is_numeric($page_number)) {
            die('Invalid page number!');
        } //incase of invalid page number
    } else {
        $page_number = 1; //if there's no page number, set it to 1
    }
    if ($page_number == 0) {
        $page_number = 1;
    }

    $item_per_page = $limit;

$getMyBidDetails = getMyBidDetails($email, $kwdDbConn);

if(noError($getMyBidDetails)){

        $getMyBidDetails = $getMyBidDetails["errMsg"][0];

        $myBids = $getMyBidDetails["bid_details"];

        $myBids      = json_decode($myBids, true);

        $myBidsCount = count($myBids);

        if(empty($myBids) || $myBidsCount == 0) { ?>

            <div class="bg-white search-query innerMT">
                <div class="half innerAll">
                    <!-- row starts here   -->
                    <div class="row half innerAll padding-left-none padding-right-none" style="text-align: center">
                        No Bid Sent Found
                    </div>
                    <!-- row ends here -->
                </div>
            </div>

       <?php exit; }
        $get_total_rows = $myBidsCount;
        $total_pages = ceil($get_total_rows / $item_per_page);

        $page_position = (string)(($page_number - 1) * $item_per_page); 
        $lastpage = ceil($total_pages);

        if ($get_total_rows == 0)
        {
            $page_number = $page_number - 1; //if there's no page number, set it to 1
            $item_per_page = $limit;
            $get_total_rows = $myBidsCount;
            $total_pages = ceil($get_total_rows / $item_per_page);

        //position of records
            $page_position = (($page_number - 1) * $item_per_page);
            $lastpage = ceil($total_pages);
        }

            $keywords    = $myBids;
            $count       = count($keywords);
            $limits      = intval($limit); 
            $pagePostion = intval($page_position);
            $limits      = intval($pagePostion + ($limits -1));

           for($i=$pagePostion; $i<$count && $i<=$limits;$i++){

            $bidKwd = explode("~~", $keywords[$i]);

            $keyword = $bidKwd[6];
            $bidPrice = $bidKwd[3];

            $pageUrl = "views/keywords/analytics/keyword_analytics.php";
            $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
            if(noError($getKwdOwnershipDetails)){
                $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];

                $askPrice = $getKwdOwnershipDetails["ask_price"];
                ?>

                <!-- white box starts here -->
                <div class="bg-white search-query innerMT">
                    <div class="half innerAll">
                        <!-- row starts here   -->
                        <div class="row half innerAll padding-left-none padding-right-none">
                            <div class="col-md-2 inner-2x innerMR">
                                <span class="txt-blue ellipses text-left"><a href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $keyword; ?>"><?php echo "#".$keyword; ?></a></span>
                            </div>
                            <div class="col-md-2 text-black">
                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                    <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$bidPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$bidPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$bidPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                </label>
                            </div>
                            <div class="col-md-2 text-black padding-left-none padding-right-none half innerML">
                                <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                            </div>

                            <?php
                            if(isset($askPrice) && !empty($askPrice)){
                                ?>

                                <div class="col-md-2 text-black innerML">
                                    <label class="text-black ellipses margin-bottom-none margin-top-none">
                                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$askPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                                <div class="col-md-3 half pull-right">
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <button class="btn-trading-wid-auto-dark"  value="<?php echo $keyword;  ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>', <?php  echo $askPrice; ?>);"  >Buy Now</button>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                            else{
                                // set ask
                                ?>
                                <div class="col-md-4 col-md-offset-1 text-black half innerMR innerT pull-right text-right">
                                        <span>
                                         Ask not set
                                        </span>
                                </div>
                                <?php
                            }
                            ?>


                        </div>
                        <!-- row ends here -->
                    </div>
                </div>
                <!-- white box ends here -->

                <?php
            }else{
                print('Error: Fetching keyword ownership details');
                exit;
            }
        }

}else{
    print('Error: Fetching bid sent details');
    exit;
}



?>

<div class="row">
      <div class="col-md-5 padding-left-none padding-right-none"></div>
        <div class="col-md-7 padding-left-none half innerR innerT">
          <div class="pagination-cont pull-right gotovalues">


    <?php

    getPaginationData($lastpage, $page_number, $limit);
    function getPaginationData($lastpage, $pageno, $limit)
    {
echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
echo '<ul class="pagination">';


if ($pageno > 1) {

    $pagenum = 1;
    print('<li class="status"><a href="#"onclick=getNewBidSentData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
}

if ($pageno > 1) {
    $pagenumber = $pageno - 1;
    print('<li class="status"><a href="#" onclick=getNewBidSentData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
}

if ($pageno == 1) {
    $startLoop = 1;
    $endLoop = ($lastpage < 5) ? $lastpage : 5;
} else if ($pageno == $lastpage) {
    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
    $endLoop = $lastpage;
} else {
    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
}

for ($i = $startLoop; $i <= $endLoop; $i++) {
    if ($i == $pageno) {
        print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
    } else {
        $pagenumber = $i;
        print('<li class="status"><a href="#" onclick=getNewBidSentData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
    }
}
if ($pageno < $lastpage) {
    $pagenumber = $pageno + 1;
    print('<li class="status"><a href="#" onclick=getNewBidSentData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

}

if ($pageno != $lastpage) {
    print('<li class="status"><a href="#" onclick=getNewBidSentData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
}


echo '</ul>';
echo '</div>';
}


?>

    </div>
  </div>
</div>

<script>
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>