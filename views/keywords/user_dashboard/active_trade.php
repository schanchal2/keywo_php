    <?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once ('../../../models/keywords/tradeModel.php');

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../../layout/header.php");

    $email      = $_SESSION["email"];

    $arr = array();

    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];
    }else{
        print('Error: Database connection');
        exit;
    }


    $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
    if(noError($getPresaleDetails)){
        $getPresaleDetails = $getPresaleDetails["errMsg"];
        $totalBidAccepted = $getPresaleDetails["bids_accepted"];
        if(empty($totalBidAccepted)){
            $totalBidAccepted = 0;
        }

    }else{
        print('Error: Fetching presale details');
        exit;
    }


    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
<div id="cover"></div>
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3 stickyPostion_activeTrade_leftPannel">
                <?php include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <!-- social-all-status-tabs -->
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
                        <ul class="nav nav-tabs pull-left">
                            <li class="active"><a href="javascript:void(0);" class="l-h10" id="activeTradeAll" >All</a></li>
                            <li><a href="javascript:void(0);" class="l-h10" id="bidReceive" >Bid receive</a></li>
                            <li><a href="javascript:void(0);" class="l-h10" id="bidSent">Bid sent</a></li>
                        </ul>

                        <?php

                        // get my bid details from mybid_details table.
                        $getMyBidDetails = getMyBidDetails($email, $kwdDbConn);
                        if(noError($getMyBidDetails)){
                            $getMyBidDetails = $getMyBidDetails["errMsg"][0];
                            $bidSent = $getMyBidDetails["bid_details"];
                            $bidSent = json_decode($bidSent, true);

                            $totalBidPlaced = count($bidSent);

                            if($totalBidPlaced > 0){
                                foreach($bidSent as $key => $bids){
                                    $bids = explode("~~", $bids);
                                    $arr[$key]["keyword"] = $bids[6];
                                    $arr[$key]["bids_price"] = $bids[3];

                                    $opts[$key]=$bids[3];
                                }
                            }

                            // get max highest bid placed
                            $HighestBidPlaced = max($opts);

                            if($HighestBidPlaced <= 0){
                                $HighestBidPlaced = number_format(0, 4);
                            }
                        }else{
                            print('Error: Fetching active bid details');
                            exit;
                        }

                        /* Get total bid received */
                        $totalActiveBids = 0;
                        $totalBidReceivedAmount = 0;
                        $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
                        if(noError($getMyKeywordDetails)) {

                            $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
                            $myKeywords = $getMyKeywordDetails["transaction_details"];
                            $myKeywords = json_decode($myKeywords, true);
                            $myKeywords = array_reverse($myKeywords);
                            $kwdCount = count($myKeywords);

                            if ($kwdCount > 0) {

                                foreach($myKeywords as $record){

                                    $keyword = $record["keyword"];

                                    $result = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                                    $no_of_active_bids = $result["errMsg"]["no_of_active_bids"];
                                    $active_bids = $result["errMsg"]["active_bids"];
                                    $active_bids = json_decode($active_bids, true);
                                    if(isset($no_of_active_bids) && !empty($no_of_active_bids)){
                                        $totalActiveBids = $totalActiveBids + $no_of_active_bids;
                                    }

                                    foreach ($active_bids as $value) {
                                        $bidDetails   = explode("~~", $value);
                                        $bidAmount = $bidDetails[3];
                                        $totalBidReceivedAmount = $totalBidReceivedAmount + $bidAmount;
                                    }
                                }
                            }
                        }


                        ?>

                        <div id="myTabContent" class="tab-content pull-right width-100-percent innerT">
                            <div class="tab-pane active in">
                                <!-- content starts here -->

                                <div id="activeTradeAjaxdata">
                                  <!-- load bid recieve and bid sent data -->
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <!-- col-md-3 starts here -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_activeTrade_rightPannel">

                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Bid Received</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none  border-all">
                            <div class="social-user-setting-name half innerTB clearfix  padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Total Bid Received</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" title="<?php echo $totalActiveBids; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($totalActiveBids, 0); ?></a>
                                    </label>

                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Total Bid Accepted</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" title="<?php echo $totalBidAccepted; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalBidAccepted}", 0); ?></a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Bid Sent</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix  padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Total Bid Placed</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                        <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                            <a href="#" title="<?php echo $totalBidPlaced; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalBidPlaced}", 0); ?></a>
                                        </label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none  padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Highest Bid Placed</span>
                                </div>
                                <div class="col-xs-4 text-right">

                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$HighestBidPlaced}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$HighestBidPlaced}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$HighestBidPlaced}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                   <div class="card right-panel-modules inner-2x innerMT">
                    <?php //include '_my_earning_calender.php'; ?>
                       <div class="bg-light-gray right-panel-modules-head">
                           <div class="row margin-none">
                               <div class="col-xs-12 padding-none">
                                   <div class="col-xs-12">
                                       <h4 class="margin-none half innerTB">Keyword Trading</h4>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="earnings-right-module">
                           <input type="hidden" id="tradeSelection" name="tradeReport" value="all" />
                           <div id='calendar'></div>
                           <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
                            <i class="fa fa-download text-white"></i>
                        </span>
                               <form id="downloadTradeData" method="post" action="downloadTradeReport.php">
                                   <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadTradeReport" type="button" value="Download Report"/>

                                   <div id="tradeDynamicParameter">

                                   </div>

                               </form>
                           </div>
                       </div>
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-md-3 ends here -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<?php
include("../../layout/transparent_footer.php");
?>
<script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>

<script type="text/javascript">
    var rootURL = '<?php echo $rootUrl; ?>';

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }


    $("#calendar").MonthPicker({
            SelectedMonth: n+'/' + new Date().getFullYear(),
            OnAfterChooseMonth: function(selectedDate) {
                // Do something with selected JavaScript date.
                // console.log(selectedDate);
            },
            MaxMonth: 0,
            SelectedMonth:0,
            i18n: {
                months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
            }
        });


    $('.nav li').on('click', function(){
        $(this).addClass('active').siblings().removeClass('active');
    });

    $('#active-trade-text').css("color", "#2378a1");
    $('#trade-main').css("border-left", "2px solid #90a1a9");
    $('#trade-main').css("background-color", "#f0f3f4");

    $( document ).ready(function() {
        getActiveTradeData();
    });

    function getActiveTradeData() {

        var limit  = 15;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "active_trade_all.php",
                data: {
                    limit  : limit
                },
                success: function (data) {
                    $("#activeTradeAjaxdata").html("");
                    $("#activeTradeAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
    }


    function getNewActiveTradeData(pageNo,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "active_trade_all.php",
            data: {
                limit         : limit,
                page          : pageNo
            },
            success: function (data) {
                $("#activeTradeAjaxdata").html("");
                $("#activeTradeAjaxdata").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

    function getBidRecieveData() {

        var limit  = 15;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "bid_recieve.php",
                data: {
                    limit  : limit
                },
                success: function (data) {
                    $("#activeTradeAjaxdata").html("");
                    $("#activeTradeAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
    }


    function getNewBidRecieveData(pageNo,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "bid_recieve.php",
            data: {
                limit         : limit,
                page          : pageNo
            },
            success: function (data) {
                $("#activeTradeAjaxdata").html("");
                $("#activeTradeAjaxdata").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

    function getBidSentData() {

        var limit  = 15;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "bid_sent.php",
                data: {
                    limit  : limit
                },
                success: function (data) {
                    $("#activeTradeAjaxdata").html("");
                    $("#activeTradeAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
    }


    function getNewBidSentData(pageNo,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "bid_sent.php",
            data: {
                limit         : limit,
                page          : pageNo
            },
            success: function (data) {
                $("#activeTradeAjaxdata").html("");
                $("#activeTradeAjaxdata").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

</script>
