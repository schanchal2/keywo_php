<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once ('../../../models/keywords/tradeModel.php');

$email = $_SESSION["email"];

// Create database connection
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print("Error: Connection Failed");
    exit;
}

$limit = cleanQueryParameter($conn, cleanXss($_POST["limit"]));

if(isset($_POST["page"])){
    $page_number = (int)filter_var($_POST["page"],FILTER_SANITIZE_NUMBER_INT,FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_number)){
        die("Invalid page number");
    } // incase of invalid number
}else{
    $page_number = 1; // if there is no page number, set 1.
}
if($page_number == 0){
    $page_number = 1;
}

$item_per_page = $limit;

$getMyKeyword = getMyKeywordDetails($conn,$email);
if(noError($getMyKeyword)){
    $getMyKeyword = $getMyKeyword["errMsg"];
    $myKeywords = $getMyKeyword["transaction_details"];
    $myKeywords = json_decode($myKeywords, true);
    $myKeywords = array_reverse($myKeywords);
    $getCountdata = count($myKeywords);

    if($getCountdata == 0) {
        ?>
        <div class="bg-white search-query innerMT">
            <div class="half innerAll">
                <!-- row starts here   -->
                <div class="row half innerAll padding-left-none padding-right-none" style="text-align: center">
                    No Bid Receive Found
                </div>
                <!-- row ends here -->
            </div>
        </div>

    <?php exit; }
    else{
        $get_total_rows = $getCountdata;
        $total_pages = ceil($get_total_rows / $item_per_page);

        $page_position = (string)(($page_number - 1) * $item_per_page);
        $lastpage = ceil($total_pages);

        if($get_total_rows == 0){
            $page_number = $page_number - 1; //if there's no page number, set it to 1
            $item_per_page = $limit;
            $get_total_rows = $getCountdata;
            $total_pages = ceil($get_total_rows / $item_per_page);
            //position of record
            $page_position = (($page_number - 1) * $item_per_page);
            $lastpage = ceil($total_pages);
        }

        $keywords    = $myKeywords;
        $count       = count($keywords);
        $limits      = intval($limit);
        $pagePostion = intval($page_position);
        $limits      = intval($pagePostion + ($limits -1));

        $pageUrl = "views/keywords/analytics/keyword_analytics.php";
        for($i=$pagePostion; $i<$count && $i<=$limits;$i++){
            $keyword = $keywords[$i]["keyword"];
            $getKwdOwnershipDetails = getKeywordOwnershipDetails($conn, $keyword);
            if(noError($getKwdOwnershipDetails)){
                $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                $askPrice = $getKwdOwnershipDetails["ask_price"];
                $highestBidAmt = $getKwdOwnershipDetails["highest_bid_amount"];

                if(empty($askPrice)){
                    $askPrice = 0;
                }
                if(isset($highestBidAmt) && !empty($highestBidAmt)){
                    ?>
                    <div class="bg-white search-query innerMT">
                        <div class="half innerAll">
                            <!-- row starts here   -->
                            <div class="row half innerAll padding-left-none padding-right-none">
                                <div class="col-md-2 inner-2x innerMR">
                                    <span class="txt-blue ellipses text-left"><a href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank" class="display-in-block-txt-blk" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $keyword; ?>"><?php echo "#".$keyword; ?></a></span>
                                </div>
                                <?php
                                if(isset($askPrice) && !empty($askPrice)){
                                    // edit ask
                                    ?>
                                    <div class="col-md-2 text-black">
                                        <label class="text-black ellipses margin-bottom-none margin-top-none">
                                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                               origPrice="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$askPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                        </label>
                                    </div>
                                    <div class="col-md-2 text-black padding-left-none padding-right-none half innerML">
                                        <!-- <input value="Edit Ask" type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#keyword-popup-set-ask"> -->
                                        <button class="btn-trading-wid-auto"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                                    </div>

                                    <?php
                                }else{
                                    // set ask
                                    ?>
                                    <div class="col-md-2 text-black">
                                        <label class="text-black ellipses margin-bottom-none margin-top-none">
                                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                               origPrice="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$askPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                        </label>
                                    </div>
                                    <div class="col-md-2 text-black padding-left-none padding-right-none half innerML">
                                        <!-- <input value="Edit Ask" type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#keyword-popup-set-ask"> -->
                                        <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                    </div>

                                <?php } ?>

                                <div class="col-md-2 text-black innerML">
                                    <label class="text-black ellipses margin-bottom-none margin-top-none">
                                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$highestBidAmt}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmt}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmt}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                                <div class="col-md-3 half pull-right">
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <!-- <input value="Accept Bid" type="button" class="btn-trading-wid-auto-dark" data-toggle="modal" data-target="#keyword-popup-set-ask"> -->
                                            <button class="btn-trading-wid-auto-dark" id="acceptKeyword" value="--><?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- row ends here -->
                        </div>
                    </div>
                <?php }
            }else{
                print('Error: Fetching keyword ownership details');
                exit;
            }
        }
    }

}else{
    print('Error: Fetching owned keyword details');
    exit;
}

?>

<div class="row">
    <div class="col-md-5 padding-left-none padding-right-none"></div>
    <div class="col-md-7 padding-left-none half innerR innerT">
        <div class="pagination-cont pull-right gotovalues">


            <?php

            getPaginationData($lastpage, $page_number, $limit);
            function getPaginationData($lastpage, $pageno, $limit)
            {
                echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
                echo '<ul class="pagination">';


                if ($pageno > 1) {

                    $pagenum = 1;
                    print('<li class="status"><a href="#"onclick=getNewBidRecieveData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
                }

                if ($pageno > 1) {
                    $pagenumber = $pageno - 1;
                    print('<li class="status"><a href="#" onclick=getNewBidRecieveData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
                }

                if ($pageno == 1) {
                    $startLoop = 1;
                    $endLoop = ($lastpage < 5) ? $lastpage : 5;
                } else if ($pageno == $lastpage) {
                    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                    $endLoop = $lastpage;
                } else {
                    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
                }

                for ($i = $startLoop; $i <= $endLoop; $i++) {
                    if ($i == $pageno) {
                        print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
                    } else {
                        $pagenumber = $i;
                        print('<li class="status"><a href="#" onclick=getNewBidRecieveData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
                    }
                }
                if ($pageno < $lastpage) {
                    $pagenumber = $pageno + 1;
                    print('<li class="status"><a href="#" onclick=getNewBidRecieveData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

                }

                if ($pageno != $lastpage) {
                    print('<li class="status"><a href="#" onclick=getNewBidRecieveData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
                }


                echo '</ul>';
                echo '</div>';
            }


            ?>

        </div>
    </div>
</div>

<script>
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>
