<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/deviceHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/walletHelper.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');

$email = $_SESSION["email"];
$userId = $_SESSION["id"];

include("../../layout/header.php"); ?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<main class="myearnings-main-container inner-7x innerT" id="my-earning">
    <div class="container innerB">
        <div class="col-xs-3 stickyPostion_referral_leftPannel">
            <?php include('my_earning.php'); ?>
        </div>

        <?php
        //left side interaction data
        $shareInteraction = 0;
        $shareEarning     = 0;
        $lastSearchTime   = '';
        $totalCount       = referralShareDetails($userId, 'count', $lastSearchTime);
        if ($totalCount['errCode'] == -1) {
            $shareInteraction = $totalCount['errMsg']['my_share_post_interactions'];
            $shareEarning     = $totalCount['errMsg']['my_share_post_earnings'];
        }
        //referral share data
        $referralData = referralShareDetails($userId, 'data', $lastSearchTime);
        if ($referralData['errCode'] == -1) {
            $searchFrom     = '';
            if (isset($referralData['errMsg']['searchFrom']) && !empty($referralData['errMsg']['searchFrom'])) {
                $searchFrom = $referralData['errMsg']['searchFrom'];
            }
            $referralData = $referralData['errMsg']['result'];
        } ?>
        <!-- col-xs-3 -->
        <div class="col-xs-6">
            <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                <!-- tabs html starts here -->
                <div class="active-trade-unordered padding-none">
<!--                    <ul class="nav nav-tabs pull-right">-->
<!--                        <li class="active"><a href="#lowest" class="l-h10" data-toggle="tab">Lowest</a></li>-->
<!--                        <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>-->
<!--                    </ul>-->
                    <div id="myTabContent" class="tab-content pull-right width-100-percent innerMT">
                        <div id="load-share-next-post-data" style="display:none;" data-count="0" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
                        <!--load Content hear-->
                        <div id="load-share-post-data">

                        </div>
                        <div id = "ajaxLoader1" class="text-center" style="display:none; ">
                            <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
                        </div>
                    </div>
                    <!-- foreach closing -->
                </div>
                <!-- tabs html ends here -->
            </div>
        </div>
        <!-- col-xs-6 -->
        <div class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_referral_rightPannel">
            <!-- Dropdown -->
            <div class="card right-panel-modules">
                <div class="bg-light-gray right-panel-modules-head">
                    <div class="row margin-none">
                        <div class="col-xs-12">
                            <h4 class="margin-none half innerTB">Interaction</h4>
                        </div>
                    </div>
                </div>
                <div class="earnings-right-module">
                    <div class="margin-none border-all">
                        <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                            <div class="col-xs-6 text-left">
                                <span>Interaction</span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $shareInteraction; ?>"><?php echo $shareInteraction; ?></a></label>
                            </div>
                        </div>
                        <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                            <div class="col-xs-6 text-left">
                                <span>Earning</span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$shareEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$shareEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$shareEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Interaction -->
            <div class="card right-panel-modules inner-2x innerMT">
                <?php //include '_my_earning_calender.php'; ?>
                <div class="bg-light-gray right-panel-modules-head">
                    <div class="row margin-none">
                        <div class="col-xs-12 padding-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Referral</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="earnings-right-module">
                    <input type="hidden" id="uploadSelection" name="uploadReport" value="share" />
                    <div id='calendar'></div>
                    <div class="text-blue innerTB text-center download-btn">
                        <span class="download-icon">
                        <i class="fa fa-download text-white"></i>
                    </span>
                        <form id="downloadUploadData" method="post" action="downloadUploadReport.php">
                            <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadUploadReport" type="button" value="Download Report"/>

                            <div id="uploadDynamicParameter">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- Calendar -->
        </div>
        <!-- col-xs-3 -->
    </div>
    <!-- container -->
</main>
<?php include("../../layout/transparent_footer.php"); ?>
<script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>

<script type="text/javascript">
    var d       = new Date();
    var n       = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }

    $("#calendar").MonthPicker({
        // SelectedMonth: n+'/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        MaxMonth: 0,
        SelectedMonth:0,
        i18n: {
            months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        }
    });

    $('#referral-share-text').css("color", "#2378a1");
    $('#referral-share').css("border-left", "2px solid #90a1a9");
    $('#referral-share').css("background-color", "#f0f3f4");

    $(document).ready(function() {
        loadSharePage();
    });

    var bottom = $(document).height() - $(window).height(); //for endless scrolling
    var successflag = 0;
    $(document).scroll(function(){
        /*
         For endless scrolling
         */
        var win = $(window);
        // Each time the user scrolls
        win.scroll(function() {
            // End of the document reached?
            if ($(document).height() - win.height() == win.scrollTop()) {
                // Do the stuff
                if(successflag == 0){
                    var privateScrollAllow = $("#load-share-next-post-data").attr("data-scroll-allow");
                    var privateDataEmpty = $("#load-share-next-post-data").attr("data-status-empty");

                    if (privateScrollAllow == "true" && privateDataEmpty == "false") {
                        $('#ajaxLoader1').show();
                        loadSharePage();
                    }
                    successflag = 1;
                }
            }else{
                // console.log("else");
                successflag = 0;
            }
        });
    });

    // JS Function to get all Post from api to Show on private timeline.
    function loadSharePage() {
        var dataCount = $("#load-share-next-post-data").attr("data-count");
        var lastDataCreateTime = $("#load-share-next-post-data").attr("data-create-time");
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime }),
            url: '../../keywords/user_dashboard/loadSharePost.php',
            success: function(postdata) {
                $('#show-error-message').hide();
                if (lastDataCreateTime == "") {
                    $('#load-share-post-data').html(postdata);
                } else {
                    $('#load-share-post-data').append(postdata);
                }
                $("#load-share-next-post-data").attr("data-create-time", $("#load-share-last-data-time-" + dataCount).attr('lastDataTime'));
                $("#load-share-next-post-data").attr("data-status-empty", $("#load-share-last-data-time-" + dataCount).attr('data-empty'));
                $("#load-share-next-post-data").attr("data-count", parseInt(dataCount) + 1);
            },
            beforeSend: function() {
                $("#load-share-next-post-data").attr("data-scroll-allow", "false");
            },
            complete: function() {
                $('#ajaxLoader1').hide();
                $("#load-share-next-post-data").attr("data-scroll-allow", "true");
            },
            error: function() {
                // alert('error on Loading Timeline');
                $('#loading-content-div').hide();
                $('#show-error-message').text('No Post Yet!!');
            }
        });
    }
</script>