<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/deviceHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/walletHelper.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');
//check for session

$email  = $_SESSION["email"];
$userId = $_SESSION["id"];

$dataCount          = cleanXSS(rawurldecode($_POST['dataCount']));
$lastDataCreateTime = cleanXSS(rawurldecode($_POST['lastDataCreateTime']));
$dataEmptyFlag      = "false";
//printArr($_POST);

//left side interaction data
$shareInteraction = 0;
$shareEarning     = 0;

$totalCount       = referralShareDetails($userId, 'count', $lastDataCreateTime);
if ($totalCount['errCode'] == -1) {
    $shareInteraction = $totalCount['errMsg']['my_share_post_interactions'];
    $shareEarning     = $totalCount['errMsg']['my_share_post_earnings'];
}
//referral share data
$referralData = referralShareDetails($userId, 'data', $lastDataCreateTime);

if ($referralData['errCode'] == -1) {
    $referralDetail = $referralData['errMsg']['result'];
}

foreach($referralDetail as $data) {
$postType     = $data['post_details']['post_type'];
$postDesc     = $data['post_short_desc'];
$likeCount    = $data['like_count'];
$viewCount    = $data['post_views_count'];
$commentCount = $data['comment_count'];
$postId       = $data['_id'];
$createdAt    = $data['created_at'];
$earning      = $data['post_earnings'];
?>

<div class="tab-pane active in" id="lowest">
    <!-- content starts here -->
    <div class="col-xs-12 earnings-container padding-none">
        <div class="">
            <ul class="earnings-list padding-none clearfix" style="list-style:none;">
                <li class="clearfix border-bottom padding-none innerMB border-all">
                    <div class="card col-xs-12 innerAll padding-bottom-none padding-top-none">
                        <div class="col-xs-9 padding-none">
                            <div class="col-xs-12 padding-none half innerMT">
                                <div class="padding-left-none">
                                    <div class="innerMR pull-left">
                                        <div class="image-circle-container">
                                            <?php switch ($postType) {
                                                case 'blog': ?>
                                                    <i class="fa fa-circle text-light-grey"></i>
                                                    <i class="fa fa-file-text text-white"></i>
                                                    <?php break; case 'video': ?>
                                                    <i class="fa fa-circle text-red"></i>
                                                    <i class="fa fa-play-circle text-white"></i>
                                                    <?php break; case 'image': ?>
                                                    <i class="fa fa-circle text-sky-blue"></i>
                                                    <i class="fa fa-image text-white"></i>
                                                    <?php break; case 'audio': ?>
                                                    <i class="fa fa-circle text-orange"></i>
                                                    <i class="fa fa-soundcloud text-white"></i>
                                                    <?php break; case 'status': ?>
                                                    <i class="fa fa-circle text-light-grey"></i>
                                                    <i class="fa fa-edit text-white"></i>
                                                    <!--<img src="--><?php //echo $rootUrlImages?><!--/apps_icon.png">-->
                                                <?php break; } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="innerMT padding-none earnings-text">
                                    <span>
                                        <?php
                                        if (isset($data["post_mention"])) {
                                            $postMention = $data["post_mention"];
                                        } else {
                                            $postMention = "";
                                        }
                                        $shortDescriptionText = getLinksOnText(rawurldecode($postDesc), $postMention, $_SESSION["account_handle"]);
                                        $shortDescription = $shortDescriptionText["text"];
                                        $descLenWithoutStriptag = strlen($shortDescription);
                                        $descLenWithStriptag = strlen(strip_tags($shortDescription));
                                        $diff = $descLenWithoutStriptag - $descLenWithStriptag;
                                        $shortDescData = ($descLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $diff) . "<a href='javascript:;'><span class='text-blue'>....</span></a>" : $shortDescription;
                                        echo $shortDescData;
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 half innerMT padding-none clearfix">
                                <div class="pull-left">
                                    <span><i class="fa fa-thumbs-o-up text-blue"></i></span>
                                    <span class="half innerMLR"><?php echo $likeCount; ?></span>
                                    <!-- Like -->
                                    <span><i class="fa fa-comments-o text-blue"></i></span>
                                    <span class="half innerMLR"><?php echo $commentCount;?></span>
                                    <!-- comments -->
                                    <span><i class="fa fa-eye text-blue"></i></span>
                                    <span class="half innerMLR"><?php echo $viewCount; ?></span>
                                    <!-- view -->
                                </div>
                                <div class="pull-left inner-2x innerML">
                                    <span class="text-blue">Earnings : </span>
                                    <div class="pull-left innerMT-3px"></div>
                                    <label class="text-black ellipses pull-right text-left max-width-80">
                                        <span class="display-in-block-txt-blk pull-right"
                                              onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                              origPrice="<?php echo number_format("{$earning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>"
                                              title="<?php echo number_format("{$earning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>"
                                              data-toggle="tooltip" data-placement="bottom">
                                            &nbsp;
                                            <?php echo formatNumberToSort("{$earning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                            <input type="button" onClick="parent.open('<?php echo $rootUrl; ?>views/social/socialPostDetails.php?post_id=<?php echo base64_encode($postId); ?>&timestamp=<?php echo $createdAt; ?>&posttype=<?php echo base64_encode($postType); ?>&email=<?php echo base64_encode($_SESSION['email']); ?>')" class="btn-trading-wid-auto innerMB text-center" value="View Post">
                            <!--<input type="button" class="btn-trading-wid-auto-dark text-center" value=" Analytics ">-->
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- content ends here -->
</div>
<?php } //foreach closing
if ($referralData['errCode'] == -1) {
    if (isset($referralData['errMsg']['dataEmpty']) && isset($referralData['errMsg']['searchFrom']) && $referralData['errMsg']['dataEmpty'] == true) {
        echo "<center>" . "You joined us at " . date("jS F Y", ($_SESSION["userJoinAt"] / 1000)) . "</center>";
        $dataEmptyFlag = "true";
    }

    if ((count($referralData['errMsg']['result']) < 10) && isset($referralData['errMsg']['searchFrom']) && !empty($referralData['errMsg']['searchFrom'])) {
        $createdAt = $referralData['errMsg']['searchFrom'];
    }
}
?>
<div id='load-share-last-data-time-<?php echo $dataCount; ?>' lastDataTime = '<?php echo $createdAt; ?>' data-count='<?php echo $dataCount; ?>' data-empty='<?php echo $dataEmptyFlag; ?>' style="display:none;"></div>
