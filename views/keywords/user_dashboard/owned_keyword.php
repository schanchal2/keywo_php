<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once("../../../helpers/sessionHelper.php");

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    $email      = $_SESSION["email"];

    include("../../layout/header.php");

    $arr = array();

    if(isset($_SESSION["accept_bid_error_code"]) && !empty($_SESSION["accept_bid_error_code"])){
        $getErrorCode = $_SESSION["accept_bid_error_code"];

        if($getErrorCode == -1){
            $successMsg = $_SESSION["accept_bid_error_message"];
        }else{
            $failedMsg = $_SESSION["accept_bid_error_message"];
        }

        // unset $_SESSION["get_error_code"]
        unset($_SESSION["accept_bid_error_code"]);
        unset($_SESSION["accept_bid_error_message"]);
    }

    if(isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_code"])){
        $getErrorCode = $_SESSION["get_error_code"];

        if($getErrorCode == -1){
            $successMsg = $_SESSION["get_error_message"];
        }else{
            $failedMsg = $_SESSION["get_error_message"];
        }

        unset($_SESSION["get_error_message"]);
        unset($_SESSION["get_error_message"]);
    }


    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];

        /* FTUE status check */
        $getFtUserDetail = getFirstTimeUserStatus($kwdConn,$email,$_SESSION['id']);
        if(noError($getFtUserDetail)){
            $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
            if($row['ftue_status'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
                print("</script>");
                exit();
            }
        }else{
            print("<script>");
            print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
            print("</script>");
            exit();
        }

    }else{
        print('Error: Database connection');
        exit;
    }

    $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
    if(noError($getMyKeywordDetails)) {

        $getMyKeywordDetails = $getMyKeywordDetails["errMsg"]; 
        $myKeywords = $getMyKeywordDetails["transaction_details"];
        $myKeywords = json_decode($myKeywords, true);
        $myKeywords = array_reverse($myKeywords);
        $kwdCount = count($myKeywords);
    }


    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
<div id="cover"></div>
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3 stickyPostion_ownKeyword_leftPannel">
                <?php include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->

            <div class="col-xs-6" >
                <!-- social-all-status-tabs -->
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common" >

                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none" id="earningAjaxdata">
<!--                        <ul class="nav nav-tabs pull-right">-->
<!---->
<!--                            <li class="active"><a href="#lowest" data-toggle="tab">Lowest</a></li>-->
<!--                            <li class="padding-right-none"><a href="#highest" data-toggle="tab">Highest</a></li>-->
<!--                        </ul>-->
                        <div id="myTabContent" class="tab-content pull-right width-100-percent inner-2x innerMT">
                            <div class="tab-pane active in"  >

                        </div>
                    </div>
                    <!-- tabs html ends here -->


                </div>

            </div>
            </div>
            <!-- col-xs-6 -->

            <!-- col-md-3 starts here -->

            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_ownKeyword_rightPannel">

                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">

                                <h4 class="margin-none half innerTB">Keyword Owned</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">

                            <div class="social-user-setting-name half innerTB clearfix border-all padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Total Keyword</span>
                                </div>
                                <div class="col-xs-6 text-right">

                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $kwdCount; ?>"><?php echo $kwdCount; ?></a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix  border-top-none border-all padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                            <?php
                            $totalInteraction = 0;
                            $totalEarning = 0;
                            foreach($myKeywords as $myKeyword)
                            {
                                $keyword = $myKeyword["keyword"];
                                $getRevenueDetails = getRevenueDetailsByKeyword($kwdDbConn,$keyword); 
                                if(noError($getRevenueDetails))
                                {
                                    $errMsg = $getRevenueDetails["data"][0]; 
                                    $interactionCount = $errMsg["app_kwd_search_count"]; 
                                    $interactionDecode = json_decode($interactionCount, true); 
                                    $interaction[] = $interactionDecode["total"]; 
                                    $totalInteraction = array_sum($interaction);
                                    $earning[]        = $errMsg["user_kwd_ownership_earnings"];
                                    $totalEarning     = array_sum($earning); 
                                }
                            }

                            ?>
                                <div class="col-xs-6 text-right">

                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $totalInteraction; ?>"><?php echo $totalInteraction; ?></a></label>
                                </div>                        
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none border-all padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earning</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$totalEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                     </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <?php //include '_my_earning_calender.php'; ?>

                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword Earning</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <input type="hidden" id="transType" name="transactionType" value="keyword_purchase" />
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
						<span class="download-icon">
							<i class="fa fa-download text-white"></i>
						</span>
                            <form id="downloadWalletData" method="post" action="downloadreport.php">
                                <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadReport" type="button" value="Download Report"/>

                                <div id="walletDynamicParameter">

                                </div>

                            </form>
                        </div>
                    </div>
              
                </div>

                <!-- Calendar -->

            </div>
            <!-- col-md-3 ends here -->

        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>
<?php
include("../../layout/transparent_footer.php");
?>
<script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>
<script type="text/javascript">

    var getErrorCode = '<?php echo $getErrorCode; ?>';
    var successMsg = '<?php echo $successMsg; ?>';
    var errorMsg = '<?php echo $failedMsg; ?>';

    var rootURL = '<?php echo $rootUrl; ?>';

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }


    $("#calendar").MonthPicker({
        SelectedMonth: n+'/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        MaxMonth: 0,
        SelectedMonth:0,
        i18n: {
            months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        }
    });


    $('#owned-text').css("color", "#2378a1");
    $('#my-owned').css("border-left", "2px solid #90a1a9");
    $('#my-owned').css("background-color", "#f0f3f4");

    if(getErrorCode != ''){
        if(getErrorCode == -1){
            if(successMsg != ''){
                showToast("success", successMsg);
            }
        }else{
            if(errorMsg != ''){
                showToast("failed", errorMsg);
            }
        }
    }

    $( document ).ready(function() {
        getEarningData();
    });


    function getEarningData() {

        var limit  = 15;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controllers/keywords/myEarningController.php",
                data: {
                    limit  : limit
                },
                success: function (data) {
                    $("#earningAjaxdata").html("");
                    $("#earningAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
    }


    function getNewEarningData(pageNo,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/myEarningController.php",
            data: {
                limit         : limit,
                page          : pageNo
            },
            success: function (data) {
                $("#earningAjaxdata").html("");
                $("#earningAjaxdata").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

</script>

<script src="<?php echo $rootUrl; ?>js/renew_keyword.js"></script>
