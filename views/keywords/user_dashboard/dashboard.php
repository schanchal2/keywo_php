<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once("../../../helpers/sessionHelper.php");
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');



$getPostAnanytics = getPostAnanyticsValue($_SESSION['id']);
//printArr($getPostAnanytics);
if(noError($getPostAnanytics)){
    $getPostTypeData        = $getPostAnanytics['errMsg']['post_summery'];
}

if(isset($getPostTypeData['blog']['post_earnings'])){
    $blogEarning = $getPostTypeData['blog']['post_earnings'];
}else{
    $blogEarning = 0;
}

if(isset($getPostTypeData['video']['post_earnings'])){
    $videoEarning = $getPostTypeData['video']['post_earnings'];
}else{
    $videoEarning = 0;
}

if(isset($getPostTypeData['image']['post_earnings'])){
    $imageEarning = $getPostTypeData['image']['post_earnings'];
}else{
    $imageEarning = 0;
}

if(isset($getPostTypeData['audio']['post_earnings'])){
    $audioEarning = $getPostTypeData['audio']['post_earnings'];
}else{
    $audioEarning = 0;
}

if(isset($getPostTypeData['status']['post_earnings'])){
    $statusEarning = $getPostTypeData['status']['post_earnings'];
}else{
    $statusEarning = 0;
}
$totalUploadInteraction = $blogEarning + $videoEarning + $imageEarning + $audioEarning + $statusEarning;
$emailId                = $_SESSION['email'];
$getLikeCount           = getPostLikeIds($_SESSION['account_handle'], 'sort', 'all');


$postFields = "post_interactions";
$userDetail = getUserInfo($emailId, $walletURLIPnotification . 'api/notify/v2/', $postFields);
if (noError($userDetail)) {
    $totalConsumptionInteraction = isset($userDetail['errMsg']['post_interactions']) ? $userDetail['errMsg']['post_interactions'] : 0;
//    $totalConsumptionInteraction = 1000;
}

$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$blockUserArray          = getBlockedUser($targetDirAccountHandler);

$likePostAllData         = getActivityBookMarkLikePosts(json_encode($getLikeCount), json_encode($blockUserArray));
if (noError($likePostAllData)) {
    $likePostAllData = $likePostAllData['errMsg'];
}

$totalVideo  = 0;
$totalAudio  = 0;
$totalImage  = 0;
$totalStatus = 0;
$totalBlog   = 0;

foreach ($likePostAllData as $date => $likeData) {
    foreach ($likeData as $key => $value) {
        $postType = $value['post_type'];
        if ($value['post_type'] == 'share') {
            $postType = $value['post_details']['parent_post']['post_id']['post_type'];
        }

        switch ($postType) {
            case 'video':
                $totalVideo = $totalVideo + $value['current_payout'];
                break;
            case 'audio':
                $totalAudio = $totalAudio + $value['current_payout'];
                break;
            case 'image':
                $totalImage = $totalImage + $value['current_payout'];
                break;
            case 'status':
                $totalStatus = $totalStatus + $value['current_payout'];
                break;
            case 'blog':
                $totalBlog = $totalBlog + $value['current_payout'];
                break;

        }
    }
}

$totalConsumptionEarn = $totalVideo + $totalAudio + $totalImage + $totalStatus + $totalBlog;
$totalUploadEarn = $blogEarning + $audioEarning + $imageEarning + $statusEarning + $videoEarning;

//month for x-axis plotting
$months = array();
for ($i = 0; $i < 6; $i++) {
    $months[] .= date('M', strtotime("-$i month"));
}
$monthsXAxis = json_encode(array_reverse($months)); //printArr($monthsXAxis);
//data for yaxis
$getGraphConsumption = graphConsumptionData($monthsXAxis);
//printArr($getGraphConsumption);
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../../layout/header.php");

    $email      = $_SESSION["email"];

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <style>
        .highcharts-contextbutton {
            display: none!important;
        }

        .canvasjs-chart-credit {
            display: none;
        }
    </style>
    <!-- container starts here -->
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container inner-2x innerB">
            <div class="col-xs-3 stickyPostion_dashbord-leftPannel">
                <?php include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <h5 class="text-blue keyword-name margin-top-none">My Earning</h5>
                <!-- row starts here -->
                <div id="pieChart">

                    <?php include "myEarningPieChart.php"; ?>

                </div>
                <!-- row ends here -->
                <!-- row starts here -->
                <div class="row inner-2x innerMT">
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-left-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll">
                            <span class="keyword-grey-span innerL half f-sz16">Content Consumption</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none l-h16">
                            <div class="comn-lft-rght-cont">
                                <label>Blog</label>
                                <label class="pull-right ellipses-market-trade-span text-right">
                                    <div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                         origPrice="<?php echo number_format("{$totalBlog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalBlog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalBlog}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Video</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                     origPrice="<?php echo number_format("{$totalVideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalVideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalVideo}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Image</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                     origPrice="<?php echo number_format("{$totalImage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalImage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalImage}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Audio</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalAudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalAudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalAudio}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Status</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                     origPrice="<?php echo number_format("{$totalStatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalStatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalStatus}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="bg-white border-all innerAll padding-top-none border-top-none">
                            <canvas id="canvas" height="150"></canvas>

                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-right-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll">
                            <span class="keyword-grey-span innerL half f-sz16">Content Upload</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none l-h16">
                            <div class="comn-lft-rght-cont">
                                <label>Blog</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$blogEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$blogEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$blogEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>

                            <div class="comn-lft-rght-cont">
                                <label>Video</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$videoEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$videoEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$videoEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>

                            <div class="comn-lft-rght-cont">
                                <label>Image</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$imageEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$imageEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$imageEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>

                            <div class="comn-lft-rght-cont">
                                <label>Audio</label>
                                <label class="pull-right ellipses-market-trade-span text-right">
                                    <div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$audioEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$audioEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$audioEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div></label>
                                <div class="clearfix"></div>
                            </div>

                            <div class="comn-lft-rght-cont">
                                <label>Status</label>
                                <label class="pull-right ellipses-market-trade-span text-right">
                                    <div class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                         origPrice="<?php echo number_format("{$statusEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$statusEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$statusEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></div>

                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="bg-white border-all innerAll padding-top-none border-top-none">
                            <canvas id="canvas2" height="150"></canvas>
                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                </div>
                <!-- row ends here -->
                <!-- row starts here -->
                <div id="keywordChart">

                    <?php include "userKeywordDashboard.php"; ?>
                </div>
            <!-- col-xs-6 -->
        </div>
        <!-- container -->

    </main>
    <?php
} else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>
<!-- graphs starts here -->
<script src="<?php echo $rootUrl ;?>frontend_libraries/graph/highcharts_myearnings.js"></script>
<script src="<?php echo $rootUrl ;?>frontend_libraries/graph/exporting.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
<script src="<?php echo $rootUrl ;?>frontend_libraries/graph/canvasjs.min.js"></script>
<?php
include("../../layout/transparent_footer.php");
?>

<!-- line chart starts here -->
<!--  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> -->
<script src="<?php echo $rootUrl ;?>frontend_libraries/graph/Chart.bundle.js"></script>
<script src="<?php echo $rootUrl ;?>frontend_libraries/graph/Chart.js"></script>
<!--<script src="--><?php //echo $rootUrl ;?><!--frontend_libraries/graph/chart_script.js"></script>-->


<!-- line chart ends here -->

<script type="text/javascript">

    var ctx   = document.getElementById("canvas");
    var xAxis = $.parseJSON('<?php echo $monthsXAxis; ?>');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: xAxis,
            datasets: [{
                label: 'Content consumption',   //'# of Votes'
                data: [<?php echo $getGraphConsumption; ?>],
                backgroundColor: "rgba(0,65,97,0.8)",
                borderWidth: 1,
                borderColor:'white'
            }]
        },

        options: {
            legend: {
                labels: {
                    fontColor: "black"
                }
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false
                    },
                    ticks: {
                        fontColor: "black",
                        beginAtZero:true
                    }
                }]
                ,
                xAxes: [{
                    ticks: {
                        fontColor: "black",
                        beginAtZero:true
                    }

                }]

            }
        }
    });


    var ctx = document.getElementById("canvas2");
    var xAxis = $.parseJSON('<?php echo $monthsXAxis; ?>');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: xAxis,
            datasets: [{
                label: 'Content Upload',   //'# of Votes'
                data: [0.6000000000000088,0.2,0.15000000000000,0.18000000000000221,0.15000000000000308,0.9000000000000308],
                backgroundColor: "rgba(0,100,150,0.8)",
                borderWidth: 1,
                borderColor:'white'
            }]
        },
        options: {
            legend: {
                labels: {
                    fontColor: "black"
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "black",
                        beginAtZero:true
                    }
                }]
                ,
                xAxes: [{
                    ticks: {
                        fontColor: "black",
                        beginAtZero:true
                    }

                }]

            }
        }
    });
</script>

