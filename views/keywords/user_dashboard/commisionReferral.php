<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/deviceHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/walletHelper.php');

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    $email = $_SESSION["email"];
    $userId = $_SESSION["id"];

    $date = date('Y-m-d', time());
    $date = explode("-", $date);
    $year = $date[0];
    $month = $date[1];
    $reportForMonth = $month."_".$year."_transaction";
    $transactionType = "affiliate_earnings";

    include("../../layout/header.php");

    $affiliateTrans = array();

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<div id="cover"></div>
    <main class="myearnings-main-container inner-7x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3 stickyPostion_referral_leftPannel">
               <?php include('my_earning.php'); ?>
            </div>

            <?php

            $getTransactionDetails = getTransactionReport($email, $userId, $reportForMonth, $transactionType);

            if(noError($getTransactionDetails)){
                $getTransactionDetails = $getTransactionDetails["errMsg"]["report"];

               foreach($getTransactionDetails as $key => $transData){

                   if($transactionType == $transData["type"] ){

                       $date = $transData["time"]/1000;
                       $date = date('jS F, Y', $date);

                       $affiliateTrans[$date]["date"] = $date;
                       $affiliateTrans[$date]["affiliate_data"][$key]["keyword"] = $transData["meta_details"]["keyword"];
                       $affiliateTrans[$date]["affiliate_data"][$key]["sold"] =  $transData["meta_details"]["gross_kwd_price"];
                       $affiliateTrans[$date]["affiliate_data"][$key]["commission"] = $transData["amount"];
                   }
               }
            }else if($getTransactionDetails["errCode"] != 21){
                print('Error: Fetching transaction details');
                exit;
            }
            ?>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                  <!--   <div class="active-trade-unordered padding-none"> -->
                        <!-- <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#lowest" class="l-h10" data-toggle="tab">Lowest</a></li>
                            <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>
                        </ul> -->

                        <div id="myTabContent" class="tab-content pull-right width-100-percent">
                            <div class="tab-pane active in" id="lowest">
                                <!-- content starts here -->
                                <div class="affiliate">
                                <?php
                                $totalCount = 0;
                                $totalCommission  = 0;

                                if(count($affiliateTrans)){
                                    foreach($affiliateTrans as $key => $transData){
                                        $date = $transData["date"];
                                        ?>
                                        <div class="earnings-day ">
                                            <h4 class="text-blue margin-bottom-none margin-none innerB">
                                                <?php echo $date; ?>
                                            </h4>
                                        </div>
                                        <div class="pagination-content text-center">
                                            <!-- pagination head starts here -->
                                            <div class="pagination-head half innerAll padding-key-market-data">
                                                <div class="row">
                                                    <div class="col-xs-4 text-white">keyword</div>
                                                    <div class="col-xs-4 text-white text-center">Sold Price</div>
                                                    <div class="col-xs-4 text-white text-center">Commission</div>
                                                </div>
                                            </div>
                                            <!-- pagination head ends here -->
                                            <!-- pagination body starts here -->
                                            <ul class="border-all" id="trading_history">
                                                <?php

                                                foreach($transData["affiliate_data"] as $key1 => $val){

                                                    $keyword = $val["keyword"];
                                                    $soldPrice = $val["sold"];
                                                    $commission = $val["commission"];

                                                    ?>
                                                    <!-- li starts here -->
                                                    <li class="half innerAll padding-bottom-none">
                                                        <!-- row starts here   -->
                                                        <div class="row padding-left-none padding-right-none">
                                                            <div class="col-md-4">
                                                                <span class="txt-blue ellipses"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo "#".$keyword; ?>"><?php echo "#".$keyword; ?></a></span>
                                                            </div>
                                                            <div class="col-md-4 text-black">

                                                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                                        &nbsp;
                                                                    <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                       origPrice="<?php echo number_format("{$soldPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$soldPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$soldPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-4  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                                        &nbsp;
                                                                    <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                       origPrice="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$commission}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <!-- row ends here -->
                                                    </li>
                                                    <!-- li ends here -->
                                                    <?php
                                                    $totalCommission = $totalCommission + $commission;
                                                }
                                                ?>
                                            </ul>
                                            <!-- pagination body ends here -->
                                        </div>

                                        <?php
                                        $totalCount = $totalCount + count($transData["affiliate_data"]);
                                    }
                                }else{

                                    ?>
                                    <div class="earnings-day ">
                                        <h4 class="text-blue margin-bottom-none margin-none innerB">
                                            <?php echo date('jS F, Y', time()); ?>
                                        </h4>
                                    </div>
                                    <div class="pagination-content text-center">
                                        <!-- pagination head starts here -->
                                        <div class="pagination-head half innerAll padding-key-market-data">
                                            <div class="row">
                                                <div class="col-xs-4 text-white">keyword</div>
                                                <div class="col-xs-4 text-white text-center">Sold Price</div>
                                                <div class="col-xs-4 text-white text-center">Commission</div>
                                            </div>
                                        </div>
                                    <!-- pagination body starts here -->

                                        <!-- li starts here -->

                                            <!-- row starts here   -->
                                                <div class="bg-white search-query innerMT">
                                                    <div class="half innerAll">
                                                        <!-- row starts here   -->
                                                        <div class="row half innerAll padding-left-none padding-right-none" style="text-align: center">
                                                            No Record Found
                                                        </div>
                                                        <!-- row ends here -->
                                                    </div>
                                                </div>
                                            <!-- row ends here -->

                                        <!-- li ends here -->
                                    </div>
                                    <?php
                                }


                                ?>

                                </div>

                                <!-- content ends here -->
                            </div>
                        </div>
                   <!--  </div> -->
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_referral_rightPannel">

                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Commission</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Keyword</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">&nbsp;<a href="#" class="display-in-block-txt-blk" title="<?php echo $totalCount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalCount}", 0); ?></a> </label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Commission</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">&nbsp;<a href="#" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                origPrice="<?php echo number_format("{$totalCommission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalCommission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalCommission}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <?php //include '_my_earning_calender.php'; ?>

                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <input type="hidden" id="transType" name="transactionType" value="<?php echo $transactionType; ?>" />
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
						<span class="download-icon">
							<i class="fa fa-download text-white"></i>
						</span>
                            <form id="downloadWalletData" method="post" action="downloadreport.php">
                                <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadReport" type="button" value="Download Report"/>

                                <div id="walletDynamicParameter">

                                </div>

                            </form>
                        </div>
                    </div>

                </div>
                <!-- Calendar -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>
<?php
include("../../layout/transparent_footer.php");
?>
<script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>
<script type="text/javascript">
    var rootURL = '<?php echo $rootUrl; ?>';

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }


    $("#calendar").MonthPicker({
        // SelectedMonth: n+'/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        MaxMonth: 0,
        SelectedMonth:0,
        i18n: {
            months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        }
    });

    $('#commission-text').css("color", "#2378a1");
    $('#commission-main').css("border-left", "2px solid #90a1a9");
    $('#commission-main').css("background-color", "#f0f3f4");

</script>
