<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once ('../../../helpers/sessionHelper.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}else{
    print('Error: Database connection');
    exit;
}
$email = $_SESSION["email"];
checkForSession($kwdDbConn);

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    if(isset($_SESSION["accept_bid_error_code"]) && !empty($_SESSION["accept_bid_error_code"])){
        $getErrorCode = $_SESSION["accept_bid_error_code"];

        if($getErrorCode == -1){
            $successMsg = $_SESSION["accept_bid_error_message"];
        }else{
            $failedMsg = $_SESSION["accept_bid_error_message"];
        }

        // unset $_SESSION["get_error_code"]
        unset($_SESSION["accept_bid_error_code"]);
        unset($_SESSION["accept_bid_error_message"]);
    }

    include("../../layout/header.php");
    require_once("../../../views/keywords/marketplace/tradePopupDialogBox.php");

    $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
    if(noError($getMyKeywordDetails)) {

        $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
        $myKeywords = $getMyKeywordDetails["transaction_details"];
        $myKeywords = json_decode($myKeywords, true);
        $myKeywords = array_reverse($myKeywords);
        $kwdCount = count($myKeywords);
    }

    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
    $getLikeCount            = getPostLikeIds($_SESSION['account_handle'], 'sort', 'all');
    $likePostAllData         = getActivityBookMarkLikePosts(json_encode($getLikeCount), json_encode($blockUserArray));
    if (noError($likePostAllData)) {
        $likePostAllData = $likePostAllData['errMsg'];
    }

    $totalVideo  = 0;
    $totalAudio  = 0;
    $totalImage  = 0;
    $totalStatus = 0;
    $totalBlog   = 0;

    $countVideo  = 0;
    $countAudio  = 0;
    $countImage  = 0;
    $countStatus = 0;
    $countBlog   = 0;

    foreach ($likePostAllData as $date => $likeData) {
        foreach ($likeData as $key => $value) {
            //printArr($value);
            $postType = $value['post_type'];
            if ($value['post_type'] == 'share') {
                $postType = $value['post_details']['parent_post']['post_id']['post_type'];
            }

            switch ($postType) {
                case 'video':
                    $totalVideo = $totalVideo + $value['current_payout'];
                    $countVideo = $countVideo + 1;
                    break;
                case 'audio':
                    $totalAudio = $totalAudio + $value['current_payout'];
                    $countAudio = $countAudio + 1;
                    break;
                case 'image':
                    $totalImage = $totalImage + $value['current_payout'];
                    $countImage = $countImage + 1;
                    break;
                case 'status':
                    $totalStatus = $totalStatus + $value['current_payout'];
                    $countStatus = $countStatus + 1;
                    break;
                case 'blog':
                    $totalBlog = $totalBlog + $value['current_payout'];
                    $countBlog = $countBlog + 1;
                    break;

            }
        }
    }

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

    <!-- container starts here -->
    <main class="myearnings-main-container inner-7x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3 stickyPostion_myEarning-leftPannel">
                <?php include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->

            <div class="col-xs-6">
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
<!--                        <ul class="nav nav-tabs pull-right">-->
<!--                            <li class="active"><a href="#lowest" class="l-h10" data-toggle="tab">Lowest</a></li>-->
<!--                            <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>-->
<!--                        </ul>-->
                        <div id="myTabContent" class="tab-content pull-right width-100-percent inner-1x innerMT">
                            <div id="load-consumption-next-post-data" style="display:none;" data-count="0" data-type ="" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
                            <!--load Content here-->
                            <div id="load-consumption-post-data">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-6 -->
            <!-- col-md-3 starts here -->
            <d iv class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_myEarning-rightPannel">
                <!-- Drop-down -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Blog</h4>
                            </div>
                        </div>
                    </div>

                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $countBlog; ?>">
                                            <?php echo $countBlog; ?>
                                        </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalBlog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalBlog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalBlog}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Blog -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Video</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $countVideo;?>">
                                            <?php echo $countVideo; ?>
                                        </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalVideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalVideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalVideo}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Video -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Image</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $countImage;?>">
                                            <?php echo $countImage; ?>
                                        </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalImage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalImage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalImage}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Image -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Audio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $countAudio;?>">
                                            <?php echo $countAudio; ?>
                                        </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalAudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalAudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalAudio}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Audio -->

                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Status</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $countStatus;?>">
                                            <?php echo $countStatus; ?>
                                        </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">     <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalStatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalStatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalStatus}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Search -->
            <div class="card right-panel-modules inner-2x innerMT">
                <?php //include '_my_earning_calender.php'; ?>
                <div class="bg-light-gray right-panel-modules-head">
                    <div class="row margin-none">
                        <div class="col-xs-12 padding-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Content Consumption</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="earnings-right-module">
                    <input type="hidden" id="likeSelection" name="LikeReport" value="all">
                    <div id='calendar'></div>
                    <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
                            <i class="fa fa-download text-white"></i>
                        </span>
                        <form id="downloadLikeData" method="post" action="downloadLikeReport.php">
                            <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadLikeReport" type="button" value="Download Report"/>

                            <div id="likeDynamicParameter">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- Calendar -->
        </div>
            <!-- col-md-3 ends here -->
        </div>
        <!-- container -->
    </main>
<?php } else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>

<?php include('../../layout/social_footer.php'); ?>

<div>

</div>
<script src="../../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>
<script type="text/javascript">

    var rootURL = '<?php echo $rootUrl; ?>';

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }


    $("#calendar").MonthPicker({
        SelectedMonth: n+'/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        MaxMonth: 0,
        SelectedMonth:0,
        i18n: {
            months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
        }
    });
</script>


<script>
    var bottom = $(document).height() - $(window).height(); //for endless scrolling
    var successflag = 0;
    $(document).scroll(function(){
        /*
         For endless scrolling
         */
        var win = $(window);
        // Each time the user scrolls
        win.scroll(function() {
            // End of the document reached?
            if ($(document).height() - win.height() == win.scrollTop()) {
                // Do the stuff
                if(successflag == 0){
                    var privateScrollAllow = $("#load-consumption-next-post-data").attr("data-scroll-allow");
                    var privateDataEmpty = $("#load-consumption-next-post-data").attr("data-status-empty");
                    var privateDataType = $("#load-consumption-next-post-data").attr("data-type");
                    if (privateScrollAllow == "true" && privateDataEmpty == "false") {
                        $('#ajaxLoader1').show();
                        loadConsumptionPage(privateDataType);
                    }
                    successflag = 1;
                }
            }else{
                // console.log("else");
                successflag = 0;
            }
        });
    });
</script>



<script src="<?php echo $rootUrl; ?>js/renew_keyword.js"></script>
