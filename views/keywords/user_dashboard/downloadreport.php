<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/deviceHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/walletHelper.php');

$email = $_SESSION["email"];
$userId = $_SESSION["id"];

// get request parameters
$reportForMonth = trim(urldecode($_POST["reportformonth"]));
$transactionType = trim(urldecode($_POST["type"]));
$reportName = trim(urldecode($_POST["reportName"]));
$returnArr = array();
$xls = array();

printArr($_POST);

header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=" . $reportName. ".xls");
header("Pragma: no-cache");
header("Expires: 0");

$getTransactionDetails = getTransactionReport($email, $userId, $reportForMonth, $transactionType);


if(noError($getTransactionDetails)){
    $getTransactionDetails = $getTransactionDetails["errMsg"]["report"];

    ?>

    <table>
        <tr>
            <th>Id</th>
            <th>IP Address</th>
            <th>Transaction Type</th>
            <th>Transaction Satus</th>
            <th>Amount</th>
            <th>Receiver User Id</th>
            <th>Receiver</th>
            <th>Sender User Id</th>
            <th>sender</th>
            <th>Date Time</th>
            <th>BTC Rate</th>
            <th>SGD Rate</th>
            <th>USD Rate</th>
            <th>Keyword</th>
            <th>Gross Keyword Price</th>
            <th>Discount</th>
            <th>Discount Percentage</th>
            <th>Discount Reciepent</th>
            <th>Affiliate User</th>
            <th>Affiliate Percentage</th>
            <th>Commision</th>
            <th>Buyer Id</th>
            <th>Raise On</th>
            <th>Payment Mode</th>
        </tr>
        <tbody>

    <?php
    $condition = '';
    foreach($getTransactionDetails as $key => $transReport){


        if($transactionType == "keyword_purchase"){
            $condition = "({$email} == {$transReport['sender']})";
        }else if($transactionType == "affiliate_earnings"){
            $condition = "({$email} == {$transReport['receiver']})";;
        }

        if(count($getTransactionDetails) > 0){
            if(($transReport["type"] == $transactionType) && $condition){

                ?>
                <tr>
                    <td> <?php echo $transReport["_id"]; ?></td>
                    <td> <?php echo $transReport["origin_ip"]; ?></td>
                    <td> <?php echo $transReport["type"]; ?></td>
                    <td> <?php echo $transReport["transaction_status"]; ?></td>
                    <td> <?php echo $transReport["amount"]; ?></td>
                    <td> <?php echo $transReport["receiver_user_id"]; ?></td>
                    <td> <?php echo $transReport["receiver"]; ?></td>
                    <td> <?php echo $transReport["sender_user_id"]; ?></td>
                    <td> <?php echo $transReport["sender"]; ?></td>
                    <td> <?php echo date('Y-m-d H:i:s', $transReport["time"]/1000); ?></td>

                    <?php
                    $currencyList = $transReport["currency_list"];
                    foreach($currencyList as $keyRate => $rate){
                        ?>
                        <td> <?php echo $rate; ?></td>
                        <?php
                    }

                    $metaDetails = $transReport["meta_details"];
                    foreach($metaDetails as $keyMeta => $metaData){
                        ?>
                        <td> <?php echo utf8_decode(urldecode($metaData)); ?></td>
                        <?php
                    }
                    ?>
                </tr>

                <?php
            }
        }
    }
?>
        </tbody>
    </table>

  <?php
}else{
   print('No Report Available');
}

?>