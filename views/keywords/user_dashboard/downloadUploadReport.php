<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');

$email = $_SESSION["email"];

// get request parameters
$type           = trim(strtolower(urldecode($_POST["type"])));
$reportName     = trim(urldecode($_POST["reportName"]));
$file           = trim(urldecode($_POST["file"]));
$returnArr      = array();
$xls            = array();
$data           = array();

if (basename($file, ".php") == "content_upload") {
    //$type = $type;
} elseif (basename($file, ".php") == "referral_share") {
    $type = "share";
}
$time            = date("m_Y",strtotime($reportName));
$likePostAllData = getContentUpload($_SESSION["id"], $type, $time);
//printArr($likePostAllData);
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=" . $reportName. ".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table>
    <tr>
        <th>Post Type</th>
        <th>Post Description</th>
        <th>Post Created At</th>
        <th>Credit</th>
    </tr>
    <tbody>
    <?php
    if (!empty($likePostAllData["errMsg"])) {
        foreach($likePostAllData["errMsg"] as $key => $value) {
            $createdAt = date("d-m-Y H:i:s", ($value["created_at"] / 1000)); ?>
            <tr>
                <td> <?php echo $value["post_type"]; ?></td>
                <td> <?php echo $value["post_short_desc"]; ?></td>
                <td> <?php echo $createdAt; ?></td>
                <td> <?php echo $value["post_earnings"]; ?></td>
            </tr>
    <?php } } ?>
    </tbody>
</table>