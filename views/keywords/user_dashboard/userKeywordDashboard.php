
<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/header/headerModel.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');
require_once("../../../helpers/sessionHelper.php");

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn))
{
    $kwdDbConn = $kwdDbConn["connection"];
}
else
{
    print('Error: Database connection');
    exit;
}

$email = $_SESSION["email"];

$userAvailableBalance = $userRequiredFields . ",social_content_sharer_earnings";
$getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);
if(noError($getUserDetails))
{
    $errMsg     = $getUserDetails["errMsg"];
    $postSharer = $errMsg["social_content_sharer_earnings"];
}

$yaxisData = array();
// This function is used for owned keyword.
$getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
if(noError($getMyKeywordDetails))
{
    $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
    $myKeywords = $getMyKeywordDetails["transaction_details"];
    $myKeyword  = json_decode($myKeywords, true);
    $myKeywords = array_reverse($myKeyword);
    $kwdCount = count($myKeywords);
}

// This function is used for Trading History.
$tradingJson = $rootUrl."json_directory/keywords/trading_history/";
$tradingJson = $tradingJson.$email.".json";
$tradingData = file_get_contents($tradingJson);
$data      = json_decode($tradingData, TRUE);
$dataCount = count($data);

$xaxisArr = array();
$monthYear = array();
for ($i=0; $i<6; $i++) {
    $xaxis = date('M', strtotime("-$i month"));

    $monthYearCount = date('Y-M', strtotime("-$i month"));

    $testing = array_push($xaxisArr, $xaxis);

    $testing = array_push($monthYear, $monthYearCount);
}

$newxAxis = json_encode($xaxisArr);
$NewxAxis = json_encode($monthYear);

$axis = json_decode($NewxAxis);
$monthYear1 = json_decode($NewxAxis);

foreach ($monthYear1 as $getaxis)
{
    $axis1[$getaxis] = 0;
    $axis2[$getaxis] = 0;
}

foreach($myKeyword as $getmyKeyword)
{
    $timeStamp    = $getmyKeyword["purchase_timestamp"];
    $kwd_price    = $getmyKeyword["kwd_price"];
    $kwd_price1[] = $getmyKeyword["kwd_price"];
    $kwdPrice     = array_sum($kwd_price1);

    $abc1 = date('Y-M',strtotime($timeStamp));

        if (stripos($NewxAxis, $abc1) !== false) {
        $axis2[$abc1] = $axis2[$abc1] + $kwd_price;
    }
}

foreach($data as $getData) {

    $kwdlasttradedprice = $getData["kwd_last_traded_price"];
    $kwdlasttradedprice1[] = $getData["kwd_last_traded_price"];
    $kwdPurchaseTime = $getData["kwd_purchase_timestamp"];
    $kwd_last_traded_price = array_sum($kwdlasttradedprice1);
    $year = date('Y', strtotime($kwdPurchaseTime));
    $abc = date('Y-M', strtotime($kwdPurchaseTime));

            if (stripos($NewxAxis, $abc) !== false) {

                $axis1[$abc] = $axis1[$abc] + $kwdlasttradedprice;
            }
}

$ownedAxis = json_encode($axis2);
$tradingAxis = json_encode($axis1);

$ownedDecode = json_decode($ownedAxis,true);
$tradingDecode = json_decode($tradingAxis,true);

$addtionOf2Arrays = array($ownedDecode,$tradingDecode);

// Addition of owned keyword and trading keyword section with same key.
$sumArray = array();
$sumOfArray = array();

foreach ($addtionOf2Arrays as $k=>$subArray) {

    foreach ($subArray as $id=>$value) {
        $sumArray[$id]+=$value;
    }
}
foreach($sumArray as $getValue)
{
    $keywordAxis = array_push($sumOfArray,$getValue);
}
$keywordAxis = json_encode($sumOfArray);

$id = $_SESSION["id"];
$month = 6;

$referralMonthly = getReferralEarninMonthly($email,$id,$month);
$errCode = $referralMonthly["errCode"];


if($errCode == 1)
{
    $referredUserCount = array(0,0,0,0,0,0);
}
else
{
    $referredUserCount = array();
    $referredUserCount = $referralMonthly["errMsg"]["data"];
}

$monthlyReferredUser = json_encode($referredUserCount);

$referralInteraction = getReferralInteraction($email,$NotificationURL);
if(noError($referralInteraction))
{
    $errMsg = $referralInteraction["errMsg"];
    $referralInteractions = $errMsg["referral_interactions"];
}

$userAvailableBalance = $userRequiredFields . ",affiliate_earning";
$getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);
if(noError($getUserDetails))
{
    $errMsgs = $getUserDetails["errMsg"];
    $commission = $errMsgs["affiliate_earning"];
}

//affiliate_earning

?>

<div class="row inner-2x innerMT">
    <!-- col-md-6 starts here -->
    <div class="col-md-6 padding-left-none">
        <!-- title starts here -->
        <div class="bg-white border-all half innerAll">
            <span class="keyword-grey-span innerL half f-sz16 l-h16">Keyword</span>
        </div>
        <!-- title ends here -->
        <!-- list starts here -->
        <div class="bg-white border-all innerAll border-top-none">
            <div class="comn-lft-rght-cont">
                <label>Owned</label>
                <?php if(!empty($kwdPrice)){ ?>
                    <label class="pull-right ellipses-market-trade-span text-right" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("{$kwdPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$kwdPrice}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$kwdPrice}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                <?php } else { ?>
                    <label class="pull-right ellipses-market-trade-span text-right" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("0",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("0",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <div class="comn-lft-rght-cont">
                <label></label>
                <div class="clearfix"></div>
            </div>
            <div class="comn-lft-rght-cont">
                <label>Trading History</label>
                <label class="pull-right ellipses-market-trade-span text-right" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                       origPrice="<?php echo number_format("{$kwd_last_traded_price}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$kwd_last_traded_price}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$kwd_last_traded_price}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- list ends here -->
        <!-- graph starts here -->
        <div class="bg-white border-all innerAll padding-top-none border-top-none">
            <canvas id="canvas3" height="150"></canvas>
        </div>
        <!-- graph ends here -->
    </div>
    <!-- col-md-6 ends here -->
    <!-- col-md-6 starts here -->
    <div class="col-md-6 padding-right-none">
        <!-- title starts here -->
        <div class="bg-white border-all half innerAll ">
            <span class="keyword-grey-span innerL half f-sz16 l-h16">Referral</span>
        </div>
        <!-- title ends here -->
        <!-- list starts here -->
        <div class="bg-white border-all innerAll border-top-none">
            <div class="comn-lft-rght-cont">
                <label>Interaction</label>
                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= isset($referralInteractions)?($referralInteractions):0 ?>"><?= isset($referralInteractions)?($referralInteractions):0 ?></a></label>
                <div class="clearfix"></div>
            </div>
            <div class="comn-lft-rght-cont">
                <label>Commission</label>
                <label class="pull-right ellipses-market-trade-span text-right" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                       origPrice="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$commission}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$commission}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>



                <div class="clearfix"></div>
            </div>
            <div class="comn-lft-rght-cont">
                <label>Post Share</label>
                <label class="pull-right ellipses-market-trade-span text-right" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                       origPrice="<?php echo number_format("{$postSharer}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$postSharer}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$postSharer}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- list ends here -->
        <!-- graph starts here -->
        <div class="bg-white border-all innerAll padding-top-none border-top-none">
            <canvas id="canvas4" height="150"></canvas>
        </div>
        <!-- graph ends here -->
    </div>
    <!-- col-md-6 ends here -->
    <!-- row ends here -->
    <!-- social-all-status-tabs -->
</div>

<script>

    var xAxisnew = $.parseJSON('<?php echo $newxAxis; ?>');
    var xAxisNew = xAxisnew.reverse();

    var dataAxisnew = $.parseJSON('<?php echo $keywordAxis; ?>');
    var dataAxisNew = dataAxisnew.reverse();

    var monthlyReferredUser = $.parseJSON('<?php echo $monthlyReferredUser; ?>');

    $(document).ready(function () {

        var ctx = document.getElementById("canvas3");

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: xAxisNew,
                datasets: [{
                    label: 'Keyword',   //'# of Votes'
                    data: dataAxisNew,
                    backgroundColor: "rgba(0,157,233,0.8)",
                    borderWidth: 1,
                    borderColor:'white'
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "black"
                        // fontSize: 18
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero:true
                        }
                    }]
                    ,
                    xAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero:true
                        }

                    }]

                }
            }
        });

        var ctx = document.getElementById("canvas4");


        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: xAxisNew,
                datasets: [{
                    label: 'Referral',   //'# of Votes'
                    data: monthlyReferredUser,
                    backgroundColor: "rgba(92,192,242,0.8)",
                    borderWidth: 1,
                    borderColor:'white'
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "black"
                        // fontSize: 18
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero:true
                        }
                    }]
                    ,
                    xAxes: [{
                        ticks: {
                            fontColor: "black",
                            beginAtZero:true
                        }

                    }]

                }
            }
        });
    });

</script>