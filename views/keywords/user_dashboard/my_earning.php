<!--===============================================
=            includes : my_earning.php            =
================================================-->
<?php 
if(isset($_SESSION["profile_pic"]) && !empty($_SESSION["profile_pic"])){
    global $cdnSocialUrl;
    global $rootUrlImages;

    $extensionUP  = pathinfo($_SESSION["profile_pic"], PATHINFO_EXTENSION);
    //CDN image path
    $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $_SESSION["account_handle"] . '/profile/' . $_SESSION["account_handle"] . '_' . $_SESSION["profile_pic"] . '.70x70.' . $extensionUP;

    //server image path
    $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$_SESSION["account_handle"].'/profile/'.$_SESSION["account_handle"].'_'.$_SESSION["profile_pic"];

    // check for image is available on CDN
    $file = $imageFileOfCDNUP;
    $file_headers = @get_headers($file);
    if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $imgSrc = $imageFileOfLocalUP;
    } else {
        $imgSrc = $imageFileOfCDNUP;
    }
}else{
    $imgSrc = $rootUrlImages."default_profile.jpg";
}


$socialFileUrlShare = basename($_SERVER['PHP_SELF'], ".php"); //echo $socialFileUrlShare; die;
//printArr($socialFileUrlShare);
//if ($socialFileUrlShare == "otherTimeline") {
//    $email = $otherEmail;
//} elseif($socialFileUrlShare == "otherFollowersList") {
//    $email = base64_decode($_GET["email"]);
//}else {
//    $email = $_SESSION["email"];
//}

?>
<div class="myearnings-left-panel">
    <div class="card my-info-card social-card">
        <div class="clearfix innerAll border-all">
            <div class="my-info-img pull-left innerMR">
                <img class="img-responsive" src="<?php echo $imgSrc; ?>">
            </div>
            <!-- my-picture  -->
            <div class="my-info-detail pull-left">
                <div class="user-name ellipses">
                    <?php echo $_SESSION["first_name"]." ".$_SESSION["last_name"]; ?>
                </div>
                <div class="user-earnings-text">
                    Earning
                </div>

                <h4 class="text-blue margin-none">
                    <span id="available-balance" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$userBalance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$userBalance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$userBalance}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                </h4>
            </div>
        </div>
        <!-- my-info-detail  -->
    </div>
    <!-- my-info-card  -->
    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4 class="margin-none half innerTB">Content Consumption
                        <?php if ($socialFileUrlShare == "content_consumption") { ?>
                            <i class="fa fa-check pull-right" aria-hidden="true"></i>
                        <?php } ?>
                    </h4>
                </div>
            </div>
        </div>
        <div class="earnings-left-module">
            <div class="margin-none">
                <?php if ($socialFileUrlShare == "content_consumption") { ?>
                    <div id="consumption_blog" href="#consumption_blog" class="social-user-setting-name content-tabs border-bottom">
                        <a>Blog</a>
                    </div>
                    <div id="consumption_video" href="#consumption_video" class="social-user-setting-name content-tabs border-bottom">
                        <a>Video</a>
                    </div>
                    <div id="consumption_image" href="#consumption_image" class="social-user-setting-name content-tabs border-bottom">
                        <a>Image</a>
                    </div>
                    <div id="consumption_audio" href="#consumption_audio" class="social-user-setting-name content-tabs border-bottom">
                        <a>Audio</a>
                    </div>
                     <div id="consumption_status" href="#consumption_status" class="social-user-setting-name content-tabs border-bottom">
                        <a>Status</a>
                    </div>
                <?php } else { ?>
                    <div class="social-user-setting-name content-tabs border-bottom">
                        <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_consumption.php">Blog</a>
                    </div>
                    <div class="social-user-setting-name content-tabs border-bottom">
                        <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_consumption.php">Video</a>
                    </div>
                    <div class="social-user-setting-name content-tabs border-bottom">
                        <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_consumption.php">Image</a>
                    </div>
                    <div class="social-user-setting-name content-tabs border-bottom">
                        <a  href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_consumption.php">Audio</a>
                    </div>
                    <div class="social-user-setting-name content-tabs border-bottom" >
                        <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_consumption.php">Status</a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- content consumption -->


    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4 class="margin-none half innerTB">Content Upload
                        <?php if ($socialFileUrlShare == "content_upload") { ?>
                        <i class="fa fa-check pull-right" aria-hidden="true"></i>
                        <?php } ?>
                    </h4>
                </div>
            </div>
        </div>
        <div class="earnings-left-module">
                <div class="margin-none">
<!--                    <div id="all" href="#all" class="social-user-setting-name border-bottom">-->
<!--                    <a>All</a>-->
<!--                </div>-->
                    <?php if ($socialFileUrlShare == "content_upload") { ?>
                <div id="blog" href="#blog" class="social-user-setting-name content-tabs border-bottom">
                    <a>Blog</a>
                </div>
                <div id="video" href="#video" class="social-user-setting-name content-tabs border-bottom">
                    <a>Video</a>
                </div>
                <div id="image" href="#image" class="social-user-setting-name content-tabs border-bottom">
                    <a>Image</a>
                </div>
                <div id="audio" href="#audio" class="social-user-setting-name content-tabs border-bottom">
                    <a>Audio</a>
                </div>
                <div id="status" href="#status" class="social-user-setting-name content-tabs border-bottom">
                    <a>Status</a>
                </div>
                    <?php }else{ ?>
                <div class="social-user-setting-name content-tabs border-bottom">
                    <a   href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_upload.php">Blog</a>
                </div>
                <div class="social-user-setting-name content-tabs border-bottom" >
                    <a   href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_upload.php">Video</a>
                </div>
                <div class="social-user-setting-name content-tabs border-bottom" >
                    <a  href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_upload.php">Image</a>
                </div>
                <div class="social-user-setting-name content-tabs border-bottom" >
                    <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_upload.php">Audio</a>
                </div>
                <div class="social-user-setting-name content-tabs border-bottom">
                  <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/content_upload.php">Status</a>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <!-- content upload -->
    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4 class="margin-none half innerTB">Keyword
                        <?php
//                        printArr($socialFileUrlShare);
                        if (($socialFileUrlShare == "owned_keyword") || $socialFileUrlShare == "active_trade" || $socialFileUrlShare == "trading_history" ) {
                            ?>
                            <i class="fa fa-check pull-right" aria-hidden="true"></i>
                        <?php } ?>
                    </h4>
                </div>
            </div>
        </div>
        <div class="earnings-left-module">
            <div class="margin-none">
                <div class="social-user-setting-name border-bottom" id="my-owned">
                    <a  id="owned-text" href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/owned_keyword.php">Owned</a>
                </div>
                <div class="social-user-setting-name border-bottom" id="trade-main">
                    <a id="active-trade-text" href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/active_trade.php">Active Trade</a>
                </div>
                <div class="social-user-setting-name border-bottom" id="trade-history-main">
                    <a id="trade-history-text"  href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/trading_history.php">Trading history</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Keyword -->
    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4 class="margin-none half innerTB">Referral</h4>
                </div>
            </div>
        </div>
        <div class="earnings-left-module">
            <div class="margin-none">
                <div class="social-user-setting-name border-bottom" id="interaction-main">
                    <a id="interaction-text" href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/referral_earning.php">Interaction</a>
                </div>
                <div class="social-user-setting-name border-bottom" id="commission-main">
                    <a id="commission-text" href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/commisionReferral.php">Commission</a>
                </div>
                <div class="social-user-setting-name border-bottom" id="referral-share">
                    <a id="referral-share-text" href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/referral_share.php">Post Share</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Referral -->
</div>
<!-- social-left-panel  -->
<!--====  End of includes : my_earning.php  ====-->
<script>
    $('.content-tabs').click(function(){
        $('#likeSelection').val($(this).text());
        $('#uploadSelection').val($(this).text());
    });
</script>