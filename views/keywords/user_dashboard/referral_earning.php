<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/header/headerModel.php');

//check for session

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
    include "../../layout/header.php";
    $email = $_SESSION["email"];

    // db connection
    $conn = createDBConnection("dbkeywords");
    if(noError($conn)){
        $conn = $conn["connection"];
    }else{
        print("Error: Connection Failed");
    }

    // Get referral interaction count
    $referralInteraction = getReferralInteraction($email,$NotificationURL);
    if(noError($referralInteraction)){
        $errMsg = $referralInteraction["errMsg"];
        $referralInteractions = $errMsg["referral_interactions"];
    }

    // Get referral commission amount
    $userAvailableBalance = $userRequiredFields . ",affiliate_earning";
    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);
    if(noError($getUserDetails)){
        $errMsgs = $getUserDetails["errMsg"];
        $commissionEarning = $errMsgs["affiliate_earning"];
    }

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3">
                <?php include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
                        <div class="earnings-day">
                            Today
                        </div>
                        <div id="myTabContent" class="tab-content pull-right width-100-percent">
                            <div class="tab-pane active in" id="lowest">
                                <!-- content starts here -->
                                <div class="col-xs-12 earnings-container padding-none innerMT" id="content_consumption">
                                    <div class="innerMB inner-2x">
                                        <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">
                                            <li class=" innerAll border-bottom">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-dark-green"></i>
                                                                    <i class="fa fa-search text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT ellipses">
                                                            <div class="">Interaction : <a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT pull-right">
                                                        <div class="pull-right ellipses">Earnings : <a href="#" class="" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="innerMTB earnings-day innerML">
                                        Yesterday
                                    </div>
                                    <div class="innerMB inner-2x">
                                        <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">

                                            <li class=" innerAll border-bottom">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-red"></i>
                                                                    <i class="fa fa-play-circle text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT ellipses">
                                                            <div class="">Interaction : <a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT pull-right">
                                                        <div class="pull-right ellipses">Earnings : <a href="#" class="" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></div>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class=" innerAll border-bottom">
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-dark-green"></i>
                                                                    <i class="fa fa-search text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT ellipses">
                                                            <div class="">Interaction : <a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT pull-right">
                                                        <div class="pull-right ellipses">Earnings : <a href="#" class="" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane fade" id="highest">
                                <!-- content starts here -->
                                <div class="col-xs-12 earnings-container padding-none innerMT" id="content_consumption">
                                    <div class="innerMB inner-2x">
                                        <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-8 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-light-grey"></i>
                                                                    <i class="fa fa-file-text text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <div class="pull-left innerMT-1px">Interaction :</div>
                                                            <label class="text-black ellipses pull-left text-left half innerMT">
                                                                <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-6 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-red"></i>
                                                                    <i class="fa fa-play-circle text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <span>Interaction : </span>
                                                            <span>700</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-6 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-sky-blue"></i>
                                                                    <i class="fa fa-image text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <div class="pull-left innerMT-1px">Interaction :</div>
                                                            <label class="text-black ellipses pull-left text-left half innerMT">
                                                                <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-6 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-orange"></i>
                                                                    <i class="fa fa-soundcloud text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <div class="pull-left innerMT-1px">Interaction :</div>
                                                            <label class="text-black ellipses pull-left text-left half innerMT">
                                                                <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-6 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-dark-green"></i>
                                                                    <i class="fa fa-search text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <div class="pull-left innerMT-1px">Interaction :</div>
                                                            <label class="text-black ellipses pull-left text-left half innerMT">
                                                                <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="innerMTB earnings-day innerML">
                                        Yesterday
                                    </div>
                                    <div class="innerMB inner-2x">
                                        <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">
                                            <li class="clearfix border-bottom padding-none">
                                                <div class="col-xs-12 innerAll">
                                                    <div class="col-xs-6 padding-none">
                                                        <div class="padding-left-none">
                                                            <div class="innerMR inner-2x pull-left">
                                                                <div class="image-circle-container">
                                                                    <i class="fa fa-circle text-light-grey"></i>
                                                                    <i class="fa fa-file-text text-white"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="innerMT padding-none earnings-text">
                                                            <div class="pull-left innerMT-1px">Interaction :</div>
                                                            <label class="text-black ellipses pull-left text-left half innerMT">
                                                                <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="700">&nbsp;700</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 innerMT padding-right-none pull-right">
                                                        <div class="pull-left inner-2x innerML innerMT-1px">Earnings :</div>
                                                        <label class="text-black ellipses pull-right text-left half innerMT">
                                                            <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="25,425 <?php echo $keywoDefaultCurrencyName; ?>">&nbsp;25,425 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- content ends here -->
                            </div>
                        </div>
                    </div>
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data">
                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Interaction</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk"title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= isset($referralInteractions)?($referralInteractions):0 ?>"><?= isset($referralInteractions)?($referralInteractions):0 ?></a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$commissionEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" class="display-in-block-txt-blk" title="<?php echo number_format("{$commissionEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo number_format("{$commissionEarning}",4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <select name="" class="earnings-select border-all half innerAll">
                                    <option>Content Consumption</option>
                                    <option>Content Upload</option>
                                    <option>Keyword</option>
                                    <option>Referral</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
                            <i class="fa fa-download text-white"></i>
                        </span>
                            <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
                        </div>
                    </div>
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<?php
include("../../layout/transparent_footer.php");
?>
<script src="../../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script type="text/javascript">
    $("#calendar").MonthPicker({
        SelectedMonth: '04/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        }
        ,
        i18n: {
            months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        }
    });

    $('#interaction-text').css("color", "#2378a1");
    $('#interaction-main').css("border-left", "2px solid #90a1a9");
    $('#interaction-main').css("background-color", "#f0f3f4");

</script>
