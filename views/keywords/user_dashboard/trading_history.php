<?php
session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once ('../../../models/keywords/keywordCdpModel.php');
require_once ('../../../models/keywords/userDashboardModel.php');
require_once ('../../../models/keywords/userCartModel.php');

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../../layout/header.php");

    $email      = $_SESSION["email"];

    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];
    }else{
        print('Error: Database connection');
    }

    $totalKwdSold = 0;
    $totalKwdPurchase = 0;
    $totalKwdEarning = 0;
    $totalProfitLoss = 0;

    $tradingJson = $rootUrl."json_directory/keywords/trading_history/";
    $tradingJson = $tradingJson.$email.".json";

    $tradingData = file_get_contents($tradingJson);
    $data      = json_decode($tradingData, TRUE);

    if(count($data) > 0){
        foreach($data as $key => $tradeValue){
            $sold = $tradeValue["kwd_last_traded_price"];
            $purchase = $tradeValue["kwd_purchase_price"];
            $earning = $tradeValue["user_kwd_ownership_earnings"];
            $profitLoss = ($sold + $earning) - $purchase;


            $totalKwdSold = $totalKwdSold + $sold;
            $totalKwdPurchase = $totalKwdPurchase + $purchase;
            $totalKwdEarning = $totalKwdEarning + $earning;
            $totalProfitLoss = $totalProfitLoss + $profitLoss;
        }
    }

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
<div id="cover"></div>
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3 stickyPostion_tradinghistory_leftPannel">
                <?php  include('my_earning.php'); ?>
            </div>
            <!-- col-xs-3 -->
            
            <div class="col-xs-6">
                <!-- social-all-status-tabs -->
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none" id="tradeHistoryAjaxdata">
                        
                    </div>
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <!-- col-md-3 starts here -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data stickyPostion_tradinghistory_rightPannel">

                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Trading History</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module" id="my-earnings-trading">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Keyword Sold</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                               origPrice="<?php echo number_format("{$totalKwdSold}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKwdSold}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKwdSold}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none  border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Keyword Purchase</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                           origPrice="<?php echo number_format("{$totalKwdPurchase}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKwdPurchase}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKwdPurchase}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span> Keyword Earning </span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalKwdEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKwdEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKwdEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Net <span class="text-color-App-Success float-left">Profit</span> / <span class="error-color float-left">Loss</span></span>
                                </div>

                                <?php

                                if ($totalKwdPurchase  > $totalKwdSold) {
                                    $colorCls = "error-color";
                                } else {
                                    $colorCls = "text-color-App-Success";
                                }

                                if($totalProfitLoss < 0){
                                    $totalProfitLoss = abs($totalProfitLoss);
                                }

                                if($totalProfitLoss == 0){
                                    $totalProfitLoss = abs($totalProfitLoss);
                                    $colorCls = "text-black";
                                }

                                ?>

                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" class="<?php echo $colorCls; ?> display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$totalProfitLoss}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalProfitLoss}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalProfitLoss}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                  <div class="card right-panel-modules inner-2x innerMT">
                    <?php //include '_my_earning_calender.php'; ?>

                      <div class="bg-light-gray right-panel-modules-head">
                          <div class="row margin-none">
                              <div class="col-xs-12 padding-none">
                                  <div class="col-xs-12">
                                      <h4 class="margin-none half innerTB">Keyword Trading</h4>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="earnings-right-module">
                          <input type="hidden" id="transType" name="transactionType" value="trade_fees" />

                          <div id='calendar'></div>
                          <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
              <i class="fa fa-download text-white"></i>
            </span>
                              <form id="downloadWalletData" method="post" action="downloadreport.php">
                                  <input class="btn-social-wid-auto text-center download-calendar-button" id="downloadReport" type="button" value="Download Report"/>

                                  <div id="walletDynamicParameter">

                                  </div>

                              </form>
                          </div>
                      </div>
              
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-md-3 ends here -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>
<?php
include("../../layout/transparent_footer.php");
?>
<script src="<?php echo $rootUrl; ?>js/marketplace.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script src="../../../js/earning_transaction.js"></script>
<script type="text/javascript">

    var d = new Date();
    var n = d.getMonth();

    if(n < 10){
        n = "0"+n;
    }

    $("#calendar").MonthPicker({
            SelectedMonth: n+'/' + new Date().getFullYear(),
            OnAfterChooseMonth: function(selectedDate) {
                // Do something with selected JavaScript date.
                // console.log(selectedDate);
            },
            MaxMonth: 0,
            SelectedMonth:0,
            i18n: {
                months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
            }
        });

    $('#trade-history-text').css("color", "#2378a1");
    $('#trade-history-main').css("border-left", "2px solid #90a1a9");
    $('#trade-history-main').css("background-color", "#f0f3f4");

    $(document).ready(function() {
        getTradingHistoryData();        
    });

     function getTradingHistoryData() {

        var limit  = 15;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controllers/keywords/myTradeHistoryController.php",
                data: {
                    limit  : limit
                },
                success: function (data) {
                    $("#tradeHistoryAjaxdata").html("");
                    $("#tradeHistoryAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
    }


    function getNewTradingHistoryData(pageNo,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/myTradeHistoryController.php",
            data: {
                limit         : limit,
                page          : pageNo
            },
            success: function (data) {
                $("#tradeHistoryAjaxdata").html("");
                $("#tradeHistoryAjaxdata").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>
