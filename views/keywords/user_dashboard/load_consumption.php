<?php
session_start();

require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');
require_once ('../../../helpers/deviceHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once('../../../IPBlocker/ipblocker.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$type               = cleanXSS(rawurldecode($_POST['type']));
$dataCount          = cleanXSS(rawurldecode($_POST['dataCount']));
$lastDataCreateTime = cleanXSS(rawurldecode($_POST['lastDataCreateTime']));
$emailId            = $_SESSION['email'];
$getLikeCount       = getPostLikeIds($_SESSION['account_handle'], 'sort', $type);
$count              = 0;
//printArr($type);
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$blockUserArray          = getBlockedUser($targetDirAccountHandler);
$likePostAllData         = getActivityBookMarkLikePosts(json_encode($getLikeCount), json_encode($blockUserArray));
//printArr(json_encode($likePostAllData));
if (noError($likePostAllData)) {
    $likePostAllData = $likePostAllData['errMsg']; ?>
    <?php
    //printArr($likePostAllData);
    if (count($likePostAllData) <= 0) {
        echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>There is nothing to show here right now.</p><p>Come back later once you have liked post.</p></b></center></li></ul>';
        die;
    }
    //printArr($likePostAllData);
    foreach ($likePostAllData as $date => $likeData) {
        $todayDate = date("m/d/y");

        if ($todayDate == $date) {
            $displayDate = 'Today';
        } else {
            $yesterdayDate = date("m/d/Y", strtotime("yesterday"));
            if ($yesterdayDate == $date) {
                $displayDate = 'Yesterday';
            } else {
                $displayDate = date('jS F Y ', strtotime($date));
            }
        }

        if ($type != "all") {
            foreach ($likeData as $key => $value) {
                if ($value["post_type"] == "share") {
                    $sharedType = $value['post_details']['parent_post']['post_id']['post_type'];
                    $postType = $sharedType;
                } else {
                    $postType = $value["post_type"];
                }
                if ($postType == $type) {
                    $count = count($value);
                }
            }
        } elseif ($type == "all") {
            foreach ($likeData as $key => $value) {
                if ($value["post_type"] == "share") {
                    $sharedType = $value['post_details']['parent_post']['post_id']['post_type'];
                    $postType = $sharedType;
                } else {
                    $postType = $value["post_type"];
                }
                //if ($postType == $type) {
                $count = count($value);
                //}
            }
        }
        if ($count == 0) {
            echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>There is nothing to show here right now.</p><p>Come back later once you have liked post.</p></b></center></li></ul>';
            die;
        }
        ?>
        <div class="earnings-day inner-2x innerMT innerML-10">
            <?php if (isset($count) && (count($likeData) > 0)) { echo $displayDate; } ?>
        </div>
        <div id="myTabContent" class="tab-content width-100-percent inner-1x innerMT">
            <div class="tab-pane active in" id="lowest">
                <!-- content starts here -->
                <div class="" id="content_consumption">
                    <ul class="card social-card earnings-list all-box-shadow padding-none clearfix"
                        style="list-style:none;">
                        <?php
                        foreach ($likeData as $key => $value) {
                            if ($value["post_type"]=="share") {
                                $sharedType = $value['post_details']['parent_post']['post_id']['post_type'];
                                $postType   = $sharedType;
                            } else {
                                $postType = $value["post_type"];
                            }
                            //if ($postType == $type ) { ?>
                                <li class="clearfix border-bottom padding-none">
                                    <div class="col-xs-12 innerAll">
                                        <div class="col-xs-9 padding-none">
                                            <div class="padding-left-none">
                                                <div class="innerMR pull-left">
                                                    <div class="image-circle-container">
                                                        <?php
                                                        switch ($postType) {
                                                            case 'blog':
                                                                $placeholder = "You liked a blog posted by ";
                                                                ?>
                                                                <i class="fa fa-circle text-light-grey"></i>
                                                                <i class="fa fa-file-text text-white"></i>
                                                                <?php break;
                                                            case 'video':
                                                                $placeholder = "You liked a video shared by "; ?>
                                                                <i class="fa fa-circle text-red"></i>
                                                                <i class="fa fa-play-circle text-white"></i>
                                                                <?php break;
                                                            case 'image':
                                                                $placeholder = "You liked a photo posted by "; ?>
                                                                <i class="fa fa-circle text-sky-blue"></i>
                                                                <i class="fa fa-image text-white"></i>
                                                                <?php break;
                                                            case 'audio':
                                                                $placeholder = "You liked an audio shared by "; ?>
                                                                <i class="fa fa-circle text-orange"></i>
                                                                <i class="fa fa-soundcloud text-white"></i>
                                                                <?php break;
                                                            case 'status':
                                                                $placeholder = "You liked a status posted by "; ?>
                                                                <i class="fa fa-circle text-dark-green"></i>
                                                                <i class="fa fa-edit text-white"></i>
                                                                <?php break;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="innerMT padding-none earnings-text">
<!--                                                <span class="text-blue">--><?php //echo $_SESSION["first_name"] . " " . $_SESSION["last_name"]; ?><!--</span>-->
                                                <?php echo $placeholder; ?>
                                                <a <?php if ($value['posted_by'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($value['user_ref']['email']); ?>&type=<?php echo 'all'; ?>" <?php } else { ?> href="<?php echo $rootUrl; ?>" <?php } ?>
                                                        target="_blank">
                                    <span class="text-color-Text-Primary-Blue ">
                                        <?php echo $value['user_ref']['first_name'] . ' ' . $value['user_ref']['last_name']; ?>
                                    </span>
                                                </a>
                                                post.
                                            </div>
                                        </div>
                                        <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                            <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                <a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="">
                                                    <?php if(isset($value["current_payout"]) && !empty($value["current_payout"])) { ?>
                                                        <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value["current_payout"]} ".$keywoDefaultCurrencyName; ?>" class="currency">
															<?php echo formatNumberToSort("{$value["current_payout"]}", 5); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                                        </span>
                                                    <?php } else { ?>
                                                    <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value["current_payout"]} ".$keywoDefaultCurrencyName; ?>" class="currency">
                                                        <span>0 <?php echo $keywoDefaultCurrencyName; ?></span>
                                                    </span>
                                                    <?php } ?>
                                                </a>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            <?php } //} ?>
                            </ul>
                            </div>
                            <!-- content ends here -->
                            </div>
                            </div>
                            <?php
                        //}
    }//End of Outer For Loop
} else {
    echo "<div><center>Please Try After Some Time</center></div>";
}
?>
<div id='load-consumption-last-data-time-<?php echo $dataCount; ?>' lastDataTime = '<?php echo $lastDataTime; ?>' datatype = '<?php echo $type; ?>' lastDataTime = '<?php echo $lastDataTime; ?>' data-empty='<?php echo $dataEmptyFlag; ?>' style="display:none;"></div>
