<?php

session_start();

require_once ('../../../config/config.php');
require_once ('../../../config/db_config.php');
require_once ('../../../helpers/coreFunctions.php');
require_once ('../../../helpers/errorMap.php');
require_once ('../../../helpers/stringHelper.php');
require_once ('../../../helpers/arrayHelper.php');
require_once('../../../models/social/socialModel.php');
require_once('../../../models/social/commonFunction.php');

$email = $_SESSION["email"];

// get request parameters
$type           = trim(strtolower(urldecode($_POST["type"])));
$reportName     = trim(urldecode($_POST["reportName"]));
$file           = trim(urldecode($_POST["file"]));
$returnArr      = array();
$xls            = array();
$data           = array();

if (basename($file, ".php") == "content_consumption") {
    $getLikeCount            = getPostLikeIds($_SESSION['account_handle'], 'sort', $type);
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
    $likePostAllData         = getActivityBookMarkLikePosts(json_encode($getLikeCount), json_encode($blockUserArray));

} //elseif (basename($file, ".php") == "content_upload") {

//} elseif (basename($file, ".php") == "referral_share") {

//}


header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=" . $reportName. ".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<table>
    <tr>
        <th>Post Type</th>
        <th>Post Owner</th>
        <th>Liked At</th>
        <th>Credit</th>
    </tr>
    <tbody>
    <?php
        foreach($likePostAllData["errMsg"] as $key => $likeData) {

            $time  = strtotime($key);
            $month = date("M",$time);
            $year  = date("Y",$time);

            $dataDate = $month . '-' . $year;

            $stamp  = strtotime($dataDate); // get unix timestamp
            $timeMs = $stamp * 1000;

            $stamp1  = strtotime($reportName); // get unix timestamp
            $timeMs1 = $stamp1 * 1000;

            if ($timeMs == $timeMs1) {

            foreach($likeData as $key => $value) {

                if ($value["post_type"] == "share") {
                    $sharedType = $value['post_details']['parent_post']['post_id']['post_type'];
                    $postType = $sharedType;
                } else {
                    $postType = $value["post_type"];
                }

                if ($postType == $type) {
                    $seconds = $value["action_at"]/ 1000;

    ?>
        <tr>
            <td> <?php echo $value["post_type"]; ?></td>
            <td> <?php echo $value["user_ref"]["first_name"] . ' ' . $value["user_ref"]["last_name"]; ?></td>
            <td> <?php echo date("d/m/Y", $seconds); ?></td>
            <td> <?php echo $value["current_payout"]; ?></td>
        </tr>
    <?php } } } } ?>
    </tbody>
</table>