<div class="clearfix">
    <div class="col-xs-12 padding-none earnings-selectlist">
        <div class="input-group width-100-percent">
            <label class="dropdownOptions dropdownOptions--bg-light-gray btn-block margin-none">
                <select class="selectpicker">
                    <option>Content Consumption</option>
                    <option>Content Upload</option>
                    <option>Keyword</option>
                    <option>Referral</option>
                </select>
            </label>
        </div>
    </div>
</div>
<div class="earnings-right-module">
    <div id='calendar'></div>
    <div class="text-blue innerTB text-center download-btn">
        <span class="download-icon">              <i class="fa fa-download text-white"></i>          </span>
        <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
    </div>
</div>
