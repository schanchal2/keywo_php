<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 13/2/17
 * Time: 5:04 PM
 */

header("Access-Control-Allow-Origin: *");
session_start();
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../models/keywords/userCartModel.php";


//getting the value of keyword
if ($_GET["status"] != "H") {
    //$keyword    = urldecode($_GET["keyword"]);
    if ($_GET["keyword"] != "+") {
        $keyword = urldecode($_GET["keyword"]);
    } else {
        $keyword = $_GET["keyword"];
    }
} else {
    //$keyword    = urldecode($_GET["keyword"]);
    if ($_GET["keyword"] != "+") {
        $keyword = urldecode($_GET["keyword"]);
    } else {
        $keyword = $_GET["keyword"];
    }
}

/*$keyword = 'trilok';*/
$email = $_SESSION["email"];

/* Get user info */
$userReqFields = "ref_email,system_mode,currencyPreference,kyc_current_level";
$userDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userReqFields);
//print_r($userDetails);
if(noError($userDetails)) {
    $userKYCLevel = $userDetails['errMsg']['kyc_current_level'];

    // Create Database Connection
    $srchConn = createDBConnection("dbsearch");
    if(noError($srchConn)) {
        $srchConn = $srchConn['connection'];
        $requireFields = 'transaction_fees';
        $getKycLevels = getUserKYCSLevelDetails($srchConn, $userKYCLevel, $requireFields);
        if (noError($getKycLevels)) {

            $getKycLevels = $getKycLevels['errMsg'][0];

        }
    }else{
        $msg = "Error : Creating database connection";
    }

} else{
    $errMsg = "getting user details: failed.";

    /*$returnArr["errCode"] = 2;
    $returnArr["errMsg"] = $errorMessage;*/
}

function getUserKYCSLevelDetails($conn, $KYCLevel, $fields){
    $returnArr = array();
    $extraArg = array();
    $column = '';

    $fields = explode(',',trim($fields,','));
    $KYCLevel = str_replace("kyc_","Level-",$KYCLevel);

    foreach($fields as $columnName){
        if(!empty($columnName)){
            $column = "{$columnName},";
        }
    }

    $column = trim($column,',');
    $query = "SELECT {$column} FROM kyc_levels where kyc_level_name = '".$KYCLevel."' ";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 64, null, $extraArg);
    }

    return $returnArr;
}

/* Create keyword database connection */
$kwdConn = createDbConnection('dbkeywords');
if (noError($kwdConn)) {
    $kwdConn = $kwdConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

/* Get admin settings */
$getAdminSettings = getAdminSettingsFromKeywordAdmin($kwdConn);
//$tradingCommPercent = $getAdminSettings["data"]["trading_commision_percent"];
$tradingCommPercent = $getKycLevels['transaction_fees'];
$keywordRenewalFees = $getAdminSettings["data"]["kwd_renewal_fees_per_year"];

echo "<script>var tradingCommPercent = " . $tradingCommPercent . "; </script>";

$result = getKeywordOwnershipDetails($kwdConn, $keyword);
//printArr($result);

$kwdId = $result["errMsg"]["id"];
$buyerEmail = $result["errMsg"]["buyer_id"];
/* If status 1 the ask is set */
$kwdOrderStatus = $result["errMsg"]["order_status"];
$kwdAskPrice = $result["errMsg"]["ask_price"];
$lastTradedPrice = $result["errMsg"]["kwd_price"];
$lastTradedTime = $result["errMsg"]["purchase_timestamp"];


$previousOwners = $result["errMsg"]["previous_owner"];
$previousOwnersJson = $result["errMsg"]["previous_owner"];
$previousOwners = json_decode($previousOwners, TRUE);
$noOfPreviousOwners = COUNT($previousOwners);

$highestBidAmtForKwd = $result["errMsg"]["highest_bid_amount"];

if (isset($kwdId) && !empty($kwdId)) {
    $kwdOwnershipStatus = TRUE;
} else {
    $kwdOwnershipStatus = FALSE;
}

if ($buyerEmail == $email) {
    $kwdSelfOwnershipStatus = TRUE;
} else {
    $kwdSelfOwnershipStatus = FALSE;
}

if (isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)) {
    $kwdHasBids = TRUE;
} else {
    $kwdHasBids = FALSE;
}

$activeBidsOnKwd = $result["errMsg"]["active_bids"];
$activeBidsOnKwd = json_decode($activeBidsOnKwd, TRUE);
foreach ($activeBidsOnKwd as $value) {
    $activeBidArr = explode("~~", $value);
    $actBidEmail = $activeBidArr[1];
    if ($email == $actBidEmail) {
        $myBidAmt = $activeBidArr[3];
        $myTradeFess = $activeBidArr[4];
        $myBidDetails = $value;

    }
}

if($kwdAskPrice > 0){
    $kwdAskPrice = $kwdAskPrice;
}else{
    $kwdAskPrice = 0;
}

if($highestBidAmtForKwd > 0){
    $highestBidAmtForKwd = $highestBidAmtForKwd;
}else{
    $highestBidAmtForKwd = 0;
}

if ($kwdOwnershipStatus) { ?>
    <!-- Modal content-->
    <div class="modal-content row">
    <?php
    if($kwdSelfOwnershipStatus){
                if ($kwdAskPrice > 0){ ?>
                    <!-- edit ask popup starts here -->
                    <div class="modal-header custom-modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title innerL half">Edit Ask</h4>
                    </div>
                    <div class="modal-body padding-none clearfix innerMT">
                        <div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 innerMB">
                                        <span class="keyword-grey-span pull-r-marketplace" data-toggle="tooltip" data-placement="bottom" title="#<?php echo $keyword; ?>" data-original-title="<?php echo $keyword; ?>">#<?php echo $keyword; ?></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="span-keyword-market pull-left">Your Ask : </span>
                                            <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $kwdAskPrice;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-left pull-right">
                                            <span class="span-keyword-market pull-left">Highest Bid : </span>
                                            <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $highestBidAmtForKwd;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                        </div>
                                    </div>
                                </div>
                                <form id="editAskForm" class="form-horizontal"  method="post">
                                    <div class="">
                                        <div class="">
                                            <div class="text-left">
                                                <span class="control-label span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type="number" value="<?php echo $kwdAskPrice; ?>" id="askAmount" name="askAmount" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="text-left pull-right">
                                                <span class="control-label span-keyword-market pull-left"><?php echo $_SESSION['CurrPreference']; ?> : &nbsp;</span>
                                                <input type="number" value="" placeholder="Enter amount" id="askAmountReverse" name="askAmountReverse" class="span-blue-keyword-market  span-input-keyword" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMTB  span-blue-keyword-market">
                                            <div class="text-left">
                                                <img class="dialog-loading-icon-delete" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>
                                                <input value="Delete" type="button" onclick="setUpdateAsk('<?php echo $keyword; ?>','3',this)" class="btn-trading-wid-auto">
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB close-btn">&nbsp;&nbsp;
                                                <img class="dialog-loading-icon" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>
                                                <input value="Set Ask" type="button" onclick="setUpdateAsk('<?php echo $keyword; ?>','2',this)" class="btn-trading-wid-auto innerMTB">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- edit ask popup ends here -->
                <?php }else{ ?>
                    <!-- set ask popup starts here -->
                    <div class="modal-header custom-modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title innerL half">Set Ask</h4>
                    </div>
                    <div class="modal-body padding-none clearfix innerMT">
                        <div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-md-12 innerMB">
                                    <span  class="keyword-grey-span pull-r-marketplace model-keyword" data-toggle="tooltip" data-placement="bottom" title="#<?php echo $keyword; ?>" data-original-title="<?php echo $keyword; ?>">#<?php echo $keyword; ?></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-left">
                                        <span class="span-keyword-market pull-left">Asking Price : </span>
                                        <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $kwdAskPrice." ".$keywoDefaultCurrencyName; ?>" title="<?php echo $kwdAskPrice;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-left pull-right">
                                        <span class="span-keyword-market pull-left">Highest Bid : </span>
                                        <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $highestBidAmtForKwd;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                    </div>
                                </div>
                            </div>
                            <form id="setAskForm" method="post">

                                <div class="">
                                    <div class="">
                                        <div class="text-left">
                                            <span class="control-label span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                            <input type="number" value="" placeholder="Enter amount" id="askAmount" name="askAmount" class="span-blue-keyword-market  span-input-keyword" />
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="text-left pull-right">

                                            <span class="control-label span-keyword-market pull-left"><?php echo $_SESSION['CurrPreference']; ?> : &nbsp;</span>
                                            <input type="number" value="" placeholder="Enter amount" id="askAmountReverse" name="askAmountReverse" class="span-blue-keyword-market  span-input-keyword" readonly/>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 innerMB">
                                        <div class="text-left">
                                            <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                        </div>
                                    </div>
                                    <div class="col-md-6 innerMB">
                                        <div class="text-left pull-right">
                                            <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB close-btn">&nbsp;&nbsp;
                                            <img class="dialog-loading-icon" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>
                                            <input value="Set Ask" type="button" onclick="setUpdateAsk('<?php echo $keyword; ?>','1',this)" class="btn-trading-wid-auto innerMTB">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                    <!-- set ask popup ends here -->
                <?php }
                }
                /*keywords self ownership status end here*/
                else{
                    /*keywords ownership status of other user starts here*/
                     //Edit Bid
                        if($myBidAmt > 0)
                        { ?>
                            <div class="modal-header custom-modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title innerL half">Edit Bid</h4>
                            </div>
                            <div class="modal-body padding-none clearfix innerMT">
                                <div>
                                <div class="col-xs-12">
                                    <div class="row">
                                       <!--  <div class="col-md-12 innerMB">
                                            <span class="keyword-grey-span pull-r-marketplace" data-toggle="tooltip" data-placement="bottom" title="#<?php echo $keyword; ?>">#<?php echo $keyword; ?></span>
                                        </div> -->
                                        <div class="col-md-4 innerMB">
                                            <span class="width-250 keyword-grey-span pull-r-marketplace ellipses">
                                                <a class="ellipses-check" data-toggle="tooltip" data-placement="bottom" title="#<?php echo $keyword; ?>">#<?php echo $keyword; ?></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left">Asking Price : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $kwdAskPrice;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $highestBidAmtForKwd;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="editBidForm" class="form-horizontal"  method="post">
                                        <div class="">
                                            <div class="">
                                                <div class="text-left">
                                                    <span class="control-label span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                    <input type="number" value="<?php echo $myBidAmt; ?>" placeholder="Enter amount" id="bidAmount" name="bidAmount" class="span-blue-keyword-market  span-input-keyword" />
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="text-left pull-right">
                                                    <span class="control-label span-keyword-market pull-left"><?php echo $_SESSION['CurrPreference']; ?> : &nbsp;</span>
                                                    <input type="number" value="" placeholder="Enter amount" id="bidAmountReverse" name="bidAmountReverse" class="span-blue-keyword-market  span-input-keyword" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-7 ">
                                                <div class="text-left">
                                                    <span class="span-keyword-market pull-left margin-top-none" id="trade_fees">Trade fees : &nbsp;</span>
                                                    <span class="span-blue-keyword-market pull-r-marketplace margin-top-none" id="trade_fees_amt">&nbsp; <?php echo ($myTradeFess > 0)? "{$myTradeFess} {$keywoDefaultCurrencyName}" : "0.0000 {$keywoDefaultCurrencyName}"; ?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 padding-right-none">
                                                <div class="text-left">
                                                    <span class="span-keyword-market pull-left margin-top-none" id="renewal_fees" >Renewal fees : </span>
                                                    <span class="span-blue-keyword-market pull-r-marketplace margin-top-none" id="renewal_fees_amt" value="<?php echo "{$keywordRenewalFees}"; ?>" > &nbsp; <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$keywordRenewalFees} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $keywordRenewalFees;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$keywordRenewalFees}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 innerMB">
                                                <div class="text-left">
                                                    <img class="dialog-loading-icon" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>
                                                     <input value="Delete" type="button" onclick="setUpdateBid('<?php echo $keyword; ?>','3', this)" class="btn-trading-wid-auto innerMTB">
                                                </div>
                                            </div>
                                            <div class="col-md-6 innerMB">
                                                <div class="text-left pull-right">
                                                    <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB close-btn">&nbsp;&nbsp;
                                                    <img class="dialog-loading-icon" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>
                                                    <input value="Edit Bid" type="button" onclick="setUpdateBid('<?php echo $keyword; ?>','2', this)" class="btn-trading-wid-auto innerMTB">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            </div>
                        <?php }
                        else { ?>
                        <!--Place bid-->
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title innerL half">Place Bid</h4>
                        </div>
                        <div class="modal-body padding-none clearfix innerMT">
                            <div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-4 innerMB">
                                        <span class="width-250 keyword-grey-span pull-r-marketplace ellipses">
                                            <a class="ellipses-check" title="#<?php echo $keyword; ?>" data-toggle="tooltip" data-placement="bottom">#<?php echo $keyword; ?></a>
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="text-left">
                                            <span class="span-keyword-market pull-left">Asking Price : </span>
                                            <!-- <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<?php //echo ($kwdAskPrice)? "{$kwdAskPrice} {$keywoDefaultCurrencyName}" : "0.0000 {$keywoDefaultCurrencyName}"; ?></span>-->
                                            <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $kwdAskPrice;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-left pull-right">
                                            <span class="span-keyword-market pull-left">Highest Bid : </span>
                                            <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $highestBidAmtForKwd;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                        </div>
                                    </div>
                                </div>
                                <form id="placeBidForm" class="form-horizontal"  method="post">
                                    <div class="">
                                        <div class="">
                                            <div class="text-left">
                                                <span class="control-label span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type = "number" type="number" value="" id="bidAmount" name="bidAmount" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="text-left pull-right">

                                                <span class="control-label span-keyword-market pull-left"><?php echo $_SESSION['CurrPreference']; ?> : &nbsp;</span>
                                                <input type="number" value="" placeholder="Enter amount" id="bidAmountReverse" name="bidAmountReverse" class="span-blue-keyword-market  span-input-keyword" readonly/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                            <div class="col-xs-7 ">
                                                <div class="text-left">
                                                    <span class="span-keyword-market pull-left margin-top-none" id="trade_fees">Trade Fees : &nbsp; </span>
                                                    <span class="span-blue-keyword-market pull-r-marketplace margin-top-none" id="trade_fees_amt">&nbsp;0.0000 <?php echo $keywoDefaultCurrencyName; ?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 padding-right-none">
                                                <div class="text-left">
                                                    <span class="span-keyword-market pull-left margin-top-none" id="renewal_fees">Renewal fees : </span>
                                                    <span class="span-blue-keyword-market pull-r-marketplace margin-top-none"  >&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$keywordRenewalFees} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo $keywordRenewalFees;  ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$keywordRenewalFees}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                </div>
                                            </div>

<!--                                        <div class="col-md-6 innerMB">-->
<!--                                            <div class="text-left">-->
<!--                                                 <input value="Delete" type="button" class="btn-trading-wid-auto innerMTB">-->
<!--                                            </div>-->
<!--                                        </div>-->
                                        <div class="col-md-6 innerMB pull-right">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB close-btn">&nbsp;&nbsp;


                                                <input value="Place Bid" type="button" onclick="setUpdateBid('<?php echo $keyword; ?>','1', this)" class="btn-trading-wid-auto innerMTB">
                                                <img class="dialog-loading-icon" src="<?php  echo $rootUrlImages ?>/login_loader.gif" style="display:none;"/>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    <?php }

                } /*keywords ownership status of other user starts here*/
    ?>
    </div>
              <?php } else {
    //Buy keyword
    echo "Buy Keyword";
}

?>
<script>
  /* Dialog box price convert */
  /*get 'userCurrPref' variable from header*/
  /* variable and regex for decimal amount validation */
  var regex = /^(\d{0,15})(\.)(\d{1,8})?$|^(\d{0,15}\.?)$/;
  var inputValue = $("#askAmount").val();
  var inputValueBid = $("#bidAmount").val();

  $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  });

  $('.model-keyword').tooltip({
      container: '.modal-content'
  });

  $("#bidAmount").keyup(function(e){
      var inputAmount = $(this).val();
      var tradeFees = 0;
      var renewalFees = $('#renewal_fees_amt').attr("value");

      var newInputValueBid = inputAmount;
      if(inputAmount != "" && inputAmount >= 0){

          if(decimalAmountValidation(newInputValueBid)){

              inputValueBid = newInputValueBid;
              $.ajax({
                  type: "POST",
                  dataType:"JSON",
                  url: rootUrl+"controllers/convertCurrency.php?curCode="+userCurrPref,
                  data: {

                  },
                  success: function(data){
                      if(data.errCode == -1){
                          exchangeRate = data.currRate;
                          var amount = parseFloat(data.currRate[userCurrPref.toLowerCase()]) * inputAmount;
                          tradeFees = parseFloat((inputAmount * data.currRate[userCurrPref.toLowerCase()])); //Convert trade price in user preferred currency
                          tradeFees = parseFloat((tradeFees * tradingCommPercent)/100); //Calculate trade fees
                          renewalFees = parseFloat((renewalFees * data.currRate[userCurrPref.toLowerCase()])); //Convert trade price in user preferred currency
                          if(userCurrPref == 'BTC')
                          {
                              $("#bidAmountReverse").val(amount.toFixed(8));
                              $("#trade_fees_amt").html(tradeFees.toFixed(8)+" "+userCurrPref);
                              // $("#renewal_fees_amt").html(renewalFees.toFixed(8)+" "+userCurrPref);
                          }else{
                              $("#bidAmountReverse").val(amount.toFixed(4));
                              $("#trade_fees_amt").html(tradeFees.toFixed(4)+" "+userCurrPref);
                              // $("#renewal_fees_amt").html(renewalFees.toFixed(4)+" "+userCurrPref);
                          }

                      }
                  }
              });

          }else{
              console.error("invalid")
              event.target.value=inputValueBid;
          }

          if (!regex.test(inputValueBid)
              && e.which != 8   // backspace
              && e.which != 46  // delete
              && (e.which < 37  // arrow keys
              || e.which > 40)) {
              e.preventDefault();
              return false;
          }

      }else{
          $(this).val($(this).val().replace(/\s+/g, ''));
          $("#bidAmountReverse").val("");
          $("#trade_fees_amt").html(tradeFees+" " + keywoDefaultCurrency);
         // $("#renewal_fees_amt").html(parseFloat(renewalFees)+" " + keywoDefaultCurrency);
      }
  });
  $("#askAmount").keyup(function(e){

      var inputAmount = $(this).val();
      if(inputAmount != "" && inputAmount >= 0){

          var newInputValue = inputAmount;
          if(decimalAmountValidation(newInputValue)){

              inputValue = newInputValue;

              $.ajax({
                  type: "POST",
                  dataType:"JSON",
                  url: rootUrl+"controllers/convertCurrency.php?curCode="+userCurrPref,
                  data: {

                  },
                  success: function(data){
                      if(data.errCode == -1){
                          exchangeRate = data.currRate;
                          var amount = parseFloat(data.currRate[userCurrPref.toLowerCase()]) * inputAmount;
                          if(userCurrPref == 'BTC')
                          {
                              $("#askAmountReverse").val(amount.toFixed(8));
                          }else{
                              $("#askAmountReverse").val(amount.toFixed(4));
                          }
                      }
                  }
              });

          }else{
              console.error("invalid")
              event.target.value=inputValue;
          }

          if (!regex.test(inputValue)
              && e.which != 8   // backspace
              && e.which != 46  // delete
              && (e.which < 37  // arrow keys
              || e.which > 40)) {
              e.preventDefault();
              return false;
          }

      }else{
          $(this).val($(this).val().replace(/\s+/g, ''));
          $("#askAmountReverse").val("");
      }
  });

  $(".close-btn").click(function(){
    $(".modal").modal("hide");
    $("#bidAmountReverse").val("");
    $("#bidAmount").val("");
  });


</script>
