			<?php
			session_start();

			header('Content-Type: text/html; charset=utf-8');

			$docrootpath = __DIR__;
			$docrootpath = explode('/views', $docrootpath);
			$docrootpath = $docrootpath[0] . "/"; //echo $docrootpath; die;

			require_once("{$docrootpath}config/config.php"); //echo "Testing";
			require_once("{$docrootpath}config/db_config.php");
			require_once("{$docrootpath}/helpers/errorMap.php");
			require_once("{$docrootpath}helpers/coreFunctions.php");
			require_once("{$docrootpath}helpers/stringHelper.php");
			require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
			require_once("{$docrootpath}models/keywords/userCartModel.php");
			error_reporting(0);

			$email = $_SESSION["email"];

			//For database connection
			$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>"; die;

			if(noError($conn)){
				$conn = $conn["connection"];
			}else{
				print_r("Database Error");
			}

			//Validating User LoggedIn and LoggedOUt status
			if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
				$login_status = 1;
			}else{
				$login_status = 0;
                header("Location: $rootUrl./views/prelogin/index.php");


			}

			include('../../layout/header.php');

            $pageUrl = "views/keywords/analytics/keyword_analytics.php";

?>
<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
            <style>
                #ellipses-viewall .ellipses-market-trade-label{width:100%!important;}
                .keymarketplace-data ul li{ padding: 8.3px 18px;}
            </style>
<main class="main-keyword inner-6x innerT" id="ellipses-viewall">
  <!-- keyword-marketplace starts here -->
  <div class="keyword-marketplace keyword-marketplace-data keyword-markt-popup-common">

    <!-- container starts here -->
    <div class="container">

      <!-- row starts here -->
      <div class="row">
<div class="col-xs-3">
<!-- keymarketplace-data-left starts here -->
  <div class="keymarketplace-data key-mar-data-floatingmenu">
<div class="row">
<div class="col-xs-12"><div class="back-to-keymar">
<a href="keyword_search.php"><i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;&nbsp;Back to Keyword Market</a>
</div></div>
</div>

<!-- row starts here -->
<div class="row">
  <!-- trade analytics col starts here -->
  <div class="col-md-12 inner-3x innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Trade Analytics
          </a>
		  <a href="view_latest_trade.php" class="list-group-item">Latest Trade</a>
		  <a href="#" class="list-group-item text-primary active">Ask</a>
		  <a href="view_bid_keyword.php" class="list-group-item">Bid</a>
		  <a href="view_newowned_keyword.php" class="list-group-item">Newly Owned</a>
      </div>
  </div>
  <!-- trade analytics col ends here -->


  <!-- interaction analytics col starts here -->
  <div class="col-md-12 half margin-top-none">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Interaction Analytics
          </a>
          <a href="view_interaction_analytics.php" class="list-group-item text-primary">All</a>
          <a href="interactionBlog.php" class="list-group-item">Blog</a>
          <a href="interactionVideo.php" class="list-group-item">Video</a>
          <a href="interactionImage.php" class="list-group-item">Image</a>
          <a href="interactionAudio.php" class="list-group-item">Audio</a>
          <!--<a href="#" class="list-group-item">Search</a>-->

      </div>
  </div>
  <!-- interaction analytics col ends here -->

  <!-- earning analytics col starts here -->
  <div class="col-md-12 half margin-top-none">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Earning Analytics
          </a>
          <a href="view_earning_analytics.php" class="list-group-item text-primary">All</a>
          <a href="earningBlog.php" class="list-group-item">Blog</a>
          <a href="earningVideo.php" class="list-group-item">Video</a>
          <a href="earningImage.php" class="list-group-item">Image</a>
          <a href="earningAudio.php" class="list-group-item">Audio</a>
          <!--<a href="#" class="list-group-item">Search</a>-->

      </div>
  </div>

  <div class="col-md-12">
  &nbsp;
  </div>
  <!-- ask col ends here -->





  </div>
<!-- row ends here -->
  </div>
<!-- keymarketplace-data-left ends here -->
</div>

		  <div class="col-xs-9">
			  <!-- keymarketplace-data-right starts here -->
			  <div class="keymarketplace-data key-mar-data-floatingmenu" id="keyword-ellipses">
				  <h3 class="heading-key-markt margin-top-none inner-3x innerMB marg-botm">Keyword Market Data</h3>
				  <!-- pagination starts here -->
				  <div class="pagination-content text-center">
					  <!-- pagination head starts here -->
					  <div class="pagination-head half innerAll padding-key-market-data">
						  <div class="row">
							  <div class="col-xs-4 text-white text-left">keywords</div>
							  <div class="col-xs-3 text-white">Ask Amount</div>
							  <div class="col-xs-5 text-white padding-left-none padding-right-none">
								  <!-- keyword-radio-btn starts here -->
								  <div class="keyword-radio-btn pull-right">
									  <div class="row">
										  <div class="col-xs-12 padding-right-none inner-2x innerML">
											  <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
												  <input type="radio" name="shorting" id="latest" value="" checked="true">
												  <label for="latest">
													  Latest
												  </label>
											  </div>
											  <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
												  <input type="radio" name="shorting" id="highest" value="">
												  <label for="highest">
													  Highest
												  </label>
											  </div>
											  <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
												  <input type="radio" name="shorting" id="lowest" value="">
												  <label for="lowest">
													  Lowest
												  </label>
											  </div>
										  </div>
									  </div>
								  </div>
								  <!-- keyword-radio-btn ends here     -->
							  </div>
						  </div>
					  </div>

                      <div id="latestAskAjaxdata">



                      </div>
                  </div>
                  <!-- pagination ends here -->
              </div>
              <!-- keymarketplace-data-right ends here -->
          </div>
      </div>
        <!-- row ends here -->
    </div>
  </div>
    <!-- keyword-marketplace ends here -->
</main>

<div id="callPageAjaxData" class="col-xs-12">
</div>

<?php include('../../layout/transparent_footer.php'); ?>
<script src="../../../js/marketplace.js"></script>
<script>

$(document).ready(function(){
    getTradeData();
});
var id = "latest";
var type = "ask_time";
orderby = "DESC";

var limit = ($("#LimitedResultData").val());
if (((typeof limit) == "undefined") || limit == "") {
    limit = 14;
}
var startDate = "";
var endDate   = "";
var pageNo    = 1;

$("input[name = 'shorting']").click(function(){
    var id = $(this).attr("id");
    if(id == "highest")
    {
        type = "ask_price";
        orderby= "DESC";
    }
    else if(id == "lowest")
    {
        type = "ask_price";
        orderby= "ASC";
    }
    else { type = "ask_time"; orderby = "ASC";}

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controllers/keywords/tradeAnalyticsController.php",
        data: {
            limit        : limit,
            page         : pageNo,
            tableName    : "latest_ask",
            id           : id,
            orderby      : orderby,
            keywordPrice : "ask_price",
            startDate    : startDate,
            endDate      : endDate,
            type         : type

        },
        success: function(data) {
            $("#latestAskAjaxdata").html("");
            $("#latestAskAjaxdata").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

});
var email = '<?php echo $email; ?>';
function getTradeData() {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controllers/keywords/tradeAnalyticsController.php",
        data: {

            limit        : limit,
            page         : pageNo,
            tableName    : "latest_ask",
            id           : id,
            orderby      : orderby,
            keywordPrice : "ask_price",
            startDate    : startDate,
            endDate      : endDate,
            type         : type

        },
        success: function(data) {
            $("#latestAskAjaxdata").html("");
            $("#latestAskAjaxdata").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

}

function getNewTradeData(pageNo,type,id,startDate,endDate,limit) {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controllers/keywords/tradeAnalyticsController.php",
        data: {

            limit        : limit,
            page         : pageNo,
            tableName    : "latest_ask",
            id           : id,
            orderby      : orderby,
            keywordPrice : "ask_price",
            startDate    : startDate,
            endDate      : endDate,
            type         : type

        },
        success: function(data) {
            $("#latestAskAjaxdata").html("");
            $("#latestAskAjaxdata").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

}
</script>
