
<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
error_reporting(0);

$email = $_SESSION["email"];

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    exit;
}

$pageUrl = "views/keywords/analytics/keyword_analytics.php";
// get current active slab rate
$currentSlabRate = getCurrentActiveSlab($conn); //echo "<pre>"; print_r($currentSlabRate); echo "</pre>";
if(noError($currentSlabRate)){
    $currentSlabRate = $currentSlabRate["errMsg"];
    $activeSlabRate  = $currentSlabRate["ITD_Price"];
}else{
    print('Error: Unable to retrieve slab rate');
    exit;
}

    $recentlySoldKeyword = recentlySoldKeyword($conn); //echo "<pre>"; print_r($recentlySoldKeyword); echo "</pre>";
    $result              = $recentlySoldKeyword["errMsg"];
//print_r($result);
if(empty($result))
{
    ?>

    <li> <div><center>No Data Available</div></li>

<?php } else{
    $rank  = 0;

foreach ($result as $key =>$value) {
$purchase_price = $value["keyword_price"];
$keyword = $value['keyword'];
$rank++;
if ($rank < 7) {
    ?>


        <!-- li starts here -->

    <li>
        <div class="comn-lft-rght-cont">

            <label class="ellipses-market-trade-label"> <a data-original-title="<?php echo $keyword; ?>" title="<?php echo $keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?>
                </a> </label>

            <span class="pull-right ellipses-market-trade-span text-right">&nbsp;<a href="javascript:;" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                    origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$purchase_price}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$purchase_price}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>


            <div class="clearfix"></div>
        </div>

    </li>


    <?php }}} ?>



    <li class="bg-dark-grey">
        <div class="common-on-hover">
            <span><a href="javascript:;" onclick="newlyOwnedKeywords();"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></span>
           <?php if($rank>7){ ?>
            <span class="pull-right"><a href="view_newowned_keyword.php">View More</a></span> <?php } ?>
            <div class="clearfix"></div>
        </div>
    </li>