
<?php


session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/"; //echo $docrootpath; die;

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/ViewallShortingModel.php");
error_reporting(0);

$email = $_SESSION["email"]; //echo $email; die;

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>"; die;

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    exit;


}

$pagename = $_GET['pagename']; //echo $pagename."<br>";
$type = $_GET['type'];
$startDate = $_GET['startDate']; //echo $startDate; echo "<br>";
$endDate = $_GET['endDate'];  //echo $endDate;//echo $type;
$total_pages = $_GET['total_pages']; //echo "Testing".$total_pages;
//echo $_SESSION['curr_pref'];


?>


<?php

if($pagename == "latesttrade" && $type == "highest")
{
    $highesttradeshorting = highesttradeshorting($pagename,$type,$conn); //echo "<pre>"; print_r($highesttradeshorting); echo "</pre>";
    $result               = $highesttradeshorting["errMsg"];
     //echo "<pre>"; print_r($result);  echo "</pre>";

    foreach ($result as $key =>$value) {
        $purchase_price = $value["trade_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;


        ?>
        <li id="latesttradevalue">
        <div class="row">
        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($purchase_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>
        <div class="col-md-5 pull-right text-right">
        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>


                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>


                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>
            </div>

            </div>


            </li>

        <?php }}} else if ($pagename == "latesttrade" && $type == "lowest"){

  $lowesttradeshorting = highesttradeshorting($pagename,$type,$conn); //echo "<pre>"; print_r($lowesttradeshorting); echo "</pre>";
 $result               = $lowesttradeshorting["errMsg"];

foreach ($result as $key =>$value) {
$purchase_price = $value["trade_price"]; //echo $purchase_price;
$keyword = $value["keyword"]; //echo $keyword;

?>
    <li id="latesttradevalue">
        <div class="row">
           <div class="col-md-4 text-left" id="tooltip-arrw--left">
               <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
            </div>
             <div class="col-md-3 text-black">
            <?php if($purchase_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
              </div>
            <div class="col-md-5 pull-right text-right">

            <?php

            //print_r($getLatestBid); die;
            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

            //printArr($checkForKeywordAvailability);
            if(noError($checkForKeywordAvailability)){

                $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
                $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
                $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                $CartStatus = $checkForKeywordAvailability['status'];
                $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

                $activeBids = json_decode($activeBids, true);
                foreach($activeBids as $key => $bidValue){
                    $bidValue = explode('~~', $bidValue);
                    $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

                }
                if(in_array($email, $bidderEmail)){
                    $bidStatus = true;
                }else{
                    $bidStatus = false;
                }

                if($email == $kwdOwnerId){

                    ?>

                    <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                            </div>
                        </div>
                        <?php
                    }else{

                        ?>
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                            </div>
                        </div>

                        <?php
                    }

                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }else{

                    if($CartStatus == "sold"){

                        if(empty($activeBids)){
                            //echo "activeBids ".$activeBids;
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>
                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                            <?php

                        }else{
                            if($bidStatus){
                                //echo "bidStatus ".$bidStatus
                                // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                                ?>

                                    <div class="row pull-right">
                                        <div class="col-xs-6 text-right padding-right-none">
                                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                        </div>
                                    </div>


                                <?php
                            }else{
                                //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                                ?>

                                    <div class="row pull-right">
                                        <div class="col-xs-6 text-right padding-right-none">
                                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                        </div>
                                    </div>

                            <?php }}

                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                            ?>

                            <div class="col-md-5 pull-right text-right">
                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                    </div>
                                </div>
                            </div>

                        <?php }}} ?>

    </div>


    </li>

<?php }}} else if($pagename == "latesttrade" && $type == "latest"){


    $latesttradeshorting = highesttradeshorting($pagename,$type,$conn); //echo "<pre>"; print_r($latesttradeshorting); echo "</pre>";

    $result               = $latesttradeshorting["errMsg"];

    foreach ($result as $key =>$value) {
        $purchase_price = $value["trade_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;

        ?>
        <li id="latesttradevalue">
        <div class="row">
        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($purchase_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <input value="Edit Ask" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>


                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>
            </div>
            </div>
            </li>

        <?php }}} else if($pagename == "viewask" && $type == "highest"){

    $askhighest = askhighest($conn);
    $result     = $askhighest["errMsg"];

    foreach ($result as $key =>$value) {
        $ask_price = $value["ask_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="askvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($ask_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price']; //print_r($kwdAskPrice);
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }

            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){



                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                        </div>
                    </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                            </div>
                        </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                </div>
                            </div>


                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){

                        ?>
                        <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                            </div>
                        </div>
                        </div>


                    <?php  }} ?>
                </div>

                </div></li>

            <?php }}}} else if($pagename == "viewask" && $type == "lowest"){

    $askhighest = asklowest($conn);
    $result     = $askhighest["errMsg"];

    foreach ($result as $key =>$value) {
        $ask_price = $value["ask_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="askvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($ask_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>
        <div class="col-md-5 pull-right text-right">

        <?php
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price']; //print_r($kwdAskPrice);
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }

            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){



                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <input value="Edit Ask" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                        </div>
                    </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                            </div>
                        </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                </div>
                            </div>


                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){

                        ?>
                        <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                            </div>
                        </div>
                        </div>


                    <?php  }} ?>
                </div>

                </div></li>

            <?php }}}} else if($pagename == "viewask" && $type == "latest"){

    $askhighest = asklatest($conn);
    $result     = $askhighest["errMsg"];

    foreach ($result as $key =>$value) {
        $ask_price = $value["ask_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="askvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($ask_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$ask_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$ask_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price']; //print_r($kwdAskPrice);
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }

            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){



                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                        </div>
                    </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                            </div>
                        </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                </div>
                            </div>


                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){

                        ?>
                        <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                            </div>
                        </div>
                        </div>


                    <?php  }} ?>
                </div>

                </div></li>

            <?php }}}} else if($pagename == "viewbid" && $type == "highest"){

    $bidhighest = bidhighest($conn);
    $result     = $bidhighest["errMsg"];

    foreach ($result as $key =>$value) {
        $bid_price = $value["bid_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="bidvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($bid_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>


                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>

                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>
            </div>

            </div></li>

        <?php }}} else if($pagename == "viewbid" && $type == "lowest"){

    $bidlowest = bidlowest($conn);
    $result     = $bidlowest["errMsg"];

    foreach ($result as $key =>$value) {
        $bid_price = $value["bid_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="bidvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($bid_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>

                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>
            </div>

            </div></li>

        <?php }}} else if($pagename == "viewbid" && $type == "latest"){

    $bidlatest = bidlatest($conn);
    $result     = $bidlatest["errMsg"];

    foreach ($result as $key =>$value) {
        $bid_price = $value["bid_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;



        ?>
        <li id="bidvalue">
        <div class="row">

        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($bid_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$bid_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$bid_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>


                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>
                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>

                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>
            </div>



            </div></li>

        <?php }}} else if($type == $type && $startDate == $startDate && $endDate == $endDate){

    //echo "Testing Pagination ";
    $datePagination = datePaginations($type,$startDate,$endDate,$conn); //echo "<pre>"; print_r($datePagination); echo "</pre>";
    $result         = $datePagination["errMsg"];


    foreach ($result as $key =>$value) {
        $purchase_price = $value["trade_price"]; //echo $purchase_price;
        $keyword = $value["keyword"]; //echo $keyword;


        ?>
        <li id="latesttradevalue">
        <div class="row">
        <div class="col-md-4 text-left" id="tooltip-arrw--left">
            <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $value['keyword']; ?>"   href="<?php echo $indexUrl; ?>?q=<?php echo $value['keyword']; ?>" title="<?php echo $value['keyword']; ?>" data-placement="bottom" data-toggle="tooltip">#<?php echo $value['keyword']; ?></a></label>
        </div>
        <div class="col-md-3 text-black">
            <?php if($purchase_price > 999) { ?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } else {?>
                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$purchase_price} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$purchase_price}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
            <?php } ?>
        </div>

        <div class="col-md-5 pull-right text-right">

        <?php

        //print_r($getLatestBid); die;
        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

        //printArr($checkForKeywordAvailability);
        if(noError($checkForKeywordAvailability)){

            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
            $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
            $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
            $CartStatus = $checkForKeywordAvailability['status'];
            $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

            $activeBids = json_decode($activeBids, true);
            foreach($activeBids as $key => $bidValue){
                $bidValue = explode('~~', $bidValue);
                $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

            }
            if(in_array($email, $bidderEmail)){
                $bidStatus = true;
            }else{
                $bidStatus = false;
            }

            if($email == $kwdOwnerId){

                ?>

                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                        </div>
                    </div>
                    <?php
                }else{

                    ?>
                    <div class="row pull-right">
                        <div class="col-xs-6 text-right padding-right-none">
                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                        </div>
                    </div>

                    <?php
                }

                if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                    //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                    ?>

                    <div class="col-md-5 pull-right text-right">
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
            }else{

                if($CartStatus == "sold"){

                    if(empty($activeBids)){
                        //echo "activeBids ".$activeBids;
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                        ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                        <?php

                    }else{
                        if($bidStatus){
                            //echo "bidStatus ".$bidStatus
                            // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>

                            <?php
                        }else{
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>
                            
                        <?php }}

                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="buyNowKeyword" value="<?php echo $keyword;  ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-buy" >Buy Now</button>
                                </div>
                            </div>
                        </div>

                    <?php }}} ?>


            </div>

            </div>

            </li>
        <?php }}}?>




<?php //include('../../layout/transparent_footer.php'); ?>
