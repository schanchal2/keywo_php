<?php
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

//include dependent files

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php"); //echo "Testing";

error_reporting(0);

$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$dbsearch = createDBConnection('dbsearch'); //echo "<pre>"; print_r($dbsearch); echo "</pre>";

if(noError($dbsearch)){
    $dbsearch = $dbsearch["connection"];
}else{
    print_r("Database Error");
}


$result = get_all_slab($conn);
$result = $result["errMsg"];

//print_r($result);
if(empty($result))
{
    ?>

    <label> No Data Available </label>

<?php } else{

$resultdata = getCurrentActiveSlab($conn); //print_r($resultdata);
$currentSlab = $resultdata["errMsg"]["id"];
foreach($result as $value) {


    ?>

    <!-- li starts here -->
    <li style="<?php if($currentSlab==$value["id"]) {echo "background:#4ea8d2;";} ?>">
        <!-- row starts here   -->
        <div class="row">
            <div class="col-md-3 text-black margin-bottom-none">
                <label class="text-black margin-bottom-none"><?php echo $value["start_limit"]; ?></label>
            </div>
            <div class="col-md-3 text-black margin-bottom-none">
                <label class="text-black margin-bottom-none"><?php echo $value["end_limit"]; ?></label>
            </div>
            <div class="col-md-3 text-black margin-bottom-none">
                <label class="text-black margin-bottom-none"><?php echo number_format($value["ITD_Price"],2); ?></label>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="text-black margin-bottom-none">
                        <?php
                        $result=getCurrencyExchangeRate($exchageCurrencyCode, $dbsearch);
                        $itdPrice=$result["exchange_rate"]["usd"]; //echo $itdPrice."<br>";
                        $btcPrice=$result["exchange_rate"]["btc"]; //echo $btcPrice."<br>";
                        echo number_format($value["ITD_Price"]*$btcPrice,8);

                        ?>
                    </label>
                </div>
            </div>
            <!-- row ends here -->
        </div>
    </li>

    <!-- li ends here -->
    <?php
}}
    ?>

<li style="display:none;"></li>
