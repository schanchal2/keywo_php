
<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
error_reporting(0);

$email = $_SESSION["email"];

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    exit;
}

$pageUrl = "views/keywords/analytics/keyword_analytics.php";
//echo $_SESSION['curr_pref'];

?>

    <?php
    $getLatestAsk = getLatestAsk($conn);
    $result       = $getLatestAsk["errMsg"];
    if(empty($result))
    {
        ?>

        <li> <div><center>No Data Available</div></li>

    <?php } else{

    $i  = 0;
    foreach ($result as $key =>$value) {
    $askPrice = $value["ask_price"];
    $keyword = $value['keyword'];
    $i++;
    if ($i < 7) {
        ?>


        <!-- li starts here -->

        <li>
            <div class="comn-lft-rght-cont">

                <label class="ellipses-market-trade-label"> <a data-original-title="<?php echo $keyword; ?>" title="<?php echo $keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?>
                    </a> </label>

                <span class="pull-right ellipses-market-trade-span text-right">&nbsp;<a href="javascript:;" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                        origPrice="<?php echo "{$askPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$askPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                 <div class="clearfix"></div>
            </div>

    </li>

    <?php  }}}?>


    <li class="bg-dark-grey">
        <div class="common-on-hover">
            <span><a href="javascript:;" onclick="askKeywords();"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></span>
            <?php if($i>7){ ?>
            <span class="pull-right"><a href="view_ask_keyword.php">View More</a></span> <?php }?>
            <div class="clearfix"></div>
        </div>
    </li>