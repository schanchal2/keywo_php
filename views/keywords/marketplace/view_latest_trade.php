<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
//require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php"); //echo "Testing "; die;
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/ViewallShortingModel.php");
error_reporting(0);

$email = $_SESSION["email"]; //echo $email; die;

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>"; die;

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    header("Location: $rootUrl./views/prelogin/index.php");


}

include('../../layout/header.php');

$pageUrl = "views/keywords/analytics/keyword_analytics.php";

?>
<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<!-- date picker css js -->
<script type="text/javascript" src="../../../frontend_libraries/momentjs/moment.min.js"></script>
<script type="text/javascript" src="../../../frontend_libraries/datepicker/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="../../../frontend_libraries/datepicker/daterangepicker.css" />
<!-- date picker css js -->
<style>
    #ellipses-viewall .ellipses-market-trade-label{width:100%!important;}
    .keymarketplace-data ul li{ padding: 8.3px 18px;}
</style>
<main class="main-keyword inner-6x innerT" id="ellipses-viewall">
    <!-- keyword-marketplace starts here -->
    <div class="keyword-marketplace keyword-marketplace-data keyword-markt-popup-common">

        <!-- container starts here -->
        <div class="container">
            <!-- row starts here -->
            <div class="row">
                <div class="col-xs-3">
                    <!-- keymarketplace-data-left starts here -->
                    <div class="keymarketplace-data key-mar-data-floatingmenu">
                        <div class="row">
                            <div class="col-xs-12"><div class="back-to-keymar">
                                    <a href="keyword_search.php"><i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;&nbsp;Back to Keyword Market</a>
                                </div></div>
                        </div>

                        <!-- row starts here -->
                        <div class="row">
                            <!-- trade analytics col starts here -->
                            <div class="col-md-12 inner-3x innerMT">
                                <div class="list-group list-cust border-all anchorTabID">
                                    <a href="#" class="list-group-item">
                                        Trade Analytics
                                    </a>
                                    <a href="#" class="list-group-item text-primary active" id="latestradeTab">Latest Trade</a>
                                    <a href="view_ask_keyword.php" class="list-group-item" id="askTab">Ask</a>
                                    <a href="view_bid_keyword.php" class="list-group-item" id="bidTab">Bid</a>
                                    <a href="view_newowned_keyword.php" class="list-group-item" id="newownedTab">Newly Owned</a>
                                </div>
                            </div>
                            <!-- trade analytics col ends here -->


                            <!-- interaction analytics col starts here -->
                            <div class="col-md-12 margin-top-none">
                                <div class="list-group list-cust border-all">
                                    <a href="#" class="list-group-item">
                                        Interaction Analytics
                                    </a>
                                    <a href="view_interaction_analytics.php" class="list-group-item text-primary">All</a>
                                    <a href="interactionBlog.php" class="list-group-item">Blog</a>
                                    <a href="interactionVideo.php" class="list-group-item">Video</a>
                                    <a href="interactionImage.php" class="list-group-item">Image</a>
                                    <a href="interactionAudio.php" class="list-group-item">Audio</a>
                                    <!--<a href="#" class="list-group-item">Search</a>-->

                                </div>
                            </div>
                            <!-- interaction analytics col ends here -->

                            <!-- earning analytics col starts here -->
                            <div class="col-md-12 margin-top-none">
                                <div class="list-group list-cust border-all">
                                    <a href="#" class="list-group-item">
                                        Earning Analytics
                                    </a>
                                    <a href="view_earning_analytics.php" class="list-group-item text-primary">All</a>
                                     <a href="earningBlog.php" class="list-group-item">Blog</a>
                                     <a href="earningVideo.php" class="list-group-item">Video</a>
                                     <a href="earningImage.php" class="list-group-item">Image</a>
                                     <a href="earningAudio.php" class="list-group-item">Audio</a>
                                    <!--<a href="#" class="list-group-item">Search</a>-->

                                </div>
                            </div>
                        </div>
                        <!-- row ends here -->
                    </div>
                    <!-- keymarketplace-data-left ends here -->
                </div>

                <div class="col-xs-9 half innerL">
                    <!-- keymarketplace-data-right starts here -->
                    <div class="keymarketplace-data key-mar-data-floatingmenu">
                        <h3 class="heading-key-markt margin-top-none innerMB marg-botm">Keyword Market Data</h3>
                        <!-- pagination starts here -->
                        <div class="pagination-content text-center">
                            <!-- pagination head starts here -->
                            <div class="pagination-head half inner-3x innerAll padding-key-market-data">
                                <div class="row">
                                    <div class="col-xs-4 text-white text-left">keywords</div>
                                    <div class="col-xs-3 text-white">Trade Price</div>
                                    <div class="col-xs-5 text-white padding-left-none padding-right-none">
                                        <!-- keyword-radio-btn starts here -->
                                        <div class="keyword-radio-btn">
                                            <div class="row">
                                                <div class="col-xs-11 padding-right-none pull-right">
                                                    <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                                        <input type="radio" name="shorting" id="latest" value="" checked="true">
                                                        <label for="latest">
                                                            Latest
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                                        <input type="radio" name="shorting" id="highest" value="">
                                                        <label for="highest">
                                                            Highest
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                                        <input type="radio" name="shorting" id="lowest" value="">
                                                        <label for="lowest" class="padding-right-none">
                                                            Lowest
                                                        </label>
                                                    </div>

                                                    <!-- <input type="text" class="form-control text-Blue input-calender calender-r-mar pull-right margin-right-none" name="daterange" value=""/>-->
                                                    <input type="text" class="form-control text-Blue input-calender calender-r-mar pull-right margin-right-none" name="datefilter" value="" />

                                                </div>
                                            </div>
                                        </div>
                                        <!-- keyword-radio-btn ends here     -->
                                    </div>
                                </div>
                            </div>
                            <!-- pagination head ends here -->

                            <!-- pagination body starts here -->
                            <div id="latestTradeAjaxdata">



                            </div>
                        </div>
                        <!-- pagination ends here -->
                    </div>
                    <!-- keymarketplace-data-right ends here -->
                </div>
            </div>
            <!-- row ends here -->
        </div>
    </div>
    <!-- keyword-marketplace ends here -->
</main>

 <div id="callPageAjaxData" class="col-xs-12">
                                </div>

<?php include('../../layout/transparent_footer.php'); ?>
<!-- date picker css ends here-->
<script type="text/javascript">
    $(function() {

        $('input[name="datefilter"]').daterangepicker();
        //  var id = $(this).attr("id"); alert(id);
    });
</script>

<script src="../../../js/marketplace.js"></script>

<script>

    $(document).ready(function(){
        getTradeData();
    });

 var id = "latest";
 var type = "trade_time";
 orderby = "DESC";

    var email = '<?php echo $email; ?>';
    var limit = ($("#LimitedResultData").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 14;
    }
    pageNo = 1;
    startDate = "";
    endDate = "";

    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        alert("cancel");
    });

    $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
        // alert(viewAllFilter);
        console.log(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');
        //endDate: moment().startOf('day').toDate(),

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {

                email     : email,
                limit     : limit,
                tableName : "latest_trades",
                id        : id,
                orderby   :  orderby,
                startDate : startDate,
                endDate   : endDate,
                keywordPrice : "trade_price",
                type      : type
            },
            success: function(data) {
                $("#latestTradeAjaxdata").html("");
                $("#latestTradeAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });
    });

    $("input[name = 'shorting']").click(function(){
        var id = $(this).attr("id");
        if(id == "highest")
        {
            type = "trade_price";
            orderby= "DESC";
        }
        else if(id == "lowest")
        {
            type = "trade_price";
            orderby= "ASC";
        }
        else { type = "trade_time"; orderby = "ASC";}

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {
                limit        : limit,
                page         : pageNo,
                tableName    : "latest_trades",
                id           : id,
                orderby      : orderby,
                keywordPrice : "trade_price",
                startDate    : startDate,
                endDate      : endDate,
                type         : type
            },
            success: function(data) {
                $("#latestTradeAjaxdata").html("");
                $("#latestTradeAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    });

    function getTradeData() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {

                limit        : limit,
                page         : pageNo,
                tableName    : "latest_trades",
                id           : id,
                orderby      : orderby,
                keywordPrice : "trade_price",
                startDate    : startDate,
                endDate      : endDate,
                type         : type

            },
            success: function(data) {
                $("#latestTradeAjaxdata").html("");
                $("#latestTradeAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }

    function getNewTradeData(pageNo,type,id,startDate,endDate,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {
                limit        : limit,
                page         : pageNo,
                tableName    : "latest_trades",
                id           : id,
                orderby      : orderby,
                keywordPrice : "trade_price",
                startDate    : startDate,
                endDate      : endDate,
                type         : type
            },
            success: function(data) {
                $("#latestTradeAjaxdata").html("");
                $("#latestTradeAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }
</script>

<!-- date pciker css starts here-->
