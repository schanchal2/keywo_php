<?php

session_start();

ini_set('default_charset','utf-8');
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
error_reporting(0);

$email = $_SESSION["email"];

$conn = createDBConnection("dbkeywords");
if(noError($conn))
{
    $conn = $conn["connection"];
}
else
{
    print_r("Database Error");
}

$searchQuery = $_POST["keyword"];

if($_GET["status"] != "H"){
    $searchQueryFinal    = cleanXSS($searchQuery);
}else{
    $searchQueryFinal    = cleanXSS($searchQuery);
}

if(isset($searchQueryFinal) && !empty($searchQueryFinal)){
    $searchQueryFinal = optimizeSearchQuery($searchQueryFinal);
    $searchQueryFinalKeyword    = explode(" ",$searchQueryFinal);

}

$userCartDetails = getUserCartDetails($email, $conn);

if(noError($userCartDetails)){
    $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
    $userCartDetails = json_decode($userCartDetails, TRUE);
}else{
    print('Error: Fetching cart details');
    exit;
}

?>

<ul>
    <?php

    $pageUrl = "views/keywords/analytics/keyword_analytics.php";

    $counter = 0;
    foreach($searchQueryFinalKeyword as $keyNo => $keyword) {

    $checkForRevenewAvailability = getRevenueDetailsByKeyword($conn,$keyword);
    if(noError($checkForRevenewAvailability)){

    $checkForRevenewAvailability = $checkForRevenewAvailability["data"][0];
    $totalPaidSearch = $checkForRevenewAvailability["user_kwd_search_count"];
    $totalKeywordEarning = $checkForRevenewAvailability["user_kwd_ownership_earnings"];
    $CountFollowedKeyword  = $checkForRevenewAvailability["follow_unfollow"];

    $keywordFollowerCount  = json_decode($CountFollowedKeyword, true);
    $keywordFollowerCounts = $keywordFollowerCount["email"];
    $keywordFollowerCount  = count($keywordFollowerCounts);



    $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

    if(noError($checkForKeywordAvailability)){

    $availabilityFlag = $checkForKeywordAvailability['errMsg']; //echo $availabilityFlag;
    $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
    $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
    $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
    $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
    $CartStatus = $checkForKeywordAvailability['status'];
    $activeBids = $checkForKeywordAvailability["active_bids"];

    if(isset($totalPaidSearch) && !empty($totalPaidSearch)){
        $totalPaidSearch = $totalPaidSearch;
    }else{
        $totalPaidSearch = 0;
    }

    if(isset($totalKeywordEarning) && !empty($totalKeywordEarning)){
        $totalKeywordEarning = $totalKeywordEarning;
    }else{
        $totalKeywordEarning = "0.0000";
    }

    if($availabilityFlag == 'keyword_not_available'){

        $activeBids = json_decode($activeBids, true);
        foreach($activeBids as $key => $bidValue){
            $bidValue = explode('~~', $bidValue);
            $bidderEmail[] = $bidValue[1];

        }
        if(in_array($email, $bidderEmail)){
            $bidStatus = true;
        }else{
            $bidStatus = false;
        }

        $counter++;

    ?>

        <li class="keywordnotavailable">
            <div class="row">
                <div class="col-md-3">
                    <div class="text-left">
                        <?php
                        $followedBy = 123;
                        if(isset($followedBy) && !empty($followedBy)){
                            $followedBy = $followedBy." Users";
                        }else{
                            $followedBy = 0;
                        }
                        ?>
                        <h5 class="text-blue keyword-name ellipses"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                        <label class="keyword-grey pull-left">Followed by : </label>
                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<?php echo $keywordFollowerCount." User"; ?></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="text-left">
                            <div class="pull-left col-xs-12">
                                <label class="keyword-grey pull-left">Interaction : </label>
                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="<?php  echo $totalPaidSearch; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                            </div>
                            <div class="pull-left col-xs-12">
                                <label class="keyword-grey pull-left">Earning : </label>
                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                            origPrice="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="text-left">
                            <div class="pull-left col-xs-12 padding-right-none padding-left-none">
                                <label class="keyword-grey pull-left">Current Bid : </label>
                                <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                origPrice="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                <?php } else{ ?>
                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                <?php } ?>
                            </div>
                            <div class="pull-left col-xs-12 padding-right-none padding-left-none">
                                <label class="keyword-grey pull-left">Asking Price : </label>
                                <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                origPrice="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                <?php } else{ ?>
                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 pull-right">
                    <div class="row">
                        <div class="text-right">
                            <div class="pull-left col-xs-6 col-md-12 padding-right-none">
                                <?php

                                if($email == $kwdOwnerId){
                                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                        ?>
                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                                        </div>
                                        <?php
                                    }else{
                                        // set ask
                                        ?>
                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                        </div>
                                        <?php
                                    }

                                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                        ?>
                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                            <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                                        </div>
                                        <?php
                                    }
                                }else{

                                    if($CartStatus == "sold"){

                                        if(empty($activeBids)){
                                            ?>
                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                                            </div>
                                            <?php
                                        }else{
                                            if($bidStatus){
                                                ?>
                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                                                </div>
                                                <?php
                                            }else{
                                                ?>
                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                                                </div>
                                                <?php
                                            }
                                        }

                                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                            ?>
                                            <div class="pull-left col-xs-6 col-md-12 innerMB ">
                                                <button class="btn-trading-dark"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                                            </div>
                                            <?php
                                        }

                                    }
                                }

                                ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

    <?php }}}}
    if($counter == 0)
    {
    ?>
    <li align="center">
        Record Not Found
    </li>
    <?php } ?>

</ul>
