<?php
session_start();

ini_set('default_charset','utf-8');
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

//include dependent files

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/acceptBidModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/landing/landingFeatureModel.php"); //echo "testing"; die;
require_once("{$docrootpath}helpers/sessionHelper.php");

error_reporting(0);

// fetch user email from session
$email = $_SESSION["email"];

// get current session id
$clientSessionId = session_id();

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

checkForSession($conn);

//Validating User LoggedIn and LoggedOUt status

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    /* Check user ftue status */
    $getFtUserDetail = getFirstTimeUserStatus($conn,$email,$_SESSION['id']);
    if(noError($getFtUserDetail)){
        $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
        if($row['ftue_status'] == 1){

            if(isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_code"])){
                $getErrorCode = $_SESSION["get_error_code"];

                if($getErrorCode == -1){
                    $successMsg = $_SESSION["get_error_message"];
                }else{
                    $failedMsg = $_SESSION["get_error_message"];
                }

                // unset $_SESSION["get_error_code"]
                unset($_SESSION["get_error_code"]);
                unset($_SESSION["get_error_message"]);
            }

            include("{$docrootpath}views/layout/header.php");
            include("{$docrootpath}views/keywords/marketplace/tradePopupDialogBox.php");
            $login_status = 1;

            $searchQuery = trim(urldecode($_GET["q"]));


            if($_GET["status"] != "H"){
                $searchQueryFinal    = cleanXSS($searchQuery);
            }else{
                $searchQueryFinal    = cleanXSS($searchQuery);
            }

            //if($searchQuery !== ','){
            //    $searchQueryFinal   = str_replace(",", " ", $searchQueryFinal); //echo $searchQueryFinal;
            //}

            if(isset($searchQueryFinal) && !empty($searchQueryFinal)){
                $searchQueryFinal = optimizeSearchQuery($searchQueryFinal);
                $searchQueryFinalKeyword    = explode(" ",$searchQueryFinal);

                if(count($searchQueryFinalKeyword) >= 10){
                    $searchQueryFinalKeyword = array_slice($searchQueryFinalKeyword, 0, 10, true);
                }
            }

            // get user cart details
            $userCartDetails = getUserCartDetails($email, $conn);

            if(noError($userCartDetails)){
                $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
                $userCartDetails = json_decode($userCartDetails, TRUE);
            }else{
                print('Error: Fetching cart details');
                exit;
            }

            ?>
            <link rel="stylesheet" href="
                <?php echo $rootUrlCss; ?>app_keyword.css
                <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />

            <!--================================================
            =            Section keywo-954 solution            =
            =================================================-->
            <!--     <style type="text/css">
                .popover {
                    padding: 10px 0;
                    max-width: 484px;
                }

                .popover-content {
                    width: 484px;
                    max-height: 200px;
                    overflow-y: scroll;
                    word-break: break-all;
                }
                </style> -->
            <!--====  End of Section keywo-954 solution  ====-->



            <!-- container starts here -->
            <main class="main-keyword inner-6x innerT">
                <div class="keyword-marketplace keyword-markt-popup-common">
                    <div class="container row-10">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="heading-key-markt margin-none">Marketplace - Buy and Trade Keywords</h3>
                            </div>
                        </div>
                        <br>
                        <!-- <h3>Marketplace - Buy and Trade Keywords</h3> -->
                        <div class="row position-relative">
                            <div class="col-md-8">
                            <p class="errMsg"></p>
                                <!-- search bar starts here -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <form id="keyword_search_form">
                                                    <input class="search-query form-control ellipses inner-3x innerR marketplace xssValidation" id="keyword_search" data-type="2" name="q" type="text" placeholder=" Search for multiple keywords separated by a space or comma to check their availability" value="<?php  echo cleanDisplayParameter($conn,$searchQuery) ;?>">
                                                    <span class="input-group-btn">
                                                <button class="btn btn-danger" onclick="searchResult();" type="button">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                                </span>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- search bar ends here -->
                                <!-- right-side-scroll starts here -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="right-side-scroll suggested">
                                            <!-- ul li starts here -->
                                            <ul class="search-box margin-none" id="keyword-ellipses">
                                                <?php
                                                if(!empty($searchQueryFinalKeyword)){

                                                    $pageUrl = "views/keywords/analytics/keyword_analytics.php";

                                                    foreach($searchQueryFinalKeyword as $keyNo => $keyword) {

                                                        $checkForRevenewAvailability = getRevenueDetailsByKeyword($conn,$keyword); //echo "<pre>"; print_r($checkForRevenewAvailability); echo "</pre>";
                                                        if(noError($checkForRevenewAvailability)){

                                                            $checkForRevenewAvailability = $checkForRevenewAvailability["data"][0];
                                                            $totalPaidSearch = $checkForRevenewAvailability["user_kwd_search_count"]; //echo "Paid search ".$totalPaidSearch."<br />";
                                                            $totalKeywordEarning = $checkForRevenewAvailability["user_kwd_ownership_earnings"];
                                                            $CountFollowedKeyword  = $checkForRevenewAvailability["follow_unfollow"];

                                                            $keywordFollowerCount  = json_decode($CountFollowedKeyword, true);
                                                            $keywordFollowerCounts = $keywordFollowerCount["email"];
                                                            $keywordFollowerCount  = count($keywordFollowerCounts);



                                                            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

                                                            //printArr($checkForKeywordAvailability);
                                                            if(noError($checkForKeywordAvailability)){

                                                                $availabilityFlag = $checkForKeywordAvailability['errMsg']; //echo $availabilityFlag;
                                                                $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                                                $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
                                                                $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
                                                                $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                                                                $CartStatus = $checkForKeywordAvailability['status'];
                                                                $activeBids = $checkForKeywordAvailability["active_bids"];

                                                                if(isset($totalPaidSearch) && !empty($totalPaidSearch)){
                                                                    $totalPaidSearch = $totalPaidSearch;
                                                                }else{
                                                                    $totalPaidSearch = 0;
                                                                }

                                                                if(isset($totalKeywordEarning) && !empty($totalKeywordEarning)){
                                                                    $totalKeywordEarning = $totalKeywordEarning;
                                                                }else{
                                                                    $totalKeywordEarning = "0.0000";
                                                                }

                                                                if($availabilityFlag == 'keyword_available'){

                                                                    ?>
                                                                    <li class="keywordavailable">
                                                                        <!-- row starts here -->
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <div class="text-left">
                                                                                    <h5 class="text-blue keyword-name ellipses"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                                    <!--========================================
                                                                                    =            keywo-954 solution            =
                                                                                    =========================================-->
                                                                                    <!--      <h5 class="text-blue keyword-name ellipses">
                                                                <a class="display-in-block-txt-blk popover--keyword"
                                                                onclick="dialogsbox('<?= urlencode($keyword); ?>');"
                                                                data-container="body"
                                                                data-placement="right"
                                                                data-content="<?= '#'.$keyword; ?>"
                                                                href="<?= $rootUrl.$pageUrl; ?>?q=<?= $keyword; ?>"
                                                                target="_blank">#<?= $keyword; ?></a></h5> -->
                                                                                    <!--====  End of keywo-954 solution  ====-->
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="row">
                                                                                    <div class="text-left">
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Interaction : </label>
                                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="<?php  echo $totalPaidSearch; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="row">
                                                                                    <div class="text-left">
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Earning : </label>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                origPrice="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                            </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 pull-right">
                                                                                <div class="row">
                                                                                    <div class="text-right">
                                                                                        <div class="pull-left col-xs-6 col-md-12">
                                                                                            <?php if(in_array($keyword, $userCartDetails)){ ?>
                                                                                                <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                                                            <?php }else{ ?>
                                                                                                <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- row ends here -->
                                                                    </li>
                                                                    <?php

                                                                }elseif($availabilityFlag == 'keyword_not_available'){
                                                                    $bidderEmail = array();

                                                                    $activeBids = json_decode($activeBids, true);
                                                                    foreach($activeBids as $key => $bidValue){
                                                                        $bidValue = explode('~~', $bidValue);
                                                                        $bidderEmail[] = $bidValue[1];
                                                                    }

                                                                    if(in_array($email, $bidderEmail)){
                                                                        $bidStatus = true;
                                                                    }else{
                                                                        $bidStatus = false;
                                                                    }

                                                                    ?>
                                                                    <li class="keywordnotavailable">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <div class="text-left">
                                                                                    <?php
                                                                                    $followedBy = 123;
                                                                                    if(isset($followedBy) && !empty($followedBy)){
                                                                                        $followedBy = $followedBy." Users";
                                                                                    }else{
                                                                                        $followedBy = 0;
                                                                                    }
                                                                                    ?>
                                                                                    <h5 class="text-blue keyword-name ellipses"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                                    <label class="keyword-grey pull-left">Followed by : </label>
                                                                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<?php echo $keywordFollowerCount." User"; ?></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="row">
                                                                                    <div class="text-left">
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Interaction : </label>
                                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="<?php  echo $totalPaidSearch; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                                                        </div>
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Earning : </label>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                        origPrice="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="col-md-6">

                                                                                <div class="row">
                                                                                    <div class="text-left" >
                                                                                        <div class="pull-left col-md-6 padding-right-none padding-left-none">
                                                                                            <label class="keyword-grey pull-left">Current Bid : </label>
                                                                                            <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                            origPrice="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                            <?php } else{ ?>
                                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                            origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="text-right">
                                                                                        <div class="pull-left col-md-6 padding-right-none padding-left-none">

                                                                                            <?php
                                                                                                if($email == $kwdOwnerId){

                                                                                                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                                                                        ?>
                                                                                                        <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                            <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    }
                                                                                                }else{
                                                                                                    if($CartStatus == "sold"){
                                                                                                        if(empty($activeBids)){
                                                                                                            ?>
                                                                                                            <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                                                                                                            </div>
                                                                                                            <?php
                                                                                                        }else{
                                                                                                            if($bidStatus){
                                                                                                                ?>
                                                                                                                <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                                                                                                                </div>
                                                                                                                <?php
                                                                                                            }else{
                                                                                                                ?>
                                                                                                                <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                                                                                                                </div>
                                                                                                                <?php
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <!-- ask price div -->
                                                                                <div class="row">
                                                                                    <div class="pull-left col-xs-6 padding-right-none padding-left-none">
                                                                                        <label class="keyword-grey pull-left">Asking Price : </label>
                                                                                        <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                        origPrice="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                        <?php } else{ ?>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                        origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                        <?php } ?>
                                                                                    </div>

                                                                                   <div class="text-right">
                                                                                       <div class="pull-left col-md-6 padding-right-none padding-right-none">
                                                                                       <?php
                                                                                       if($email == $kwdOwnerId){
                                                                                           if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                                                               ?>
                                                                                               <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                   <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                                                                                               </div>
                                                                                               <?php
                                                                                           }else{
                                                                                               // set ask
                                                                                               ?>
                                                                                               <div class="pull-left col-xs-12 col-md-12 innerMB">
                                                                                                   <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                                                                               </div>
                                                                                               <?php
                                                                                           }
                                                                                       }else{
                                                                                           if($CartStatus == "sold"){
                                                                                               if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                                                                   ?>
                                                                                                   <div class="pull-left col-xs-12 col-md-12 innerMB ">
                                                                                                       <button class="btn-trading-dark"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                                                                                                   </div>
                                                                                                   <?php
                                                                                               }
                                                                                           }
                                                                                       }
                                                                                       ?>
                                                                                       </div>
                                                                                   </div>
                                                                                </div>
                                                                                <!-- ask price div end-->
                                                                            </div>
                                                                            <div class="clearfix"></div>

                                                                        </div>
                                                                    </li>
                                                                    <?php

                                                                    $bidderEmail = array();

                                                                }elseif($availabilityFlag == "keyword_blocked"){
                                                                    ?>
                                                                    <li class="block">
                                                                        <!-- row starts here -->
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <div class="text-left">
                                                                                    <h5 class="text-blue keyword-name ellipses"><a class="display-in-block-txt-blk" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom">#<?php echo $keyword; ?></a></h5>
                                                                                    <label class="keyword-grey pull-left">Followed by : </label>
                                                                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;<?php echo $keywordFollowerCount." User"; ?></span>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="row">
                                                                                    <div class="text-left">
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Interaction : </label>
                                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <div class="row">
                                                                                    <div class="text-left">
                                                                                        <div class="pull-left col-xs-12">
                                                                                            <label class="keyword-grey pull-left">Earning : </label>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                        origPrice="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 pull-right">
                                                                                <div class="row">
                                                                                    <div class="text-right">
                                                                                        <div class="pull-left col-xs-6 col-md-12 inner-3x innerML text-center">
                                                                                            <label class="keyword-grey pull-left"></label>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;Not Available </span>
                                                                                            <!--                                                                                <input value="Unavailable" type="button" class="btn-trading-dark"/>-->
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <?php
                                                                }

                                                            }else if($checkForKeywordAvailability["errCode"] == 4){ ?>
                                                                <li class="block">
                                                                    <!-- row starts here -->
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <div class="text-left">
                                                                                <h5 class="text-blue keyword-name ellipses"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                                <label class="keyword-grey pull-left">Followed by : </label>
                                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<?php echo $keywordFollowerCount." User"; ?></span>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="row">
                                                                                <div class="text-left">
                                                                                    <div class="pull-left col-xs-12">
                                                                                        <label class="keyword-grey pull-left">Interaction : </label>
                                                                                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>

                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="row">
                                                                                <div class="text-left">
                                                                                    <div class="pull-left col-xs-12">
                                                                                        <label class="keyword-grey pull-left">Earning : </label>
                                                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                                                    origPrice="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 pull-right">
                                                                            <div class="row">
                                                                                <div class="text-right">
                                                                                    <div class="pull-left col-xs-6 col-md-12 inner-3x innerML text-center">
                                                                                        <label class="keyword-grey pull-left"></label>
                                                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;Not Available </span>
                                                                                        <!--                                                                                <input value="Unavailable" type="button" class="btn-trading-dark"/>-->
                                                                                    </div>
                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php }else{
                                                                print('Error: check keyword availability');
                                                            }
                                                        }else{
                                                            print('Error: fetching keyword revenue details');
                                                        }

                                                    } // end of foreach loop
                                                } // end of keyword count validation

                                                ?>
                                                <?php

                                                if (empty($searchQueryFinal))
                                                { ?>
                                                    <!-- row starts here -->
                                                    <div id="SuggestedKeywordsLoad">
                                                    </div>
                                                <?php } ?>
                                            </ul>
                                            <!-- ul li ends here -->
                                        </div>
                                        <div class="footer-right-side-scroll padding-none">
                                            <div class="row">
                                                <form id="test">
                                                <div class="col-md-6  innerT half">
                                                    <div class="keyword-radio-btn">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="keyword-sorting-update" id="suggestedlisting">
                                                                    <?php
                                                                    if (isset($searchQueryFinal) && !empty($searchQueryFinal))
                                                                    { ?>
                                                                        <div class="radio radio-primary radio-div">
                                                                            <input type="radio" name="ownerFilter" class="filter_radio_btn" onclick="allKeyword('<?php echo $searchQueryFinal; ?>')" id="all" value="" checked="checked">
                                                                            <label for="all">
                                                                                All
                                                                            </label>
                                                                        </div>
                                                                        <div class="radio radio-primary radio-div">
                                                                            <input type="radio" name="ownerFilter" class="filter_radio_btn" onclick="unownedKeyword('<?php echo $searchQueryFinal; ?>')" id="unowned" value="">
                                                                            <label for="unowned">
                                                                                Unowned
                                                                            </label>
                                                                        </div>
                                                                        <div class="radio radio-primary radio-div">
                                                                            <input type="radio" name="ownerFilter" class="filter_radio_btn" onclick="ownedKeyword('<?php echo $searchQueryFinal; ?>')" id="owned" value="">
                                                                            <label for="owned">
                                                                                Owned
                                                                            </label>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 height-30">
                                                    <div class="footerul-r-side-scrl innerAll">
                                                        <ol class="breadcrumb pull-right">
                                                            <li><a href="#">Agreement</a></li>
                                                            <li><a href="#">FAQ's</a></li>
                                                            <?php
                                                            if (empty($searchQueryFinal))
                                                            { ?>
                                                                <li><a href="#">How it Works</a>&nbsp;&nbsp;<a href="javascript:;" onclick="SuggestedKeywords(this);"><i class="fa fa-refresh fa-1x fa-fw" aria-hidden="true"></i></a></li>
                                                            <?php } else{?>
                                                                <li><a href="#">How it Works</a>&nbsp;&nbsp;<a href="javascript:;" id='loader' onclick="SearchedKeywords('<?php echo $searchQueryFinal; ?>');"><i class="fa fa-refresh fa-1x fa-fw" aria-hidden="true"></i></a></li>
                                                            <?php } ?>
                                                        </ol>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- right-side-scroll ends here -->
                            </div>
                            <div class="col-md-4">
                                <?php

                                // get current active slab
                                $resultdata = getCurrentActiveSlab($conn);
                                if(noError($resultdata)){
                                    $resultdata = $resultdata["errMsg"];
                                    $kwdCurrentPrice = $resultdata["ITD_Price"];
                                    $currentSlab = $resultdata["start_limit"]."-".$resultdata["end_limit"];

                                    // get total number of keyword sold
                                    $totalKeywordSold = getTotalKeyword_used($conn);
                                    if(noError($totalKeywordSold)){
                                        $totalKeywordSold = $totalKeywordSold["errMsg"]["total_used_kwd"];

                                        // get total keyword owner
                                        $totalKwdOwner = getAllMyKeywordOwnersCount($conn);
                                        if(noError(($totalKwdOwner))){
                                            $totalKwdOwner = $totalKwdOwner["errMsg"]["kwd_owner_count"];

                                            // get total number of trade so far
                                            $totalKwdTrade = getHighestTradeCount($conn);
                                            if(noError($totalKwdTrade)){
                                                $totalKwdTrade = $totalKwdTrade["errMsg"]["count"];

                                                // get total trade profit so far
                                                $totalTradeProfit = getTotalTradeProfitSoFar($conn);
                                                if(noError($totalTradeProfit)){
                                                    $totalTradeProfit = $totalTradeProfit["errMsg"]["total_trade_profit"];

                                                    // get yesterday total interaction and earning details.
                                                    $yesterdayEarningNInteraction = getKeywordYesterdayEarningIntraction($conn);
                                                    if(noError($yesterdayEarningNInteraction)){
                                                        $yesterdayEarningNInteraction = $yesterdayEarningNInteraction["errMsg"];
                                                        $yesterdayTotalInteraction = $yesterdayEarningNInteraction["total_interaction_count"];
                                                        $yesterdayTotalPayout = $yesterdayEarningNInteraction["total_interaction_earning"];

                                                        if(empty($yesterdayTotalInteraction)){
                                                            $yesterdayTotalInteraction = 0;
                                                        }

                                                        if(empty($yesterdayTotalPayout)){
                                                            $yesterdayTotalPayout = number_format(0, 4);
                                                        }

                                                        // get today total ineraction and earning
                                                        $lifetimeInteractionNEarning = getKeywordLifetimeEarningIntraction($conn);
                                                        if(noError($lifetimeInteractionNEarning)){
                                                            $lifetimeInteractionNEarning = $lifetimeInteractionNEarning["errMsg"];
                                                            $lifetimeIneraction = $lifetimeInteractionNEarning["keyword_lifeTime_intractions"];
                                                            $lifetimePayout = $lifetimeInteractionNEarning["keyword_lifeTime_earnings"];

                                                            if(empty($lifetimeIneraction)){
                                                                $lifetimeIneraction = 0;
                                                            }

                                                            if(empty($lifetimePayout)){
                                                                $lifetimePayout= number_format(0, 4);
                                                            }
                                                        }else{
                                                            print('Error: Unable to get lifetime earning and interaction details');
                                                            exit;
                                                        }
                                                    }else{
                                                        print('Error: Unable to get yesterdays earning and interaction details');
                                                        exit;
                                                    }
                                                }else{
                                                    print('Error: Unable to get total trade profit');
                                                    exit;
                                                }
                                            }else{
                                                print('Error: Unable to get latest trade count');
                                                exit;
                                            }
                                        }else{
                                            print('Error: Unable to get keyword owner');
                                            exit;
                                        }
                                    }else{
                                        print('Error: Unable to get total sold keyword');
                                    }
                                }else{
                                    print('Error: Unable to get slab details');
                                    exit;
                                }

                                if(!isset($kwdCurrentPrice)){
                                    $kwdCurrentPrice = 0.00;
                                }

                                ?>
                                <!-- row keyword-info-blocks div starts here -->
                                <div class="row keyword-info-blocks">
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key ellipses">
                                                <a onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                            origPrice="<?php echo number_format("{$kwdCurrentPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdCurrentPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom" style="color:white"><?php echo formatNumberToSort("{$kwdCurrentPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                            </div>
                                            <div class="keyword-num-status">current price</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key-dark">
                                                <?php echo (isset($currentSlab) && !empty($currentSlab))? $currentSlab : 0;  ?>
                                            </div>
                                            <div class="keyword-num-status light-blue-col"><a type="button" class="btn-trading-wthout-bg" data-toggle="modal" data-target="#keyword-popup-markt-slab" onclick="getSlabList()">current slab <i class="fa fa-link" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key-dark">
                                                <span style="color:white" title="<?= isset($totalKeywordSold)?($totalKeywordSold):0 ?>" data-toggle="tooltip" data-placement="bottom"><?= isset($totalKeywordSold)?($totalKeywordSold):0 ?></span>
                                            </div>
                                            <div class="keyword-num-status light-blue-col">keyword Sold</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key">
                                                <span style="color:white" title="<?= isset($totalKwdOwner)?($totalKwdOwner):0 ?>" data-toggle="tooltip" data-placement="bottom"><?= isset($totalKwdOwner)?($totalKwdOwner):0 ?></span>
                                            </div>
                                            <div class="keyword-num-status">Keywords owners</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key">
                                                <span style="color:white" title="<?= isset($totalKwdTrade)?($totalKwdTrade):0 ?>" data-toggle="tooltip" data-placement="bottom"><?= isset($totalKwdTrade)?($totalKwdTrade):0 ?></span>
                                            </div>
                                            <div class="keyword-num-status">Keywords traded</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <?php
                                    if(!isset($totalTradeProfit)){
                                        $totalTradeProfit = 0.00;
                                    }
                                    ?>
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key-dark ellipses">
                                                <a onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                   origPrice="<?php echo number_format("{$totalTradeProfit}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalTradeProfit}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom" style="color:white"><?php echo formatNumberToSort("{$totalTradeProfit}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                            </div>
                                            <div class="keyword-num-status light-blue-col">profit from trading</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">

                                            <div class="keyword-info-num bg-blue-key-dark">
                                                <span style="color:white" title="<?= isset($lifetimeIneraction)?($lifetimeIneraction):0 ?>" data-toggle="tooltip" data-placement="bottom"><?= isset($lifetimeIneraction)?($lifetimeIneraction):0 ?></span>
                                            </div>
                                            <div class="keyword-num-status light-blue-col">lifetime interactions</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">

                                            <div class="keyword-info-num bg-blue-key">
                                                <span style="color:white" title="<?= isset($yesterdayTotalInteraction)?($yesterdayTotalInteraction):0 ?>" data-toggle="tooltip" data-placement="bottom"><?= isset($yesterdayTotalInteraction)?($yesterdayTotalInteraction):0 ?></span>
                                            </div>
                                            <div class="keyword-num-status">yesterday's interactions</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <?php
                                    if(!isset($lifetimePayout)){
                                        $lifetimePayout = 0.00;
                                    }
                                    ?>
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key ellipses">
                                                <a onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                   origPrice="<?php echo number_format("{$lifetimePayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$lifetimePayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom" style="color:white"><?php echo formatNumberToSort("{$lifetimePayout}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></div>
                                            <div class="keyword-num-status">lifetime payout</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                    <!-- col-xs-6 starts here -->
                                    <?php
                                    if(!isset($yesterdayTotalPayout)){
                                        $yesterdayTotalPayout = 0.00;
                                    }
                                    ?>
                                    <div class="col-xs-6 inner-2x innerMB">
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key-dark ellipses">
                                                <a onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                   origPrice="<?php echo number_format("{$yesterdayTotalPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$yesterdayTotalPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom" style="color:white"><?php echo formatNumberToSort("{$yesterdayTotalPayout}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                </div>
                                            <div class="keyword-num-status light-blue-col">yesterday's payout</div>
                                        </div>
                                    </div>
                                    <!-- col-xs-6 ends here -->
                                </div>
                                <!-- row keyword-info-blocks div starts here -->
                            </div>
                        </div>
                        <div class="horizontal-line"></div>
                        <div class="row innerB">
                            <div class="keyword-market-botm" id="keyword-ellipses">
                                <div class="col-md-6">
                                    <!-- trade analytics tabs starts here -->
                                    <div class="trade-analytics-tabs">
                                        <h6 class="head-small-key-markt innerL">trade analytics</h6>
                                        <!-- tabs starts here -->
                                        <!-- container starts here -->
                                        <div class="">
                                            <div class="row">
                                                <div class="col-md-12" id="tooltip-arrw--left">
                                                    <div class="tab_container tabs tab-links">
                                                        <input id="tab1" type="radio" name="tabs" checked>
                                                        <label class="tabs-head" for="tab1"><span>latest trade</span></label>
                                                        <input id="tab2" type="radio" name="tabs">
                                                        <label class="tabs-head" for="tab2"><span>ask</span></label>
                                                        <input id="tab3" type="radio" name="tabs">
                                                        <label class="tabs-head" for="tab3"><span>bid</span></label>
                                                        <input id="tab4" type="radio" name="tabs">
                                                        <label class="tabs-head" for="tab4"><span>Newly Owned</span></label>
                                                        <section id="content1" class="tab-content">
                                                            <table class="table">
                                                                <tbody>
                                                                <ul class="border-all" id="latesttradekeyword">
                                                                </ul>
                                                                </tbody>
                                                            </table>
                                                        </section>
                                                        <section id="content2" class="tab-content">
                                                            <table class="table">
                                                                <tbody>
                                                                <ul class="border-all" id="askkeyword">
                                                                </ul>
                                                                </tbody>
                                                            </table>
                                                        </section>
                                                        <section id="content3" class="tab-content">
                                                            <table class="table">
                                                                <tbody>
                                                                <ul class="border-all" id="bidkeyword">
                                                                </ul>
                                                                </tbody>
                                                            </table>
                                                        </section>
                                                        <section id="content4" class="tab-content">
                                                            <table class="table">
                                                                <tbody>
                                                                <ul class="border-all" id="loaderFirstPurchase">
                                                                </ul>
                                                                </tbody>
                                                            </table>
                                                        </section>
                                                    </div>
                                                </div>
                                                <div class="col-md-6"></div>
                                            </div>
                                        </div>
                                        <!-- container ends here -->
                                        <!-- tab ends here -->
                                    </div>
                                    <!-- trade analytics tabs ends here -->
                                </div>
                                <div class="col-md-3 half innerR">
                                    <!-- if tooltip shows css error den give dis id ="tooltip-arrw--left" to col-md-3 in same line -->
                                    <h6 class="head-small-key-markt innerL">interaction analytics</h6>
                                    <!-- ul starts here -->
                                    <ul class="border-all" id="interactionAnalytic">


                                        <!-- li ends here -->
                                        <!-- li footer starts here -->

                                        <!-- li footer ends here -->
                                    </ul>
                                    <!-- ul ends here -->
                                </div>
                                <div class="col-md-3">
                                    <h6 class="head-small-key-markt innerL">earning analytics</h6>
                                    <!-- if tooltip shows css error den give dis id ="tooltip-arrw--left" to col-md-3 in same line -->
                                    <!-- ul starts here -->
                                    <ul class="border-all" id="earningAnalytic">

                                    </ul>
                                    <!-- ul ends here -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- edit bid popup ends here -->
                    <!-- market slab popup starts here -->
                    <div id="keyword-popup-markt-slab" class="modal fade in keyword-popup current-slab-popup" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content row">
                                <div class="modal-header custom-modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title">Keyword Market Slab</h4>
                                </div>
                                <div class="modal-body padding-none">
                                    <div>
                                        <div class="keyword-market-botm">
                                            <!-- table starts here -->
                                            <div class="pagination-content text-center">
                                                <div class="pagination-head half innerAll bg-grey">
                                                    <div class="row">
                                                        <div class="col-md-3 text-white">Start Limit</div>
                                                        <div class="col-md-3 text-white">End Limit</div>
                                                        <div class="col-md-3 text-white"><?php echo $keywoDefaultCurrencyName; ?> Price</div>
                                                        <div class="col-md-3 text-white padding-left-none padding-right-none">Current BTC Price</div>
                                                    </div>
                                                </div>
                                                <ul class="border-all keyword-analytics-li-pad  keyword-slab-ul" id="slabsListKeyword">
                                                    <!-- li starts here -->
                                                    <li style="display:none;"></li>
                                                    <!-- li ends here -->
                                                </ul>
                                            </div>
                                            <!-- table ends here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- market slab popup ends here -->
                    <!-- container ends here -->
                    <!-- popup starts here -->
                    <!-- delete ask confirmation  popup ends here -->
                </div>
            </main>

        <?php }else{
            print("<script>");
            print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
            print("</script>");
            exit();
        }

    }
    // to handle error message or success message after successfully verified by 2FA feature.

}else {
    header("Location: $rootUrl./views/prelogin/index.php");
} ?>
<?php include('../../layout/transparent_footer.php'); ?>

<script type="text/javascript">

    var getErrorCode = '<?php echo $getErrorCode; ?>';
    var successMsg = '<?php echo $successMsg; ?>';
    var errorMsg = '<?php echo $failedMsg; ?>';

    var keywords1 = '<?php echo $searchQueryFinal ;?>'; //alert(keywords1);
    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);
    // searchResult(rootUrl);
</script>
<script src="../../../js/marketplace.js"></script>
<script>
    var rootUrl = "<?php echo $rootUrl; ?>";

    function getSlabList() {
        $("#slabsListKeyword").load("slabList.php");
    }
</script>
<!-- jquery for tootltip -->
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });


    if(getErrorCode != ''){
        if(getErrorCode == -1){
            if(successMsg != ''){
                showToast("success", successMsg);
            }
        }else{
            if(errorMsg != ''){
                showToast("failed", errorMsg);
            }
        }
    }

</script>
<!-- jquery for tootltip -->