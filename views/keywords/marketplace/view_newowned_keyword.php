<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
//require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
error_reporting(0);

$email = $_SESSION["email"];

$awsLaunchTimestamp = "2016-11-26 18:00:00";
// convert into timestamp
$awsLaunchTimestamp = strtotime($awsLaunchTimestamp);

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
	$conn = $conn["connection"];
}else{
	print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
	$login_status = 1;
}else{
	$login_status = 0;
    header("Location: $rootUrl./views/prelogin/index.php");
}
//echo $login_status;


// get current active slab rate
$currentSlabRate = getCurrentActiveSlab($conn); //echo "<pre>"; print_r($currentSlabRate); echo "</pre>";
if(noError($currentSlabRate)){
	$currentSlabRate = $currentSlabRate["errMsg"];
	$activeSlabRate  = $currentSlabRate["ITD_Price"];
}else{
	print('Error: Unable to retrieve slab rate');
	exit;
}

include('../../layout/header.php');

$pageUrl = "views/keywords/analytics/keyword_analytics.php";

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<style>
	#ellipses-viewall .ellipses-market-trade-label{width:100%!important;}
	.keymarketplace-data ul li{ padding: 8.3px 18px;}
</style>
<main class="main-keyword inner-6x innerT" id="ellipses-viewall">
	<!-- keyword-marketplace starts here -->
	<div class="keyword-marketplace keyword-marketplace-data keyword-markt-popup-common">

		<!-- container starts here -->
		<div class="container">

			<!-- row starts here -->
			<div class="row">
				<div class="col-xs-3">
					<!-- keymarketplace-data-left starts here -->
					<div class="keymarketplace-data key-mar-data-floatingmenu">
						<div class="row">
							<div class="col-xs-12"><div class="back-to-keymar">
									<a href="keyword_search.php"><i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;&nbsp;Back to Keyword Market</a>
								</div></div>
						</div>

						<!-- row starts here -->
						<div class="row">
							<!-- trade analytics col starts here -->
							<div class="col-md-12 inner-3x innerMT">
								<div class="list-group list-cust border-all">
									<a href="#" class="list-group-item">
										Trade Analytics
									</a>
									<a href="view_latest_trade.php" class="list-group-item">Latest Trade</a>
									<a href="view_ask_keyword.php" class="list-group-item">Ask</a>
									<a href="view_bid_keyword.php" class="list-group-item">Bid</a>
									<a href="#" class="list-group-item text-primary active">Newly Owned</a>
								</div>
							</div>
							<!-- trade analytics col ends here -->


							<!-- interaction analytics col starts here -->
							<div class="col-md-12 margin-top-none">
								<div class="list-group list-cust border-all">
									<a href="#" class="list-group-item">
										Interaction Analytics
									</a>
									<a href="view_interaction_analytics.php" class="list-group-item text-primary">All</a>
									<a href="interactionBlog.php" class="list-group-item">Blog</a>
                                    <a href="interactionVideo.php" class="list-group-item">Video</a>
                                    <a href="interactionImage.php" class="list-group-item">Image</a>
                                    <a href="interactionAudio.php" class="list-group-item">Audio</a>
									<!--<a href="#" class="list-group-item">Search</a>-->

								</div>
							</div>
							<!-- interaction analytics col ends here -->

							<!-- earning analytics col starts here -->
							<div class="col-md-12 margin-top-none">
								<div class="list-group list-cust border-all">
									<a href="#" class="list-group-item">
										Earning Analytics
									</a>
									<a href="view_earning_analytics.php" class="list-group-item text-primary">All</a>
									 <a href="earningBlog.php" class="list-group-item">Blog</a>
                                     <a href="earningVideo.php" class="list-group-item">Video</a>
                                     <a href="earningImage.php" class="list-group-item">Image</a>
                                     <a href="earningAudio.php" class="list-group-item">Audio</a>
									<!--<a href="#" class="list-group-item">Search</a>-->

								</div>
							</div>
							<!-- earning analytics col ends here -->

							<!-- ask col starts here -->
							<!--  <div class="col-md-12">-->
							<!--    <div class="pull-left col-xs-12 keyword-markt-ask half">-->
							<!--                                     <label class="pull-left">Newly Owned : </label>-->
							<!--                                     <span class="pull-right">7,436</span>-->
							<!--                                  </div>-->
							<!--  </div>-->
							<!--  <div class="col-md-12">-->
							<!--  &nbsp;-->
							<!--  </div>-->
							<!-- ask col ends here -->





						</div>
						<!-- row ends here -->
					</div>
					<!-- keymarketplace-data-left ends here -->
				</div>

				<div class="col-xs-9 half innerL">
					<!-- keymarketplace-data-right starts here -->
					<div class="keymarketplace-data key-mar-data-floatingmenu" id="keyword-ellipses">
						<h3 class="heading-key-markt margin-top-none inner-3x innerMB marg-botm">Keyword Market Data</h3>
						<!-- pagination starts here -->
						<div class="pagination-content text-center">
							<!-- pagination head starts here -->
							<div class="pagination-head half innerAll padding-key-market-data">
								<div class="row">
									<div class="col-xs-4 text-white text-left">keywords</div>
									<div class="col-xs-3 text-white">Price</div>

								</div>
							</div>

                            <div id="latestbuyedAjaxdata">



                            </div>
                        </div>
                        <!-- pagination ends here -->
                    </div>
                    <!-- keymarketplace-data-right ends here -->
                </div>
            </div>
            <!-- row ends here -->
        </div>
    </div>
    <!-- keyword-marketplace ends here -->
</main>

<?php include('../../layout/transparent_footer.php'); ?>
<script src="../../../js/marketplace.js"></script>


<script>

	$(document).ready(function(){

        getTradeData();

	});

	var email = '<?php echo $email; ?>';
    var id = "latest";
    var type = "purchase_timestamp";
    orderby = "DESC";
    var limit = ($("#LimitedResultData").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 14;
    }
    var pageNo = 1;
    var startDate = "";
    var endDate = "";

    function getTradeData() {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {

                limit        : limit,
                page         : pageNo,
                tableName    : "recent_sold_keyword",
                id           : id,
                orderby      : orderby,
                keywordPrice : "keyword_price",
                startDate    : startDate,
                endDate      : endDate,
                type         : type

            },
            success: function(data) {
                $("#latestbuyedAjaxdata").html("");
                $("#latestbuyedAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }

    function getNewTradeData(pageNo,type,id,startDate,endDate,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controllers/keywords/tradeAnalyticsController.php",
            data: {

                limit        : limit,
                page         : pageNo,
                tableName    : "recent_sold_keyword",
                id           : id,
                orderby      : orderby,
                keywordPrice : "keyword_price",
                startDate    : startDate,
                endDate      : endDate,
                type         : type

            },
            success: function(data) {
                $("#latestbuyedAjaxdata").html("");
                $("#latestbuyedAjaxdata").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }
</script>
