<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php"); //echo "Testing"; die;

error_reporting(0);

// fetch user email from session
$email = $_SESSION["email"]; //echo $email;

// get current session id
$clientSessionId = session_id(); //echo $clientSessionId;

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    header("Location: $rootUrl./views/prelogin/index.php");
}

include('../../layout/header.php');

$searchQuery = trim(urldecode($_GET["q"]));
if($_GET["status"] != "H"){
    $searchQueryFinal    = cleanXSS($searchQuery);
}else{
    $searchQueryFinal    = cleanXSS($searchQuery);
}

if(isset($searchQueryFinal) && !empty($searchQueryFinal)){
    $searchQueryFinal = optimizeSearchQuery($searchQueryFinal);
    $searchQueryFinalKeyword 	= explode(" ",$searchQueryFinal); //print_r($searchQueryFinalKeyword);
}

$lastSearchKeyword = end($searchQueryFinalKeyword); // This we use for fetching last Search keyword

// get user cart details
$userCartDetails = getUserCartDetails($email, $conn);

if(noError($userCartDetails)){
    $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
    $userCartDetails = json_decode($userCartDetails, TRUE);
}else{
    print('Error: Fetching cart details');
    exit;
}

?>
<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-5x innerT">
    <!-- keyword-marketplace starts here -->
    <div class="keyword-marketplace keyword-marketplace-data keyword-analytics">
        <!-- container starts here -->
        <div class="container">
            <!-- row starts here -->
            <div class="row">
                <div class="col-md-6">
                    <!-- keyword-analy-sort starts here-->
                    <div class="keyword-analy-sort keyword-market-botm">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6 padding-left-none">
                                    <h5 class="text-blue keyword-name margin-top-none half innerMB">#<?php  echo $lastSearchKeyword ;?></h5>
                                    <div class="row keyword-grey">
                                        <div class="col-md-4 half innerL">
                                            <span class="text-black ellipses"><a href="#" class="display-in-block" title="<?php  echo "#".$searchQuery ;?>" data-toggle="tooltip" data-placement="bottom"><label class="txt-blue">#</label><?php  echo $searchQuery ;?></a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 padding-right-none">
                                    <?php

                                    $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $lastSearchKeyword); //echo "<pre>"; print_r($kwdRevenueDetails); echo "</pre>"; die;

                                    if(noError($kwdRevenueDetails)){

                                    $kwdTableName                = $kwdRevenueDetails["table_name"];
                                    $kwdRevenueDetails           = $kwdRevenueDetails["data"][0];
                                    $checkForKeywordAvailability = $kwdRevenueDetails["keyword_details"];
                                    $keyword = $kwdRevenueDetails["keyword"];
                                    $follow_unfollow = $kwdRevenueDetails["follow_unfollow"];

                                    $follow_unfollowjson = json_decode($follow_unfollow, true); //echo "<pre>"; print_r($follow_unfollowjson); echo "<pre>";

                                    $followeremail = $follow_unfollowjson["email"];

                                    if (in_array($email, $followeremail)) { ?>
                                        <input value="Unfollow" type="button" onclick="followUnfollow();" class="btn-trading-wid-auto pull-right">
                                    <?php } else { ?>
                                        <input value="Follow" type="button" onclick="followUnfollow();" class="btn-trading-wid-auto pull-right">
                                    <?php }} ?>
                                </div>
                            </div>
                            <!-- tabs starts here -->
                            <!-- highest bid current bid starts here -->
                            <div class="row">
                                <div class="col-md-12 innerMB padding-left-none padding-right-none">
                                    <div class="">
                                        <?php
                                        $checkForKeywordAvailability = checkForKeywordAvailability($lastSearchKeyword,$conn); //echo "<pre>"; print_r($checkForKeywordAvailability); echo "</pre>";
                                        if (noError($checkForKeywordAvailability)) {
                                            $availabilityFlag = $checkForKeywordAvailability['errMsg']; //echo $availabilityFlag;
                                            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                            $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
                                            $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
                                            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                                            $CartStatus = $checkForKeywordAvailability['status'];
                                            $activeBids = $checkForKeywordAvailability["active_bids"];
                                            $keyword = $lastSearchKeyword;

                                        if($availabilityFlag == 'keyword_available'){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="pull-right">
                                                    <?php if(in_array($keyword, $userCartDetails)){ ?>
                                                        <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-wid-auto-dark" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                    <?php }else{ ?>
                                                        <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-wid-auto-dark" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }elseif($availabilityFlag == 'keyword_not_available'){

                                            /* 1. if email == buyerid
                                             *      1.1. if user not set his ask price then "Set Price"
                                             *      1.2. if user set keyword ask price then "Edit Price"
                                             *      1.3. if user has bid on his keyword then "Accept Bid"
                                             *
                                             * 2. if email != buyerid
                                             *      2.1. if active bid filed is empty in ownership table then "Place Bid"
                                             *      2.2. if active bid is set in json format then
                                             *          2.2.1. if email == bidderemail then "Edit Bid"
                                             *          2.2.2. if email != bidderemail then "Place bid"
                                             *          2.2.3   if set price is set in ownership table then "Buy Now"
                                             *
                                             *
                                             */

                                            $activeBids = json_decode($activeBids, true);
                                            foreach($activeBids as $key => $bidValue){
                                                $bidValue = explode('~~', $bidValue);
                                                $bidderEmail[] = $bidValue[1];

                                            }
                                            if(in_array($email, $bidderEmail)){
                                                $bidStatus = true;
                                            }else{
                                                $bidStatus = false;
                                            } ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php  if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                    if($highestBidAmtForKwd > 999) {
                                                        ?>
                                                        <label class="keyword-grey text-black ellipses margin-none"><span class="text-blue">Current Bid : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}";  ?> " data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </label>

                                                    <?php }elseif($highestBidAmtForKwd <= 999){ ?>
                                                        <label class="keyword-grey text-black ellipses margin-none"><span class="text-blue">Current Bid : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" ><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </label>
                                                    <?php }

                                                }else{ ?>
                                                    <label class="keyword-grey text-black ellipses margin-none"><span class="text-blue">Current Bid : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="0.0000 <?php echo $keywoDefaultCurrencyName; ?>">0.0000 <?php echo $keywoDefaultCurrencyName; ?></a></label>

                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                    if($kwdAskPrice > 999){
                                                        ?>
                                                        <label class="keyword-grey text-black ellipses margin-none"><span class="text-blue">Asking Price : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}";  ?> " data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </label>
                                                        <?php
                                                    }else if($kwdAskPrice <= 999){
                                                        ?>
                                                        <label class="keyword-grey text-black ellipses margin-none"><span class="text-blue">Asking Price : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </label>
                                                    <?php    }
                                                }else{ ?>
                                                    <label class="keyword-grey text-black ellipses margin-bottom-none"><span class="text-blue">Asking Price : </span><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="0.0000 <?php echo $keywoDefaultCurrencyName; ?>">0.0000 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                <?php }  ?>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="pull-right">
                                                    <?php
                                                    if($email == $kwdOwnerId){
                                                        if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                            ?>
                                                            <button class="btn-trading-wid-auto-dark" id="acceptKeyword" value="--><?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                                                        <?php    }

                                                    }else{
                                                        if($CartStatus == "sold"){
                                                            if(empty($activeBids)){
                                                                ?>
                                                                <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>

                                                            <?php }else{
                                                                if($bidStatus){
                                                                    ?>
                                                                    <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>

                                                                <?php  }else{ ?>
                                                                    <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                                                <?php     }
                                                            }
                                                        }
                                                    }
                                                    ?>
<!--                                                    <input value="Place Bid" type="button" class="btn-trading-wid-auto">-->
                                                </div>
                                                <div class="pull-right innerMR inner-2x">
                                                    <?php
                                                    if($email == $kwdOwnerId){
                                                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                            ?>
                                                            <button class="btn-trading-wid-auto"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>

                                                        <?php    }else{ ?>
                                                            <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>

                                                        <?php  }

                                                    }else{
                                                        if($CartStatus == "sold"){
                                                            if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                                ?>
                                                                <button class="btn-trading-wid-auto-dark"  value="<?php echo $keyword;  ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>', <?php  echo $kwdAskPrice; ?>);"  >Buy Now</button>
                                                            <?php }
                                                        }

                                                    }
                                                    ?>
<!--                                                    <input value="Buy Now " type="button" class="btn-trading-wid-auto-dark">-->
                                                </div>
                                            </div>
                                        </div>
                                        <?php } else if($availabilityFlag == "keyword_blocked"){ ?>
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="pull-right">
                                                    <span class="keyword-grey text-black ellipses margin-none">&nbsp;Not Available </span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php }} ?>
                                    </div>
                                </div>
                            </div>
                            <!-- highest bid current bid ends here -->

                            <div class="row">
                                <div class="col-md-12 padding-none">
                                    <!-- keyword - 30 days starts here -->
                                    <div class="pagination-content text-center table-keyword-marketplace">
                                        <table class="table margin-none">
                                            <thead class="pagination-head text-white ">
                                        <td>
                                            <select class="select-analytics border-all pull-left" id="topsorting">
                                                <option selected id="30 Days">30 Days</option>
                                                <option id="3 Months">3 Months</option>
                                                <option id="6 Months">6 Months</option>
                                                <option id="9 Months">9 Months</option>
                                                <option id="12 Months">12 Months</option>
                                            </select>
                                        </td>
                                        <td>
                                            Interaction
                                        </td>
                                        <td>
                                            Earning
                                        </td>

                                            <tbody class=" keyword-analytics-li-pad shortingDateWise">

                                            </tbody>
                                            
                                            </table>
                                    </div>
                                    <!-- keyword - 30 days ends here -->
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                            <!-- tab ends here -->

                        </div>
                    </div>
                    <!-- keyword-analy-sort ends here -->
                </div>
                <div class="col-md-6">
                    <!-- srch-new-keyword starts here -->
                    <p class="errMsg"></p>
                    <div class="srch-new-keyword">
                        <!-- search bar starts here -->
                        <div id="keyword-analytics-input">
                            <div class="input-group col-md-12 padding-left-none padding-right-none inner-2x innerMB">
                                <form id="keyword_search_form">
                                    <input class="search-query form-control ellipses inner-3x innerR" placeholder="Search new Keyword..." id="keyword_search" data-type="2" name="q" type="text" value="<?php  echo $searchQuery ;?>">
                                        <span class="input-group-btn">
                                          <button class="btn btn-danger" onclick="searchResult();" type="button">
                                          <span class="glyphicon glyphicon-search"></span>
                                          </button>
                                        </span>
                                </form>
                            </div>
                        </div>
                        <!-- search bar ends here -->
                        <!-- keyword search block starts here -->
                        <div class="row keyword-info-blocks">
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status light-blue-col">All Earnings</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">5 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status">Daily Earnings</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">1 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status light-blue-col">Earning Per Interaction</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,545</div>
                                    <div class="keyword-num-status">Interaction</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">12,124</div>
                                    <div class="keyword-num-status light-blue-col">Total Searches</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,54,658</div>
                                    <div class="keyword-num-status">Total Post</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark padng-analytic-blocks">2,545 <?php echo $keywoDefaultCurrencyName; ?><span class="analytic-blocks-span">(next 365 days)</span></div>
                                    <div class="keyword-num-status light-blue-col">Expected Income</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,546</div>
                                    <div class="keyword-num-status">Followers</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                            <!-- col-md-6 starts here -->
                            <div class="col-md-4 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">12,124</div>
                                    <div class="keyword-num-status light-blue-col">Number of Trades</div>
                                </div>
                            </div>
                            <!-- col-md-6 ends here -->
                        </div>
                        <!-- keyword search block ends here -->
                    </div>
                    <!-- srch-new-keyword ends here -->
                </div>
            </div>
            <!-- row ends here -->
            <div class="row">
                <div class="col-md-8 half innerLR">
                    <!-- main-bg-graph starts here -->
                    <div class="main-bg-graph bg-white">
                        <!-- graph heading starts here -->
                        <div class="col-md-12 bg-white border-all innerAll">
                            <div class="row">
                                <div class="col-md-4 padding-right-none">
                                    <h4 class="text-blue">Keyword Performance</h4>
                                </div>
                                <div class="col-md-4 padding-right-none">
                                    <div class="pull-left">
                                        <span class="span-keyword-market pull-left">Select Type : &nbsp;</span>
                                        <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                            <option value="All" selected="All">All</option>
                                            <option value="">option1</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 padding-right-none">
                                    <div class="pull-left">
                                        <span class="span-keyword-market pull-left">Select Duration : &nbsp;</span>
                                        <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                            <option value="All" selected="All">30 Days</option>
                                            <option value="">29 Days</option>
                                            <option value="">28 Days</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- graph heading ends here -->
                        <div class="col-md-12 border-all border-top-none">
                            <h5 class="text-blue inner-2x innerMT"> &nbsp;Payouts</h5>
                            <!-- graph body starts here -->
                            <!-- <div id="container" style="height:250px;width:100%;"></div> -->
                            <div id="container" style="width:100%;"></div>
                            <!-- graph body ends here -->
                            <h5 class="text-blue pull-right" style="position: absolute;
                        right: 12px;
                        bottom: 10px;">Interaction</h5>
                        </div>
                    </div>
                    <!-- main-bg-graph ends here -->
                </div>
                <div class="col-md-4">
                    <!-- keyword-analytics-status starts here -->
                    <div class="keyword-analytics-status">
                        <div class="">
                            <div class="col-md-12">
                                <!-- row starts here -->
                                <div class="row display-flex-keyword">
                                    <div class="col-md-8 bg-white innerAll">
                                        <span class="keyword-grey-span">Interactions</span>
                                        <div class="comn-lft-rght-cont innerMT">
                                            <label>Daily Average</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Peak Performance</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Monthly Growth</label>
                                            <span>&nbsp;: 50%</span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                                        <div class="bg-blue-key innerAll text-white width-100-percent">
                                            <span class="">Today</span>
                                            <h2 class="innerMT ellipses">450</h2>
                                            <span>UP 10%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- row ends here -->
                                <!-- row starts here -->
                                <div class="row display-flex-keyword inner-2x innerMT">
                                    <div class="col-md-8 bg-white innerAll">
                                        <span class="keyword-grey-span">Earning</span>
                                        <div class="comn-lft-rght-cont innerMT">
                                            <label>Daily Average</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Peak Performance</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Monthly Growth</label>
                                            <span>&nbsp;: 50%</span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                                        <div class="bg-blue-key innerAll text-white width-100-percent">
                                            <span class="">Today</span>
                                            <h2 class="innerMT ellipses">450</h2>
                                            <span>UP 10%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- row ends here -->
                                <!-- row starts here -->
                                <div class="row display-flex-keyword inner-2x innerMT">
                                    <div class="col-md-8 bg-white innerAll">
                                        <span class="keyword-grey-span">Earning Per Interactions</span>
                                        <div class="comn-lft-rght-cont innerMT">
                                            <label>Daily Average</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Peak Performance</label>
                                            <span>&nbsp;: 500</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comn-lft-rght-cont half innerMT">
                                            <label>Monthly Growth</label>
                                            <span>&nbsp;: 50%</span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                                        <div class="bg-blue-key innerAll text-white width-100-percent">
                                            <span class="">Today</span>
                                            <h2 class="innerMT ellipses">450</h2>
                                            <span>UP 10%</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- row ends here -->
                            </div>
                        </div>
                    </div>
                    <!-- keyword-analytics-status ends here -->
                </div>
            </div>
        </div>
        <!-- container ends here -->
    </div>
    <!-- keyword-marketplace starts here -->
</main>
<br/>
<br/>
<!-- graphs starts here -->
<?php include('../../layout/transparent_footer.php'); ?>
<script src="../../../frontend_libraries/graph/highcharts.js"></script>
<script src="../../../frontend_libraries/graph/exporting.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
    Highcharts.chart('container', {

        chart: {
            height:280
        },
        title: {
            text: ' '
        },

        xAxis: {
            categories: ['15', '30', '45', '60', '75', '90','105','120', '135','150', '165','180', '195','210'],

            tickInterval: 0
        },

        yAxis: {
            endOnTick: false,        //hide ending number of y axis
            tickInterval: 1000,       //intervals in y-axis
            lineWidth: 1,
            gridLineWidth: 1,
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return (this.value / 100) + " " + keywoDefaultCurrency ;
                }
            }
        },

        tooltip: {
            headerFormat: '<b>{ }</b>',   //series.name
            pointFormat: '<b>Interaction</b> = {point.x},<br/> <b>Earning</b> = {point.y}'
        },

        series: [{
            data: [15, 635, 809, 947, 1402, 3634, 5268,3634, 5268, 1402, 3634, 5268,3634, 5268],
            pointStart: 0
        }]
    });


</script>
<!-- graphs starts here -->


<!--<script src="../../../js/analytics.js"></script>-->
<script src="../../../js/marketplace.js"></script>

<script>

    $(document).ready(function(){

        var keyword = '<?php echo $lastSearchKeyword ; ?>';
        var id = encodeURIComponent($('#topsorting').find(":selected").text()); //alert(id);
        var duration = encodeURIComponent($('#graphDuration').find(":selected").text()); //alert(duration);
        var type = $('#graphType').find(":selected").text(); //alert(type);

        $('.shortingDateWise').load("keywordSortingByDate.php?id="+id+"&keyword="+keyword);
        $('#plottingGraphs').load("plottingGraph.php?keyword="+keyword+"&duration="+duration+"&type="+type);
    });

    $("#topsorting").change(function() {

        var keyword = '<?php echo $lastSearchKeyword ; ?>';
        var id      = $('#topsorting').find(":selected").text();

        $.ajax({
            type: 'GET',
            url: 'keywordSortingByDate.php',
            dataType: 'html',
            data: {
                id      : id,
                keyword : keyword
            },
            async: true,
            success: function (data) {
                $('.shortingDateWise').html(data);
            }
        });
    });

</script>


