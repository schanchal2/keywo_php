

<div id="keyword-popup-ask-bid" class="modal fade keyword-popup keyword-markt-popup-common" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <!-- <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                 <h4 class="modal-title">Edit Bid</h4>
             </div>-->
            <div class="modal-body padding-none clearfix innerMT">
                <div class="keyword-dialog">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- edit bid popup ends here -->


<div id="keyword-popup-confirm-buy" class="modal fade keyword-popup keyword-markt-popup-common" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" id="buyResponse">Are you sure that you want to buy this keyword</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                    <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <input value="Yes" type="button" id="buy-yes-btn" class="btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;
                                    <input value="No" type="button" id="buy-no-btn" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Buy now popup starts here -->
<div id="keyword-popup-confirm-accept" class="modal fade keyword-popup  keyword-markt-popup-common" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" >Are you sure that you want to accept bid for this keyword.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                    <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <input value="Yes" type="button" class="btn-trading-wid-auto-dark innerMTB" onclick="acceptBid();">&nbsp;&nbsp;
                                    <input value="No" type="button" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Buy now popup ends here -->

<!-- delete bid confirmation  popup starts here -->
<div id="keyword-popup-delete-bid" class="modal fade keyword-popup  keyword-markt-popup-common" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" id="deleteResponse">Are you sure that you want to delete bid for this keyword.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                    <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <input value="Yes" type="button" id="delete-yes-btn" class="btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;
                                    <input value="No" type="button" id="delete-no-btn" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- delete bid confirmation  popup ends here -->

<!-- delete ask confirmation  popup starts here -->
<div id="keyword-popup-delete-ask" class="modal fade keyword-popup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" id="deleteaskResponse">Are you sure that you want to delete ask for this keyword.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                    <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <input value="Yes" type="button" id="delete-ask-yes-btn" class="btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;
                                    <input value="No" type="button" id="delete-ask-no-btn" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
