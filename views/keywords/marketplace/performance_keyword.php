<?php
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

//include dependent files

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
error_reporting(0);

  //echo $rootUrl."views/keywords/marketplace";

$email = $_SESSION["email"]; //echo $email;

$clientSessionId = session_id();

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
    exit;
}

//echo $login_status;

$searchQueryFinal = optimizeSearchQuery(urldecode($_GET["q"]));

$searchQueryFinal = optimizeSearchQuery(cleanXSS($_GET["q"]));
$searchQueryFinal = str_replace(",", " ", $searchQueryFinal); //echo $searchQueryFinal;
$searchQueryFinalKeyword = explode(" ", $searchQueryFinal);
//print_r($searchQueryFinalKeyword);

$userCartDetails = getUserCartDetails($email, $conn);

if(noError($userCartDetails)){
    $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
    $userCartDetails = json_decode($userCartDetails, TRUE);
}else{
    print('Error: Fetching cart details');
    exit;
}

$pageUrl = "views/keywords/analytics/keyword_analytics.php?q=";
?>

<div class="performance">

            <?php
            $pageUrl = "views/keywords/analytics/keyword_analytics.php";

            foreach($searchQueryFinalKeyword as $keyNo => $keyword) {

            $checkForRevenewAvailability = getRevenueDetailsByKeyword($conn,$keyword); //echo "<pre>"; print_r($checkForRevenewAvailability); echo "</pre>";
            if(noError($checkForRevenewAvailability)){

            $checkForRevenewAvailability = $checkForRevenewAvailability["data"][0];
            $totalPaidSearch = $checkForRevenewAvailability["user_kwd_search_count"]; //echo "Paid search ".$totalPaidSearch."<br />";
            $totalKeywordEarning = $checkForRevenewAvailability["user_kwd_ownership_earnings"]; //echo "Earning ".$totalKeywordEarning;

            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

            //printArr($checkForKeywordAvailability);
            if(noError($checkForKeywordAvailability)){

                $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
                $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
                $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                $CartStatus = $checkForKeywordAvailability['status'];
                $activeBids = $checkForKeywordAvailability["active_bids"];

                if(isset($totalKeywordEarning) && !empty($totalKeywordEarning))
                {
                    $totalKeywordEarning = $totalKeywordEarning;
                }
                else
                {
                    $totalKeywordEarning = 0;
                }
                if(isset($totalPaidSearch) && !empty($totalPaidSearch)){
                    $totalPaidSearch = $totalPaidSearch;
                }else{
                    $totalPaidSearch = 0;
                }

                if($availabilityFlag == 'keyword_available'){


                    ?>

                    <div class="row">
                        <div class="col-xs-12 padding-left-none padding-right-none">
                            <div class="text-left">

                                <div class="row grey-block">
                                    <div class="col-md-7  padding-left-none padding-right-none">
                                        <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                    </div>
                                    <div class="col-md-5  padding-left-none padding-right-none">
                                        <span class="more-info-marketp pull-right innerAll"><a href='<?php echo "{$rootUrl}{$pageUrl}?q={$keyword}"; ?>'>more info</a></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xs-12">
                            <label class="keyword-grey pull-left">Interaction :</label>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                        </div>
                        <div class="col-xs-12">
                            <label class="keyword-grey pull-left">Earning : </label>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                        origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                            </div>
                        <div class="col-xs-12 text-right">&nbsp;
                        </div>
                        <div class="col-xs-12">
                            <?php if(in_array($keyword, $userCartDetails)){ ?>
                                <input value="Remove" type="button" id="hdr_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'hdr')" />
                            <?php }else{ ?>
                                <input value="Add To Cart" id="hdr_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'hdr')" />
                            <?php } ?>
                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                    </div>
                <?php }if($availabilityFlag == "keyword_not_available"){

                    $activeBids = json_decode($activeBids, true);
                    foreach($activeBids as $key => $bidValue){
                        $bidValue = explode('~~', $bidValue);
                        $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

                    }
                    if(in_array($email, $bidderEmail)){
                        $bidStatus = true;
                    }else{
                        $bidStatus = false;
                    }


                    ?>

                    <div class="row">
                    <div class="col-xs-12 padding-left-none padding-right-none">
                        <div class="text-left">
                            <div class="row grey-block">
                                <div class="col-md-7  padding-left-none padding-right-none">
                                    <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                </div>
                                <div class="col-md-5  padding-left-none padding-right-none">
                                    <span class="more-info-marketp pull-right innerAll"><a href='<?php echo "{$rootUrl}{$pageUrl}?q={$keyword}"; ?>'>more info</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <label class="keyword-grey pull-left">Interaction :</label>
                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                    </div>
                    <div class="col-xs-12">
                        <label class="keyword-grey pull-left">Earning : </label>
                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                    origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                    </div>
                    <div class="col-xs-12">
                        <label class="keyword-grey pull-left">Asking Price :</label>

                        <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                        origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                        <?php }else {?>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                        origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                        <?php } ?>
                        </div>
                    <div class="col-xs-12">
                        <label class="keyword-grey pull-left">Current Bid : </label>
                        <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                        <?php }else {?>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12 text-right">&nbsp;
                    </div>
                    <div class="pull-left col-xs-6 col-md-12">
                        <?php if($email == $kwdOwnerId){
                            if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>


                                    <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>&nbsp;


                                <?php
                            }else{
                                // set ask
                                ?>


                                    <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Set Ask</button>&nbsp;


                                <?php
                            }

                            if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                ?>


                                    <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>&nbsp;


                                <?php
                            }
                        }else{

                            if($CartStatus == "sold"){

                                if(empty($activeBids)){
                                    ?>


                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>&nbsp;


                                    <?php
                                }else{
                                    if($bidStatus){
                                        ?>


                                            <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>&nbsp;


                                        <?php
                                    }else{
                                        ?>


                                            <button class="btn-trading" value="<?php echo $keyword; ?>"" type="button" onclick="openTradeDialog(this);" >Place Bid</button>&nbsp;


                                        <?php
                                    }
                                }

                                if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                    ?>


                                    <button class="btn-trading-dark"  value="<?php echo $keyword;  ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>', <?php  echo $kwdAskPrice; ?>);"  >Buy Now</button>&nbsp;

                                    <?php
                                }

                            }
                        }

                        ?>

                    </div>
                        <div class="col-xs-12">&nbsp;</div>

                        </div>
                    <?php }}elseif($availabilityFlag == "keyword_blocked" || $checkForKeywordAvailability["errCode"] == 4){ ?>

                    <div class="row">
                        <div class="col-xs-12 padding-left-none padding-right-none">
                            <div class="text-left">

                                <div class="row grey-block">
                                    <div class="col-md-7  padding-left-none padding-right-none">
                                        <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                    </div>
                                    <div class="col-md-5  padding-left-none padding-right-none">
                                        <span class="more-info-marketp pull-right innerAll"><a href='<?php echo "{$rootUrl}{$pageUrl}?q={$keyword}"; ?>'>more info</a></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xs-12">
                            <label class="keyword-grey pull-left">Interaction :</label>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>

                        </div>
                        <div class="col-xs-12">
                            <label class="keyword-grey pull-left">Earning : </label>
                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                        </div>
                        <div class="col-xs-12">
                            <span class="keyword-grey-span pull-r-marketplace">&nbsp;Not Available </span>
                        </div>
                    </div>

                <?php }}} ?>

</div>
<?php /*include('../../layout/transparent_footer.php'); */?>
<script src="<?php echo $rootUrl; ?>js/marketplace.js"></script>
<script src="<?php echo $rootUrl; ?>js/header.js"></script>

<!-- jquery for tootltip -->
<script>
    $(document).ready(function(){
      //  alert("hello");
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!-- jquery for tootltip -->

