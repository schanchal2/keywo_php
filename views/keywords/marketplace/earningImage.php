			<?php
session_start();

			header('Content-Type: text/html; charset=utf-8');

			$docrootpath = __DIR__;
			$docrootpath = explode('/views', $docrootpath);
			$docrootpath = $docrootpath[0] . "/";

			require_once("{$docrootpath}config/config.php"); //echo "Testing";
			require_once("{$docrootpath}config/db_config.php");
			require_once("{$docrootpath}/helpers/errorMap.php");
			require_once("{$docrootpath}helpers/coreFunctions.php");
			require_once("{$docrootpath}helpers/stringHelper.php");
			require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
			require_once("{$docrootpath}models/keywords/userCartModel.php");
			error_reporting(0);

			$email = $_SESSION["email"]; //echo $email; die;

			//For database connection
			$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>"; die;

			if(noError($conn)){
				$conn = $conn["connection"];
			}else{
				print_r("Database Error");
			}

			//Validating User LoggedIn and LoggedOUt status
			if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
				$login_status = 1;
			}else{
				$login_status = 0;
                header("Location: $rootUrl./views/prelogin/index.php");


			}

			include('../../layout/header.php');

?>
<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	 <!-- date picker css js -->
	 	 <script type="text/javascript" src="../../../frontend_libraries/momentjs/moment.min.js"></script>
	 	 <script type="text/javascript" src="../../../frontend_libraries/datepicker/daterangepicker.js"></script>
	 	 <link rel="stylesheet" type="text/css" href="../../../frontend_libraries/datepicker/daterangepicker.css" />
	 <!-- date picker css js -->
<main class="main-keyword inner-6x innerT">
  <!-- keyword-marketplace starts here -->
  <div class="keyword-marketplace keyword-marketplace-data keyword-markt-popup-common">

    <!-- container starts here -->
    <div class="container">

      <!-- row starts here -->
      <div class="row">
<div class="col-xs-3">
<!-- keymarketplace-data-left starts here -->
<div class="keymarketplace-data key-mar-data-floatingmenu">
<div class="row">
<div class="col-xs-12"><div class="back-to-keymar">
<a href="index.php"><i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;&nbsp;Back to Keyword Market</a>
</div></div>
</div>

<!-- row starts here -->
<div class="row">
  <!-- trade analytics col starts here -->
  <div class="col-md-12 inner-3x innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Trade Analytics
          </a>
          <a href="view_latest_trade.php" class="list-group-item">Latest Trade</a>
		  <a href="view_ask_keyword.php" class="list-group-item text-primary item">Ask</a>
		  <a href="view_bid_keyword.php" class="list-group-item">Bid</a>
		  <a href="view_newowned_keyword.php" class="list-group-item">Newly Owned</a>
      </div>
  </div>
  <!-- trade analytics col ends here -->


  <!-- interaction analytics col starts here -->
  <div class="col-md-12 half innerMT">
      <div class="list-group list-cust border-all" id="pageName">
          <a href="#" class="list-group-item">
              Interaction Analytics
          </a>
          <a href="view_interaction_analytics.php" class="list-group-item">All</a>
          <a href="interactionBlog.php" class="list-group-item">Blog</a>
          <a href="interactionVideo.php" class="list-group-item">Video</a>
          <a href="interactionImage.php" class="list-group-item">Image</a>
          <a href="interactionAudio.php" class="list-group-item">Audio</a>

      </div>
  </div>
  <!-- interaction analytics col ends here -->

  <!-- earning analytics col starts here -->
  <div class="col-md-12 half innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Earning Analytics
          </a>
          <a href="view_earning_analytics.php" class="list-group-item text-primary">All</a>
          <a href="earningBlog.php" class="list-group-item">Blog</a>
          <a href="earningVideo.php" class="list-group-item">Video</a>
          <a href="earningImage.php" class="list-group-item text-primary active">Image</a>
          <a href="earningAudio.php" class="list-group-item">Audio</a>

      </div>
  </div>
  <!-- earning analytics col ends here -->

  <!-- ask col starts here -->

  <div class="col-md-12">
  &nbsp;
  </div>
  <!-- ask col ends here -->





  </div>
<!-- row ends here -->
  </div>
<!-- keymarketplace-data-left ends here -->
</div>

<div class="col-xs-9">
<!-- keymarketplace-data-right starts here -->
<div class="keymarketplace-data key-mar-data-floatingmenu" id="keyword-ellipses">
<h3 class="heading-key-markt margin-top-none inner-3x innerMB marg-botm">Keyword Market Data</h3>
<!-- pagination starts here -->
<div class="pagination-content text-center">
<!-- pagination head starts here -->
  <div class="pagination-head half inner-3x innerAll padding-key-market-data">
		<div class="row">
		<div class="col-xs-4 text-white">keywords</div>
		<div class="col-xs-3 text-white">EarningCount</div>
		<div class="col-xs-4 text-white padding-right-none">
		<!-- keyword-radio-btn starts here -->
		<!-- keyword-radio-btn ends here     -->
		</div>
		</div>
  </div>
<!-- pagination head ends here -->

<!-- pagination body starts here -->
<div id="interactionBlogdataLoad">
</div>

</div>
</div>
</div>

      </div>

    </div>
  </div>

  <div id="callPageAjaxData">
  </div>
 

</main>
<?php
   include("../../layout/transparent_footer.php");

   ?>
<!-- date picker css ends here-->
<script type="text/javascript">
$(function() {
    $('input[name="daterange"]').daterangepicker();
});

$(document).ready(function() {
    getSubcatData();
});

function getSubcatData() {

var limit = 14;
var postType  = "image";
var type = "total_interaction_earning";

$.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controllers/keywords/subCategoryOfSocialController.php",
        data: {
            limit: limit,
            postType : postType,
            type: "total_interaction_earning"
        },
        success: function(data) {
            $("#interactionBlogdataLoad").html("");
            $("#interactionBlogdataLoad").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });
}

function getSubcatNewData(pageNo,limit,type) {
 
 $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controllers/keywords/subCategoryOfSocialController.php",
        data: {
            limit: limit,
            page: pageNo,
            postType : "image",
            type: "total_interaction_earning"
        },
        success: function(data) {
            $("#interactionBlogdataLoad").html("");
            $("#interactionBlogdataLoad").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

}

</script>
<!-- date pciker css starts here-->
