    <?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/header/headerModel.php");

$email = $_SESSION["email"];
/*$sessionId = session_id();*/
$upc = $_SESSION['CurrPreference'];

if(isset($_POST["keyword"]) && !empty($_POST["keyword"])){
    $keyword = $_POST["keyword"];
    $keyword = implode(' ', $keyword);
}

$errCode = 1;
$errMsg = '';
if (isset($email) && !empty($email)) {
//Keywords database connection
    $kwdConn = createDbConnection('dbkeywords');

    if (noError($kwdConn)) {
        $kwdConn = $kwdConn["connection"];

            $renewUsrBalanceField = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
            $renewUsrBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $renewUsrBalanceField);

            if (noError($renewUsrBalanceDetails)) {
                $getUserBalance = calculateUserBalance($renewUsrBalanceDetails);

                if (noError($getUserBalance)) {
                    $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                } else {
                    $userBalance = 0;
                }

                /* Get user admin settings from keyword admin */
                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdConn);
                if (noError($adminSettings)) {
                    $errMsg = "getting admin settings of buyer user success.";
                    $renewalAmount = number_format($adminSettings["data"]["kwd_renewal_fees_per_year"], 8);

                        $getExchangeRate = getExchangeRates($upc);
                        if (noError($getExchangeRate)) {
                            $getExchangeRate = $getExchangeRate['currRate'];
                        } else {
                            print("Error: getting exchange rate");
                        }

                        $upc = strtolower($upc);

                        $userPaymentAmountITD = number_format((float)($renewalAmount), 8);
                        $totalRenewalAmt = 0;

                        $getPresaleDetails = getUserCartDetails($email, $kwdConn);
                        if(noError($getPresaleDetails)){
                            $getPresaleDetails = $getPresaleDetails["errMsg"];
                            $getRenewKwd = json_decode($getPresaleDetails["user_renew_kwd_in_cart"], true);
                            foreach($getRenewKwd as $key => $renewAmt){
                                $totalRenewalAmt = number_format((float)($totalRenewalAmt + $userPaymentAmountITD),4);
                            }
                        }else{
                            print('Error: Fetching renew keyword details');
                        }

                        if ($userBalance >= $totalRenewalAmt) {

                           // $userBalance = number_format($userBalance, 2);
                           // $totalRenewalAmt = number_format($totalRenewalAmt, 2);
                            ?>

                            <div class="col-md-12 inner-2x innerMT">
                                <div class="current-wallet-balance">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="current-w1 innerAll innerPL-5">
                                                <h5>Current Wallet Balance is : <span
                                                        onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                        origPrice="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>"
                                                        title="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip"
                                                        data-placement="bottom"><?php echo formatNumberToSort($userBalance, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Your payment amount : <span
                                                        onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                        origPrice="<?php echo "{$totalRenewalAmt} {$keywoDefaultCurrencyName}"; ?>"
                                                        title="<?php echo "{$totalRenewalAmt} {$keywoDefaultCurrencyName}"; ?>"
                                                        data-toggle="tooltip"
                                                        data-placement="bottom"><?php echo formatNumberToSort($totalRenewalAmt, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-6">


                                            <div class="pull-right inner-3x innerMT"><input value="Renew Now" id="renew-now-btn"
                                                                                            onclick='renewOwnedKeyword("wallet", "<?php echo $keyword; ?>")'
                                                                                            type="button"
                                                                                            class="btn-pay-now"
                                                                                            style="background-color:rgb(150, 151, 152);"
                                                                                            disabled></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } else {

                            $purchaseRemainingITD = $totalRenewalAmt - $userBalance;
                            $userPaymentAmountBTC = number_format((float)($purchaseRemainingITD * $getExchangeRate['btc']), 8);
                            $userPaymentAmountSGD = number_format((float)($purchaseRemainingITD * $getExchangeRate['sgd']), 4);

                            $purchaseAmt = $totalRenewalAmt - $userBalance;
                            $userBalance = number_format($userBalance, 4);
                            $totalRenewalAmt = number_format($totalRenewalAmt, 4);
                            $purchaseAmt = number_format($purchaseAmt, 4);
                            ?>

                            <div class="col-md-12 inner-2x innerMT">
                                <!-- current-wallet-balance div starts here -->
                                <div class="current-wallet-balance">
                                    <div class="row inner-1x innerAll innerPB-20 innerPL-5">
                                        <div class="col-md-7">
                                            <div class="current-w1">
                                                <h5>Current Wallet Balance is : <span
                                                        onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                        origPrice="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>"
                                                        title="<?php echo $userBalance; ?>" data-toggle="tooltip"
                                                        data-placement="bottom"><?php echo formatNumberToSort($userBalance, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Your payment amount : <span
                                                        onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                        origPrice="<?php echo "{$totalRenewalAmt} {$keywoDefaultCurrencyName}"; ?>"
                                                        title="<?php echo $totalRenewalAmt; ?>"
                                                        data-toggle="tooltip"
                                                        data-placement="bottom"><?php echo formatNumberToSort($totalRenewalAmt, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Please Purchase <span
                                                        onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                        origPrice="<?php echo $purchaseAmt; ?> <?php echo $keywoDefaultCurrencyName; ?>"
                                                        title="<?php echo $purchaseAmt; ?>"
                                                        data-toggle="tooltip"
                                                        data-placement="bottom"><?php echo formatNumberToSort($purchaseAmt, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span> to settle payment now</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-5" id="payment-mode-selection">
                                            <h5>Purchase <?php echo $keywoDefaultCurrencyName; ?> with<i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="radio radio-primary radio-div margin-top-none">
                                                        <input type="radio" name="payment_mode" id="Bitcoin" value="Bitcoin" checked="true">
                                                        <label for="Bitcoin">
                                                            Bitcoin
                                                        </label>
                                                    </div>
                                                    <span class="pull-right"><?php echo $userPaymentAmountBTC; ?></span>

                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <div class="radio radio-primary radio-div margin-top-none">
                                                        <input type="radio" name="payment_mode" id="PayPal" value="PayPal">
                                                        <label for="PayPal">
                                                            PayPal
                                                        </label>
                                                    </div>
                                                    <span class="pull-right"><?php echo formatNumberToSort($userPaymentAmountSGD, 4); ?>
                                                        SGD</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <!--<input value="Proceed to Purchase" type="button" class="btn-pay-now">-->
                                            <input value="Proceed to Purchase" onclick='renewKeywordByOthers("other")'
                                                   type="button"
                                                   class="btn-pay-now" style="background-color:rgb(150, 151, 152);"
                                                   disabled>
                                        </div>
                                    </div>
                                </div>
                                <!-- current-wallet-balance div ends here -->
                            </div>
                            <!--Bitgo payment dailog box -->
                            <div id="procceedToPayment" class="modal fade paymentModal" role="dialog">
                            </div>
                            <!--Bitgo payment dailog box ends -->

                        <?php }
                } else {
                    print("Error: getting user settings");
                    exit;
                }
            } else {
                print("Error: getting user details");
                exit;
            }
    } else {
        print("Error: Database connection");
        exit;
    }
} else {
    print("Error: Mendatory field not found");
    exit;
}