<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 17/2/17
 * Time: 7:49 PM
 */

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

//get email id from session
$email = $_SESSION["email"];
$sessionId = session_id();

require_once "{$docrootpath}views/layout/header.php";

if(isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_message"]) && isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_message"])){
    unset($_SESSION["get_error_code"]);
    unset($_SESSION["get_error_message"]);
}

if(isset($email) && !empty($email)){
    $kwdDbConn = createDBConnection('dbkeywords');
    if (noError($kwdDbConn)) {
        $kwdDbConn = $kwdDbConn["connection"];

        //Get Presale Status
        $presaleStatus = getPresaleStatus($kwdDbConn);
        if(noError($presaleStatus)){
            $presaleStatus = $presaleStatus["errMsg"]["presale_status"];
            if($presaleStatus != "1")
            {
                echo "Presale is closed";
                exit;
            }
        }else{
            echo "Error getting status";
            exit;
        }
    }else{
        /* return error message */
        echo "Error connecting database";
        exit;
    }
}else{
    $redirectURL = "{$rootUrl}index.php";
    print("<script>");
    print("window.location='".$redirectURL."'");
    print("</script>");
    exit;
}

$getCartDetails = getUserCartDetails($email, $kwdDbConn);
if(noError($getCartDetails)){
    $getCartDetails = $getCartDetails['errMsg'];
    $userCartAmount = $getCartDetails['user_balance'];
    $userCart = $getCartDetails['user_cart'];
    $userCart = json_decode($userCart, true);
    $userCartCount = count($userCart);

    if($userCartCount > 0){

        $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
        /*echo "getAdminSettingsFromKeywordAdmin</br>";
        printArr($adminSettings);die;*/
        if(noError($adminSettings)) {
            $errMsg = "getting admin settings of buyer user success.";
            $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];

            $userTotalCartPrice = getUserTotalCartPrice($kwdDbConn, $email);
            if(noError($userTotalCartPrice)){
                $userTotalCartPrice = $userTotalCartPrice["errMsg"]["total_cart_price"];

                if($firstBuyStatus){
                    $firstBuyCashBack = number_format(($userTotalCartPrice * $firstBuyPercent)/100,8);
                }else{
                    $firstBuyCashBack = 0.00000000;
                }

                $userPaymentAmount = number_format((float)($userTotalCartPrice - $firstBuyCashBack),2);
                $userPaymentAmountBTC = number_format((float)($userPaymentAmount * $getExchangeRate['btc']),8);
                $userPaymentAmountSGD = number_format((float)($userPaymentAmount * $getExchangeRate['sgd']),2);
            }else{
                echo "Error getting user cart total price";
                exit;
            }

        } else {
            print("Error: getting user settings");
            exit;
        }
    }else{
        $redirectURL = "{$rootUrl}views/keywords/marketplace/keyword_search.php";
        print("<script>");
        print("window.location='".$redirectURL."'");
        print("</script>");
        exit;
    }
}else{
    echo "Error getting user cart details";
    exit;
}


?>

<!-- Checkout page html starts from here -->

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<!-- container starts here -->


<main class="main-keyword inner-6x innerT">

    <div class="keyword-checkout keyword-marketplace checkout-floatingmenu keyword-markt-popup-common">

        <!-- container starts here -->
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-12 padding-left-none padding-right-none">
                            <!-- checkout keyword list starts here -->
                            <div class="checkout-keyword-list">
                                <div class="checkout-list border-All trasactionHistory">


                                    <!-- table starts here -->
                                    <table class="table table-sm m-0 fixed-custom-table" id="checkoutCartTable">
                                        <!--<thead class="">
                                        <tr>

                                            <th>Keyword</th>
                                            <th>
                                                <?php /*echo $_SESSION['CurrPreference']; */?>

                                            </th>
                                            <th>itd</th>
                                            <th></th>
                                        </tr>
                                        </thead>-->

                                        <?php

                                        include "{$docRoot}controllers/keywords/loadUserCheckoutCart.php";

                                        ?>

                                    </table>
                                    <!-- table ends here -->


                                </div>
                            </div>
                            <!-- checkout keyword list ends here -->

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 padding-left-none padding-right-none inner-2x innerMT">
                            <!-- add more keywords starts here -->
                            <div class="addmore-keywords text-center">
                                <a href="<?php echo "{$rootUrl}views/keywords/marketplace/keyword_search.php"; ?>"> Add More Keywords <i class="fa fa-angle-right pull-right"
                                                                                                                                         aria-hidden="true"></i></a>
                            </div>
                            <!-- add more keywords ends here -->

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12  padding-left-none padding-right-none inner-2x innerMT">
                            <!-- terms and conditions starts here -->
                            <div class="terms-n-conditions">
                                <div class="list-group list-cust border-all">
                                    <a class="list-group-item">Important Terms and Conditions</a>
                                    <a class="list-group-item text-primary">All keywords must be renewed every 12 months.</a>
                                    <a class="list-group-item">Please review keyword spelling before purchase. <br /> Keywo will not rectify spelling errors.</a>
                                    <a class="list-group-item">Keyword once purchased will not be refunded.</a>
                                    <a class="list-group-item">Keywo does not gurantee any specific income on keywords investment.</a>
                                    <a class="list-group-item">Keywords payouts  are given based on network income and algorithem.</a>
                                    <a class="list-group-item">Keywo reserves right to make changes to the system at anytime as it deems fit.</a>
                                    <a class="list-group-item">50% revenue is added to the Keywo Pool and the other 50% is collected by company to cover for development. </a>
                                </div>
                            </div>
                            <!-- terms and conditions ends here -->

                        </div>
                    </div>


                </div>
                <div class="col-md-8">
                    <div class="row innerMB" id="unavailableKeywords" style="display: none;">
                        <div class="col-md-12">
                            <div class="col-md-12 bg-red text-white border-all pull-right innerAll"> Oops! The keyword(s) <span id="unavailableKeywordsspan"></span> might be unavailable at the moment.<br> Either these keywords are purchased OR under payment process by other user.<br>Please check their availability again after 15 mins.<br>To proceed payment remove these keyword(s) from cart.<span class="errMsgTxtDivCross unavailable-keyword-box" onclick="closeErrImgCheckoutPayment();"><i class="fa fa-times" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="agreement-licence innerAll border-All innerMB">
                                <h4>Keyword Licence Purchase Agreement</h4>
                                <div class="agreement-scroll">
                                <!--<p>
                                    This Agreement details the manner in which the Licensee is being authorized to derive economic benefit from the terms licensed by it on the Licencor's platform (as described below).</p>
                                <p> By Clicking on "I Agree" at the end of the contract the Licensee acknowledges to be bound by all the terms as mentioned in this Agreement. This act of acceptance will be construed to be a legal equivalent to your signature on a written or digital/e-contract. Only by clicking "I Agree" will you be able to pay the fees to be granted a license to exploit the economic benefits derived on this platform from the specific Key Word (as described below) chosen by You.
                                    <br><br></p>

                                <h4>1.GRANT OF LICENCE</h4>
                                <p> 1.1. The Licensor hereby grants to the Licensee an exclusive, terminable, transferable license for the Term(as defined below) to derive economic benefit from the monetization of a digital asset, i.e. a query related to a specific word or words ("Key Word") on www.searchtrade.com ("Platform") in accordance with the terms of this License ("License").
                                </p>

                                <p> 1.2. You hereby understand that the Licensor does not guarantee the availability of the Key Word until such time that a confirmation message is sent to You. You further understand and acknowledge that until such time that the License is granted to You, others parties may also bid to acquire the Key Word.
                                </p>
                                <p>1.3. The Licensee shall pay license fees to the Licensor ("Consideration"). Such fees may either be determined by the Licensor or the Licensee may need to bid against other parties to acquire a license to a specific Key Word. The winning bid would be construed as a binding offer accepted by the Licensor.</p>
                                <p>1.4. The License shall be subject to the following:</p>
                                <br>-->
                                <?php include "term.php"; ?>

                                </div>
                                <br> <div class="text-center"><input value="I Accept Above Mentioned Terms & Conditions" type="button" class="btn-accept-licence"></div><br>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="clientSid" value="<?php echo $sessionId; ?>" >
                    <div class="row" id="paymentBox">
                        <?php

                            include "{$docRoot}views/keywords/cart/checkoutPaymentBox.php";
                        ?>
                    </div>
                </div>
            </div>
            <!-- container ends here -->

            <!-- Latest compiled and minified JavaScript -->
<!--            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"-->
<!--                    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"-->
<!--                    crossorigin="anonymous"></script>-->
            <br/>
            <br/>
            <!--      Confirmation dialog while click on payment now     -->
            <div id="keyword-popup-confirmation" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="cancelPurchase();">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB text-center">
                                            <span class="text-black word-wrap" id="response">Are you sure you want to buy keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left">
                                                <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">

                                                <input value="Yes" type="button" onclick="paymentAfterConfirmation();" id="btnYes" class="yes-btn btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;
                                                <input value="No" type="button" class="btn-trading-wid-auto innerMTB" id="btnNo" onclick="cancelPurchase();">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>


<?php



?>
<?php include('../../layout/transparent_footer.php'); ?>

<script src="<?php echo "{$rootUrlJs}checkout.js"; ?>" ></script>
<script>
    $(document).ready(function(){

    })
//    $('#keyword-popup-confirmation').modal({
//        backdrop: 'static',
//        keyboard: false
//    })
</script>
