<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 20/3/17
 * Time: 5:24 PM
 */


/*header("Access-Control-Allow-Origin: *");*/
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";

$clientSessionId = $_POST['client_id'];
$totalPriceInBtc = $_POST['finalBtcAmount'];
$totalPriceInUSD = $_POST['finalUsdAmount'];
$recieverAddress = $_POST['recieverAddress'];
$orderId  = $_POST['encryptedCustom'];
$keywordWithTags = $_POST['keywordWithTags'];
$keywordWithTags = rtrim($keywordWithTags,',');
$amountDue = $_POST["amount_due"];
$walletBalance = $_POST["wallet_balance"];
$amtDueInBTC = $_POST["amount_due_in_btc"];

$clientSessionId           = urldecode($clientSessionId);
//get current session Id and refferer page
$currentSessionId    = session_id();

/*echo "<pre>";
print_r($_POST);
echo "</pre>";*/

if ($clientSessionId == $currentSessionId) {?>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header innerAll">
                <h4 class="margin-none text-blue">Keyword Purchase</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-minus-circle text-color-Gray"></i></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 keywo-banner half" style="padding: 19px 20px; background:#0b6796;">
                        <img src="<?php echo $rootUrlImages?>/logo.svg" class="img-responsive" alt="Keywo" style="height: 46px;">
                    </div>
                </div>
                <div class="row innerMT inner-2x">
                    <div class="col-xs-12">
                        <div class="purchase-keywords-list">
                            Purchase of keyword <a class="text-blue half innerMLR" data-toggle="tooltip" title="<?php  echo "#".$keywordWithTags; ?>"><?php  echo "#".$keywordWithTags; ?></a>
                        </div>
                    </div>
                </div>
                <div class="row innerMTB">
                    <div class="col-xs-3">
                        Payable amount
                    </div>
                    <div class="col-xs-1">
                        :
                    </div>
                    <div class="col-xs-8">
                        <?php  echo $totalPriceInUSD." ".$keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="col-xs-3">
                        Wallet blanace
                    </div>
                    <div class="col-xs-1">
                        :
                    </div>
                    <div class="col-xs-8">
                        <?php  echo number_format($walletBalance, 2)." ".$keywoDefaultCurrencyName;  ?>
                    </div>
                    <div class="col-xs-3">
                        Payment Due
                    </div>
                    <div class="col-xs-1">
                        :
                    </div>
                    <div class="col-xs-8">
                        <?php  echo $amountDue.  " {$keywoDefaultCurrencyName}         ({$amtDueInBTC} BTC)";  ?>
                    </div>
                </div>
                <div class="row" id="paymentDetails">
                    <div class="col-xs-3 padding-none">
                        <a><img class="img-responsive" src="https://chart.googleapis.com/chart?chs=135x150&cht=qr&chl=bitcoin:<?php echo $recieverAddress; ?>" alt = 'Bitcon Address' disabled="disabled" /></a>
                    </div>
                    <div class="col-xs-9 padding-left-none innerMT">
                        <div class="">
                            Send excatly <span class="text-blue"><?php echo $amtDueInBTC." BTC"; ?> </span> to this address
                        </div>
                        <div class="innerMTB form-group">
                            <input type="text" name="" class="bg-light-purple form-control" id="btcAddress" value="<?php echo $recieverAddress; ?>" disabled>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 half innerL">
                                <div class="btn btn-social-wid-auto pull-left" id="copyTextButton">
                                    Copy Address
                                </div>

                                <div id="msg" style="color: green" class="pull-right"></div>
                            </div>
                            <div class="col-xs-2 text-right">
                                <span id="countdown" ></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="inner-3x text-center" id="paymentStatus">
                            Waiting for payment...
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        var start = new Date();
        var end = new Date();
        var t,c;
        end.setMinutes(end.getMinutes()+1);
        //var seconds = 900;
        var seconds = ((end - start)/1000) ;

        function secondPassed() {
            var minutes = Math.round((seconds - 30) / 60);
            var remainingSeconds = seconds % 60;
            if (remainingSeconds < 10) {
                remainingSeconds = "0" + remainingSeconds;
            }
            document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds;
            console.log(minutes + ":" + remainingSeconds)

        }

        function countdown() {
            // starts countdown
            secondPassed();
            if (seconds != 0) {
                seconds--;
                t = setTimeout(countdown, 1000);
            } else {
                stopTimer();
                $("#paymentStatus").html("Sorry, We did not receive your payment. Please try again.");
                $("#paymentStatus").addClass('text-red');
            }
        }

        function stopTimer() {
            //pauses countdown
            clearTimeout(t);
            clearInterval(c);
        }

        //Check confirmation for payment on every 10 seconds
        function checkConfirmation()
        {
            c = setInterval(checkForBitgoConfirmation, 10000);
        }

        $(document).ready(function(){
            countdown();
            checkConfirmation();

        });

        //Code to copy text to clipboard
        document.getElementById("copyTextButton").addEventListener("click", function() {
            copyToClipboardMsg(document.getElementById("btcAddress"), "msg");
        });

        function copyToClipboardMsg(elem, msgElem) {
            var succeed = copyToClipboard(elem);
            var msg;
            if (!succeed) {
                msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
            } else {
                msg = "Text copied to the clipboard."
            }
            if (typeof msgElem === "string") {
                msgElem = document.getElementById(msgElem);
            }
            msgElem.innerHTML = msg;
            setTimeout(function() {
                msgElem.innerHTML = "";
            }, 2000);
        }


        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
    </script>

<?php } else {


}

?>
