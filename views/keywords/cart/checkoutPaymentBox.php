<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/header/headerModel.php");

$email = $_SESSION["email"];
/*$sessionId = session_id();*/
//$currencyPref = $_SESSION["CurrPreference"];
$upc = $_SESSION['CurrPreference'];


$errCode = 1;
$errMsg = '';
if (!empty($email)) {
//Keywords database connection
    $kwdConn = createDbConnection('dbkeywords');

    if (noError($kwdConn)) {
        $kwdConn = $kwdConn["connection"];

        $presaleStatus = getPresaleStatus($kwdConn);
        if (noError($presaleStatus)) {
            $presaleStatus = $presaleStatus["errMsg"]["presale_status"];

            /*$buyerBalanceFields = $userRequiredFields . ",cashback,first_buy_status";*/
            $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";
            $buyerBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $buyerBalanceFields);
            if (noError($buyerBalanceDetails)) {

                $firstBuyStatus = $buyerBalanceDetails["errMsg"]["first_buy_status"];
                $getUserBalance = calculateUserBalance($buyerBalanceDetails);
                if (noError($getUserBalance)) {
                    $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                } else {
                    $userBalance = 0;
                }

                /* Get user admin settings from keyword admin */
                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdConn);
                if (noError($adminSettings)) {
                    $errMsg = "getting admin settings of buyer user success.";
                    $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];

                    $userTotalCartPrice = getUserTotalCartPrice($kwdConn, $email);
                    if (noError($userTotalCartPrice)) {
                        $userTotalCartPrice = $userTotalCartPrice["errMsg"]["total_cart_price"];

                        $getExchangeRate = getExchangeRates($upc);
                        if (noError($getExchangeRate)) {
                            $exchangeRates = $getExchangeRate['currRate'];
                        } else {
                            print("Error: getting exchange rate");
                        }

                        $upc = strtolower($upc);

                        if ($firstBuyStatus) {
                            $firstBuyCashBack = number_format(($userTotalCartPrice * $firstBuyPercent) / 100, 8);
                        } else {
                            $firstBuyCashBack = 0.00000000;
                        }

                        $cartPriceAfterDiscount = number_format((float)$userTotalCartPrice - $firstBuyCashBack, 2);

                        $userPaymentAmount = number_format((float)($userTotalCartPrice - $firstBuyCashBack), 4);

                        if ($userBalance >= $userPaymentAmount) {

                            ?>

                            <div class="col-md-12 inner-2x innerMT">
                                <div class="current-wallet-balance">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="current-w1 innerAll innerPL-5">
                                                <h5>Current Wallet Balance is : <span
                                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                            origPrice="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>"
                                                            title="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip"
                                                            data-placement="bottom"><?php echo formatNumberToSort($userBalance, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Your payment amount : <span
                                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                            origPrice="<?php echo "{$userPaymentAmount} {$keywoDefaultCurrencyName}"; ?>"
                                                            title="<?php echo "{$userPaymentAmount} {$keywoDefaultCurrencyName}"; ?>"
                                                            data-toggle="tooltip"
                                                            data-placement="bottom"><?php echo formatNumberToSort($userPaymentAmount, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pull-right inner-3x innerMT"><input value="Pay Now"
                                                                                            onclick='prePaymentProcess("wallet")'
                                                                                            type="button"
                                                                                            class="btn-pay-now"
                                                                                            id="pay-now-by-wallet"
                                                                                            style="background-color:rgb(150, 151, 152);"
                                                                                            disabled></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php } else {

                            $purchaseRemainingITD = $userPaymentAmount - $userBalance;
                            $userPaymentAmountBTC = number_format((float)($purchaseRemainingITD * $exchangeRates['btc']), 8);
                            $userPaymentAmountSGD = number_format((float)($purchaseRemainingITD * $exchangeRates['sgd']), 2);
                            ?>

                            <div class="col-md-12 inner-2x innerMT">
                                <!-- current-wallet-balance div starts here -->
                                <div class="current-wallet-balance">
                                    <div class="row inner-1x innerAll innerPB-20 innerPL-5">
                                        <div class="col-md-7">
                                            <div class="current-w1">
                                                <h5>Current Wallet Balance is : <span
                                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                            origPrice="<?php echo "{$userBalance} {$keywoDefaultCurrencyName}"; ?>"
                                                            title="<?php echo number_format($userBalance,4,'.',''); ?>" data-toggle="tooltip"
                                                            data-placement="bottom"><?php echo formatNumberToSort($userBalance, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Your payment amount : <span
                                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                            origPrice="<?php echo "{$userPaymentAmount} {$keywoDefaultCurrencyName}"; ?>"
                                                            title="<?php echo number_format($userPaymentAmount,4,'.',''); ?>"
                                                            data-toggle="tooltip"
                                                            data-placement="bottom"><?php echo formatNumberToSort($userPaymentAmount, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span></h5>
                                                <h5>Please Purchase <span
                                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                            origPrice="<?php echo "{$purchaseRemainingITD} {$keywoDefaultCurrencyName}"; ?>"
                                                            title="<?php echo number_format($purchaseRemainingITD,4,'.',''); ?>"
                                                            data-toggle="tooltip"
                                                            data-placement="bottom"><?php echo formatNumberToSort($purchaseRemainingITD, 4); ?>
                                                        <?php echo $keywoDefaultCurrencyName; ?></span> to settle payment now</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-5" id="payment-mode-selection">
                                            <h5>Purchase <?php echo $keywoDefaultCurrencyName; ?> with<i class="fa fa-question-circle pull-right"
                                                                    aria-hidden="true"></i></h5>
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="radio radio-primary radio-div margin-top-none">
                                                        <input type="radio" name="payment_mode" id="Bitcoin" class="paymentByGateway"
                                                               value="Bitcoin"
                                                               checked="true">
                                                        <label for="Bitcoin">
                                                            Bitcoin
                                                        </label>
                                                    </div>
                                                    <span class="pull-right"><?php echo $userPaymentAmountBTC; ?></span>

                                                </div>
                                            </div>
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="radio radio-primary radio-div margin-top-none">
                                                        <input type="radio" name="payment_mode" id="PayPal" class="paymentByGateway"
                                                               value="PayPal">
                                                        <label for="PayPal">
                                                            PayPal
                                                        </label>
                                                    </div>
                                                    <span class="pull-right"><?php echo formatNumberToSort($userPaymentAmountSGD, 2); ?>
                                                        SGD</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-12 text-center">
                                            <!--<input value="Proceed to Purchase" type="button" class="btn-pay-now">-->
                                            <input value="Proceed to Purchase" onclick='prePaymentOthers("other")'
                                                   type="button"
                                                   class="btn-pay-now" style="background-color:rgb(150, 151, 152);"
                                                    id="pay-by-others"
                                                   disabled>
                                        </div>
                                    </div>
                                </div>
                                <!-- current-wallet-balance div ends here -->
                            </div>
                            <!--Bitgo payment dailog box -->
                            <div id="procceedToPayment" class="modal fade paymentModal" role="dialog">
                            </div>
                            <!--Bitgo payment dailog box ends -->

                        <?php }

                    } else {
                        echo "Error getting user cart total price";
                        exit;
                    }

                } else {
                    print("Error: getting user settings");
                    exit;
                }
            } else {
                print("Error: getting user details");
                exit;
            }

        } else {
            echo "error: Presale status is closed";
            exit;
        }

    } else {
        print("Error: Database connection");
        exit;
    }
} else {
    print("Error: Session Expired");
    exit;
}
