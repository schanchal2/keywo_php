<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 17/2/17
 * Time: 7:49 PM
 */

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

//get email id from session
$email = $_SESSION["email"];
$currPreference = strtolower($_SESSION['CurrPreference']);
$sessionId = session_id();


$keyword = urldecode($_POST["keywordToRenew"]);
$clientSessionIdFrmOwnedKwd = urldecode($_POST["clientSessionIdKeywordRenew"]);
$totalUPCAmt = 0;
$totalITDAmt = 0;
$totalBTCAmt = 0;

if(isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_message"]) && isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_message"])){
    $getErrorCode = $_SESSION["get_error_code"];
    $getErrorMsg = $_SESSION["get_error_message"];
}


require_once "{$docrootpath}views/layout/header.php";

    if(isset($email) && !empty($email)){

        // create keywrod database connection
        $kwdDbConn = createDBConnection("dbkeywords");
       if(noError($kwdDbConn)){
            $kwdDbConn = $kwdDbConn["connection"];

            // get admin setting details
            $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
            if(noError($getAdminSetting)){
                $getAdminSetting = $getAdminSetting["data"];

                $getRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

                // calculate btc amount from renewal amount
                $btcRate = number_format($getExchangeRate["btc"], 8);
                $calcBtcOfRenewalAmt =  number_format(($getRenewalAmt*$btcRate), 8);

                // calculate upc amount from renewal amount.
                $upcRate = $getExchangeRate[$currPreference];
                $calcUpcRateOfRenewalAmt = number_format(($upcRate * $getRenewalAmt), 8);

                $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
                if(noError($getPresaleDetails)){
                    $getPresaleDetails = $getPresaleDetails["errMsg"];
                    $getRenewKwd = json_decode($getPresaleDetails["user_renew_kwd_in_cart"], true);

                    if(count($getRenewKwd) > 0){




?>

<!-- Checkout page html starts from here -->

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<!-- container starts here -->
<main class="main-keyword inner-6x innerT">

    <div class="keyword-checkout keyword-marketplace checkout-floatingmenu keyword-markt-popup-common">

        <!-- container starts here -->
        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-xs-12 padding-left-none padding-right-none">
                            <!-- checkout keyword list starts here -->
                            <div class="checkout-keyword-list">
                                <div class="checkout-list border-All trasactionHistory">
                                    <!-- table starts here -->
                                    <table class="table table-sm m-0 fixed-custom-table" id="renewMoreKeyword">
                                        <?php

                                        include "{$docRoot}controllers/keywords/loadUserRenewCart.php";

                                        ?>
                                    </table>
                                    <!-- table ends here -->
                                </div>
                            </div>
                            <!-- checkout keyword list ends here -->

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 padding-left-none padding-right-none inner-2x innerMT">
                            <!-- add more keywords starts here -->
                            <div class="addmore-keywords text-center">
                                <a  href="" data-toggle="modal" data-target="#keyword-popup-newkeyword" id="renewOwnershipKeyword"> Add More Keywords To Renew <i class="fa fa-angle-right pull-right"
                                                                                                                                         aria-hidden="true"></i></a>
                            </div>
                            <!-- add more keywords ends here -->

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12  padding-left-none padding-right-none inner-2x innerMT">
                            <!-- terms and conditions starts here -->
                            <div class="terms-n-conditions">
                                <div class="list-group list-cust border-all">
                                    <a class="list-group-item">Important Terms and Conditions</a>
                                    <a class="list-group-item text-primary">All keywords must be renewed every 12 months.</a>
                                    <a class="list-group-item">Please review keyword spelling before purchase. <br /> Keywo will not rectify spelling errors.</a>
                                    <a class="list-group-item">Keyword once purchased will not be refunded.</a>
                                    <a class="list-group-item">Keywo does not gurantee any specific income on keywords investment.</a>
                                    <a class="list-group-item">Keywords payouts  are given based on network income and algorithem.</a>
                                    <a class="list-group-item">Keywo reserves right to make changes to the system at anytime as it deems fit.</a>
                                    <a class="list-group-item">50% revenue is added to the Keywo Pool and the other 50% is collected by company to cover for development. </a>
                                </div>
                            </div>
                            <!-- terms and conditions ends here -->

                        </div>
                    </div>


                </div>
                <div class="col-md-8">
                    <div class="row innerMB" id="unavailableKeywords" style="display: none;">
                        <div class="col-md-12">
                            <div class="col-md-12 bg-red text-white border-all pull-right innerAll"> Oops! The keyword(s) <span id="unavailableKeywordsspan"></span> might be unavailable at the moment.<br> Either these keywords are purchased OR under payment process by other user.<br>Please check their availability again after 15 mins.<br>To proceed payment remove these keyword(s) from cart.<span class="errMsgTxtDivCross unavailable-keyword-box" onclick="closeErrImgCheckoutPayment();"><i class="fa fa-times" aria-hidden="true"></i></span> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="agreement-licence innerAll border-All innerMB">
                                <h4>Keyword Licence Agreement (Renewal)</h4>
                                <div class="agreement-scroll">
                                    <?php include "term.php"; ?>

                                </div>
                                <br> <div class="text-center"><input value="I Accept Above Mentioned Terms & Conditions" type="button" class="btn-accept-licence" id="btn-kwd-renew-licence"></div><br>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="clientSid" value="<?php echo $sessionId; ?>" >
                    <div class="row" id="paymentBox">
                        <?php

                        include "{$docRoot}views/keywords/cart/renewalCheckoutPaymentBox.php";
                        ?>
                    </div>
                </div>
            </div>
            <!-- container ends here -->

            <!-- Latest compiled and minified JavaScript -->
            <!--            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"-->
            <!--                    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"-->
            <!--                    crossorigin="anonymous"></script>-->
            <br/>
            <br/>
            <!--      Confirmation dialog while click on payment now     -->
            <div id="keyword-popup-confirmation" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="cancelConfirmation();">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB text-center">
                                            <span class="text-black" id="response">Are you sure you want to buy keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left">
                                                <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Yes" type="button" id="pay-for-renew" class="renew-yes-btn btn-trading-wid-auto-dark innerMTB" onclick='renewKeywordLicence();'>&nbsp;&nbsp;
                                                <input value="No" type="button" class="btn-trading-wid-auto innerMTB" id="change_to_ok" onclick="cancelConfirmation();">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--      Confirmation dialog while click on payment now     -->
            <div id="keyword-renew-popup-confirmation" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Confirmation</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB text-center">
                                            <span class="text-black" id="response-other">Are you sure you want to buy keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left">
                                                <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Yes" type="button" id="renew-by-other" class="renew-yes-btn btn-trading-wid-auto-dark innerMTB" onclick='renewConfirmation();'>&nbsp;&nbsp;
                                                <input value="No" type="button" class="btn-trading-wid-auto innerMTB" onclick="cancelRenewDialog();">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <input type="hidden"  id="oldRenewKeyword" value="<?php echo $keyword; ?>" />
            <input type="hidden" id="renewalAmount" value="<?php echo number_format($getRenewalAmt, 4); ?>" />
            <!--  add new keywords popup starts here   -->
            <div id="keyword-popup-newkeyword" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">

                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Add Keywords to Renew</h4>
                        </div>

                        <div class="modal-body innerAll" id="tooltip-arrw--left" >
                            <div style="height:330px; overflow-y:auto; overflow-x:hidden;">
                                <div class="" id="loadRenewKeywordWithinDays">

                                </div>
                            </div>
                            <div class="row innerB">
                                <div class="col-xs-10" >
                                    <div id="show-error" class="text-center text-color-Red"></div>
                                </div>
                                <div class="col-xs-2">
                                    <input value="Add" type="button" id="addMoreKeyword" class="btn-trading-wid-auto pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--   add new keywords popup ends here     -->

        </div>
</main>

<?php include('../../layout/transparent_footer.php'); ?>

<script src="<?php echo "{$rootUrlJs}checkout.js"; ?>" ></script>
<script src="<?php echo "{$rootUrlJs}renew_keyword.js"; ?>" ></script>


<?php
                }else{
                        $redirectURL = $rootUrl."views/keywords/user_dashboard/owned_keyword.php";
                        print("<script>");
                        print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
                        print("</script>");
                }
            }else{
                print('Error: Fetching renew keyword details');
            }
        }else{
            print('Error: Fetching admin setting details.');
        }
   }else{
       print('Error: Database connection');
   }
}else{
    print("Can't process you request, Please try after sometime.");
}


?>

<script>
    var getErrorCode = '<?php echo $getErrorCode; ?>';
    var getErrorMsg = '<?php echo $getErrorMsg; ?>';

    if(getErrorCode != ''){
        showToast("failed",getErrorMsg);
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        updateUserTimezone(rootUrl);
    });
</script>
