This Agreement details the manner in which the Licensee is being authorized to derive economic benefit from the terms licensed by it on the Licencor's platform (as described below).
<br>
<br>By Clicking on "I Agree" at the end of the contract the Licensee acknowledges to be bound by all the terms as mentioned in this Agreement. This act of acceptance will be construed to be a legal equivalent to your signature on a written or digital/e-contract. Only by clicking "I Agree" will you be able to pay the fees to be granted a license to exploit the economic benefits derived on this platform from the specific Key Word (as described below) chosen by You.
<h4>1. GRANT OF LICENCE</h4>
1.1. The Licensor hereby grants to the Licensee an exclusive, terminable, transferable license for the Term(as defined below) to derive economic benefit from the monetization of a digital asset, i.e. a query related to a specific word or words ("Key Word") on www.searchtrade.com ("Platform") in accordance with the terms of this License ("License").
<br>
<br>1.2. You hereby understand that the Licensor does not guarantee the availability of the Key Word until such time that a confirmation message is sent to You. You further understand and acknowledge that until such time that the License is granted to You, others parties may also bid to acquire the Key Word.
<br>
<br>1.3. The Licensee shall pay license fees to the Licensor ("Consideration"). Such fees may either be determined by the Licensor or the Licensee may need to bid against other parties to acquire a license to a specific Key Word. The winning bid would be construed as a binding offer accepted by the Licensor.
<br><br>1.4. The License shall be subject to the following:<br><br>a. Payment of Consideration to the Licensor;<br>
<br>b. You shall not apply (directly or indirectly) or assist any third party (whether directly or indirectly) for registration of any Key Word or trademarks or domain names which are similar or deceptively similar to Your Key Word in Your or any other country.
<br><br>c. Any transfer of the Key Word shall be as per the terms of this License.<br>
<br>d. The Licensee is not entitled to lease/mortgage/sub-let etc. any rights in the Key word.<br>
<br>1.5. In event of death of the Licensee the License shall stand terminated and the rights in the Key Word shall revert to the Licensor.
<br>
<br>1.6. The Licensee acknowledges understands and agrees that Key Word may be subject to intellectual property protection and remains the property of its owners. The Licensor does not claim any sort of ownership or any other rights in the Key Word.
<br>
<br>1.7. Any use of the Key Word which is not in compliance with the Agreement will be illegal and shall be deemed to be a material breach of the terms of this License and shall entitle the Licensor to take legal action and claim damages against you for any real or notional loss(es).
<h4>2. SCOPE AND USAGE</h4>
2.1. Licenses are granted only to those individuals or entities ("Users") that create an account and register with us. Once registered on searchtrade.com, you may access your account using your email address and password.
<br>
<br>2.2. Upon registration Users will be provided with a SearchTrade Wallet which will be connected to your SearchTrade account through which all transactions to STN or from STN will transpire.
<br>
<br>2.3. Your License is associated with your SearchTrade account. We therefore recommend that to keep your account secured should use a strong password preferably entailing a combination of alphabets (both upper and lower case), numbers and special characters for your SearchTrade.
<br>
<br>2.4. Upon grant of the License, the Licensee shall be entitled to receive consideration ("Share") received from the exploitation of Key Word in the form and manner specified by the Licensee and more particularly provided at. It is hereby clarified, the Share(if any), that is due and payable to the Licensee shall be paid in the form of bitcoins.
<br>
<br>2.5. Such Share will be deposited on a daily basis or such interval as may be determined by the Licensor to the SearchTrade Wallet connected with your SearchTrade account.
<br>
<br>2.6. The Licensor acknowledges that most jurisdictions have not decided on the per-say legality of bitcoins. You acknowledge and understand that bitcoins or any other crypto-currency may not be recognized as a legal form of consideration and may also be prohibited in Your jurisdiction. You also understand the risks that this may impose on your Share and after having such understanding have applied for this License.
<br><br>2.7. The License shall be subject to the following:<br>
<br>a. Usage of the Key Word in accordance with applicable laws;<br>
<br>b. Usage of the Keyword in a manner which, that causes an adverse effect on the reputation and/or goodwill of the Licensor or any other person;
<br>
<br>c. You have no other rights or claims in the Key Word for any medium other than the Platform other than those already provided under this License;
<h4>3. PROVISION OF THE SERVICES BY THE LICENSOR</h4>
3.1. The Licensor is constantly researching and innovating in order to improve the user experience. As a Licensee You understand that it is Your duty to periodically check for any updates to the Agreement and the Licensor shall not be liable to inform You of any update. Consequently, Licensor reserves the right to change the whole or part of the Agreement or the use of the Key Word from time to time. In the event of such change and subsequent change(s) to the Agreement, the Agreement shall be deemed to have been suitably amended, modified and updated so as to give effect to such changes. Continued usage of the Key Word and/or any services offered by the Licensor and/or any access to Your SearchTrade account would mean that you consent and agree to be bound by the Terms of the amended Agreement.
<br><br>3.2. During the Term of the License, the Licensor may:<br>
<br>a. change the content and information on the Platform, including but not limited to the manner of exploitation of the Key Word without notice to You;
<br>
<br>b. provide all or any of services either free-of-cost or on a charge basis or a combination of the two.  This shall also include the right to alter, increase, decrease of waive any fee charged by the Platform;
<br>
<br>c. take appropriate actions, including suspension of your access to the Key Word where we reasonably suspect or are made aware that the Key Word is being used in a manner that is in breach of terms of the Agreement or applicable law;
<br><br>d. limit the number of searches that an account may make with respect to a Key Word.<h4>4.
    REPRESENTATIONS AND WARRANTIES</h4>
4.1. You may apply for a License and will be granted a License only if You:
<br>a. are eligible to form a binding contract with STN;<br>
<br>b. are of the legal age to contract in Your jurisdiction;<br>
<br>c. are not a person barred from contracting under the laws or directions of any governmental authority in your jurisdiction;
<br>
<br>d. comply with all applicable local, state, national and international laws, rules and regulations at the time being in force, while using the Key Word therein;
<br><br>e. have read the terms of the Agreement and agree to be bound by the same;<br>
<br>f. comply with the terms of the License.<br>
<br>4.2. In the event You are applying for a License on behalf of another legal entity, You confirm that:<br>
<br>a. You have the authority to act on behalf of such a legal entity and bind it;<br>
<br>b. this Agreement will apply to such legal entity as well. In such cases, reference to "you" or "your" in the Agreement shall refer to such legal entity and its affiliates (where applicable) on whose behalf You have acted.