
<?php
// echo "Testing"; die;
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}models/social/socialModel.php");

error_reporting(0);

//post_interactions

$email = $_SESSION["email"];
$clientSessionId = session_id();

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$postFields = "post_interactions";
$userDetail = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $postFields);
if (noError($userDetail)) {
    $totalLikes = isset($userDetail['errMsg']['post_interactions']) ? $userDetail['errMsg']['post_interactions'] : 0;
//    $totalConsumptionInteraction = 1000;
}

$userCartDetails = getUserCartDetails($email, $conn);

if(noError($userCartDetails)){
    $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
    $userCartDetails = json_decode($userCartDetails, TRUE);
}else{
    print('Error: Fetching cart details');
    exit;
}

$keyword = $_GET['keyword'];
$id      = $_GET['id'];

$m= date("m");

$de= date("d");

$y= date("Y");

$addedResult      = 0;
$addedEarning     = 0;

$addedvideo       = 0;
$addedblog        = 0;
$addedaudio       = 0;
$addedimage       = 0;
$addedstatus      = 0;

$earningsvideo    = 0;
$earningsblog     = 0;
$earningsaudio    = 0;
$earningsimage    = 0;
$earningsstatus   = 0;
$totalEarning     = 0;
$totalInteraction = 0;

$requestUrl= $socialUrl;
$totalPost = getTotalPost($keyword,$socialUrl);
if(noError($totalPost))
{
    $errMsg = $totalPost["errMsg"];
    $totalPostCount = $errMsg["Count"];
}
//var_dump($keyword);
$kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $keyword);
if(noError($kwdRevenueDetails)) {
    $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
    $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
    $totalPaidInteraction = $kwdRevenueDetails["app_kwd_search_count"];
    $totalPaidSearch = $kwdRevenueDetails["user_kwd_search_count"];
    $search_startFrom = $kwdRevenueDetails["search_startFrom"];
    $followUnfollow = $kwdRevenueDetails["follow_unfollow"];
    $followUnfollowDetail = json_decode($followUnfollow, true);
    $followUnfollowKeyword = $followUnfollowDetail["email"];
    $followUnfollowKeywordCount = count($followUnfollowKeyword);


    $date1 = date_create($search_startFrom);
    $date2 = date_create(date('Y-m-d'));
    $diff = date_diff($date1, $date2);
    $search_startFrom = $diff->format("%a%");
    $search_startFrom = intval($search_startFrom);


    $kwdAnaliticsDetails = getkeywordAnaliticsDetails($conn, $keyword); //echo "<pre>"; print_r($kwdAnaliticsDetails); echo "</pre>";
    $checkForKeywordAvailability = checkForKeywordAvailability($keyword, $conn);

    if (noError($checkForKeywordAvailability)) {

        $kwdTableName = getKeywordOwnershipTableName($keyword);   //echo $kwdTableName;
        $availabilityFlag = $checkForKeywordAvailability['errMsg'];
        $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
        $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
        $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
        $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
        $CartStatus = $checkForKeywordAvailability['status'];
        $activeBids = $checkForKeywordAvailability["active_bids"];
        $previous_owner = $checkForKeywordAvailability["previous_owner"];

        $previous_ownerJson = json_decode($previous_owner, true);

        if (noError($kwdAnaliticsDetails)) {
            $kwdAnalyticsTableName = $kwdAnaliticsDetails["table_name"];
            $kwdAnaliticsDetails = $kwdAnaliticsDetails["data"][0];

            $array = array(
                '7 Days' => array('xaxis' => 7, 'points' => 1, 'month' => 7),
                '30 Days' => array('xaxis' => 6, 'points' => 5, 'month' => 30),
                '3 Months' => array('xaxis' => 6, 'points' => 15, 'month' => 90),
                '6 Months' => array('xaxis' => 6, 'points' => 30, 'month' => 180),
                '9 Months' => array('xaxis' => 6, 'points' => 45, 'month' => 273),
                '12 Months' => array('xaxis' => 6, 'points' => 60, 'month' => 365)
            );

            if (array_key_exists($id, $array)) {

                $xaxis1 = $array[$id]['xaxis'];
                $points = $array[$id]['points'];
                $montharray = $array[$id]['month'];

                $search_Interaction = 0;
                $video = 0;
                $blog = 0;
                $image = 0;
                $status = 0;
                $audio = 0;
                $deduct = 0;
                $searchInteraction = 0;
                $socialInteraction = 0;
                $totalCount = 0;
                $daywiseDataArray = array();

                for ($j = 0; $j < $montharray; $j++) {

                    $datawiseDate = date('d-m-y', mktime(0, 0, 0, $m, ($de - $j), $y));
                    $monthwiseDate = lcfirst(date('M', mktime(0, 0, 0, $m, ($de - $j), $y)));

                    $currentMonth = $kwdAnaliticsDetails[$monthwiseDate];
                    $currentMonth = json_decode($currentMonth, true);
                    $daywiseDataArray[$datawiseDate] = $currentMonth["analytics"][$datawiseDate];
                }

                foreach ($daywiseDataArray as $month) {
                    $searchInteraction = $month["search"]["search_Interaction"];
                    $searchEarning = $month["search"]["search_Earning"];
                    $socialInteraction = $month["social"];

                    $socialEarning = $month["social_Earning"];

                    $socialvideo = $socialInteraction["video"];
                    $socialblog = $socialInteraction["blog"];
                    $socialaudio = $socialInteraction["audio"];
                    $socialimage = $socialInteraction["image"];
                    $socialstatus = $socialInteraction["status"];

                    $earningvideo = $socialEarning["video"];
                    $earningblog = $socialEarning["blog"];
                    $earningaudio = $socialEarning["audio"];
                    $earningimage = $socialEarning["image"];
                    $earningstatus = $socialEarning["status"];

                    $addedResult = $addedResult + $searchInteraction;
                    $addedEarning = $addedEarning + $searchEarning;
                    $addedvideo = $addedvideo + $socialvideo;
                    $addedblog = $addedblog + $socialblog;
                    $addedaudio = $addedaudio + $socialaudio;
                    $addedimage = $addedimage + $socialimage;
                    $addedstatus = $addedstatus + $socialstatus;

                    $earningsvideo = $earningsvideo + $earningvideo;
                    $earningsblog = $earningsblog + $earningblog;
                    $earningsaudio = $earningsaudio + $earningaudio;
                    $earningsimage = $earningsimage + $earningimage;
                    $earningsstatus = $earningsstatus + $earningstatus;

                }

                $totalInteraction = $addedResult + $addedvideo + $addedblog + $addedaudio + $addedimage + $addedstatus;
                $totalEarning = $addedEarning + $earningsvideo + $earningsblog + $earningsaudio + $earningsimage + $earningsstatus;

            }
        }
    }
}

?>

<div class="shortingDateWise">
    <div class="col-md-6 innerMB padding-left-none padding-right-none">
        <!-- keyword - 30 days starts here -->
        <div class="pagination-content text-center table-keyword-marketplace">
            <table class="table margin-none">
                <thead class="pagination-head text-white ">
                <td>
                    <select class="select-analytics border-all pull-left" id="topsorting">
                        <option selected id="<?php echo $id; ?>" hidden><?php echo $id; ?></option>
                        <option id="7 Days">7 Days</option>
                        <option id="30 Days">30 Days</option>
                        <option id="3 Months">3 Months</option>
                        <option id="6 Months">6 Months</option>
                        <option id="9 Months">9 Months</option>
                        <option id="12 Months">12 Months</option>
                    </select>
                </td>
                <td>
                    Interaction
                </td>
                <td>
                    Earning
                </td>
                <tbody class=" keyword-analytics-li-pad">
                <tr>
                    <td>
                        <a here="#"  class="display-in-block-txt-blk text-blue ellipses ellipses-wid half innerT"  title="#Blog" data-toggle="tooltip" data-placement="bottom">
                            <span class=" -wid">#Blog</span>
                        </a>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none"><a href="#" class="display-in-block-txt-blk ellipses ellipses-wid" title="<?php echo $addedblog; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $addedblog; ?></a></label>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none">
                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                               origPrice="<?php echo number_format("{$earningsblog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningsblog}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningsblog}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                            </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a here="#"  class="display-in-block-txt-blk text-blue ellipses ellipses-wid half innerT"  title="#Audio" data-toggle="tooltip" data-placement="bottom">
                            <span class=" -wid">#Audio</span>
                        </a>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none"><a href="#" class="display-in-block-txt-blk ellipses ellipses-wid" title="<?php echo $addedaudio; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $addedaudio; ?></a></label>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none">
                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                               origPrice="<?php echo number_format("{$earningsaudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningsaudio}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningsaudio}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a here="#"  class="display-in-block-txt-blk text-blue ellipses ellipses-wid half innerT"  title="#Video" data-toggle="tooltip" data-placement="bottom">
                            <span class=" -wid">#Video</span>
                        </a>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none"><a href="#" class="display-in-block-txt-blk ellipses ellipses-wid" title="<?php echo $addedvideo; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $addedvideo; ?></a></label>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none">
                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                               origPrice="<?php echo number_format("{$earningsvideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningsvideo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningsvideo}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a here="#"  class="display-in-block-txt-blk text-blue ellipses ellipses-wid half innerT"  title="#Image" data-toggle="tooltip" data-placement="bottom">
                            <span class=" -wid">#Image</span>
                        </a>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none"><a href="#" class="display-in-block-txt-blk ellipses ellipses-wid" title="<?php echo $addedimage; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $addedimage; ?></a></label>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none">
                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                               origPrice="<?php echo number_format("{$earningsimage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningsimage}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningsimage}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a here="#"  class="display-in-block-txt-blk text-blue ellipses ellipses-wid half innerT"  title="#Status" data-toggle="tooltip" data-placement="bottom">
                            <span class=" -wid">#Status</span>
                        </a>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none"><a href="#" class="display-in-block-txt-blk ellipses ellipses-wid" title="<?php echo $addedstatus; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $addedstatus; ?></a></label>
                    </td>
                    <td>
                        <label class="text-black  margin-bottom-none">
                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                               origPrice="<?php echo number_format("{$earningsstatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningsstatus}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningsstatus}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- keyword - 30 days ends here -->
        <div class="bg-white innerAll innerMT border-all">
            <div class="row">
                <?php if($availabilityFlag == 'keyword_available'){ ?>
                <div class="col-md-6 half innerLR">
                    <label class="keyword-grey text-black ellipses margin-none innerB"><span class="text-blue">Highest Bid : </span>
                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </label>
                    <label class="keyword-grey text-black ellipses margin-bottom-none"><span class="text-blue">Asking Price  : </span>
                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </label>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 padding-right-none">
                            <div class="pull-right innerB">
                                <?php if(in_array($keyword, $userCartDetails)){ ?>
                                <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-wid-auto-darkk" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                <?php }else{ ?>
                                    <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-wid-auto-dark" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }else if($availabilityFlag == 'keyword_not_available'){
                    $activeBids = json_decode($activeBids, true);
                    foreach($activeBids as $key => $bidValue){
                        $bidValue = explode('~~', $bidValue);
                        $bidderEmail[] = $bidValue[1];

                    }
                    if(in_array($email, $bidderEmail)){
                        $bidStatus = true;
                    }else{
                        $bidStatus = false;
                    }
                    ?>
                    <div class="col-md-6 half innerLR">

                        <label class="keyword-grey text-black ellipses margin-none innerB"><span class="text-blue">Highest Bid : </span>
                            <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                            <?php } else {?>
                                <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                        <?php } ?>
                        </label>

                        <label class="keyword-grey text-black ellipses margin-bottom-none"><span class="text-blue">Asking Price  : </span>
                            <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                            <?php } else {?>
                                <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                            <?php } ?>
                        </label>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 padding-right-none">
                                <div class="pull-right innerB">
                                    <?php
                                    if($email == $kwdOwnerId){
                                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                    ?>
                                    <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                                    <?php    }

                                    }else{
                                        if($CartStatus == "sold"){
                                            if(empty($activeBids)){
                                                ?>
                                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>

                                            <?php }else{
                                                if($bidStatus){
                                                    ?>
                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>

                                                <?php  }else{ ?>
                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                                <?php     }
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="col-md-12 padding-right-none">
                                    <div class="pull-right">
                                        <?php
                                        if($email == $kwdOwnerId){
                                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                        ?>
                                            <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>

                                        <?php }else{ ?>
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                        <?php  }

                                        }else{
                                            if($CartStatus == "sold"){
                                                if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                    ?>
                                                    <button class="btn-trading-dark"  value="<?php echo $keyword;  ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>', <?php  echo $kwdAskPrice; ?>);"  >Buy Now</button>
                                                <?php }
                                            }

                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else if($checkForKeywordAvailability["errCode"] == 4 || $availabilityFlag == "keyword_blocked"){  ?>

                <div class="col-md-6 half innerLR">
                    <label class="keyword-grey text-black ellipses margin-none innerB"><span class="text-blue">Highest Bid : </span>
                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </label>
                    <label class="keyword-grey text-black ellipses margin-bottom-none"><span class="text-blue">Asking Price  : </span>
                        <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                    </label>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 padding-right-none">
                            <div class="pull-right innerB">
                                Not Available
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- keyword search block starts here -->
        <div class="row keyword-info-blocks">
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key-dark ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("{$totalEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="keyword-num-status light-blue-col">All Earnings</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <?php $dailyEarning = $totalEarning/$search_startFrom; ?>

                    <div class="keyword-info-num bg-blue-key ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                         origPrice="<?php echo number_format("{$dailyEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$dailyEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$dailyEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="keyword-num-status">Daily Earnings</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <?php $earningPerInteraction = $totalEarning/$totalInteraction;
                    if($earningPerInteraction == 'nan') { $earningPerInteraction = 0; }
                    ?>
                    <div class="keyword-info-num bg-blue-key-dark ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                    origPrice="<?php echo number_format("{$earningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningPerInteraction}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                    </div>
                    <div class="keyword-num-status light-blue-col">Earning Per Interaction</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key ellipses" title="<?php echo $totalInteraction; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $totalInteraction; ?>
                    </div>
                    <div class="keyword-num-status">Interaction</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key-dark"><?php echo $totalLikes; ?></div>
                    <div class="keyword-num-status light-blue-col">Total Likes</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key ellipses" title="<?php echo $totalPostCount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $totalPostCount; ?>
                    </div>
                    <div class="keyword-num-status">Total Post</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <?php $dailyEarning = $totalEarning/$search_startFrom;
                     $income = $dailyEarning*365;?>
                    <div class="keyword-info-num bg-blue-key-dark padng-analytic-blocks ellipses">
                        <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                         origPrice="<?php echo number_format("{$income}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$income}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$income}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                        </span><span class="analytic-blocks-span">(next 365 days)</span></div>
                    <div class="keyword-num-status light-blue-col">Expected Income</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key ellipses" title="<?php echo $followUnfollowKeywordCount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $followUnfollowKeywordCount; ?>
                    </div>
                    <div class="keyword-num-status">Followers</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
            <!-- col-md-6 starts here -->
            <div class="col-md-4 inner-2x innerMB">
                <div class="keyword-info">
                    <div class="keyword-info-num bg-blue-key-dark ellipses" title="<?php echo sizeof($previous_ownerJson); ?>" data-toggle="tooltip" data-placement="bottom"><?php echo sizeof($previous_ownerJson); ?>
                    </div>
                    <div class="keyword-num-status light-blue-col">Number of Trades</div>
                </div>
            </div>
            <!-- col-md-6 ends here -->
        </div>
        <!-- keyword search block ends here -->
    </div>
</div>

<script>

    $("#topsorting").change(function() {

        var keyword = '<?php echo $keyword ; ?>'; //alert(keyword);
        var id      = $('#topsorting').find(":selected").text(); //alert(id);

        $.ajax({
            type: 'GET',
            url: 'keywordSortingByDate.php',
            dataType: 'html',
            data: {
                id      : id,
                keyword : keyword
            },
            async: true,
            success: function (data) {
                $('#shortingDateWise').html(data);
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>