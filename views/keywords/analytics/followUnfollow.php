<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();
if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php"); //echo "Testing"; die;

error_reporting(0);

// fetch user email from session
$email = $_SESSION["email"];

// get current session id
$clientSessionId = session_id(); //echo $clientSessionId;

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}


$keywords    = $_POST['keyword'];
$action      = $_POST['user_email'];
$currentTime = date("h:i:sa");

?>


<div id="followunfollow">

    <?php

    $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $keywords);

    if(noError($kwdRevenueDetails)) {

        $kwdTableName = $kwdRevenueDetails["table_name"];
        $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
        $id = $kwdRevenueDetails["id"];
        $checkForKeywordAvailability = $kwdRevenueDetails["keyword_details"];
        $keyword = $kwdRevenueDetails["keyword"];
        $follow_unfollow = $kwdRevenueDetails["follow_unfollow"];

        $follow_unfollowjson = json_decode($follow_unfollow, true); //echo "<pre>"; print_r($follow_unfollowjson); echo "<pre>";

        $followeremail = $follow_unfollowjson["email"];

        if(!empty($id) && in_array($email, $followeremail)) {
            $buttoVal = 'Follow';

            $follow_unfollowjson["email"] = array_diff($follow_unfollowjson["email"], array("email" => $email));
            $appKwdSearchCountJson = json_encode($follow_unfollowjson);

             $query = "update $kwdTableName set follow_unfollow = '$appKwdSearchCountJson' where keyword = '$keyword'";
        }
           else if (!in_array($email, $follow_unfollowjson["email"]) && !empty($id)) {
               $buttoVal = 'Unfollow';
                $follow_unfollowjson["email"][] = $email;
                $appKwdSearchCountJson = json_encode($follow_unfollowjson);
                $appKwdSearchCountJson = json_encode($follow_unfollowjson);

                $query = "update $kwdTableName set follow_unfollow = '$appKwdSearchCountJson' where keyword = '$keyword'";
            }

        else if(empty($id))
        {
            $buttoVal = 'Unfollow';

            $follow_unfollowjson["email"][] = $email;
            $appKwdSearchCountJson = json_encode($follow_unfollowjson);

            $query = "INSERT into ".$kwdTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches,search_startFrom,follow_unfollow) VALUES('".$keywords."','','','','','','','','','','".$appKwdSearchCountJson."')";
        }

        $result = runQuery($query, $conn);
        if (noError($result)) {
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"] = -1;

            $returnArr["errMsg"] = $res;

        } else {

            $returnArr["errCode"] = 10;

            $returnArr["errMsg"] = $result["errMsg"];

        }

        return $returnArr;

    }

    ?>

    <input type="button" id = "<?php echo cleanXSS($keyword); ?>" class="btn-social-wid-auto-dark pull-right innerMR" onclick = "followUnfollow('<?php echo cleanXSS($keyword); ?>','<?php echo $email; ?>');" value="<?php echo $buttoVal;?>"/>

</div>
<?php }
else{
    $returnArr["errCode"] = 53;
    $errMsg = "Please Login To System.";
    $returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);
}?>