<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
echo "<!DOCTYPE html>";

session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php"); //echo "Testing"; die;
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}helpers/sessionHelper.php");

error_reporting(0);

// fetch user email from session
$email = $_SESSION["email"]; //echo $email;

// get current session id
$clientSessionId = session_id(); //echo $clientSessionId;

//For database connection
$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}
checkForSession($conn);
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    if(isset($_SESSION["get_error_code"]) && !empty($_SESSION["get_error_code"])){
        $getErrorCode = $_SESSION["get_error_code"];

        if($getErrorCode == -1){
            $successMsg = $_SESSION["get_error_message"];
        }else{
            $failedMsg = $_SESSION["get_error_message"];
        }

        // unset $_SESSION["get_error_code"]
        unset($_SESSION["get_error_code"]);
        unset($_SESSION["get_error_message"]);
    }

    include("{$docrootpath}views/layout/header.php");
    include("{$docrootpath}views/keywords/marketplace/tradePopupDialogBox.php");

    $searchQuery = trim(urldecode($_GET["q"]));
    if($_GET["status"] != "H"){
        $searchQueryFinal    = cleanXSS($searchQuery);
    }else{
        $searchQueryFinal    = cleanXSS($searchQuery);
    }

    if(isset($searchQueryFinal) && !empty($searchQueryFinal)){
        $searchQueryFinal = optimizeSearchQuery($searchQueryFinal);
        $remove_slash = stripslashes($searchQueryFinal);
        $var = str_replace(array(' '), array('+'), $remove_slash);
        $searchQueryFinalKeyword    = explode(" ",$searchQueryFinal);
    }

    $lastSearchKeyword = end($searchQueryFinalKeyword); // This we use for fetching last Search keyword

// get user cart details
    $userCartDetails = getUserCartDetails($email, $conn);

    if(noError($userCartDetails)){
        $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
        $userCartDetails = json_decode($userCartDetails, TRUE);
    }else{
        print('Error: Fetching cart details');
        exit;
    }

    $keywordSearch = $_GET["keywordSearch"];

?>
    <link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_keyword.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
    <main class="inner-7x innerTB">
        <!-- keyword-marketplace starts here -->
        <div class="container keyword-marketplace keyword-marketplace-data keyword-analytics">
            <!-- container starts here -->
            <div class="row">
                <div class="col-md-6 padding-left-none">
                    <div class="row">
                        <div class="col-xs-12">
                            <h5 class="text-blue keyword-name margin-top-none half innerMB pull-left">#<?= isset($keywordSearch)?($keywordSearch):($lastSearchKeyword); ?></h5>
                            <?php

                            $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $lastSearchKeyword); //printArr($kwdRevenueDetails);

                            if(noError($kwdRevenueDetails)) {

                                $kwdTableName = $kwdRevenueDetails["table_name"];
                                $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                                $checkForKeywordAvailability = $kwdRevenueDetails["keyword_details"];
                                $keyword = $kwdRevenueDetails["keyword"];
                                $follow_unfollow = $kwdRevenueDetails["follow_unfollow"];

                                $follow_unfollowjson = json_decode($follow_unfollow, true); //echo "<pre>"; print_r($follow_unfollowjson); echo "<pre>";

                                $followeremail = $follow_unfollowjson["email"];

                                if (in_array($email, $followeremail)) {
                                    $buttoVal = 'Unfollow';
                                } else {
                                    $buttoVal = 'Follow';
                                }
                            }

                            ?>
                            <input value="<?php echo $buttoVal; ?>" type="button" id="<?php echo 'fu_'.cleanXSS($lastSearchKeyword); ?>" class="btn-social-wid-auto-dark pull-right" onclick="followUnfollow('<?php echo cleanXSS($lastSearchKeyword); ?>', '<?php echo $email; ?>')" />
                        </div>
                    </div>
                    <div class="row innerT half">
                        <div class="col-xs-12 padding-none">
                            <ul class="keyhash">
                                <?php
                                foreach($searchQueryFinalKeyword as $keyNo => $keyword) {
                                    ?>
                                    <li title="<?php echo "#".$keyword; ?>" ><a title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $indexUrl; ?>?q=<?php echo $var;?>&keywordSearch=<?php echo $keyword; ?>">#<?php echo $keyword; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <!-- search bar starts here -->
                    <div id="keyword-analytics-input">
                        <div class="input-group col-md-12 padding-left-none padding-right-none inner-2x innerMB">
                            <form id="keyword_search_form">
                                <input class="search-query form-control ellipses inner-3x innerR" placeholder="Search new Keyword..." id="keyword_search" data-type="2" name="q" type="text" value="<?php echo cleanDisplayParameter($conn,$searchQuery) ; ?>">
                                <p class="errMsg"></p>
                                <span class="input-group-btn">
                                          <button class="btn btn-danger" onclick="searchResult();" type="button">
                                          <span class="glyphicon glyphicon-search"></span>
                                          </button>
                                        </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row keyword-analy-sort keyword-market-botm" id="shortingDateWise">

            </div>
            <div class="row" id="plottingGraphs">

            </div>
        </div>
    </main>

<?php }else{
    header("Location: $rootUrl./views/prelogin/index.php");
} ?>

    <!-- graphs starts here -->
    <script src="../../../frontend_libraries/graph/highcharts.js"></script>
    <script src="../../../frontend_libraries/graph/exporting.js"></script>
<?php include('../../layout/transparent_footer.php'); ?>
    <script src="../../../js/marketplace.js"></script>
    <script>
        var rooturl = '<?php echo $rootUrl; ?>'; //alert(rooturl);
        $(document).ready(function(){

            var lastkeyword    = '<?php echo $lastSearchKeyword ; ?>';
            var keywordsearch  = '<?php echo $keywordSearch?>';
            if(keywordsearch == '')
            {
                var keyword = lastkeyword;
            }
            else
            {
                var keyword = keywordsearch;
            }
            var id = '7%20Days';
            var duration = '7%20Days'; //alert(duration);
            var type = 'Interaction'; //alert(type);

            $('#shortingDateWise').load("keywordSortingByDate.php?id="+id+"&keyword="+keyword);
            $('#plottingGraphs').load("plottingGraph.php?keyword="+keyword+"&duration="+duration+"&type="+type);
        });
    </script>