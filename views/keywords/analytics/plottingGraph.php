<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}/helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php"); //echo "Testing"; die;

error_reporting(0);

// Database Connection

$conn = createDBConnection('dbkeywords'); //echo "<pre>"; print_r($conn); echo "</pre>";

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

// Getting value pass as a parameter

$duration = $_GET["duration"]; //echo $duration;
$type     = $_GET["type"];
$keyword  = $_GET["keyword"];


$m= date("m");
$month = lcfirst(date('M'));
$currentDate = date('d-m-y');
$previousMonth = lcfirst(date('M',strtotime('-1 day')));
$previousdate = date('d-m-y', strtotime('-1 days'));
$de= date("d");

$y= date("Y");


$kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $keyword);
if(noError($kwdRevenueDetails)) {
    $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
    $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
    $search_startFrom = $kwdRevenueDetails["search_startFrom"]; //echo "<pre>"; print_r($search_startFrom); echo "</pre>";
    $date1 = date_create($search_startFrom);
    $date2 = date_create(date('Y-m-d'));
    $diff = date_diff($date1, $date2);
    $search_startFrom = $diff->format("%a%");



    $kwdAnaliticsDetails = getkeywordAnaliticsDetails($conn, $keyword);
    if (noError($kwdAnaliticsDetails)) {
        $kwdAnalyticsTableName = $kwdAnaliticsDetails["table_name"];
        $kwdAnaliticsDetails = $kwdAnaliticsDetails["data"][0];

        $currentMonth = $kwdAnaliticsDetails[$month];

        $currentMonth = json_decode($currentMonth, true);
        $searchInteractionCurrentDateWise =$currentMonth["analytics"][$currentDate];

        $searchInteractionCurrentDate     = $searchInteractionCurrentDateWise["search"]["search_Interaction"];

        $searchEarningCurrentDate         = $searchInteractionCurrentDateWise["search"]["search_Earning"];
        $socialvideoCurrentDate           = $searchInteractionCurrentDateWise["social"]["video"];
        $socialblogCurrentDate            = $searchInteractionCurrentDateWise["social"]["blog"];
        $socialaudioCurrentDate           = $searchInteractionCurrentDateWise["social"]["audio"];
        $socialimageCurrentDate           = $searchInteractionCurrentDateWise["social"]["image"];
        $socialstatusCurrentDate          = $searchInteractionCurrentDateWise["social"]["status"];

        $earningsocialvideoCurrentDate    = $searchInteractionCurrentDateWise["social_Earning"]["video"];
        $earningsocialblogCurrentDate     = $searchInteractionCurrentDateWise["social_Earning"]["blog"];
        $earningsocialaudioCurrentDate    = $searchInteractionCurrentDateWise["social_Earning"]["audio"];
        $earningsocialimageCurrentDate    = $searchInteractionCurrentDateWise["social_Earning"]["image"];
        $earningsocialstatusCurrentDate   = $searchInteractionCurrentDateWise["social_Earning"]["status"];

        $dailyTotalInteraction = $searchInteractionCurrentDate + $socialvideoCurrentDate + $socialblogCurrentDate + $socialaudioCurrentDate + $socialimageCurrentDate + $socialstatusCurrentDate;
        $dailyTotalEarning     = $searchInteractionCurrentDate + $earningsocialvideoCurrentDate + $earningsocialblogCurrentDate + $earningsocialaudioCurrentDate + $earningsocialimageCurrentDate + $earningsocialstatusCurrentDate;

        if($currentMonth != $previousMonth)
        {
            $currentMonth = $kwdAnaliticsDetails[$previousMonth];
            $currentMonth = json_decode($currentMonth, true);
        }
        $searchInteractionPreviousDateWise = $currentMonth["analytics"][$previousdate];

        $searchInteractionCurrentDate     = $searchInteractionPreviousDateWise["search"]["search_Interaction"];

        $searchEarningCurrentDate         = $searchInteractionPreviousDateWise["search"]["search_Earning"];
        $socialvideoCurrentDate           = $searchInteractionPreviousDateWise["social"]["video"];
        $socialblogCurrentDate            = $searchInteractionPreviousDateWise["social"]["blog"];
        $socialaudioCurrentDate           = $searchInteractionPreviousDateWise["social"]["audio"];
        $socialimageCurrentDate           = $searchInteractionPreviousDateWise["social"]["image"];
        $socialstatusCurrentDate          = $searchInteractionPreviousDateWise["social"]["status"];

        $earningsocialvideoCurrentDate    = $searchInteractionPreviousDateWise["social_Earning"]["video"];
        $earningsocialblogCurrentDate     = $searchInteractionPreviousDateWise["social_Earning"]["blog"];
        $earningsocialaudioCurrentDate    = $searchInteractionPreviousDateWise["social_Earning"]["audio"];
        $earningsocialimageCurrentDate    = $searchInteractionPreviousDateWise["social_Earning"]["image"];
        $earningsocialstatusCurrentDate   = $searchInteractionPreviousDateWise["social_Earning"]["status"];

        $previousTotalInteraction = $searchInteractionCurrentDate + $socialvideoCurrentDate + $socialblogCurrentDate + $socialaudioCurrentDate + $socialimageCurrentDate + $socialstatusCurrentDate;
        $previousTotalEarning     = $searchInteractionCurrentDate + $earningsocialvideoCurrentDate + $earningsocialblogCurrentDate + $earningsocialaudioCurrentDate + $earningsocialimageCurrentDate + $earningsocialstatusCurrentDate;

        $todaysInteraction = ($dailyTotalInteraction/$previousTotalInteraction)*100;

        if(empty($previousTotalInteraction))
        {
            $todaysInteraction = $dailyTotalInteraction;
        }
        $todaysEarning     = ($dailyTotalEarning/$previousTotalEarning)*100;

        if(empty($previousTotalEarning))
        {
            $todaysEarning = $dailyTotalEarning;
        }
        $array = array(
            '7 Days' => array('xaxis' => 7, 'points' => 1, 'month' => 7, 'section' => 2),
            '30 Days' => array('xaxis' => 6, 'points' => 5, 'month' => 30, 'section' => 10),
            '3 Months' => array('xaxis' => 6, 'points' => 15, 'month' => 90, 'section' => 30),
            '6 Months' => array('xaxis' => 6, 'points' => 30, 'month' => 180, 'section' => 60),
            '9 Months' => array('xaxis' => 6, 'points' => 45, 'month' => 270, 'section' => 90),
            '12 Months' => array('xaxis' => 6, 'points' => 60, 'month' => 360, 'section' => 120)
        );



            if (array_key_exists($duration, $array)) {
    
                $xaxis1 = $array[$duration]['xaxis'];
                $points = $array[$duration]['points'];
                $montharray = $array[$duration]['month'];
                $section = $array[$duration]['section'];

            $xaxisArr = array();
            for ($ii = 1; $ii <= $xaxis1; $ii++) {
                $xaxis = $ii * $points;
                $testing = array_push($xaxisArr, $xaxis);
            }

            $previousdate = date('d', strtotime('-1 day'));
            $xaxisSevenDays = date('d', strtotime('-6 day'));
            $newxAxis = json_encode($xaxisArr);
            $addedEarning = 0;
            $monthArray = array();
            $GraphIntractionData = array();
            $GraphEarningData = array();

            
            $search_Interaction = 0;
            $video = 0;
            $blog = 0;
            $image = 0;
            $status = 0;
            $audio = 0;
            $deduct = 0;
            $searchInteraction=0;
            $socialInteraction=0;
            $totalCount=0;
            $daywiseDataArray=array();
            $avarageEarningGrowth=array();
            $avarageInteractionGrowth=array();

            for($j=0; $j<$montharray; $j++) {

                $datawiseDate = date('d-m-y', mktime(0, 0, 0, $m, ($de - $j), $y));
                $monthwiseDate = lcfirst(date('M', mktime(0, 0, 0, $m, ($de - $j), $y)));

                $currentMonth = $kwdAnaliticsDetails[$monthwiseDate];
                $currentMonth = json_decode($currentMonth, true);

                $daywiseDataArray[$datawiseDate]=$currentMonth["analytics"][$datawiseDate];

            }
            $array_keys = array_keys($daywiseDataArray);

            $set                   = $points;     // Bunch
            $slot                  = $xaxis1;
            $avgGrowth             = $section; //echo "AverageGrowth ".$avgGrowth;
            $finalEarningArray     = array();
            $finalInteractionArray = array();

                $sortArray = array();
                for ($z=0,$counter=0; $z < $montharray; $z++) {
                    //$ceilValue = (string)(ceil($array_keys[$z] / $set));
                    if($z%$set==0){
                        $counter++;
                    }
                    if (array_key_exists($counter, $sortArray)) {
                        array_push($sortArray[$counter], $array_keys[$z]);
                    } else {
                        $sortArray[$counter] = array($array_keys[$z]);
                    }
                }
                for ($i = 1; $i <= count($sortArray); $i++) {
                    if (is_array($sortArray[$i])) {
                        $searchInteraction = 0;
                        $searchEarning = 0;
                        $socialInteraction = 0;
                        $socialEarning = 0;

                        for ($c = 0; $c < count($sortArray[$i]); $c++) {
                            $searchInteraction = $searchInteraction + $daywiseDataArray[$sortArray[$i][$c]]["search"]["search_Interaction"];
                            $searchEarning = $searchEarning + $daywiseDataArray[$sortArray[$i][$c]]["search"]["search_Earning"];
                            $socialArrayKeys = array_keys($daywiseDataArray[$sortArray[$i][$c]]["social"]);
                            for ($p = 0; $p < count($socialArrayKeys); $p++) {
                                $socialInteraction = $socialInteraction + $daywiseDataArray[$sortArray[$i][$c]]["social"][$socialArrayKeys[$p]];
                                $socialEarning     = $socialEarning + $daywiseDataArray[$sortArray[$i][$c]]["social_Earning"][$socialArrayKeys[$p]];
                            }
                        }
                        $totalInteractionCount = $searchInteraction + $socialInteraction;
                        $totalEarningCount = $searchEarning + $socialEarning;
                        $finalInteractionArray[$i] = $totalInteractionCount;
                        $finalEarningArray[$i] = $totalEarningCount;
                    } else {
                        $finalInteractionArray[$i] = 0;
                        $finalEarningArray[$i] = 0;
                    }
                $GraphIntractionData[] = $finalInteractionArray[$i];
                $GraphEarningData[] = $finalEarningArray[$i];
            }

                foreach($daywiseDataArray as $month)
                {
                    $searchInteraction  = $month["search"]["search_Interaction"];
                    $searchEarning      = $month["search"]["search_Earning"];
                    $socialInteraction  = $month["social"];

                    $socialEarning      = $month["social_Earning"];

                    $socialvideo        = $socialInteraction["video"];
                    $socialblog         = $socialInteraction["blog"];
                    $socialaudio        = $socialInteraction["audio"];
                    $socialimage        = $socialInteraction["image"];
                    $socialstatus       = $socialInteraction["status"];

                    $earningvideo       = $socialEarning["video"];
                    $earningblog        = $socialEarning["blog"];
                    $earningaudio       = $socialEarning["audio"];
                    $earningimage       = $socialEarning["image"];
                    $earningstatus      = $socialEarning["status"];

                    $addedResult        = $addedResult+$searchInteraction;
                    $addedEarning       = $addedEarning+$searchEarning;
                    $addedvideo         = $addedvideo+$socialvideo;
                    $addedblog          = $addedblog+$socialblog;
                    $addedaudio         = $addedaudio+$socialaudio;
                    $addedimage         = $addedimage+$socialimage;
                    $addedstatus        = $addedstatus+$socialstatus;

                    $earningsvideo      = $earningsvideo+$earningvideo;
                    $earningsblog       = $earningsblog+$earningblog;
                    $earningsaudio      = $earningsaudio+$earningaudio;
                    $earningsimage      = $earningsimage+$earningimage;
                    $earningsstatus     = $earningsstatus+$earningstatus;

                }

                $totalInteraction   = $addedResult+$addedvideo+$addedblog+$addedaudio+$addedimage+$addedstatus;
                $totalEarning       = $addedEarning+$earningsvideo+$earningsblog+$earningsaudio+$earningsimage+$earningsstatus;

        }

        $GraphDataNew = json_encode($GraphIntractionData);

        $GraphDataEarningNew = json_encode($GraphEarningData);


?>


<div id="plottingGraphs">
    <div class="col-md-8 padding-none">
        <!-- main-bg-graph starts here -->
        <div class="main-bg-graph bg-white border-all innerTB">
            <!-- graph heading starts here -->
            <div class="col-md-12 bg-white  innerAll border-bottom">
                <div class="row">
                    <div class="col-md-4 padding-right-none">
                        <h4 class="text-blue">Keyword Performance</h4>
                    </div>
                    <div class="col-md-4 padding-right-none">
                        <div class="pull-left">
                            <span class="span-keyword-market pull-left">Select Type : &nbsp;</span>
                            <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid" id="graphType">
                                <option selected id="<?php echo $type; ?>" hidden><?php echo $type; ?></option>
                                <option id="Interaction">Interaction</option>
                                <option id="Earning">Earning</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 padding-right-none">
                        <div class="pull-left">
                            <span class="span-keyword-market pull-left">Select Duration : &nbsp;</span>
                            <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid" id="graphDuration">
                                <option selected id="<?php echo $duration; ?>" hidden><?php echo $duration; ?></option>
                                <option id="7 Days">7 Days</option>
                                <option id="30 Days">30 Days</option>
                                <option id="3 Months">3 Months</option>
                                <option id="6 Months">6 Months</option>
                                <option id="9 Months">9 Months</option>
                                <option id="12 Months">12 Months</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- graph heading ends here -->
            <div class="col-md-12  padding-none">
                <div class="text-blue inner-2x innerAll f-sz13"><?php echo $type; ?></div>
                <!-- graph body starts here -->
                <!-- <div id="container" style="height:250px;width:100%;"></div> -->
                <div id="container" style="width:100%;"></div>
                <!-- graph body ends here -->
                <h5 class="text-blue pull-right" style="position: absolute;
                        right: 12px;
                        bottom: 10px;">Dates</h5>
            </div>
        </div>
        <!-- main-bg-graph ends here -->
    </div>
    <div class="col-md-4">
        <!-- keyword-analytics-status starts here -->
        <div class="keyword-analytics-status">
            <div class="">
                <div class="col-md-12">
                    <!-- row starts here -->
                    <div class="row display-flex-keyword">
                        <div class="col-md-8 bg-white innerAll">
                            <span class="keyword-grey-span">Interactions</span>
                            <div class="comn-lft-rght-cont innerMT">
                                <label>Daily Average</label>
                                <?php
                                $dailyInteraction = intval($totalInteraction/$search_startFrom); ?>
                                <span title="<?php echo $dailyInteraction; ?>" data-toggle="tooltip" data-placement="bottom">&nbsp;: <?php echo $dailyInteraction; ?></span>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label>Peak Performance</label>
                                <span title="<?php echo(max($GraphIntractionData)); ?>" data-toggle="tooltip" data-placement="bottom"> : <?php echo(max($GraphIntractionData)); ?></span>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label>Monthly Growth</label>
                                <?php $totalNumberInteraction = array_sum($finalInteractionArray);
                                $totalNumberInteractions = $totalNumberInteraction*($xaxis1/100); ?>
                                <span title="<?php echo $totalNumberInteractions; ?>" data-toggle="tooltip" data-placement="bottom"> : <?php echo $totalNumberInteractions."%"; ?> </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                            <div class="bg-blue-key innerAll text-white width-100-percent">
                                <span class="">Today</span>
                                <h2 class="innerMT ellipses"><?php echo intval($dailyTotalInteraction); ?></h2>
                                <?php
                                $todaysInteractionValue = intval($todaysInteraction);
                                if($todaysInteractionValue == 0)
                                        {
                                             $variable = "equal";
                                        }
                                        if($todaysInteractionValue > 0)
                                        {
                                             $variable = "UP";
                                        }
                                        if($todaysInteractionValue < 0)
                                        {
                                             $variable = "DOWN";
                                        }


        ?>
                                <span> <?php echo $variable ; ?> <?php echo intval($todaysInteraction)."%"; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                    <!-- row starts here -->
                    <div class="row display-flex-keyword inner-2x innerMT">
                        <div class="col-md-8 bg-white innerAll">
                            <span class="keyword-grey-span">Earning</span>
                            <div class="comn-lft-rght-cont innerMT">
                                <label class="ellipses">Daily Average :
                                <?php $dailyEarning = ($totalEarning/$search_startFrom); ?>

                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                origPrice="<?php echo number_format("{$dailyEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$dailyEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$dailyEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>

                                </label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label class="ellipses">Peak Performance :
                                <?php $graphEarningData = max($GraphEarningData);  ?>
                                <span class="ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$graphEarningData}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$graphEarningData}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$graphEarningData}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label class="ellipses">Monthly Growth :
                                <?php $totalNumberEarning = array_sum($finalEarningArray);
                                      $totalNumberEarnings = $totalNumberEarning*$xaxis1/100; ?>
                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$totalNumberEarnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalNumberEarnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalNumberEarnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                            <div class="bg-blue-key innerAll text-white width-100-percent">
                                <span class="">Today</span>
                                <h2 class="innerMT ellipses"><?php echo intval($dailyTotalEarning); ?></h2>

                                <?php
                                $todaysEarningValue = $todaysEarning;
                                if($todaysEarningValue == 0)
                                {
                                    $variable = "equal";
                                }
                                if($todaysEarningValue > 0)
                                {
                                    $variable = "UP";
                                }
                                if($todaysEarningValue < 0)
                                {
                                    $variable = "DOWN";
                                }


                                ?>
                                <span><?php echo $variable; ?> <?php echo $todaysEarning."%"; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                    <!-- row starts here -->
                    <div class="row display-flex-keyword inner-2x innerMT">
                        <div class="col-md-8 bg-white innerAll">
                            <span class="keyword-grey-span">Earning Per Interactions</span>
                            <div class="comn-lft-rght-cont innerMT">
                                <label class="ellipses">Daily Average :
                                <?php
                                $dailyInteraction = ($totalInteraction/$search_startFrom);
                                $dailyEarning = ($totalEarning/$search_startFrom);
                                $earningPerInteraction = intval($dailyEarning/$dailyInteraction);
                                ?>
                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$earningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earningPerInteraction}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label class="ellipses">Peak Performance
                                    <?php

                                    $maximumInteraction = max($GraphIntractionData);
                                    $maximumEarning     = max($GraphEarningData);

                                    $peakPerformance    = intval($maximumEarning/$maximumInteraction);
                                    ?>
                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$peakPerformance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$peakPerformance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$peakPerformance}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont half innerMT">
                                <label class="ellipses">Monthly Growth
                                    <?php
                                    $totalNumberInteraction = array_sum($finalInteractionArray);
                                    $totalNumberInteraction = $totalNumberInteraction*($xaxis1/100);

                                    $totalNumberEarning = array_sum($finalEarningArray);
                                    $totalNumberEarning = $totalNumberEarning*($xaxis1/100);

                                    $totalEarningPerInteraction = intval($totalNumberEarning/$totalNumberInteraction);

                                    ?>
                                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$totalEarningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalEarningPerInteraction}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalEarningPerInteraction}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                                </span>
                                </label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4 keyword-info padding-left-none padding-right-none display-flex-keyword">
                            <div class="bg-blue-key innerAll text-white width-100-percent">
                                <span class="">Today</span>
                                <h2 class="innerMT ellipses"><?php
                                    $earninPerInteractionToday = intval($dailyTotalEarning/$dailyTotalInteraction);
                                    echo number_format($earninPerInteractionToday,2)?></h2>

                                <?php
                                $earningPerInteractionUpTo = intval($todaysEarning/$todaysInteraction);

                                if($earningPerInteractionUpTo == 0)
                                {
                                    $variable = "equal";
                                }
                                if($earningPerInteractionUpTo > 0)
                                {
                                    $variable = "UP";
                                }
                                if($earningPerInteractionUpTo < 0)
                                {
                                    $variable = "DOWN";
                                }


                                ?>

                                <span><?php echo $variable; ?> <?php
                                    echo number_format($earningPerInteractionUpTo,2)."%"; ?></span>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                </div>
            </div>
        </div>
        <!-- keyword-analytics-status ends here -->
    </div>
</div>
<?php }} ?>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->

<script type="text/javascript">

    var type =  "<?php echo $type; ?>"; //alert(type);
    var jsonVar=  $.parseJSON("<?php print_r($GraphDataNew) ; ?>");
    //var jsonVar=  jsonVar.reverse();

    if(type == 'Interaction')
    {
        var jsonVar=  $.parseJSON("<?php print_r($GraphDataNew) ; ?>"); //alert(jsonVar);
        var jsonVar=  jsonVar.reverse();

    }
    else
    {
        var jsonVar=  $.parseJSON("<?php print_r($GraphDataEarningNew); ?>"); //alert(jsonVar);
        var jsonVar=  jsonVar.reverse();
    }



    var xAxisNew = $.parseJSON("<?php echo $newxAxis; ?>"); //alert(xAxisNew);

    function DrawGraph(jsonVar) {
        Highcharts.chart('container', {

            chart: {
                height: 322
            },
            title: {
                text: ' '
            },

            xAxis: {
                categories: xAxisNew,
                tickInterval: 0
            },

            yAxis: {
                endOnTick: false,        //hide ending number of y axis
                tickInterval: 10,       //intervals in y-axis
                lineWidth: 1,
                gridLineWidth: 1,
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        if(type == 'Interaction') { return (this.value / 1); }
                        else { return (this.value / 1) + " " + keywoDefaultCurrency; }
                    }
                }
            },

            tooltip: {
                headerFormat: '<b>{ }</b>',   //series.name
                pointFormat: '<b><?php echo $type; ?></b> = {point.y}'
            },

            series: [{
                data: jsonVar,
                pointStart: 0
            }]
        });
    }

</script>

<script>
    $(document).ready(function(){
        DrawGraph(jsonVar);

    });

    $("#graphDuration").change(function() {

        var keyword  = '<?php echo $keyword ; ?>'; //alert(keyword);
        var type     = $('#graphType').find(":selected").text(); //alert(type);
        var duration = $('#graphDuration').find(":selected").text(); //alert(duration);

        $.ajax({
            type: 'GET',
            url: 'plottingGraph.php',
            dataType: 'html',
            data: {
                duration : duration,
                keyword  : keyword,
                type     : type
            },
            async: true,
            success: function (data) {
                $('#plottingGraphs').html(data);
            }
        });

    });

    $("#graphType").change(function() {

        var keyword  = '<?php echo $keyword ; ?>';
        var type     = $('#graphType').find(":selected").text();
        var duration = $('#graphDuration').find(":selected").text();

        $.ajax({
            type: 'GET',
            url: 'plottingGraph.php',
            dataType: 'html',
            data: {
                duration : duration,
                keyword  : keyword,
                type     : type
            },
            async: true,
            success: function (data) {
                $('#plottingGraphs').html(data);
            }
        });

    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });

</script>