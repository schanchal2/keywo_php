var ctx = document.getElementById("canvas");


var myChart = new Chart(ctx, {
    type: 'line',
    data: {          
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
        datasets: [{
            label: 'Content consumption',   //'# of Votes'
            data: [0.6000000000000088,0.2,0.15000000000000,0.18000000000000221,0.15000000000000308,0.9000000000000308],
            backgroundColor: "rgba(0,65,97,0.8)",
            // borderWidth: 1,
            // hoverBackgroundColor: "rgba(232,105,90,0.8)",
            // hoverBorderColor: "orange",

 // backgroundColor: [
 //                'rgba(255, 99, 132, 0.2)',
 //                'rgba(54, 162, 235, 0.2)',
 //                'rgba(255, 206, 86, 0.2)',
 //                'rgba(75, 192, 192, 0.2)',
 //                'rgba(153, 102, 255, 0.2)',
 //                'rgba(255, 159, 64, 0.2)'
 //            ],


            // dataColor:'white',
            // pointLabelFontColor:'#ff0000'
            // borderColor: [
            //     'rgba(255 ,99 ,132, 1)',
            //     'rgba(54, 162, 235, 1)',
            //     'rgba(255, 206, 86, 1)',
            //     'rgba(75, 192, 192, 1)',
            //     'rgba(153, 102, 255, 1)',
            //     'rgba(255, 159, 64, 1)'
            // ],
            borderWidth: 1,
            borderColor:'white'
        }]
    },

    options: {
       legend: {
            labels: {
                fontColor: "black"
                // fontSize: 18
            }
        },
         // chartArea: {
         //            // backgroundColor: 'rgba(251, 85, 85, 0.4)'
         //             backgroundColor: "rgba(159,170,174,0.8)"
         //        },
        scales: {
            yAxes: [{
                gridLines: {
            drawBorder: false
        },
                ticks: {
                    fontColor: "black",
                    beginAtZero:true
                }
            }]
            ,
            xAxes: [{
                ticks: {
                    fontColor: "black",
                    beginAtZero:true
                }

            }]

        }
    }
});


var ctx = document.getElementById("canvas2");


var myChart = new Chart(ctx, {
    type: 'line',
    data: {          
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
        datasets: [{
            label: 'Content Upload',   //'# of Votes'
            data: [0.6000000000000088,0.2,0.15000000000000,0.18000000000000221,0.15000000000000308,0.9000000000000308],
            backgroundColor: "rgba(0,100,150,0.8)",
            // borderWidth: 1,
            // hoverBackgroundColor: "rgba(232,105,90,0.8)",
            // hoverBorderColor: "orange",

 // backgroundColor: [
 //                'rgba(255, 99, 132, 0.2)',
 //                'rgba(54, 162, 235, 0.2)',
 //                'rgba(255, 206, 86, 0.2)',
 //                'rgba(75, 192, 192, 0.2)',
 //                'rgba(153, 102, 255, 0.2)',
 //                'rgba(255, 159, 64, 0.2)'
 //            ],


            // dataColor:'white',
            // pointLabelFontColor:'#ff0000'
            // borderColor: [
            //     'rgba(255 ,99 ,132, 1)',
            //     'rgba(54, 162, 235, 1)',
            //     'rgba(255, 206, 86, 1)',
            //     'rgba(75, 192, 192, 1)',
            //     'rgba(153, 102, 255, 1)',
            //     'rgba(255, 159, 64, 1)'
            // ],
            borderWidth: 1,
            borderColor:'white'
        }]
    },
    options: {
       legend: {
            labels: {
                fontColor: "black"
                // fontSize: 18
            }
        },
         // chartArea: {
         //            // backgroundColor: 'rgba(251, 85, 85, 0.4)'
         //             backgroundColor: "rgba(159,170,174,0.8)"
         //        },
        scales: {
            yAxes: [{
                ticks: {
                     fontColor: "black",
                    beginAtZero:true
                }
            }]
            ,
            xAxes: [{
                ticks: {
                     fontColor: "black",
                    beginAtZero:true
                }

            }]

        }
    }
});







