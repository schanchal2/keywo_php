<?php

	//require_once("showCaptcha.php");

	function checkClientIpInfo($clientIp, $user_agent, $searchkey){

        global $docRoot;

        $requests = array();
        $userArr = array();
        $parentArr = array();
        $probability = 0;
        $userCurrentOS = getOS();
        $userCurrentBrowser = getBrowserVersion($user_agent);
        $searchType = getType($searchkey);
        $ipExist = 0;
        $i = 0;
        $ip = explode(".", $clientIp);
        $ipArr = array();
        $jsonIpDir = $docRoot."IPBlocker";
        if(!is_dir($jsonIpDir)){
            mkdir($jsonIpDir, 0777);
        }

        $getFirstByteIpVal = $ip[0];
        $ipCeilVal = ceil($getFirstByteIpVal/20);
        $ipInnerDir = "IP_".$ipCeilVal;
        $subIpDir = $jsonIpDir."/".$ipInnerDir;
        $logFilePath = $jsonIpDir."/blockedIpLog/".date("Y")."/".date("m")."/".date("d");

        if(!is_dir($logFilePath)){
            mkdir($logFilePath, 0777, true);
        }

        $logFileUrl = $logFilePath."/".date("H")."_O_Clock_Log.xml";

        /* if(file_exists($logFileUrl)){
            $file = file($logFileUrl); // Get file content in array
            $file = array_filter($file);
            array_pop($file);  // Removes last tag </rootElement> from array
            $fp = fopen($logFileUrl,'w');
            fwrite($fp,implode('',$file));
            fclose($fp);
        } */

        $logDir = $jsonIpDir."/".$ipInnerDir;
        try{
            if(!is_dir($subIpDir)){
                mkdir($subIpDir, 0777, true);
            }

            $ipJsonFile = $subIpDir."/".$ip[0].".json";
            //if ip json file is not exist create new json file.
            //else check is current ip address is exist in json file or not.
            if(!file_exists($ipJsonFile)){
                $fp = fopen($ipJsonFile, "w");
                $request[$i]["search_key"] = $searchkey;
                $request[$i]["search_type"] = $searchType;
                $request[$i]["user_os"] = $userCurrentOS;
                $request[$i]["user_browser"] = $userCurrentBrowser;
                $request[$i]["last_request_time"] = time();
                $userArr["requests"] = $request;
                $userArr["user_ip"] = $clientIp;
                $userArr["last_update"] = time();
                $userArr["request_count"] = 0;
                $userArr["probability"] = 0;
                $userArr["search_count"] = 1;
                $parentArr[$clientIp] = $userArr;
                fwrite($fp, json_encode($parentArr));
                fclose($fp);


            }else{

                $fp1 = fopen($ipJsonFile, "r+");
                $json = fread($fp1, filesize($ipJsonFile));
                $resultJson = json_decode($json, true);

                foreach($resultJson as $key => $val){

                    //Check current ip address is exist in json file or not.
                    if( $key == $clientIp){

                        /* $mode = (file_exists($logFileUrl)) ? 'a' : 'w';
                        $myFile = fopen($logFileUrl, $mode); //or die("Unable to open file!");
                        $xmlFileContent = file_get_contents($logFileUrl);
                        if (trim($xmlFileContent) == '') {
                            $xml = sprintf('<?xml version="1.0" encoding="utf-8" ?>');
                            $xml .= "\n\n<rootElement>\n";
                        } */
                        //if ip address exist change flage.
                        $ipExist = 1;
                        $sameTimeDiffCount = 0;
                        $sameSearchKeyCount = 0;
                        $sameSearchTypeCount = 0;
                        $samebrowserCount = 0;
                        $sameOSCount = 0;
                        $ipRecords = $val;
                        $request_count = $ipRecords["request_count"];
                        $userRequests = $ipRecords["requests"];
                        $searchCount  = $ipRecords["search_count"]++;

                        //Check last 10 requests for getting probability.
                        if($request_count > 9 ){

                            for($i = $request_count - 10; $i <= $request_count; $i++ ){
                                if($i+1 == $request_count+1){
                                    $requestTimeDiff = ( time() - $ipRecords["requests"][$i]["last_request_time"]);
                                }else{

                                    $requestTimeDiff = ($ipRecords["requests"][$i+1]["last_request_time"] - $ipRecords["requests"][$i]["last_request_time"]);

                                }
                                //check if time difference between two request is less than 10 sec or not.
                                if($requestTimeDiff < 15){
                                    $sameTimeDiffCount++;
                                }
                                if( $ipRecords["requests"][$i]["search_key"] == $searchkey){
                                    $sameSearchKeyCount++;
                                }
                                if( $ipRecords["requests"][$i]["search_type"] == $searchType){
                                    $sameSearchTypeCount++;
                                }
                                if( $ipRecords["requests"][$i]["user_browser"] == $userCurrentBrowser){
                                    $samebrowserCount++;
                                }
                                if( $ipRecords["requests"][$i]["user_os"] == $userCurrentOS){
                                    $sameOSCount++;
                                }
                            }
                            //If rquest coming from different browser,OS and time diff between every request is less than 3 sec then probability will be 20%.
                            if(($samebrowserCount > 10 || $sameOSCount > 10) && $sameTimeDiffCount > 10){
                                $probability = 20;
                            }
                            //If rquest coming from same browser,OS and time diff between every request is less than 3 sec then probability will be 50%.
                            if($sameTimeDiffCount > 10 && $samebrowserCount > 10 && $sameOSCount > 10){
                                $probability = 50;
                            }

                            //if user searched same keyword for last 10 requests then increase probability with 20%.
                            if($sameSearchKeyCount > 10){
                                $probability = $probability + 20;
                            }
                            //if user searched same type of keyword for last 10 requests then increase probability with 10%.
                            if($sameSearchTypeCount > 10){
                                $probability = $probability + 10;
                            }
                            //If total probability of user is greater than 75% then show capcha


                        }
                        $lastProbability = $ipRecords["probability"];

                        if($probability > 75 || $lastProbability > 75 ){
                            //Show capcha
                            $userRequests[$request_count + 1]["search_key"] = $searchkey;
                            $userRequests[$request_count + 1]["search_type"] = $searchType;
                            $userRequests[$request_count + 1]["user_os"] = $userCurrentOS;
                            $userRequests[$request_count + 1]["user_browser"] = $userCurrentBrowser;
                            $userRequests[$request_count + 1]["last_request_time"] = time();
                            $ipRecords["requests"] = $userRequests;
                            $ipRecords["user_ip"] = $clientIp;
                            $ipRecords["last_update"] = time();
                            $ipRecords["probability"] = 80;
                            $requestCount = $ipRecords["request_count"]++;
                            $resultJson[$clientIp] = $ipRecords;
                            $fp2 = fopen($ipJsonFile, "w");
                            fwrite($fp2,json_encode($resultJson));
                            fclose($fp2);

                            $xml  = "<request> \n";
                            $xml .= "<ipaddress>$clientIp</ipaddress> \n";
                            $xml .= "<searchKey>$searchkey</searchKey> \n";
                            $xml .= "<searchType>$searchType</searchType> \n";
                            $xml .= "<userCurrentOS>$userCurrentOS</userCurrentOS> \n";
                            $xml .= "<userCurrentBrowser>$userCurrentBrowser</userCurrentBrowser> \n";
                            $xml .= "<requestTime>".date("Y-m-d H:i:s")."</requestTime> \n";
                            $xml .= "<requestCount>".$requestCount."</requestCount> \n";
                            $xml .= "<probability>".$ipRecords["probability"]."</probability> \n";
                            $xml .= "<captcha>Yes</captcha> \n";
                            $xml .= "</request> \n";
                            $xml .= "</rootElement> \n";
                            //logFunctionFileReadWrite($logFileUrl,$xml);
                             // showcap($ipJsonFile);
                             // die;
                            // header('Location: https://searchtrade.com/staging/showCaptcha.php');


                        }else{
                            $ipRecords["probability"] = 0;
                        }
                        $userRequests[$request_count + 1]["search_key"] = $searchkey;
                        $userRequests[$request_count + 1]["search_type"] = $searchType;
                        $userRequests[$request_count + 1]["user_os"] = $userCurrentOS;
                        $userRequests[$request_count + 1]["user_browser"] = $userCurrentBrowser;
                        $userRequests[$request_count + 1]["last_request_time"] = time();
                        $ipRecords["requests"] = $userRequests;
                        $ipRecords["user_ip"] = $clientIp;
                        $ipRecords["last_update"] = time();
                        $requestCount = $ipRecords["request_count"]++;
                        $resultJson[$clientIp] = $ipRecords;

                        $xml  = "<request> \n";
                        $xml .= "<ipaddress>$clientIp</ipaddress> \n";
                        $xml .= "<searchKey>$searchkey</searchKey> \n";
                        $xml .= "<searchType>$searchType</searchType> \n";
                        $xml .= "<userCurrentOS>$userCurrentOS</userCurrentOS> \n";
                        $xml .= "<userCurrentBrowser>$userCurrentBrowser</userCurrentBrowser> \n";
                        $xml .= "<requestTime>".date("Y-m-d H:i:s")."</requestTime> \n";
                        $xml .= "<requestCount>".$requestCount."</requestCount> \n";
                        $xml .= "<searchCount>".$searchCount."</searchCount> \n";
                        $probability = (isset($ipRecords["probability"])) ? $ipRecords["probability"] : 0;
                        $xml .= "<probability>".$ipRecords["probability"]."</probability> \n";
                        $xml .= "<captcha>No</captcha> \n";
                        $xml .= "</request> \n";
                        $xml .= "</rootElement> \n";
                      //  logFunctionFileReadWrite($logFileUrl,$xml);
                    }

                }



                //if not exist in json file then add new entry in json file.
                if($ipExist == 0 ){
                    /* $mode = (file_exists($logFileUrl)) ? 'a' : 'w';
                    $myFile = fopen($logFileUrl, $mode); //or die("Unable to open file!");
                    $xmlFileContent = file_get_contents($logFileUrl);
                    if (trim($xmlFileContent) == '') {
                        $xml = sprintf('<?xml version="1.0" encoding="utf-8" ?>');
                        $xml .= "\n\n<rootElement>\n";
                    } */
                    $parentArr = array();
                    $i = 0;
                    $request[$i]["search_key"] = $searchkey;
                    $request[$i]["search_type"] = $searchType;
                    $request[$i]["user_os"] = $userCurrentOS;
                    $request[$i]["user_browser"] = $userCurrentBrowser;
                    $request[$i]["last_request_time"] = time();
                    $ipRecords["requests"] = $request;
                    $ipRecords["user_ip"] = $clientIp;
                    $ipRecords["last_update"] = time();
                    $ipRecords["request_count"] = 0;
                    $ipRecords["probability"] = 0;
                    $ipRecords["search_count"] = 1;
                    $resultJson[$clientIp] = $ipRecords;

                    $xml  = "<request> \n";
                    $xml .= "<ipaddress>$clientIp</ipaddress> \n";
                    $xml .= "<searchKey>$searchkey</searchKey> \n";
                    $xml .= "<searchType>$searchType</searchType> \n";
                    $xml .= "<userCurrentOS>$userCurrentOS</userCurrentOS> \n";
                    $xml .= "<userCurrentBrowser>$userCurrentBrowser</userCurrentBrowser> \n";
                    $xml .= "<requestTime>".date("Y-m-d H:i:s")."</requestTime> \n";
                    $xml .= "<requestCount>0</requestCount> \n";
                    $xml .= "<searchCount>".$ipRecords["search_count"]."</searchCount> \n";
                    $probability = (isset($ipRecords["probability"])) ? $ipRecords["probability"] : 0;
                    $xml .= "<probability>".$ipRecords["probability"]."</probability> \n";
                    $xml .= "<captcha>No</captcha> \n";
                    $xml .= "</request> \n";
                    $xml .= "</rootElement> \n";
                    //logFunctionFileReadWrite($logFileUrl,$xml);
                }
                $fp2 = fopen($ipJsonFile, "w");
                fwrite($fp2,json_encode($resultJson));
                fclose($fp2);

            }
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"]["message"] = "ip requests checked successfully.";
            $returnArr["errMsg"]["search_count"] = $searchCount+1;

        }catch (Exception $e) {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Error in checking ip requests.";
        }

        return $returnArr;
    }
	//get users browser and version of browser.
	function getBrowserVersion($agent){
        $browser = array(
            "Navigator"            => "/navigator(.*)/i",
            "Firefox"              => "/firefox(.*)/i",
            "Internet Explorer"    => "/msie(.*)/i",
            "Safari"			   => "/safari(.*)/i",
            "Google Chrome"        => "/chrome(.*)/i",
            "MAXTHON"              => "/maxthon(.*)/i",
            "Opera"                => "/opr(.*)/i",
        );
        foreach($browser as $key => $value){
            if(preg_match($value, $agent)){
                $version = getVersion($key, $value, $agent);
            }
        }
        return $version;
    }

	//get users version of browser.
	function getVersion($browser, $search, $string){
        $version = "";
        $browser = strtolower($browser);
        preg_match_all($search,$string,$match);
        switch($browser){
            case "firefox": $version = "firefox ".str_replace("/","",$match[1][0]);
                break;

            case "internet explorer": $version = "internet explorer ".substr($match[1][0],0,4);
                break;

            case "opera": $version =  "opera ".str_replace("/","",substr($match[1][0],0,5));
                break;

            case "navigator": $version = "navigator ".substr($match[1][0],1,7);
                break;

            case "maxthon": $version = "maxthon ".str_replace(")","",$match[1][0]);
                break;

            case "google chrome": $version =  "google chrome ".substr($match[1][0],1,10);
                break;

            case "safari": $version = "safari ".str_replace(")","",$match[1][0]);
        }
        return $version;
    }


// function to get operating system
$user_agent     =   $_SERVER['HTTP_USER_AGENT'];
function getOS() {
    global $user_agent;
    $os_platform    =   "Unknown OS Platform";
    $os_array       =   array(
        '/windows nt 10/i'     =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iPhone',
        '/ipod/i'               =>  'iPod',
        '/ipad/i'               =>  'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }
    return $os_platform;
}

// function to get browser name
function getBrowser() {
    global $user_agent;
    $browser        =   "Unknown Browser";
    $browser_array  =   array(
        '/msie/i'       =>  'Internet Explorer',
        '/firefox/i'    =>  'Firefox',
        '/safari/i'     =>  'Safari',
        '/chrome/i'     =>  'Chrome',
        '/edge/i'       =>  'Edge',
        '/opera/i'      =>  'Opera',
        '/netscape/i'   =>  'Netscape',
        '/maxthon/i'    =>  'Maxthon',
        '/konqueror/i'  =>  'Konqueror',
        '/mobile/i'     =>  'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }
    }
   return $browser;
}

?>
