<?php

//check for captcha
$captcha = "";
$clientIp = getClientIP();

 //$clientIp ="198.268.2.73";
if(isset($_POST["ipJsonFile"])){
    $ipJsonFile = $_POST["ipJsonFile"];
}
if (isset($_POST['g-recaptcha-response'])) {
    $captcha = $_POST['g-recaptcha-response'];
}
if (!$captcha) {
    $retArray["errCode"] = 2;
    $retArray["errMsg"]  = "Incorect captcha,try again.";
}else{
    $response            = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $captchaPrivatekey . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']), true);
    //hardcoded for captcha issue
    $response['success'] = true;

    if ($response['success'] == false) {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "Incorect captcha,try again.";
    } else {
        $fp1 = fopen($ipJsonFile, "r+");
        $json = fread($fp1, filesize($ipJsonFile));
        $resultJson = json_decode($json, true);
        foreach($resultJson as $key => $val){
            //Check current ip address is exist in json file or not.
            if( $key == $clientIp){
                $ipRecords = $val;
                $request_count = $ipRecords["request_count"];
                $userRequests = $ipRecords["requests"];
                $ipRecords["probability"] = 0;
                $resultJson[$clientIp] = $ipRecords;
                $fp2 = fopen($ipJsonFile, "w");
                fwrite($fp2,json_encode($resultJson));
                fclose($fp2);
            }
        }
        header('Location:'.$rootUrl."views/dashboard/index.php");
    }
}


function showcap($ipJsonFile){
    global $captchaPublickey;
    //$publickey = "6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r";
    ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
        .captchaDiv{
            width: 302px;
            margin: auto;
        }
    </style>
    <form method="POST">
        <div class="captchaDiv">
            <div class="g-recaptcha" data-sitekey="<?php echo $captchaPublickey; ?>"></div>
        </div>
        <input type="text" name="ipJsonFile" value = "<?php echo $ipJsonFile ?>" style="display:none;">
        <center>
            <button class = "rad-button wwt flat">Submit</button>
        </center>
    </form>

    <div><center>System dectects suspicious traffic form your computer Network.<br>Please fill the captcha to continue searching...</center></div>

<?php }?>
